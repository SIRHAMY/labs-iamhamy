---
title: "Monoverse: XXXX.YYYY - NAME"
date: {{ .Date }}
tags: [
    "projects",
    'active',
    'monoverse',
    'art'
]
comments: false
---

_Part of [The Monoverse](https://labs.hamy.xyz/projects/the-monoverse/)._

Monoverse: [XXXX - NAME](LINK)

# Overview

# Instances

* XXXX - NAME - [NFT](LINK)
    * Description:
        * DESCRIPTION STUFF
