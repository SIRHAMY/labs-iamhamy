const playbackTypes = {
    fileUpload: "fileUpload",
    microphone: "microphone"
}

var chosenPlaybackType = null;

// File upload paradigm taken from https://stackoverflow.com/questions/43710173/upload-and-play-audio-file-js
var foundElement = document.getElementById("audio-file-input");
foundElement.addEventListener(
        "change",
        fileUploadAction,
        false);

var microphoneElement = document.getElementById("microphone-input-button");
microphoneElement.addEventListener(
    "click",
    microphoneInputChosenAction,
    false);

// Actions

async function microphoneInputChosenAction() {
    console.log("microhponeinputchosen called!");
    document.getElementsByClassName("visualizer-area")[0]
        .classList
        .toggle("hidden", false);
    startVisualizerAction(playbackTypes.microphone);
    hideInputMenu();
}

async function fileUploadAction(event) {
    chosenPlaybackType = playbackTypes.fileUpload;

    await uploadFile(event);
    document.getElementsByClassName("visualizer-area")[0]
        .classList
        .toggle("hidden", false);
    
    startVisualizerAction(chosenPlaybackType);
    hideInputMenu();
}

async function startVisualizerAction(chosenPlaybackType) {
    startVisualization(chosenPlaybackType);
}

// Utilities

async function uploadFile(fileUploadEvent) {
    var files = fileUploadEvent.target.files;
    document.getElementById("audio-source")
        .setAttribute("src", URL.createObjectURL(files[0]));
    document.getElementById("audio")
        .load()
    return true;
}

function hideInputMenu() {
    document.getElementById("play-option-selection")
        .classList
        .toggle("hidden", true);
    document.getElementById("audio-file-input")
        .classList
        .toggle("hidden", true);
}