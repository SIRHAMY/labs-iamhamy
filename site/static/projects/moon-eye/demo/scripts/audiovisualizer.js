// consts
var byteTimeDomainDataThresholdStrategies = {
    aByteExceedsThreshold: "aByteExceedsThreshold",
    recentWaveFormDiffsExceeded: "recentWaveFormDiffsExceeded",
    recentWaveFormDiffsExceededTimeRange: "recentWaveFormDiffsExceededTimeRange"
}

var waveformDiffMagnitudeThresholds = [
    0.05,
    0.1,
    0.2,
    0.3,
    0.4,
    0.5
]

// Tweakers
var pupilDilationFactor = 1.01; // the growth factor applied to pupil on threshold
var pupilShrinkFactor = 0.995
var byteTimeDomainDataThresholdStrategy = byteTimeDomainDataThresholdStrategies.recentWaveFormDiffsExceededTimeRange;
var isDebug = false; // turn on if you want dem debug messages

// Strategy-specific tweakers
var audioThreshold = 1.25; // threshold at which to dilate for aByteExceedsThreshold

// recentWaveFormDiffsExceeded
var waveformHistoryMaxLength = 15; // number of old waveforms to hold for calculating threshold for recentWaveFormDiffsExceeded
var waveformHistoryLengthMilliseconds = 5000;
var waveformDiffMagnitudeForThresholdIndex = 1;
var waveformHistoryOfIncreasesLength = 2 * waveformHistoryLengthMilliseconds;

var audioContext;
var source;

function startVisualization(chosenPlaybackType) {
    audioContext = new (window.AudioContext || window.webKitAudioContext)();
    if(chosenPlaybackType === playbackTypes.microphone) {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            console.log('getUserMedia supported.');
            navigator.mediaDevices.getUserMedia (
            // constraints - only audio needed for this app
            {
                audio: true
            })
        
            // Success callback
            .then(function(stream) {
                startVisualizer(
                    stream,
                    chosenPlaybackType);
            })
        
            // Error callback
            .catch(function(err) {
                console.log('The following getUserMedia error occured: ' + err);
            });
        } else {
            console.log('getUserMedia not supported on your browser!');
        }
    } else if (chosenPlaybackType === playbackTypes.fileUpload) {
        startVisualizer(
            null,
            chosenPlaybackType
        );
    } else {
        console.error("Unknown playback type selected!");
    }
}

// Visualizer

// Strategies

// RecentWaveformsStrategy

var lastWaveformByteTimeDomainDataArray = null;
var lastWaveformChangeAverages = [];
var totalOfAverages = 0;

var totalIncreases = 0;
var cycleIncreaseHistory = [false]
var lastThresholdCheckTime = new Date().getTime();

async function startVisualizer(
    stream,
    chosenPlaybackType) {

    console.log("AudioVisualizer starting.");

    // Create the audio context
    var analyzer = audioContext.createAnalyser();

    // Create the audio source
    var audioElement = document.getElementById("audio");
    console.log(audioElement);
    
    if(chosenPlaybackType === playbackTypes.microphone) {
        source = audioContext.createMediaStreamSource(stream);
    } else if(chosenPlaybackType === playbackTypes.fileUpload) {
        source = audioContext.createMediaElementSource(audioElement);
    } else {
        console.error("Unknown playback type chosen");
    }

    source.connect(analyzer);

    if(chosenPlaybackType == playbackTypes.microphone) {
        volume = audioContext.createGain();
        volume.gain.value = 0;
        analyzer.connect(volume);
        volume.connect(audioContext.destination);
    } else {
        source.connect(audioContext.destination);
    }

    // Configure analyzer
    analyzer.fftSize = 2048;
    var bufferLength = analyzer.frequencyBinCount;
    var dataArray = new Uint8Array(bufferLength);

    var pupilContainerElement = document.getElementById("eye-ball-pupil-container");

    // HG: There's a weird issue where the pupilContainerElement won't
    // load before we start making modifications to it, resulting in
    // initializing pupil to size of 0. This is bad. To combat,
    // we wait for the pupilContainerElement to have a size. If this
    // doesn't work then there's obvs a bug somewhere else in the code.
    while(pupilContainerElement.width.baseVal.value === 0) {
        console.log("Waiting for pupilContainerElement to load...");
        await sleep(50);
    }

    // Grab pixel value of container for usage later
    var pupilContainerSizePx = pupilContainerElement.width.baseVal.value;

    var biggestSize = (pupilContainerSizePx / 2) * (3 / 4); // Want there to be room for ocean
    var initialPupilSize = pupilContainerSizePx / 8;

    var pupilElement = document.getElementById("eye-ball-pupil");
    pupilElement.r.baseVal.value = initialPupilSize;
    pupilElement.cx.baseVal.value = pupilContainerSizePx / 2;
    pupilElement.cy.baseVal.value = pupilContainerSizePx / 2;

    function draw() {
        var drawVisual = requestAnimationFrame(draw);
        analyzer.getByteTimeDomainData(dataArray);

        let thresholdExceeded = analyzeByteTimeDomainDataForThresholdPass(
            dataArray,
            bufferLength);

        if(thresholdExceeded) {
            if(isDebug) {
                console.log("threshold exceeded");
            }
            
            var proposedRadius = pupilElement.r.baseVal.value * pupilDilationFactor;
            if(proposedRadius > biggestSize) {
                proposedRadius = biggestSize;
            }
            pupilElement.r.baseVal.value = proposedRadius;

            totalIncreases += 1;
            cycleIncreaseHistory.push(true)
        } else {
            var currentRadius = pupilElement.r.baseVal.value;
            if(currentRadius > initialPupilSize) {
                pupilElement.r.baseVal.value = pupilElement.r.baseVal.value * pupilShrinkFactor;
            }
            cycleIncreaseHistory.push(false)
        }

        // check if we need to change our increase threshold
        // check if history is too long and make shorter
        // if we haven't checked ourselves in awhile, check and update
        while(cycleIncreaseHistory.length > waveformHistoryOfIncreasesLength) {
            lastCycle = cycleIncreaseHistory.splice(0, 1);
            if(lastCycle) {
                totalIncreases -= 1
            }
        }

        currentTime = new Date().getTime()
        if(currentTime > (lastThresholdCheckTime + waveformHistoryOfIncreasesLength)) {
            lastThresholdCheckTime = currentTime
            // We want to maintain a rate of somewhere between 40 - 60 %
            hitRate = totalIncreases / cycleIncreaseHistory.length
            // console.log("hitRate = ", hitRate)
            if(hitRate < 0.46) {
                waveformDiffMagnitudeForThresholdIndex -= 1;
                if(waveformDiffMagnitudeForThresholdIndex < 0) {
                    waveformDiffMagnitudeForThresholdIndex = 0
                }
            } else if(hitRate > 0.51) {
                waveformDiffMagnitudeForThresholdIndex += 1;
                if(waveformDiffMagnitudeForThresholdIndex >= waveformDiffMagnitudeThresholds.length) {
                    waveformDiffMagnitudeForThresholdIndex = waveformDiffMagnitudeThresholds.length - 1
                }
            }
            // console.log("magnitudeThresholdIndex = ", waveformDiffMagnitudeForThresholdIndex)
        }
    }

    // Start animation
    draw();
}

function analyzeByteTimeDomainDataForThresholdPass(
    byteTimeDomainDataArray,
    bufferLength
) {
    if(byteTimeDomainDataThresholdStrategy === byteTimeDomainDataThresholdStrategies.aByteExceedsThreshold) {
        for(var i = 0; i < bufferLength; i ++) {
            var data = byteTimeDomainDataArray[i] / 128.0;
            if(data > audioThreshold) {
                return true;
            }
        }
        return false;
    } else if (byteTimeDomainDataThresholdStrategy === byteTimeDomainDataThresholdStrategies.recentWaveFormDiffsExceeded) {
        if(lastWaveformByteTimeDomainDataArray === null) {
            // clone array if we don't have one yet
            lastWaveformByteTimeDomainDataArray = byteTimeDomainDataArray.slice();
            lastWaveformChangeAverages.push(findAverageOfArray(byteTimeDomainDataArray));
        }

        // we return true if the newest avg is bigger than the avg of the last avgs
        var newestAverage = findAverageOfArray(byteTimeDomainDataArray);
        var exceededThreshold = newestAverage > findAverageOfArray(lastWaveformChangeAverages) + waveformDiffMagnitudeThresholds[waveformDiffMagnitudeForThresholdIndex];
        lastWaveformChangeAverages.push(newestAverage);
        if(lastWaveformChangeAverages.length > waveformHistoryMaxLength) {
            lastWaveformChangeAverages.splice(0, 1);
        }
        return exceededThreshold;
    } else if (byteTimeDomainDataThresholdStrategy == byteTimeDomainDataThresholdStrategies.recentWaveFormDiffsExceededTimeRange) {
        if(lastWaveformChangeAverages === null) {
            lastWaveformChangeAverages = [[0, new Date().getTime()]]
        }

        var earliestAllowedTimeMilliseconds = new Date().getTime() - waveformHistoryLengthMilliseconds
        while(
            lastWaveformChangeAverages.length > 0 &&
            lastWaveformChangeAverages[0][1] < earliestAllowedTimeMilliseconds) {
            deletedItem = lastWaveformChangeAverages.splice(0, 1)
            totalOfAverages -= deletedItem[0][0]
        }

        var newestAverage = findAverageOfArray(byteTimeDomainDataArray);
        var averageOverTime = totalOfAverages / lastWaveformChangeAverages.length
        
        lastWaveformChangeAverages.push([newestAverage, new Date().getTime()])
        totalOfAverages += newestAverage;
        
        return (newestAverage - waveformDiffMagnitudeThresholds[waveformDiffMagnitudeForThresholdIndex]) > averageOverTime
    }
    else {
        console.error("Unknown threshold strategy used!");
    }
}

function findAverageOfArray(array) {
    let total = 0;
    array.forEach(value => total += value);
    return total / array.length;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
