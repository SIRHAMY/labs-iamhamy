// tweakers

var isStationary = true; // whether the map moves or not
var maxIconImageSizePixels = 30; // the largest we'll go for pixels. all icons will be shrunk to this

// inner constants/globals

var config = null;
var map = null;
var iconsByName;
var isCanceled = false;
var allMarkers = [];

// actions

async function startVisualizationAction() {
    console.log("startVisualizatoinAction called!");

    isCanceled = false;

    if(map === null) {
        await initMapAsync(config);
    }
    await startVisualizationAsync(config);
}

async function restartVisualizationAction() {
    console.log("restartVisualizationAction called!");

    isCanceled = true;
    await removeAllMarkersAsync();
    startVisualizationAction();
}

// blogic

async function initMapAsync(config) {
    console.log("initMapAsync called!");

    // Input iconPaths here as
    iconsByName = {};
    // iconsByName[NAMEOFICON] = L.icon...
    /** example icon usage
     * L.icon({
        iconUrl: 'leaf-green.png',
        shadowUrl: 'leaf-shadow.png',

        iconSize:     [38, 95], // size of the icon
        shadowSize:   [50, 64], // size of the shadow
        iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
        shadowAnchor: [4, 62],  // the same for the shadow
        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
     */

    // iconsByName["trumpku"] = [
    //     L.icon({
    //         iconUrl: "resources/images/trumpku.png",
    //         iconSize: [50, 50],
    //         iconAnchor: [25, 50]
    //     })];
    // iconsByName["trumpjoke"] = [
    //     L.icon({
    //         iconUrl: "resources/images/trumpjoke.png",
    //         iconSize: [40, 60],
    //         iconAnchor: [20, 60]
    //     })];
    // console.log(iconsByName);

    // for(let emotion of Object.keys(bitmojiJson.comics)) {
    //     // console.log(emotion);
    //     iconsByName[emotion] = [];
    //     for(let comic of bitmojiJson.comics[emotion]) {
    //         var iconUrl = "./resources/images/" + bitmojiJson.mojiId + "/" + comic;
    //         // console.log(iconUrl);
    //         iconsByName[emotion].push(L.icon({
    //             iconUrl: iconUrl,
    //             iconSize: [50, 50],
    //             iconAnchor: [25, 50]
    //         }));
    //     }
    // }

    // if we were given images, then we iterate over them to load icons
    if(config.data.imgTagBlobMap !== undefined
        && config.data.imgTagBlobMap !== null) {
        Object.keys(config.data.imgTagBlobMap).forEach(
            imgTag => {
                iconsByName[imgTag] = [
                    L.icon({
                        iconUrl: config.data.imgTagBlobMap[imgTag],
                        iconSize: [maxIconImageSizePixels, maxIconImageSizePixels],
                        iconAnchor: [30, 60]
                    })
                ]
            }
        )
    }

    // sleeping to make sure that L.icons created
    // hamy : maybe it should be an await
    await sleep(3000);
    console.log("Beyond creating icons");

    map = L.map('mapid')

    const osmUrl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
    const osmAttribution = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
    const osmTileLayer = new L.TileLayer(osmUrl, {minZoom: 2, maxZoom: 15, attribution: osmAttribution});

    map.setView(
        // no default start? default to NYC cause center of the universe
        new L.latLng(
            config.data.startLat !== undefined && config.data.startLat !== null
                ? config.data.startLat
                : 40.7128, 
            config.data.startLng !== undefined && config.data.startLng !== null
                ? config.data.startLat
                : 74.0060),
        config.data.startZoom !== undefined && config.data.startZoom !== null
            ? config.data.startZoom
            : 10);
    map.addLayer(osmTileLayer);

    document.getElementById("restart-button")
            .addEventListener(
                "click",
                () => restartVisualizationAction());
    
    bounds = map.getBounds();
    console.log("initMap completed!");
    return true;
}

async function startVisualizationAsync(config) {
    console.log("startVisualizationAsync called!");
    locations = config.data.locations
    let bounds = map.getBounds();
    // var latLngBounds = L.latLngBounds(bounds._southWest, bounds._northEast);
    var lastLocation = null;
    for(let location of locations) {
        console.log("mapping location: ", location)

        // allows for cancellation from outer properties
        if(isCanceled) {
            break;
        }

        if(isStationary) {
            // Skip point if outside our viewbox
            if(!bounds.contains([location.lat, location.lng])) {
                continue;
            }
        } else {
            // If there's no last location, then this is the first location
            // and we don't need to pan
            if(lastLocation != null) {
                // source: https://stackoverflow.com/questions/20916953/get-distance-between-two-points-in-canvas
                var a = location.lat - lastLocation.lat;
                var b = location.lng - lastLocation.lng;
                console.log("a ", a);
                console.log("b", b);
                var distanceInCoordinates = Math.sqrt(a*a + b*b);
                console.log(distanceInCoordinates);
                var ratioToLargestDistance = distanceInCoordinates / 180.0;
                // The time is arbitrarily calculated based on me wanting a
                // spin around the world to be 60 seconds
                var durationToPanIn = ratioToLargestDistance * 3000;
                if(durationToPanIn < 1) {
                    durationToPanIn = 1;
                }
                // I think there's a weird edge case where durationToPanIn is
                // larger than the leaflet options allow
                if(durationToPanIn > 10) {
                    durationToPanIn = 10;
                }
                console.log("durationToPanIn", durationToPanIn);
                // docs: https://leafletjs.com/reference-1.3.4.html#map-panto
                map.panTo(
                    new L.LatLng(location.lat, location.lng),
                    { duration: durationToPanIn });

                console.log("sanity", map.getCenter());
                console.log("Lat: ", location.lat);
                console.log("Lng: ", location.lng);
    
                // We need to give the map time to pan before we go on with execution
                var isPanning = true;
                var lastCenter = null;
                while(isPanning) {
                    var currentCenter = map.getCenter();
                    // We need to account for weird rounding in JS
                    // Also map stops moving randomly sometimes so we just go
                    // on to the next one if we haven't moved since last time
                    if((Math.abs(currentCenter.lat - location.lat) <= 0.0001
                        && Math.abs(currentCenter.lng - location.lng) <= 0.0001)
                        || (lastCenter != null
                            && (lastCenter.lat === currentCenter.lat
                                && lastCenter.lng === currentCenter.lng))) {
                            break;
                    }
                    lastCenter = currentCenter;
                    await sleep(config.data.transitionDurationMilliseconds);
                }
            }
        }

        console.log("Creating stuff");

        let marker;
        if(location.imgTag !== undefined
            && location.imgTag !== null
            && iconsByName.hasOwnProperty(location.imgTag)) {
            marker = L.marker(
                [location.lat, location.lng],
                { icon: getRandomEntry(iconsByName[location.imgTag]) });
        } else {
            marker = L.marker([location.lat, location.lng])
        }
        marker.addTo(map);
        await animateDude(marker);
        allMarkers.push(marker);

        lastLocation = location;
        await sleep(config.data.transitionDurationMilliseconds);
    }

    console.log("All markers placed - visualization complete.");
    return true;
}

// TODO: Probably remove in favor of function call or something
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function getRandomEntry(array) {
    var randomInt = Math.floor(Math.random() * array.length);
    return array[randomInt];
}

async function animateDude(marker) {
    console.log("Animating dude");
    // Increase for a bit then back
    var changeAmount = 8;
    var increaseAmount = 1.15;
    var icon = marker.options.icon;
    for(let i = 0; i < changeAmount; i++) {
        var currentSizeArray = icon.options.iconSize;
        icon.options.iconSize = [currentSizeArray[0] * increaseAmount, currentSizeArray[1] * increaseAmount];
        marker.setIcon(icon);
        await sleep(50);
    }

    for(let i = 0; i < changeAmount; i++) {
        var currentSizeArray = icon.options.iconSize;
        icon.options.iconSize = [currentSizeArray[0] / increaseAmount, currentSizeArray[1] / increaseAmount];
        marker.setIcon(icon);        
        await sleep(50);
    }
}

async function removeAllMarkersAsync() {
    console.log("removeAllMarkerscalled!");

    while(allMarkers.length > 0) {
        map.removeLayer(allMarkers[0]);
        allMarkers.splice(0, 1);
    }
    return true;
}

/**
 * config object
 * LatLongInitial - latLong to start at
 * function startGeolog - callback to start the geolog processing
 */

 /**
  * What we want for V0
  * * Stationary view
  * * Just pop up markers at all the locations
  * * Ability to say what pics we want at each location
  * * Ability to parse through a given csv with arbitrary columns + output
  * to json that Geolog can read
  * Overview
  * * Initialize map with config obj
  * * Start processing the data obj
  * ** For each item, we hit process of animating insertion/deletion (small/big/small)
  * ** Once that's done, do a callback or promise which would allow us to move onto next item
  */