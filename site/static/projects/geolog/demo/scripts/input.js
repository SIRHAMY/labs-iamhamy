// constants

const uploadOptionsContainerId = "input-container__upload-section";
const imageUploadContainerId = "input-container__image-upload-container";

document.getElementById("input-section__csv-upload")
    .addEventListener(
        "change",
        uploadCsvAction,
        false);

// actions

function uploadCsvAction(uploadEvent) {
    uploadAndTransformCsv(
        uploadEvent.target.files[0],
        /* papaParsedCsvCallback */ uploadCsvActionCompleted);
}

async function uploadCsvActionCompleted(papaParsedCsv) {
    console.log("uploadcsvactioncompleted called!");
    console.log("parsedCsv: ", papaParsedCsv);

    await configurePapaParsedCsvDataAsync(papaParsedCsv);

    // if we have any img tags, we create a section so that people can
    // add them
    if(config.data.locations.some(
        location => 
            location.imgTag !== undefined
            && location.imgTag !== null)) {
        config.data.imgTagBlobMap = {};
        createImgUploadAreaForLocations(config.data.locations);
    } else {
        dataConfigurationCompleteActionAsync();
    }
}

function imgUploadAction(uploadFileEvent) {
    // we have to grab the uploadFileEvent real quick otherwise it goes away
    let eventTarget = uploadFileEvent.target;
    let imgTag = eventTarget.imgTag;

    // create a blob for the image file
    let imgUrl = URL.createObjectURL(eventTarget.files[0]);

    // set the imgTag in map to point to blob
    config.data.imgTagBlobMap[imgTag] = imgUrl;
}

async function uploadImgSubmitActionAsync() {
    console.log("uploadImageSubmitAction called!");
    dataConfigurationCompleteActionAsync();
}

async function dataConfigurationCompleteActionAsync() {
    makeMapVisible();
    startVisualizationAction();
}

// helpers

function uploadAndTransformCsv(
    file,
    papaParsedCsvCallback) {
    console.log("I should upload the csv");

    // PapaParse config object: https://www.papaparse.com/docs#config
    papaConfig = {
        complete: papaParsedCsvCallback,
        header: true,
        skipEmptyLines: true
    }

    // https://www.papaparse.com/docs#local-files says it's this easy
    Papa.parse(
        file,
        papaConfig
    )
}

async function configurePapaParsedCsvDataAsync(papaParsedCsv) {
    // iterate over locations data
    // pop into my json blob
    // let jsonData = {};
    let locationsData = [];
    papaParsedCsv.data.forEach(
        dataRow => {
            let jsonDataRow = {
                lat: parseFloat(dataRow.lat),
                lng: parseFloat(dataRow.lng),
                imgTag: dataRow.imgTag
            }
            locationsData.push(jsonDataRow);
        })

    // this is the master shape of the config
    config = {
        data: {
            locations: locationsData
        },
        imgTagBlobMap: null
        // data: JSON.parse(data)
    }       

    return true;
}

function makeMapVisible() {
    document.getElementById("input-container")
        .classList
        .toggle("hidden", true);
    document.getElementById("map-container")
        .classList
        .toggle("hidden", false);
}

async function coordinateStringToLatLngAsync(coordinateString) {

}

function createImgUploadAreaForLocations(locationsData) {
    // hide the upload section
    document.getElementById(uploadOptionsContainerId)
        .classList
        .toggle("hidden", true);

    // figure out what img tags we need uploads for
    let foundImgTagsMap = new Map();
    locationsData.forEach(
        location => {
            if(location.imgTag !== undefined
                && location.imgTag !== null
                && !foundImgTagsMap.has(location.imgTag)
                // HG: we treat blank space as no imgTag
                && location.imgTag.trim() !== "") {
                foundImgTagsMap[location.imgTag] = 1;
            }
        }
    )

    // HG: if there are no images in the map, then we should go straight to
    // the completed state
    if(Object.keys(foundImgTagsMap).length === 0) {
        uploadImgSubmitActionAsync();
        return;
    }

    Object.keys(foundImgTagsMap)
        .forEach(imgTag => createImgUploadButtonForImgTag(imgTag));

    addSubmitButtonAndUnhideImgUploadContainer();
}

function createImgUploadButtonForImgTag(
    imgTag
) {
    let imageUploadContainerNode = document.getElementById(imageUploadContainerId);

    let text = `Upload file for the "${imgTag}" imgTag:`;
    let paragraphElement = document.createElement("p");
    paragraphElement.textContent = text;
    imageUploadContainerNode.appendChild(paragraphElement);

    let inputElement = document.createElement("input");
    inputElement.type = "file";
    inputElement.imgTag = imgTag;
    // this event creates the URL blob we'll use later
    inputElement.addEventListener(
        "change",
        imgUploadAction,
        false
    )

    imageUploadContainerNode.appendChild(inputElement);
}

function addSubmitButtonAndUnhideImgUploadContainer() {
    let imageUploadContainerNode = document.getElementById(imageUploadContainerId);
    let submitButton = document.createElement("button");
    submitButton.textContent = "Confirm img choices";
    submitButton.classList = submitButton.classList + "hamy-button";

    submitButton.addEventListener(
        "click",
        async () => uploadImgSubmitActionAsync()
    )
    imageUploadContainerNode.appendChild(submitButton);

    document.getElementById(imageUploadContainerId)
        .classList
        .toggle("hidden", false);
}