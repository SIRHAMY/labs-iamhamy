---
title: "HamForGood"
date: 2020-04-03T22:24:15-04:00
tags: [
    "hamforgood"
]
comments: false
---

`HamForGood` is a [HAMY.LABS](https://labs.hamy.xyz) initiative investing 10% of all HAMY.LABS project profits towards Causes that better the world. These investments are left to grow, making 1% yearly disbursements so that 1) good Causes are continuously supported and 2) the principal grows year over year so that the giving power increases over time.

# HamForGood

* HamForGood applies to _all_ HAMY.LABS projects
* HamForGood invests 10% of project profits into the HamForGood Fund
* Each year, the HamForGood Fund will give at least 1% of its value to Charities supporting selected Causes
    * Each Cause will have a % distribution which will determine how much of the 1% it gets
    * Each Cause will have 1 or more Charities that support that Cause, distributions will be split evenly between these
* Disbursements will happen yearly

An example:

Say a shirt is sold from [my shop](https://shop.hamy.xyz/) for $30 and total costs were $20. This leaves us with $10 of profit. So we take 10% of that profit ($1) for HamForGood and invest it into the HamForGood Fund. By end of year, we should have at least $0.01 of that to give away

# HamForGood Fund

The idea is that by investing money instead of giving now, we can actually increase our giving power over a long time horizon and increase the positive impact we have. [My calculations](/posts/2021-hamforgood-refactor) put this horizon at around 45 years, 80 if we have some serious headwinds.

* HamForGood Fund invests in low-cost Index Funds - preferably with good ESG ratings
* The expectation is that 3-4% yearly withdrawal rate is sustainable and thus that 1% will lead to sustainable principal growth
* Currently will be held and managed personally by Hamilton but long-term goal is to create a DAF to make it more official (but that takes a lot of capital to setup and continue running so gotta get some $ first) 

# Projects

All HAMY LABS project profits feed into HamForGood so all projects are HamForGood projects.

* [All Projects](/projects)

# Causes + Organizations

Causes are selected based on need and alignment on values. Usually we add a new Cause if we see some problem with the world that we're not currently covering. In general, we choose Causes and Organizations to support based on:

* Problem magnitude to the world
* Need
* How efficient the organization is at solving the problem
* Relevance to HAMY LABS - its Domains, Projects, and members
* Support 1 global organization and 1 local organization (top down and bottom up)

Links:

* [All HAMY LABS financials (including HamForGood)](https://docs.google.com/spreadsheets/d/1-NjX6vmudm5iHwCuOKvlvs1HymWRkEqW9wr8Esn6g7I/edit?usp=sharing)

# FAQ

## Why HamForGood?

I created `HamForGood` as a system to help the world through a medium I enjoy - building projects. My hope is to keep it simple, sustainable, and impactful.

## Why invest in a Fund (HamForGood Fund) vs giving directly?

As I've gotten older, I've come to the conclusion that some games are better played long than short. I think money is one of these things and that's why I believe in and work towards Financial Independence in my daily life through [the Simple (slow and relatively sure) Path to Wealth](https://blog.hamy.xyz/posts/the-simple-path-to-wealth/).

I recognized that to achieve Financial Independence, I need to live off the earnings of my investments. In that strategy, you maximize the amount of money you can live off of by living below your means and allowing your means to grow exponentially.

Charitable giving seems to be a similar paradigm - you want to maximize the amount you can give so that Charities can do more work but you have limited assets to do so. If we apply the ideas of Financial Independence here, it seems we should be able to exponentially increase our giving power by giving sustainably long term.

This is not a fast pay off, but it's fast enough that I think it's attainable and it's powerful enough that I think it's worth it.

By my estimates, this approach will give vastly more money within 50 years. See [HamForGood Refactor Calculations](https://labs.hamy.xyz/posts/2021-hamforgood-refactor/)
