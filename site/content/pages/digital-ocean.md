---
title: "Digital Ocean - Get $100 in Digital Ocean credit"
date: 2020-11-14T02:17:59Z
tags: [
    'digital-ocean'
]
comments: false
---

I build a lot of my [projects](/projects) on Digital Ocean. They're simple, they're powerful, and they're relatively cheap (starting at $5).

They have a referral deal where my referrals get $100 and once they've spent $25, I get $25 as well. So help me help you help me: [Grab your $100 credit here](https://m.do.co/c/a4a1893448be)