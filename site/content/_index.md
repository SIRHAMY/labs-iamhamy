---
date: "2017-06-26T18:27:58+01:00"
title: "HAMY.LABS"
---

Welcome to **HAMY.LABS**, where I talk about all the stuff I'm trying to do.

<- Use that to navigate

Or check out some of my featured stuff like

* [moon-eye](/projects/moon-eye)
* or my post on [how I host my blogs on Google Kubernetes Engine](/posts/labs-hosted-on-gke/)
* or [how to get updates when I post new stuff](https://hamy.xyz/subscribe)

 