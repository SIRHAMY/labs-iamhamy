---
title: "Custom audio visualizations"
date: 2020-11-23T15:39:44Z
tags: [
    'audio-reactive',
    'products'
]
comments: false
---

I've worked with artists around the world to create custom audio visualizations with code.

* Music videos
* Audio visualizations

Need one for your project? [Let's talk](https://hamy.xyz/connect).

# Example Work

<iframe width="560" height="315" src="https://www.youtube.com/embed/D_Fku3jmwk8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/wM5IomRjjlU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/X3lw0VcgVic" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Further Reading

Read about [my audio-reactive creations](/tags/audio-reactive)