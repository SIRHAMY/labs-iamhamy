---
title: "Will I get cancer?"
date: 2019-03-10T23:12:15-04:00
tags: [ 
    "projects",
    "willigetcancer",
    "tikkunolam",
    "hamforgood"
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/willigetcancer/willigetcancer-screenshot2.png"
---

_Will I get cancer? is live! Check it out at [willigetcancer.xyz](https://willigetcancer.xyz)_

# Will I get cancer? (WIGC)

[Will I get cancer?](https://willigetcancer.xyz) (WIGC from here on out) is a portal for cancer data exploration as relevant to the individual. It leverages open data sets and wraps them in an intuitive interface.

# How was it built?

I detail WIGC's system design as part of my tutorial on [Zeit Now / GitLab CI integration](/posts/zeit-now-gitlab-ci-integration).

# Why was it built?

I built WIGC as part of [Tikkun Olam Tuesdays](https://blog.hamy.xyz/posts/introducing-tikkun-olam-tuesdays/), an initiative I implemented this year to help make improving the world habitual. Cancer is a pretty big deal (if you don't think so, you obvs haven't played around with WIGC long enough) so I figured it was as good a domain to tackle first as any. I'd always wondered (and kinda assumed I would) whether I would get cancer or not so decided to take that exploration and turn it into something more sizable / shareable. This was that.

The primary goal of the site is to raise awareness about people's own risks for cancer in the hope that making that knowledge more personal would lead more people to actually act rather than nod their head and move on.

My secondary goals included

* Working with serverless
* trying out different react frameworks/libraries
* using pre-made styles
* posting to dev.to/ProductHunt
* trying out different promotion paradigms

# learn more

_Related posts filed under [#willigetcancer](/tags/willigetcancer)_