---
title: "thevalueofmoney"
date: 2020-01-05T10:35:46-05:00
tags: [
    "projects",
    "thevalueofmoney"
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/thevalueofmoney/MoneyOverTime-coldbrew-web.png"
---

thevalueofmoney is live at https://thevalueofmoney.xyz

# what is it?

`thevalueofmoney` is a hub for various calculators / calculations / data surrounding the question of "What is the value of money?".

[MoneyOverTime Calculator](https://thevalueofmoney.xyz/calculators/moneyovertime) is the first calculator which calculates and graphs the value of money over a given time horizon, with options to control additional investments, investment frequency, and interest rates.

# why was it built?

I think a lot about money, not because I think it's the most important thing in the world, but because I know that it underlies almost every interaction that occurs in the world. It's not something that I always try to optimize for but it is something that I'm very careful to manage so that it doesn't become a hindrance in my pursuits now or in the future.

As a part of these common thoughts, I often wonder about the expected value of different monetary tradeoffs. Should I get this coffee? What am I really sacrificing by buying my lunch at this restaurant vs making my own dinner?

These calculations aren't hard per se, but they are a bit tedious so I wanted to build a one-stop shop for quickly getting these answers to my common questions.

Thus `thevalueofmoney` was born.

# how was it built?

I'm currently using:

* NextJS - web framework
* Netlify - hosting

# learn more

[Read more about the project](/tags/thevalueofmoney)