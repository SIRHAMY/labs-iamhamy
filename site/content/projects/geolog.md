---
title: "Geolog"
date: 2018-07-25T08:27:45-04:00
description: "About the page"
draft: false
---

**Try it now!** [geolog demo](/projects/geolog/demo)

Geolog is mapping software that produces timelapse videos over arbitrary location data. An example output:

<iframe width="560" height="315" src="https://www.youtube.com/embed/AnYlr8gjOLo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# How do I use it?

Navigate to the demo linked to at the top of this page. Upload a csv with appropriate columns and data format.

columns:

* lat - latitude for the point
* lng - longitude for the point
* imgTag - optional tag to use for displaying points with specific images

# I found a bug!

[Shoot me an email or dm](https://hamy.xyz/connect)