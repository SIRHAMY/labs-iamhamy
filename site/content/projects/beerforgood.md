---
title: "Beer for Good"
date: 2019-06-13T15:35:12-04:00
tags: [
    "projects",
    "beerforgood"
]
comments: false
draft: true
---

Beer for Good is live! [Check it out](https://beerforgood.xyz)

# what is it?

Beer for Good is a service that solicits donors for "beers" for causes that help "heal the world" which are then funneled towards organizations working in those areas. It works off the idea that people don't like giving money directly to causes, however are more willing to give when that money is directly tied to a material gain - in this case by giving beers and also by showing what direct impact those beers have.

The value I hope to provide via this project is allowing people an easy place to give (and continue giving) towards the causes they care about without having to do the footwork of finding and vetting useful causes to give to themselves - as we'll do that on our end. Is this a big value add? Maybe not, but I thought it was worth a shot.

# why was it built?

I built this as part of my ongoing [Tikkun Olam Tuesdays](https://blog.hamy.xyz/posts/introducing-tikkun-olam-tuesdays/) effort in which I devote a little bit of time each week towards healing the world. My previous project in this domain [Mine for Good](/projects/mineforgood) tried to do something similar wrt raising funds for causes that are then funneled to supporting organizations. However, I realized that that paradigm wouldn't make much money (seriously, check out [my analysis on browser mining revenue](https://blog.hamy.xyz/posts/ad-revenue-vs-browser-crypto-mining-revenue/)) and thought that direct funding would probably make a better choice. So here it is.

# how was it built?

Really it's just a simple SPA hosted on Netlify and utilizing Stripe for payment processing.

* [NextJS](https://nextjs.org/) - I've used NextJS for my last few projects and I really do love it. It's super simple to get off the ground and it's got a lot of cool features if you're trying to dive into more things like static site generation and server-side rendering. Obvs I haven't used it for anything huge but for my side projects it's been perfect.
* [Semantic UI](https://semantic-ui.com/) - I'm really not a big fan of css tweaking which some people find funny cause I am pretty meticulous with [my art creations](https://www.instagram.com/hamy.art/). For me it's just kind of boring - the cool parts of apps are what's actually making things happen, not how the button is aligned or the font size (unless this is a visual art piece then ofc that matters). As such, I delegate a lot of this stuff to ui frameworks that give me a nice feal that's super easy to get up and running with at the expense of having a pretty plain, uniform frontend. At least it's better than my brutalist frontend /shrug.
* [Netlify](https://www.netlify.com/) - I'm using Netlify again cause it's just so easy to set up serving and CI/CD with GitLab, plus it's free. For my more serious projects (like this site for instance), I'm using a more powerful cloud env (GCE rn), but for these side-projects Netlify, is more than enough.
* [Stripe](https://stripe.com/) - I'm using Stripe for payment processing because it's pretty easy to get up and running and it works. As a boon, they also have drastically reduced fees for non-profits, so if/when I end up non-profiting for these Tikkun Olam projects, I'll get the reduced fees basically for free engineering-wise.

# learn more

[Read more about the project](/tags/beerforgood)