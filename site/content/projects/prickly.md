---
title: "prickly"
date: 2020-05-31T16:07:00Z
tags: [
    "projects",
    'audio-reactive',
    'art'
]
comments: false
coverimage: "https://storage.googleapis.com/iamhamy-static/labs/projects/prickly/prickly-screenshot.png"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/wM5IomRjjlU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# what is it?

#prickly is an audio visualizer focused around triangular forms.

# how can I get it?

prickly is available now on [vicidual](/projects/vicidual). I'm also doing commissions with these visualizers so if you're intersted [reach out to me](https://hamy.xyz/connect)

# how was it built?

I built it with p5js - a continuation of my exploration of web-based audio visualizations (see also: [monoform](/projects/monoform) and [maudern](/projects/maudern)).

# learn more

[Read more about the project](/tags/prickly)