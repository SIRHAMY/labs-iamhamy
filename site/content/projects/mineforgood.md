---
title: "Mine for Good"
date: 2019-05-26T12:25:48-04:00
tags: [
    "projects",
    "mineforgood",
    "tikkunolam",
    "hamforgood"
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/mineforgood/mineforgood-screenshot2.png"
---

_Mine for Good is live! Check it out at [mineforgood.xyz](https://mineforgood.xyz)_

# what is it?

Mine for Good is an app that lets you utilize the power of your machine's internet browser to support causes you care about. It does this using a browser-based crypto miner which in turn rewards select crypto tokens (right now [Monero](https://www.getmonero.org/)) which are then converted into USD and distributed among select organizations for each cause.

The organizations supported via mining for each cause are listed on each cause page. Take the [Mine for Climate Change](https://mineforgood.xyz/climatechange/) page for example.

To read more about how it works, check out the site's [FAQ](https://mineforgood.xyz/faq/)

# why was it built?

This year, I've been thinking more and more about the state of the world and my privilege to be able to change it for the better. To that end, I've introduced a new endeavor called [Tikkun Olam Tuesdays](https://blog.hamy.xyz/posts/introducing-tikkun-olam-tuesdays/) which boils down to habitual mindfulness and intent around the theme of healing the world.

I've always wanted to build something related to crypto and was particularly interested in the ability to mine in-browser. This interest coupled with my theme of healing the world directly led to the idea of mining in-browser with proceeds benefitting charities.

To be frank, this is a super ineffective way to actually heal the world but I like building shit so I decided to go ahead with it and see what happens. To show just how ineffective it is, I decided to include the calculation as part of the primary miner UI. I'll keep it a mystery and [let you see for yourself](https://mineforgood.xyz/climatechange/) in an effort to drive more traffic to my site. _\*commence evil cackle\*_

# how was it built?

With my mind, duh.

* [NextJS](https://nextjs.org/) - I've used this framework for my past few projects ([Will I get cancer?](/projects/willigetcancer) being another) and it's really growing on me. It's quick to get started, has a ton of documentation and examples, and is feature-filled (server-side rendering and the ability to export as static right out of the box being two highlights for me)
* [React](https://reactjs.org/) - Within the framework, everything's written in React / JS / CSS / HTML etc. 
* [Netlify](https://www.netlify.com/) - For hosting, I'm using Netlify cause it's free, super easy, and has lots of nice features like custom domain && Let's Encrypt, branch-based CI, and GitLab integration baked in
* [Minero](https://minero.cc/) - I toyed around with building my own mining pool to support browser-based mining, but eventually decided it wasn't worth it right now and instead to just go with a pre-built solution to hasten release. They take a 10% cut but I think that's only fair as mining is not really fiscally viable and they're really doing all the heavy lifting for this project. Given the demise of several other similar projects (CoinHive being the most well-known of these), it's probably best to let them take the risk so I can move on to other projects.

# learn more

[Read more about the project](/tags/mineforgood)