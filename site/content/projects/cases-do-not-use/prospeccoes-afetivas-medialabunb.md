---
title: "case: webz featured in Prospeccoes Afetivas em Tempos de Pandemia @ MediaLab UnB"
date: 2020-05-31T16:21:31Z
tags: [
    "projects",
    'webz',
    'cases'
]
comments: false
coverimage: 'https://storage.googleapis.com/iamhamy-static/labs/projects/webz/cases/prospeccoes-afetivas-screenshot.png'
---

Images from my [webz](/projects/webz) series were featured in [Prospeccoes Afetivas em Tempos de Pandemia @ MediaLab UnB](https://prospeccoesafetivas.medialab.unb.br/) a virtual exhibition on the topic of art, society, and their interaction, particularly in the unfamiliar conditions brought on by coronatine.

Here's the prompt:

> What do we affect and what affects us? It is in the relation of forces between art and society, in a conjunction and coupling between sign and power that a philosophy of resistance by affection is combined. The purpose of this exhibition are affections that appear in different artistic proposals, many of which are perceived in the mediatized sensations that are available today, but also in the passage through the sensory perceptions of each work exposed here.
> The curatorship sought to attract forces instead of reproducing and inventing art forms. This proposal, too, moves away from the harassment that we continue to receive from interactive digital media, so that we continue to consume to save late capitalism. Our homes are no longer just spaces for family living.
> We are living in a new cyberspace, beyond what was already imagined by science fiction and found in literature. In the re-design of a new collaboration space, in which we can meet to share ideas of art, design and technology, the exhibition entitled Affective Prospecting presents adventures that arose in times of pandemic.


and here's my response in video form:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QluzJ8WahmI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

There's some really cool work from other artists, so check out the [exhibition](https://prospeccoesafetivas.medialab.unb.br/) while it's live. You can check out more webz images on [@hamy.art](https://www.instagram.com/hamy.art/) and learn more about webz, how it works and how it was built, on the [webz project page](/projects/webz).