---
title: "CASE STUDY: Live from quarantine - Griffin Hanekamp x monoform"
date: 2020-04-16T23:25:32-04:00
tags: [
    "projects",
    "monoform",
    'cases'
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/monoform/monoform-x-griffin-hanekamp-screenshot.png"
---

For those following along, I haven't exactly been idle during quarantine. So far I've published [coronation](/projects/coronation) - a data monolith visualizing the spread of coronavirus around the globe - and [covidart](/projects/covidart) - a series of art creations under [HamForGood](/hamforgood) with 80% of the proceeds benefitting those affected by and fighting the COVID-19 virus.

But just because I've been doing coronatine-related things doesn't mean I've let go of any of my [goals](https://blog.hamy.xyz/posts/2020-q1-review/). On the contrary, I've actually found this time to be a perfect time to jump on some more collabs that have been sitting on the back burner.

One such collab was working with my good friend [Griffin Hanekamp](https://www.instagram.com/griffin_hanekamp/) to visualize his newest mix [2020.04.11 Live from Quarantine](https://soundcloud.com/griffinsounds/20200411-live-from-quarantine). 

Here's the visualization:

<iframe width="560" height="315" src="https://www.youtube.com/embed/y5hHeb_KOcY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I built this on top of [monoform](/projects/monoform), a minimalist visualizer built around the display of 3d objects. Due to the mix title and #times, I thought it'd be fun to toss in a virus model and see how it worked. I think it went well.

In the process I also overhauled the sound engine, giving it a sense of "velocity". Real objects don't move with absolute position, but instead have velocities at which they traverse this plane. Adding in this velocity and adding capping rules to optimize for eye and screen capture yielded a much smoother bounce and a much more pleasing visualization.