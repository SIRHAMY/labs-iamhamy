---
title: "coffeedash"
date: 2020-04-16T23:01:35-04:00
tags: [
    "projects",
    "coffeedash"
]
comments: false
draft: true
---

# what is it?

coffeedash is an endless runner game in the likes of Temple Run. I wanted to build something light-hearted and to give back during the COVID-19 crisis.

It's part of [HamForGood](/hamforgood), with 80% of its proceeds going to support the fight against the virus and those affected by it.

# how can I get it?

TBD

# how was it built?

I built coffeedash in Unity. I wanted to learn a new technology and figured a game engine would be cool to learn. I considered Unreal, Godot, and Unity but eventually went with Unity for its wide platform and user support, maturity, and expansive asset store.

# learn more

[Read more about the project](/tags/coffeedash)