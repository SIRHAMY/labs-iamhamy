---
title: "careboi"
date: 2020-05-10T13:59:47Z
tags: [
    "projects",
    "careboi"
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/careboi/careboi-screenshot2.png"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/WIGYBUw9oGI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# what is it?

[CareBoi](https://careboi.xyz) is a creative tool that allows you to create custom care emojis. The hope is that people will be able to use it to express their care for the nouns they hold dear, adding a little bit of positivity to the world.

If you end up making your own CareBoi, please let me know! You can [tag me on Insta](https://www.instagram.com/sir.hamy/) or just add the hashtag [#careboi](https://www.instagram.com/explore/tags/careboi/).

# how can I get it?

[CareBoi](https://careboi.xyz/) is available in a browser near you! Just go to careboi.xyz

# how was it built?

I built CareBoi using:

* NextJS - a full-packaged React bundle, dealing with routing, packaging, and even the ability to export to static from the get-go
* Netlify - cause it's free and super easy
* MaterialUI - I'd been using Semantic UI but it's not very well supported these days so I went to something that was getting support
* P5 - perfect for adhoc creative projects and something I've used extensively in the past for [visualizers](/projects/monoform) and [generative art](/projects/covidart)

# learn more

[Read more about the project](/tags/careboi)