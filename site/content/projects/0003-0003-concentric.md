---
title: "Monoverse: 0003.0003 - Concentric"
date: 2022-04-08T05:10:50Z
tags: [
    "projects",
    'active',
    'monoverse',
    'art'
]
comments: false
---

_Part of [The Monoverse](https://labs.hamy.xyz/projects/the-monoverse/)._

Monoverse: [0003 - Forms](/projects/monoverse-0003-forms)

# Overview

On concentrics.

# Instances

* 0000 - [NFT](https://opensea.io/assets/matic/0x2953399124f0cbb46d2cbacd8a89cf0599974963/21067349529728974219416647855437048644621326606714483021912751861972879802369/)
    * Description:
        * Built with Unity.
* 0001 - [NFT](https://opensea.io/assets/matic/0x2953399124f0cbb46d2cbacd8a89cf0599974963/21067349529728974219416647855437048644621326606714483021912751863072391430145/)
    * Description:
        * Built with Unity
