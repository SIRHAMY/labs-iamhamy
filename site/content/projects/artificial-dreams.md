---
title: "ARTIFICAL_DREAMS"
date: 2022-08-27T16:50:15Z
tags: [
    "projects",
    'active',
    'art'
]
comments: false
---

A picture is worth a thousand words. A word is worth a thousand pictures.

ARTIFICIAL_DREAMS.DIGITAL_CONSUMPTION."feasting in virtual reality".1

![feasting in virtual reality](https://storage.googleapis.com/iamhamy-static/labs/projects/artificial_dreams/feasting_in_virtual_reality_1_small.jpg)

# Overview

ARTIFICIAL_DREAMS is an exploration of how Artificial Intelligence models visualize the world. Most of these models have been trained on data representing how humans visualize the world through art, photographs, and other visual artifacts we’ve created throughout history. By exploring their perspectives, we’re indirectly exploring our own perspectives - just in representations that never came to pass.

We can say that a goal or outcome that has not been realized is an aspiration, a DREAM. We can say that an entity that is not organic is ARTIFICIAL. Thus we create ARTIFICIAL_DREAMS.

# Build

## Creating the images

I've been utilizing services that wrap modern text-to-image AI model implementations rather than building my own. This is a departure from previous projects like [Blinder](https://labs.hamy.xyz/projects/blinder/) but inline with projects like [Statutes](https://labs.hamy.xyz/projects/statutes/).

Ultimately I didn't feel that my own implementation would give me an unfair advantage over existing public offerings so I decided to use those.

A few I've been playing with:

* [Midjourney AI](https://www.midjourney.com/home/) - This is the best quality AI I've found. Here's my guide to [Generate art with the best AI for Creators](https://labs.hamy.xyz/posts/the-best-text-to-image-ai-for-creators/)
* [Night Cafe Studio](https://nightcafe.studio/) - Good quality, reasonable prices. Seems to run on Stable Diffusion which is similar to DALL-E 2's model if my understanding is correct. (Guide: [Generate Art with Text using AI](https://labs.hamy.xyz/posts/generate-art-with-text/))
* [Craiyon](https://www.craiyon.com/) - Looks promising but doesn't support images larger than a thumbnail right now which hinders its usefulness. (previously DALL-E Mini).
* DALL-E 2 - Lul I wish, been on the waitlist for months.

# Selected Works

Here are a few select works from this collection. For a more complete view, check [Instagram: hamy.art](https://www.instagram.com/hamy.art/). 

ARTIFICIAL_DREAMS.DIGITAL_CONSUMPTION."feasting in virtual reality".1

![feasting in virtual reality](https://storage.googleapis.com/iamhamy-static/labs/projects/artificial_dreams/feasting_in_virtual_reality_1_small.jpg)

ARTIFICIAL_DREAMS.FAST_TRAVEL."biking in virtual reality"

![biking in virtual reality](https://storage.googleapis.com/iamhamy-static/labs/projects/artificial_dreams/biking_in_virtual_reality_small.jpg)

ARTIFICIAL_DREAMS.FAST_TRAVEL."a dog with a virtual reality headset"

![a dog with a virtual reality headset](https://storage.googleapis.com/iamhamy-static/labs/projects/artificial_dreams/a_dog_with_a_virtual_reality_headset_small.jpg)