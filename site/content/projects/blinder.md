---
title: "blinder"
date: 2019-08-09T07:50:36-04:00
tags: [
    "projects",
    "blinder",
    "art"
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/blinder/blinder-screenshot-square.jpg"
---

[See all blinder outputs](https://art.hamy.xyz/blinder)

![Example blinder output](https://storage.googleapis.com/iamhamy-static/art/projects/blinder/1565239668.9458745lottemeijer_oyy71fLMZCY.jpg)

# what is it?

Blinder explores the relationship between eyes and the human perception of art. It uses machine learning and creative frameworks to understand and manipulate images.

# why was it built?

I saw a whole fine art gallery displaying a collection of photos with the eyes crossed out. I was like, I can do that. So I did that.

# how was it built?

The general control flow goes like this:

* Initiate script
* If image not given, grab a random image from [Unsplash](https://unsplash.com/)
* Use `opencv` to find the faces, then eyes in the image
* Draw on image based on selected DrawMode

An image in debug mode:

![A blinder image in debug mode](https://storage.googleapis.com/iamhamy-static/art/projects/blinder/1565358193.0545468hkoffishall_8YijGwqP2Rk.jpg)

[See more debug outputs](https://art.hamy.xyz/blinder)

## tech stack

* Python - I'm joining a new team at FB that uses a lot of Python so I decided to use Python for this project to knock some of the rust off. Turns out this was a pretty good decision as Python's got a very mature community of libraries for just about anything.
* [Unsplash](https://unsplash.com/) - I just love this service. Unlimited, free, high-quality, use-for-anything images with a simple api that works. If you've got a project that needs images, use it. It's great.

# exhibitions

* [ClarksRealityGallery](https://www.instagram.com/p/CBcfiaBhLis/?utm_source=ig_web_copy_link)

# faq

* Why do the names look so weird e.g. `1565237052.9555256melinacesarini_oAO-U6PFuaE`?

Each name is a unique key consisting of the timestamp it was created, the username of the artist that took the picture, and the unique id of the image on Unsplash. This name is used as a key for Blinder's identification and manipulation pipeline so I found it satisfying to preserve.

* The project is supposed to draw on the eyes of the subjects but a lot of times it's not on their eyes. Is this intentional?

Lol no, it's supposed to draw on their eyes, but the model I'm using doesn't seem that good. This is good and bad. Good because it adds in some extra randomness that can lead to happy little accidents. Bad cause sometimes it's drawing in just random places that don't make sense / look good. May find a better model at some point.

# learn more

[Read more about the project](/tags/blinder)
