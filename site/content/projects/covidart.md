---
title: "covidart"
date: 2020-04-13T10:22:42-04:00
tags: [
    "projects",
    "hamforgood",
    "covidart",
    "art"
]
coverimage: "https://storage.googleapis.com/iamhamy-static/labs/projects/covidart/entire-encouragement-publish-portrait.png"
comments: false
---

[![entire encouragement](https://storage.googleapis.com/iamhamy-static/labs/projects/covidart/entire-encouragement-publish-portrait.png)](https://shop.hamy.xyz/collections/covidart)

# what is it?

covidart is an art series I cooked up while quarantining during COVID-19. It's part of my [HamForGood](/hamforgood) portfolio with 80% of the proceeds going to benefit organizations fighting the virus.

# how can I get it?

You can see the collection on [@hamy.art](https://www.instagram.com/hamy.art/) and purchase on [HAMY.SHOP](https://shop.hamy.xyz/collections/covidart)

# how was it built?

The covidart series was created using various creative code libraries / creations I've made in the past like [webz](/projects/webz)

# exhibitions

* [MediaLab UnB](https://labs.hamy.xyz/projects/cases/prospeccoes-afetivas-medialabunb/)

# learn more

[Read more about the project](/tags/covidart)