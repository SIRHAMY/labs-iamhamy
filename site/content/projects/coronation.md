---
title: "coronation"
date: 2020-03-10T19:27:57-04:00
tags: [
    "projects",
    "web",
    "threejs",
    "coronation",
    "art"
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/coronation/coronation-screenshot.png"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/eXF-nbkJ2uQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# what is it?

[coronation.xyz](https://coronation.xyz) is a site that visualizes the progress of the COVID-19 infection around the globe. It uses features of global data to highlight and modify the globe to reflect the status of the infection over time.

# how was it built?

I decided to go with web for this project because it's a universal platform and I figured if I was building something to reflect on the global situation then I shoul make it as globally available as possible.

Though my recent web projects (see [maudern](/projects/maudern) and [monoform](/projects/monoform)) have largely been built with [p5js](p5js.org), I decided to go with [three.js](threejs.org) instead. I didn't do this based on the library features per se, instead I did it based on the availability of a key package. I knew the hardest part about this would be to construct a 3d globe, overlay it with geojson data, and have the ability to update parts of that overlay based on the geojson data so I looked for js implementations of such globes and eventually found [three-globe](https://github.com/vasturiano/three-globe). After looking at the docs, I confirmed that it should do everything I needed then dove into a quick example to make sure I could get it working. Once that proved fruitful, I decided to go with it - threejs is something I've wanted to play around with for awhile and the globe thing was pretty full-featured.

For projects like this I've historically gone the single page app route, using ReactJS, SemanticUI, and NextJS (see [thevalueofmoney](/projects/thevalueofmoney), [mineforgood](/projects/mineforgood), and [willigetcancer](/projects/willigetcancer)) but I'd just done a major [revamp](https://archive.hamy.xyz/entries/iamhamy-redesign-build-focus/) of my sites so [Hugo](https://gohugo.io/) - the static site generator I use to [build my sites](https://labs.hamy.xyz/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free/) - was top of mind.

I thought about what my site really needed to have - a landing page on which to display my canvas and a way to style things non-manually - and realized that Hugo offered all of these features plus the benefit of having a significantly ligher payload. The smaller payload is important to me as I just ran the calculations and found that [I can serve about half a million users / month for free](https://labs.hamy.xyz/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free/) in my current hosting paradigm but this is based on the bandwidth I use so adding additional payload would substantially eat into this number of users. I ran a quick test and found that one of my sites using my previous stack of ReactJS, SemanticUI, and NextJS clocked in at around 234kb per page (served by me) whereas my Hugo builds were closer to 107kb - meaning I'd reduce my user capacity by half to ~230k / month. In hindsight, Hugo was a pretty good choice as the geojson I'm using is 138 kb, bringing my Hugo-based site to a total of ~247 kb vs the ~372 kb required if using the previous stack.

All in all, doesn't _really_ matter as I'd just offload some of my big payload items (css and geojson) to my static resource repo if I started getting close to the Netlify free tier cap, but saving ~30% of used bandwidth per load isn't half bad either.

As implied in the previous section, I'm again using [Netlify](https://www.netlify.com/) to host this project. It's easy and it just works. Plus free. Did I mention free?

# learn more

[Read more about the project](/tags/coronation)