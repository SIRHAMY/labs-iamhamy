---
title: "Monoverse 0003: Forms"
date: 2022-04-08T05:12:23Z
tags: [
    "projects",
    'active',
    'monoverse',
    'art'
]
comments: false
---

_Part of [The Monoverse](https://labs.hamy.xyz/projects/the-monoverse/)._

# Overview

On Forms.

# Monos

* [0003 - Concentric](/projects/0003-0003/concentric)
