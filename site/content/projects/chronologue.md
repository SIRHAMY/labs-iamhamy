---
title: "Chronologue"
date: 2019-08-15T14:01:44-04:00
tags: [
    "projects",
    "chronologue"
]
comments: false
draft: true
---

# what is it?

Chronologue aims to provide a useful summary dashboard for your personal data over time.

# why was it built?

I write a lot of [release notes](https://blog.hamy.xyz/tags/release-notes). As part of these, I usually reflect on what I did, pulling relevant stats to get a better view of what really happened. I found that this took quite a long time to gather the data from each repository (typically one per activity), export it, and format it into some usable artifact.

Thus this project aims to systematize and automate a good portion of this process to make it less painful.

# how was it built?

# learn more

[Read more about the project](/tags/chronologue)