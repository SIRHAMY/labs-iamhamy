---
title: "zima-blue"
date: 2019-07-28T23:24:00-04:00
tags: [
    "zima-blue",
    'art'
]
comments: false
coverImage: "https://storage.googleapis.com/iamhamy-static/labs/projects/zima-blue/zima-blue-screenshot-square.jpg"
---

[See all zima-blue outputs](https://art.hamy.xyz/zima-blue/)

![Example zima-blue output](https://storage.googleapis.com/iamhamy-static/art/projects/zima-blue/1555166377965448418.jpg)

# what is it?

zima-blue is an exploration of universal understanding. Also it's a clone of Netflix's zima-blue - you gotta see it to get it. #nospoilers

# why was it built?

Because I saw zima-blue and was like, hey I could do that.

# how was it built?

I use built-in Go image manipulation libraries to draw a rectangle of randomized location and shape onto an image pulled from Unsplash's api.

# learn more

[Read more about the project](/tags/zima-blue)
