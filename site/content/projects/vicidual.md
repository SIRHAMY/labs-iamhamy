---
title: "vicidual"
date: 2020-05-16T14:33:54Z
tags: [
    "projects",
    "vicidual",
    'audio-reactive',
    'art'
]
comments: false
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/hgLL5ZpqskU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# what is it?

[vicidual](vicidual.xyz) is an audio-reactive visualization platform that runs directly in your browser. This means it can provide audio visuals to you on any device anywhere in the world, assuming you have internet connection.

# how can I get it?

It's free and available now at [vicidual.xyz](vicidual.xyz).

# how was it built?

I built vicidual with a tech stack that I'm pretty used to - no need to learn new things for this if the current tech stack works perfectly.

* Netlify - I'm using Netlify for hosting because it's free, easy, and just works. Read more about [how I host my sites on Netlify](https://labs.hamy.xyz/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free/).
* NextJS x MaterialUI - Frontend is a necessary thing but over the years I've found it's gotten more and more complicated with respect to builds and boilerplate. NextJS takes care of a lot of that boilerplate for me so I can just get going and MaterialUI does much the same for styling - this is my go-to starter package for new apps as it's fast to set up and provides pretty much everything I'd ever need (even if it's a little overkill for these small projects)
* p5js - I wanted to provide a way to continually build and share the visualizations I've been creating, which I've mostly been building with p5js (for examples [monoform](/projects/monoform) and [maudern](/projects/maudern)) so I leveraged iFrames to enable the display (and possible interaction with) any web-based visualization.

# learn more

[Read more about the project](/tags/vicidual)