---
title: "LineTimes"
date: 2020-12-19T04:26:35Z
tags: [
    "projects",
    'business'
]
comments: false
---

Today I'm releasing [LineTimes](https://linetimes.xyz) - a site that calculates and aggregates the wait time at stores in your area. 

Now live in Manhattan, NY!

![LineTimes screenshot](https://storage.googleapis.com/iamhamy-static/labs/projects/linetimes/linetimes-screenshot.png)

_LineTimes screenshot_

# Stats

_last updated 2021.05.16_

* Status: Archived
* Revenue: $0
* Reach: 0
* Cost: $90 / month
* Time: 70 hours

# Overview

I've lived in Manhattan, NY for about 3 years now. There are a lot of great things about living in a densely populated city (in non-COVID times). We have unparalleled entertainment options, great restaurants, and the best public transportation in the United States.

But there are also some downsides like being a virus breeding ground, sky high rents, and (surprise!) extremely long waits at grocery stores.

I don't know about you but until I moved to New York, I don't think I'd ever waited in line at a store for > 30 minutes unless it was for the release of the new Halo game or Harry Potter book. But that's the thing, everybody's gotta eat and many people choose to go to the store around the same times each week cause I guess we're not so different after all?

Put that together and you get some unreasonably long lines to acquire your food prep materials in the #greatestcityonearth.

I stood in 20 / 30 / 40 minute lines at Trader Joe's for months before finally deciding I should build something to allow me to avoid those lines. I came up with the idea more than a year ago and now I'm finally releasing that to the world. 10 months into a pandemic.

Check the LineTimes, skip the wait.

At a high level, LineTimes works by looking at how many people are in the store, comparing that to historical records of how long line times were for that amount of people, and making a prediction based on that. 

It's probably not the best timing on my part to release a service aimed at solving problems caused by crowds when crowds are basically illegal. But it's rarely ever the 'right' time, I've finally got it built, and am paying for compute so now seems as good a time as any.

I've opened the service up to work for Trader Joe's in Manhattan to limit the scope of the initial build to the core experience needed to validate whether this is a useful path or not. I figured that I'd only experienced this problem personally in those stores so if other people didn't think it was a problem in those same stores then it's likely not worth pursuing. 

In entrepreneurship I think this is called problem validation. This is supposed to be a fast, cheap process to make sure you're working on something valuable. Assessing myself, I think I spent way more time trying to get this launched than I should have. A lot of that was spent learning new technologies and thinking about how to keep my business processes lean. I think I would've had to bite that bullet eventually as I figured out how I wanted to build and run [my businesses](/tags/business) but it still hurts a bit to see the number of hours put in vs the output I was able to create.

Still I understand that it's a learning process and that this is truthfully the first real 'business' I've ever launched - despite saying I was going to be [building a business each month](https://blog.hamy.xyz/posts/2020-h1-review/#looking-forward) for literal months. So I think it's important to cut myself a little slack, be grateful I was able to launch, and to make changes so I can be better next time. 

If you've got a few minutes, I'd really appreciate it if you could click around [LineTimes](https://linetimes.xyz) and [send me](https://hamy.xyz/connect) any feedback you may have!

# Build

I knew I wanted to pick a technology stack that was simple, would just work, that I'd enjoy working in, and that would be flexible and scalable enough to work for pretty much any business / idea I could think of. I knew that the learning process would be long so I wanted to make sure that whatever I learned had a high likelihood of high utilization down the road. Through these criteria I figured I could find a nice balance of efficiency, satisfaction, and power to build my projects on top of.

My tech stack:

* **C#** - C# has been [my go-to](https://labs.hamy.xyz/posts/2019-programming-language-hierarchy/) ['boring language'](https://panelbear.com/blog/boring-tech/) for years now - it's performant, battle tested, has great community support, and is statically typed (a near must-have for me these days). I was first introduced to C# while [I worked at APT](https://www.linkedin.com/in/hamiltongreene/) and have used it on and off for various projects in the time since. If it's something I want to work for a long time and that I think may have performance bottlenecks, this is the tool I reach for most often. Also dotnet core has come a long way so I don't even feel bad about investing in Microsoft land these days.
* **Postgres** - I'll be honest - I'm not a database expert. I knew I wanted something that just worked, was battle tested, and had good community support. I was more familiar with SQL than NoSQL and knew that SQL would work for my usecases. So I chose Postgres cause people seem to really like it and it fit my usecases. I haven't worked with it enough to really come to a conclusion - no complaints so far.
* **Nextjs** - In many technical aspects I've found myself drifting towards the less is more ideology, choosing smaller and leaner technologies to build with for the additional flexibility and minimized cruft they offer. Frontend development is one area where I'm highly polarized on both sides of the spectrum. I don't love messing with stylings and build packs and stuff so I typically either go very barebones (like [my statically-generated sites](https://labs.hamy.xyz/projects/iamhamy-3/)) or very plug and play like NextJS. If it's anything that needs to be very interactive or that I think will grow to contain a lot of code I'll typically go with the plug and play route as I think the cruft is worth it to for the added helpers over the long term. That's what I did here in anticipation of adding more complex features and to force myself to learn these things in preparation for new projects down the road.
* **Google App Engine** - One big thing I knew I wanted to do in this project was to host it in the cloud. I firmly believe that almost every service can and should be hosted in the cloud and I wanted to give myself firsthand experience building in it myself. I currently run my personal sites on Netlify but didn't think it was flexible enough for all of my future businesses so I decided to try a more traditional cloud offering. I first went with Digital Ocean as I'd previously used them and they typically have great prices but I found their PaaS App Platform to be wanting (though I have hopes that it will improve as it matures). So I moved over to Google App Engine which I knew would work and was flexible enough to provide for any future business needs I could think of.

Pretty much all of the time cost for this project was spent building the software and the money cost goes towards running it on Google Cloud. It took me a lot of time to ramp up on each of these technologies as I hadn't built anything significant in them in several months if ever. On the bright side I think that investment was well worth it and will pay dividends as I build more projects in the coming months.

# Next steps

My current plans are to 1) validate whether people find LineTimes useful and, if so, 2) build additional features to help them avoid long wait times. While I'm doing that I'll be spinning up some new businesses and throwing in some new [art](/tags/art) to spice things up as we roll into the new year.

In art and business.

-HAMY.OUT