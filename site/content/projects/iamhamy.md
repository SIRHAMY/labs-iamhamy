---
title: "IAmHamy"
date: 2019-02-18T16:03:43-05:00
tags: [ "iamhamy", "kubernetes", "k8s", "google-cloud-engine" ]
comments: false
---

IAmHamy is a series of sites I created to host my online presence, cause everyone has a brand and you might as well tout it as best you can.

In the current iteration, it's built with static resources served via managed Nginx instances orchestrated with Kubernetes via Google Kubernetes Engine. You can read more about why I made this decision and relevant details in [the move's announcement post](https://labs.hamy.xyz/posts/labs-hosted-on-gke/).

I have two blogs (https://blog.hamy.xyz and https://labs.hamy.xyz) that utilize [Hugo](https://gohugo.io/) to generate static sites from raw content. I like this paradigm because it allows me to focus on my content with the ability to plug and play with community themes created by people who like to build that kind of stuff and the customizability that comes with simple, open-source software.

The top-most domain within "my domain" (https://hamy.xyz) is a collection of static pages holding basic universal information about The Hamniverse, like [how to contact me](https://hamy.xyz/connect), [how to subscribe to updates](https://hamy.xyz/subscribe), and [who I am](https://hamy.xyz/about).