---
title: "SMASH_THE_BUTTON"
date: 2022-10-06T13:20:47Z
tags: [
    "projects",
    'fsharp',
    'art',
    'active'
]
---

# Overview

[SMASH_THE_BUTTON](https://smashthebutton.xyz) is a website that allows you to smash the living daylights out of a button and see a satisfying counter increase. 

* You: Boop
* It: ++

I built it as an experiment in building simple, reliable, and performant real-world services in a functional programming paradigm using [fsharp](https://fsharp.org/). 

# How it was built

I built SMASH_THE_BUTTON in a mostly FP style. Its general architecture intentionally resembles [CloudSeed](https://cloudseed.xyz/) as I'm considering migrating it to fsharp and it's [the most efficient software architecture](https://youtu.be/wVncTzd2VHY) I've found for most things.

* Frontend - Svelte / SvelteKit
* Backend - Fsharp / Dotnet
    * Web framework - Giraffe
    * DB ORM - Dapper, Postgres
* DB - Postgres
* Hosting: Google Cloud - Cloud Run containers + Hosted Postgres

Further Reading: 

* [Hosting full stack apps on Google Cloud for less than $10 / month](https://youtu.be/wVncTzd2VHY)
* [$10 / month Managed Postgres Database on Google Cloud](https://youtu.be/0OR89wszEWA)

# Other Notes

* Naming - I originally wanted to call it `PushTheButton` but all the domains were being parked and sold for >$10k and HAMY LABS is too #poor for that so I opted for the $2 smash url