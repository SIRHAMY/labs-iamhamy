---
title: "HAMY LABS now incorporates HamForGood in every project"
date: 2020-07-13T18:09:50-04:00
tags: [
    'hamforgood'
]
comments: false
---

Today I'm announcing a change to how [HamForGood](/hamforgood) and LABS work together to make the world a better place. The new HamForGood is as follows:

* HamForGood now applies to _all_ HAMY LABS projects whereas it used to apply to a select few
* HamForGood now gives 20% of proceeds to select causes whereas it previously gave 80%
* These 20% of proceeds will be split with 10 / 20% going to fighting climate change and the remaining 10 / 20% going to the cause for which the project was made. If no cause is specified, those funds will go to a discretionary budget which will be distributed among causes HamForGood currently supports.
* This will apply to all current and previous HAMY LABS and HamForGood projects starting today, July 13th 2020.

I believe that this new paradigm has a few benefits over the old paradigm.

* consistency - because all projects contribute to this giving, this should create a more consistent flow of funds and serve as a constant reminder of what I'm working for
* simplicity - by putting the same rules across all my projects, I decrease the complexity of my book keeping significantly
* flexibility - because all projects now contribute to HamForGood, I can focus on the things I think will be most impactful, even if they aren't directly related to the specific area that needs the most help.

The main reason I made this change is that I felt the old paradigm lent itself too much to throwing something one-off together and then forgetting about it. It was reactive and short-term.

To combat this, I wanted to integrate the HamForGood system directly into my creation process ensuring that every effort I made was paying dividends to the causes I care about. I think this paradigm is a much more proactive approach, taking into account the ebbs and flow of life in its consistency, simplicity, and flexibility.

I came to the 20% number in a few ways:

* Looking at leading giving initiatives like [1% for the planet](https://www.onepercentfortheplanet.org/model)
* Going over my earnings data and calculating the average amount of giving in various scenarios
* My want for a nice number and enjoyment of the 80/20 split

This is still a work in progress. Expect more updates in the future. Would love to hear your thoughts, so lmk what you think.

Prosperity and happiness,

-HAMY.OUT

