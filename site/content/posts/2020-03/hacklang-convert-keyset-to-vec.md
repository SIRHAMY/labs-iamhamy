---
title: "Hack: How to convert a keyset to a vec"
date: 2020-03-16T12:40:25-05:00
tags: [
    "hack",
    "hacklang",
    "keyset",
    "vec"
]
comments: false
---

# problem

I've been using a lot more Hack / hacklang [at work](https://blog.hamy.xyz/posts/2019-in-review/#work) and have run into several cases where there's not great documentation. One of those cases is when trying to convert a keyset to a vec.

# solution

To convert a keyset to a vec you can use the `Vec\keys` function from the standard library.

An example:

```
$my_keyset = keyset['a', 'b'];
$my_vec = Vec\keys($my_keyset);
```