---
title: "Hack: Get count of keys in a dict"
date: 2020-03-09T12:36:51-05:00
tags: [
    "hack",
    "hacklang",
    "dict"
]
comments: false
---

# problem 

I've been using a lot more hacklang at work and as such have run into a lot of use cases that don't seem to have too much documentation. One such use case is how to get the number of keys in a dict.

# solution

The hacklang standard library is actualy pretty uniform so similar to other Traversable's you might be trying to get the count of, you can get the count of keys in a dict with `C\count`.

For example:

```
$my_dict = dict[
    'a' => 1,
    'b' =>2
];
$my_count = C\count($my_dict);
```