---
title: "How to install GLFW on Ubuntu"
date: 2020-03-02T22:43:29-05:00
tags: [
    "ubuntu",
    "linux",
    "p5",
    "python",
    "glfw"
]
comments: false
---

# problem

I've been working through [The Nature of Code](https://amzn.to/31BX8Pq)'s coding exercises with p5py - Python's port of P5 - in an effort to get back into creative coding and learn more about the p5 creative coding ecosystem. One of p5py's main dependencies is the GLFW package and I was having trouble figuring out how to install it on my Ubuntu machine, so here it is for future users.

# solution

If you're running Debian Linux (like Ubuntu and Linux Mint), then you should be able to install GLFW by opening up a terminal and running:

`sudo apt-get install libglfw3`

This will attempt to install the `libglfw3` package using admin privileges if its available for your OS distro.