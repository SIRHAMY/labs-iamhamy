---
title: "Hack: How to convert a keyset to a darray"
date: 2020-03-23T12:44:22-05:00
tags: [
    "hack",
    "hacklang",
    "darray",
    "keyset"
]
comments: false
---

# problem

I've been using a lot more hacklang at work and have run across many usecases that don't have the greatest documentation. One such usecase is how to convert from a keyset to a darray. This is how I solved that.

# solution

The darray actually has a pretty flexible constructor so you can create a darray from a keyset by simply passing the keyset into the darray.

For example:

```
$my_keyset = keyset['a', 'b'];
$my_darray = darray($my_keyset);
```