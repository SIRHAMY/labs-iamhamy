---
title: "How to play the Elder Scrolls Online on Linux with Steam Proton"
date: 2020-03-30T17:00:49-05:00
tags: [
    "steam",
    "elderscrollsonline",
    "linux",
    "steamproton"
]
comments: false
---

I'm a big Linux fan / user and also a fan of the Elder Scrolls universe so naturally there came a time when I wanted to give Elder Scrolls Online a whirl. I've used Lutris in the past for Skyrim so I thought I'd try it again for ESO when I stumbled upon Steam Proton.

As far as I can tell Steam Proton is a compatibility layer between Steam games and Wine (which is similar to how Lutris works) but it has the added benefit of being supported (to some degree) by Valve vs those random people online making one-off fixes to things.

I did a quick lookup on ProtonDB and the game looks to be well supported, so I decided to try it out. Here's how you can set up your Steam Client to be able to install and play ESO on your linux box.

# solution

Really the main step to setting this up is just getting your Steam client ready for Proton.

To do this:

* Click the `Steam` menu item near the top of your Steam Client window
* Select `Settings`
* Under the `Account` tab click `Change...` in the `Beta Participation` section
* This will produce a pop-up with a drop-down, select `Steam Beta Update` and `ok`
* Restart Steam
* Once it's all loaded up, go to your Library and find The Elder Scrolls Online
* Right-click ESO and select `Properties`
* Check the box saying "Force the use of a specific Steam Play compatibility tool" then select `Steam Linux Runtime` (or another player like Proton) from the dropdown

If you've done all of this correctly, then you should now have the option to install ESO directly in Steam!