---
title: "HAMY LABS: 2022 Colors Update"
date: 2022-05-16T04:21:33Z
tags: [
    'colors',
    'branding',
    'iamhamy'
]
comments: false
---

It's come to my attention that my previous color scheme wasn't working well for my projects. 

* White - FFFFFF
* Black - 000000
* Red - BC002D

The 3-color scheme I utilize requires 3 colors that work well together for:

* Websites / UIs
* Print
* Art

When limited to just 3 colors, it's imperative that each color works well together to provide sufficient flexibility in an otherwise rigid system.

The previous color scheme had issues where [Black and Red have very low contrast](https://coolors.co/contrast-checker/000000-bc002d) and is hard to see while [White and Red boasted "good" contrast](https://coolors.co/contrast-checker/ffffff-bc002d) but wasn't recommended for small letters.

I really like the White, Black, Red scheme so I needed to find a red that would do better than "good" when paired with white - even if it meant performing worse with black.

In the end, I ended up with Crimson UA (A71930) which is a darker, flatter red which boosts the contrast with white up to 7.43. The tradeoff is that it loses contrast with black but some simple guidelines can keep that from ever happening.

As an added bonus, Crimson UA also happens to be the [Atlanta Falcons' choice of red](https://teamcolorcodes.com/atlanta-falcons-color-codes/) for their similar white / black / red color scheme. This gives additional confidence that the combination works well in a variety of usecases and is a nice nod to the hometown.

New color scheme:

* White - FFFFFF
* Black - 000000
* Red - A71930

See on Coolors: https://coolors.co/000000-ffffff-a71930