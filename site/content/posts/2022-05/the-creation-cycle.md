---
title: "The Creation Cycle"
date: 2022-05-30T15:52:01Z
tags: [
    'creation-cycle',
    'product',
    'systems'
]
comments: false
---

The Creation Cycle is a generic process by which any form / scope of creation can effectively implement iterations of continuous impact and learning. I believe it's the most effective way to create meaningful things. 

![Diagram of The Creation Cycle](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/the-creation-cycle/the-creation-cycle.png)

_Diagram of The Creation Cycle_

* Observe - Understand the domain.
* Create - Execute on the domain's most important Problems and Opportunities.
* Reflect - Learn from this Cycle. Improve the next Cycle.
* Share - Share Cycle's outcomes with the world.

Notes:

* You do not have to go through the Creation Cycle in the order listed, though typically this is the order it's used in
* The Creation Cycle is built to be iterative. When one Cycle ends, the next begins.
* The Creation Cycle is useful at every scope and is likely best used in recursive iterations

Example Usage - Multiple Scopes:

* Creation Cycle for top-level business roadmap
    * Creation Cycle for each org's roadmap
        * Creation Cycle for each team's roadmap
            * Creation Cycle for each individual feature

Personally, I use the Creation Cycle to structure most of my life's processes.

* Planning and coding software features
* Ideating and validating businesses
* Ideating and writing blog posts
* Personal reflections and life plans