---
title: "Netlify: Hugo Site Build Failure - Configuration Error, Invalid Filename"
date: 2021-02-14T14:45:47Z
tags: [
    'netlify',
    'hugo',
    'error'
]
comments: false
---

# Problem

I host my core sites on Netlify using Hugo (read: [How I scale my sites to 500k users per month](https://labs.hamy.xyz/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free/)). Recently, my build started failing with errors:

```
Configuration error

Deploy did not succeed: Invalid filename 'tags/c#/index.xml'. Deployed filenames cannot contain # or ? characters
```

# Solution

In my case, the solution was pretty simple. I could see that the page that was failing was a `tags` page, which Hugo generates based on the tags frontmatter (read: metadata) attached to my posts.

This meant that one of my posts was using the tag `c#` which was causing Netlify to fail. A quick search revealed a post that was doing this and I was able to resolve by changing `c#` -> `csharp` in my tags section.