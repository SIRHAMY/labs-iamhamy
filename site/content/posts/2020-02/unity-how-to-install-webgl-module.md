---
title: "Unity: How to install the WebGL build module"
date: 2020-02-26T22:55:23-05:00
tags: [
    "unity",
    "webgl"
]
comments: false
---

# problem

I was working on one of my new [projects](/projects) and trying to build my Unity project for WebGL, but when I clicked 

# solution

UnityHub makes installing new modules super easy!

* Open up UnityHub
* Go to Installs
* Click the hamburger button (the three dots in the top right corner) of the Unity install you want to add the WebGL module to
* Select `Add Modules`
* Select the `WebGL Build Support` module
* wait for it to finish downloading and installing

Profit!