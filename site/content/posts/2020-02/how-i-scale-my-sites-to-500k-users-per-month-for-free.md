---
title: "How I scale my sites to 500k* users per month for free*"
date: 2020-02-26T17:16:40-05:00
tags: [
    "netlify",
    "iamhamy"
]
comments: false
---

Now that I've got you into this post, I should come out and say that I don't _actually_ serve 500k users / month and I don't serve those users I do serve for free. However, I still think it's fair to say that my current infrastructure can handle over 500k users / month for free but to further explain that we'll need to first understand my current infrastructure.

Let's do that.

![my site architecture](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-scale-my-sites-to-500k-free/site-architecture.jpg)

# my site

The infra for each of my sites consists of 3 main parts:

1. My host
2. My code repo
3. My static asset distributor

I'll talk about these in this order then we can go into the asterisks.

## my host

I currently run my sites on Netlify which I chose primarily for its simplicity and practicality. I moved to [Netlify from Google Kubernetes Engine (which saved me ~$1,000 / year)](https://labs.hamy.xyz/posts/i-moved-to-netlify-from-gke-and-saved-60-per-month/), to [GKE from a Digital Ocean droplet](https://labs.hamy.xyz/posts/labs-hosted-on-gke/), and to DO from some sort of hosted Wordpress. All that to say that this is an iteration and may not be a static state for long.

Anywho, Netlify provides me with an extremely simple plug-and-play hosting paradigm and a pretty generous free tier with 100GB of bandwidth and 300 build minutes each month. It's the size of this free tier that gives my site most of its ability to scale.

My primary sites all use Hugo as their static site generator. I like Hugo because it's pretty simple, pretty fast, and has pretty good docs. I like [JAMStack](https://jamstack.org/), which is kinda the genre of static site generators and means sites are mostly pre-generated and light, because it encourages speed at a basic level which I enjoy.

![site build specifics](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-scale-my-sites-to-500k-free/site-build.jpg)

## my code repo

I currently use GitLab to store my code. I first went there because they had unlimited private repos for free when GitHub did not, but I've stayed because they really do have a lot of cool features and I just haven't really seen a reason to move anywhere else.

I lean heavily on GitLab CI to automate builds and deploys for my projects that need / would benefit from it.

## my static asset distributor

It's important to call out that I don't serve my assets directly on my site. I do this for a few reasons:

* It would blow up my git repo size
* It would blow up my bandwidth (on the download side from Netlify)
* It would blow up my build times (on the build / deploy side)

Either of these alone would force me out of Netlify's free tier (or at least get me unreasonably close for what I'm using it for). So I don't serve my static assets directly from my site.

Instead, I use Google Cloud Storage to serve these static assets. I probably could've used Amazon's S3 here but I started with GCS while [I was running on Google Kubernetes Engine](https://labs.hamy.xyz/posts/i-moved-to-netlify-from-gke-and-saved-60-per-month/) and it worked so I've stuck with it.

That's really all there is to my site. Now let's talk about my 500k figure.

![site costs and visitors](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-scale-my-sites-to-500k-free/site-costs.jpg)

# serving 500k* users for free*

So I said that I served 500k users for free each month, but that's not totally accurate. In 2019, [I served about 1,700 users each month](https://blog.hamy.xyz/posts/2019-in-review/#iamhamy) and in 2020 have paid ~$0.15 / month for site-related costs.

However, if we look at where those costs come from (and don't come from), we'll see how I can serve 500k users for free each month on my current architecture.

![Netlify bandwidth contributions](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-scale-my-sites-to-500k-free/capacity-contributions.jpg)

When we break down the different payloads that go into serving my site, we see that there are 3 primary buckets:

1. my site
2. images and other static assets that get served by me through Google Cloud Storage
3. 3rd party js / embeds (IG, Analytics, Ads)

Only 1 (my site) contributes to my Netlify free tier bandwidth capacity. 2 and 3 (my static assets and 3rd party embeds) do not contribute to this bandwidth capacity. The $0.15 / month I mentioned earlier goes solely to the distribution of these static assets, _not_ to the serving of my site itself.

![site behavior](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-scale-my-sites-to-500k-free/site-behavior.jpg)

If we look at some stats from my site, we find that the average payload of a page (across my top 4 sites with 98% of my traffic) is ~107KB and that the average number of pages a visitor goes to in a session is about 1.74. If we then divide the 100GB of bandwidth we get in the Netlify free tier by the average payload of a page multiplied by the number of pages in a session, we get a peak capacity of ~533,901 visitors each month.

![site capacity](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-scale-my-sites-to-500k-free/site-capacity.jpg)

_To see my full calculation on visitor capacity, check out [I can serve ~533,000 visitors (~927,000 page views) of my sites per month for free with Netlify](https://labs.hamy.xyz/posts/how-many-visitors-can-the-netlify-free-tier-support/)_

Now you could argue that those static assets I serve through Google Cloud Storage are integral to the user experience on my site, so to say that I'm really serving my site you'd have to include the cost of those assets. You would not be wrong. But my sites would still load.

# fin

That's really all there is to my sites. I still hold that they can scale to ~500k users / month for free but I get that it's a contentious point. Moving forward, I hope to keep my sites (and projects in general) as low maintenance as possible. Netlify has really done that for me and I hope that cloud infra continues to develop and make available simple, powerful tools like this.

No I'm not sponsored by Netlify but I am enamored.

Come back next time for more contentious points in creating things!

-HAMY.OUT