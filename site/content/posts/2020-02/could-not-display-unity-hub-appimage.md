---
title: "Unity: Troubleshoot Ubuntu install - Could not display `UnityHub.AppImage`"
date: 2020-02-03T11:48:52-05:00
tags: [
    "unity",
    "troubleshoot",
    "linux"
]
comments: false
---

# problem

I'm trying to install the Unity game engine on my desktop running Ubuntu 18.04. When I went to the download page and clicked Install, I got a file named `UnityHub.AppImage`. It looks like an executable so I double clicked it but after a few seconds got the error:

```
Could not display "UnityHub.AppImage"

There is no application installed for "AppImage application bundle" files.

Do you want to search for an application to open this file?
```

I keep clicking no because I feel like I shouldn't need another program to run this. What am I doing wrong?

# solution

First some background `.AppImage` is a file format that is kinda like an `.exe` - it's a compressed format that holds all the dependencies required to run an application. So you're right in thinking that this thing should be executable.

Where you're probably stuck is that `.AppImage` files aren't executable by default (probably for security purposes). So if you want to run your `UnityHub.AppImage`, you first need to set it as executable.

To do this:

* Right click on `UnityHub.AppImage`
* Select `Properties` in the dropdown menu
* Navigate to the `Permissions` tab
* Check the box next to `Execute`
* Double click your `UnityHub.AppImage` to execute