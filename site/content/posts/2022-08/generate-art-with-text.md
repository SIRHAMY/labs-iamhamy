---
title: "Generate Art With Text using AI (Free and No Code)"
date: 2022-09-13T15:04:50Z
tags: [
    'artificial-intelligence',
    'art'
]
comments: false
---

I've been building [art with technology](https://labs.hamy.xyz/tags/art/) for a long time. One of the coolest advances I've seen in the past few years has been improvements to the ability to create high quality images based on text input. This has a lot of implications on the world ranging from how fast you can iterate on an idea to what our definitions of "art" (and the "artists" that create it) become.

This technology has now reached a level of maturity where it's generally accessible to the public. In this video I'm going to show you how to generate images from text for free, no coding knowledge required.

Let's get started generating art with text using AI.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dneSnVKqOTk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Table of Contents

* [Text-to-Image](#text-to-image)
* [Create Text-To-Image](#create-text-to-image)
* [Future Explorations](#future-explorations)

![Text-to-Image](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/text-to-image-ai/text-to-image-ai-diagram.jpg)

# Text-to-Image

Let's start with an overview of what Text-to-Image is as it will provide a good foundation for us to use it effectively to generate the images we want.

The three main things we need to know are:

* **An AI has been trained on a bajillion images.** These images encapsulate everything this AI understands about how text and images relate.
* **When we input text, the AI tries to build an image that matches the text.** It is using the knowledge it has based on its training to create outputs that match best.
* **Modern text-to-image models will use "Diffusion" to allow for 1 text to create many different images.** Diffusion is similar to how Noise / Random are used in generative art contexts to increase the size of the output opportunity space.

# Create Text-to-Image

Now that we understand how this process works, we can start generating images from text.

For this post, we're going to be using [Night Cafe Studio](https://nightcafe.studio/). This is the best widely-available text-to-image service I've found to-date and is how I've been generating my own AI art for projects like [ARTIFICIAL_DREAMS](https://labs.hamy.xyz/projects/artificial-dreams/).

Pros:
* Free (10 credits to start, 5 extra per day)
* Cheap if you want to expand ($20 for 200 credits)
* High Quality image Outputs, with ability to enhance resolution
* Stable and fast - lots of people using it to produce a lot of images
* Great features - ability to tweak parameters, batch create outputs, and enhance resolutions all from simple interface is killer

Cons:
* Not as good as DALL-E 2

Creating your first image:

* Create an account in Night Cafe Studio - this should grant you 10 free tokens
* Click `Create` to go to the creation page
* Choose a Creation Method: Choose Stable - Anecdotally this produces the highest quality outputs
* Choose a text prompt - This can be anything:
    * A car eating a banana
    * A balloon in the shape of a smiley face
    * A vending machine on mars
* Choose a Style - I like to start with Oil
* Wait a few seconds as Night Cafe processes your inputs to create your output image

Eventually Night Cafe should output your image. Your first AI-generated Image!

Here's mine, using text input "a vending machine on mars":

!["a vending machine on mars"](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/text-to-image-ai/a-vending-machine-on-mars.jpg)

# Future Explorations

This space is still new and I think we're just getting started understanding all the different ways we can leverage this technology. Here are a few explorations I'd recommend trying to get a feel for the technology's capabilities:

* **Try different styles and text permutations** - for example, I've found that "x wearing a hat" and "x with a hat" often produce different kinds of results.
* **Play around with the Advanced Options** - There's a toggle on the Create page that will give you access to things like Modifiers, Random Seed, Prompt Weight, and Runtime
* **Check out Night Cafe's Explore page** to see what others are creating and use it as a jumping off point for your own explorations!

# Next Steps

If you're interested in learning more or have questions about text-to-image generation with AI - leave a comment or message me and I'll see if I can answer them.

You can read more about my latest AI art project on the [ARTIFICIAL_DREAMS project page](https://labs.hamy.xyz/projects/artificial-dreams/).