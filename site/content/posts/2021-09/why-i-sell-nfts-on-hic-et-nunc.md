---
title: "Selling NFTs - Tezos vs Ethereum"
date: 2021-09-23T01:11:26Z
tags: [
    'nfts',
    'art',
    'business'
]
comments: false
---

# Overview

I started minting and selling [NFTs on hic et nunc](https://www.hicetnunc.xyz/hamy.art/creations) this month. In this post I'll share my learnings from the research I did into the NFT space and why I decided HEN was the right choice for me as an Artist / Technologist / Entrepreneur.

I'll be assuming a basic understanding of:

* What NFTs are
* What blockchain / crypto is
* Basic NFT / crypto terminology 

<iframe width="560" height="315" src="https://www.youtube.com/embed/EgImL1DtKvc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Choosing an NFT Marketplace

When choosing an NFT marketplace to mint and list your tokens, there are two big categories of dependent choices you need to make:

* The blockchain you'll use
* The NFT Marketplace to list with

The blockchain is the backbone of NFTs. It's where the NFTs are actually stored and thus is intimately intertwined in everything you do with your NFTs - from creating them to selling them to destroying them (if you so choose). So your choice here not only determines (in most cases) the NFT Marketplaces you can use, but also your experience with core NFT interactions.

The primary example of this is the requirement for gas (payment to the blockchain) to store things on the chain. So if you choose an expensive blockchain, creating NFTs will be expensive. If you choose a cheaper one, creating NFTs will be less expensive.

NFT Marketplaces are where NFTs are actually listed and sold. This has a close parallel to retail platforms we already know and use - Ebay, Amazon, etc. Each Markeplace has its own quirks and flavor of clientele, some are bigger and more mainstream, some are smaller and more niche. Just as your blockchain choice will limit your markeplace options, so too could your desired marketplace sway your choice in blockchain.

# Choosing a blockchain

The big two NFT-capable blockchains I analyzed were Ethereum and Tezos. I did this mostly due to my awareness of them, perceived maturity, and adoption, though I'll note other reasonable options exist out there that also support NFTs like Solana.

## Ethereum

[Ethereum](https://ethereum.org/en/) (which runs on ETH) has become something of the de facto NFT blockchain. Some of the biggest NFT Marketplaces are on this chain - OpenSea, Rarible, SuperRare, Foundation - and thus some of the biggest artists of today have also chosen to mint on Ethereum.

Pros:

* Huge usage, coverage, and buyer pool
* De facto chain for NFTs
* Cutting edge chain - so unlikely to go obsolete any time soon
* Great support - easy to get started with wallets, credit cards, etc

Cons:

* Expensive - minting an NFT can be $20-50 USD
* Uses lots of energy - bad for environment ([about 2.8 days worth of energy needed to power a house per transaction](https://blog.ethereum.org/2021/05/18/country-power-no-more/))
* Hard to get noticed - very commercialized with some really heavy hitters on these marketplaces

_Note: With Ethereum 2.0, energy consumption is expected to go down to 20 mins of TV per transaction_

## Tezos

Tezos is a much smaller fish though has some very unique properties that give it promise despite its size. Most of those don't have to deal with NFTs - at least not til you get more advanced.

It started to grow an NFT community because 1) it was NFT-capable and 2) it's Proof of Stake native which makes it much more energy efficient (and environmentally friendly) than other chains ([about 1Mx by some estimates](https://medium.com/tqtezos/proof-of-work-vs-proof-of-stake-the-ecological-footprint-c58029faee44)).

It came on my radar as some artists I follow began to host their NFTs exclusively [hic et nunc](https://www.hicetnunc.xyz/), a leading NFT marektplace built on Tezos.

Pros:

* Still maturing - get in on the ground floor
* Cheap - $5 / coin, < $1 to create NFT
* Cool technical components - everything Ethereum has and more
* Way less energy consumption (see above links)

Cons

* Still newish and maturing
* Hard to get started - not a big non-technical support community to lean on

## My blockchain

I ended up going with Tezos. Both seemed to have the capabilities I wanted but what really drew me in over Ethereum was:

* Not bad for environment - A key decision inline with my core values and HamForGood goals
* Cheap - Could start out with $50 and try everything I wanted
* Growing community - Feels like ecosystem will only get better
    * As evidence of this, OpenSea is looking to add Tezos support

# Marketplace choice

While Ethereum definitely has the largest, most mature marketplaces I didn't want to build on that blockchain so those are barred to me. As such I didn't do that much research on them so if you want to know more about Ethereum-based marketplaces, you'll have to find that somewhere else.

One notable exception is that OpenSea - one of the largest marketplaces in the world - [announced earlier this year](https://opensea.io/blog/announcements/tezos-nfts-are-coming-to-opensea/) it was trying to support Tezos. So that may be a first mover on cross chain NFT exchanges (at least in the mainstream).

When it comes to marketplaces on Tezos, there's really only three big plyers:

* hic et nunc
* kalamint
* objkt.com

hic et nunc dwarfs kalamint in trade volume with $19M traded to Kalamint's $470k over the last 30 days. objkt.com is even bigger with $20.5M but it doesn't support direct minting and has the ability to list tokens from both kalamint and HEN so choosing either still gets you access to objkt.com's distribution.

All this led me to believe that hic et nunc was the right choice for me and is where I've started creating and trading NFTs: https://www.hicetnunc.xyz/hamy.art/creations

Pros

* Biggest NFT marketplace on Tezos (besides objkt.com which you get for free)
* Active developer and community support - via GitHub, Twitter, and Discord
* Plays nicely with others in ecosystem
    * objkt.com - listings automatically get pushed over here
* Seems to be growing (as a blockchain and marketplace)

Cons

* Still in development (I've been commenting on [their GitHub](https://github.com/hicetnunc2000/hicetnunc/issues) to resolve issues)
* Not super user friendly - need some technical know-how to get started

# End

For now I'll be committing to HEN as my NFT Marketplace of choice. You can find and acquire my NFTs [@hamy.art on HEN](https://www.hicetnunc.xyz/hamy.art/creations).

I've found NFTs to be a cool way to support artists and convey value through ownership. 

I'll also be keeping an eye on a few things:

*  Ethereum 2.0 - it's possible the energy savings there may sway my blockchain decision.
* OpenSea Tezos support - this would cement Tezos as another core player in the NFT mainstream

BTC is dead, long live XTZ
-HAMY.OUT
