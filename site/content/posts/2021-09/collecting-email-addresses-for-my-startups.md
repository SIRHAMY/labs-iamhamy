---
title: "Collecting Email Addresses for My Startups"
date: 2021-09-02T00:33:46Z
tags: [
    'business',
    'startups',
    'email'
]
comments: false
---

# Overview

Gathering emails is a crucial part of any business - from validation stage to enterprise stage. I build [lots of businesses](/tags/business) and I gather emails to help me validate the problems I'm solving, tune my market positioning, and gather leads for conversations and potential customers.

So far, none of my [projects](/projects) have made it out of the validation phase. This means:

* Lots of projects
* Not many users
* Few emails
* Very low budget

I found that [SendInBlue](https://sendinblue.com) was the best fit for me and my portfolio of validation phase startups.

I wanted to find the best email service for me that catered to all of my needs as a serial early-startup creator. It was surprisingly hard, so I thought I'd write up my findings and share it to save future creators some time.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pe-e88Y49-0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Emails for Startups

Emails are a fast and cheap way to:

* Validate the problem you're trying to solve is real
* Tune your market positioning - how you're attempting to solve the problem
* Gather leads for potential customers

It's a better indicator than site visitor count because there's more skin in the game. Anyone can come to your site to look around - they're not committing anything. But giving your email away (assuming it's a real email) assumes intent to be notified - they're saying they're willing to be bothered by you in order to get updates about your product. Not as good as money, but still some sort of commitment made.

Of course, validating startup ideas faster means you can try more ideas and invest less in bad ideas and, hopefully, become a more effective entrepreneur. The reason that emails are so fast and cheap are that they're super easy to set up:

* Set up a website
* Embed an email form

You can do this in a few hours and updating your product descriptions / problem statements to reflect new ideas and information is just as easy. Faster iteration -> More startups.

# Choosing an email service

Now let's get into how to choose an email service. Before SendInBlue, I was using MailChimp and had been for awhile. But I found them too expensive and not geared towards what I was trying to accomplish.

So I had to think about what it is I wanted from my email service before I could find one.

## Email Service for Startups

I think the needs of startups change throughout their lifetime - depending on where they are and what they're trying to do.

* Ideation Phase - Validating ideas and iterating fast
    * Sign up forms - to sign people up
    * Lots of lists + contacts - for future leads
    * Cheap - to protect the runway
* Growth phase - Converting customers and finding new leads
    * Marketing tools
    * Automation
    * A/B tests
    * APIs
* Enterprise Phase - Everything and more, but you now have the resources to pay for it

For me, I'm squarely in the ideation phase. I've got:

* Lots of projects
* Not many users
* Very few emails I need to send

But I also want to keep an eye on what I'll need in future phases so my choices now don't obstruct me in the future.

## Email Service Options

**MailChimp**

* Great marketing and tools - said to be an industry leader
* A bit expensive at around $10/month if you have more than 1 list

As an entrepreneur starting up many new startups, you'll probably end up with a bunch of lists - one list per startup. But many of those lists will never send an email so it doesn't make sense to pay for it if you can help it.

**ConvertKit**

* Great marketing tools but even more expensive
* $29 / month after 1000 subscribers

**ProductHunt - Ship**

* Provides a bunch of different startup tools including email signups and marketing
* $59 / month for >1 project

**Airtable**

* Free for up to 1,200 records in each Base (so 1,200 records in each startup list)
* But it doesn't have the marketing tooling that other options have which will be useful as you grow

**SendInBlue**

* Free, unlimited contacts and lists
* You just pay for the emails you send (300 / month free)
* Also has marketing tools and APIs like the others

# The Email Service for my Startups

For me, SendInBlue checked all the boxes. It was cheap and fully featured and lets me gather emails for every single project I create without having to worry about adding extra cost drag for projects that may never turn a profit.

It made so much sense that it's now my go-to for creating email lists for my startups.

In fact, I'm currently building a startup making it easy to launch a scalable full stack app: [CloudSeed](https://cloudseed.xyz).

It's still pre-alpha so I'm not letting many people use it yet. But I have started gathering emails so I can ensure I'm:

* Solving a real problem
* Solving all needs of my customers
* Gathering leads for future conversations and potential customers

In one night I was able to set up a landing page, add some descriptions, and start gathering emails. SendInBlue allows me to do that cheaper than everyone else, without sacrificing any of the features I may need - so it's my choice for my startups.

Let me know what email service you went with and why that was the best choice for you! I'm always searching for ways to iterate harder, better, faster, and stronger.

-HAMY.OUT