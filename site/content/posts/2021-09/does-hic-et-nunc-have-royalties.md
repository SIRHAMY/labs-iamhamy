---
title: "Set Royalties on NFTs on hic et nunc (HEN)"
date: 2021-09-22T04:06:23Z
tags: [
    'hic-et-nunc',
    'nfts',
    'royalties'
]
comments: false
---

# Overview

I sell [NFTs on HEN](https://www.hicetnunc.xyz/hamy.art/creations). I set royalties so that I can continue to earn off my NFTs as they're traded around the secondary market. Typically I like to set this to 10%.

Here's how to set royalties on your NFTs minted through hic et nunc:

# Setting Royalties on NFTs on HEN

You can set royalties during the minting process:

* Click the Menu button
* Click `OBJKT (mint)`
* Set `royalties` to a value between 10-25

Once you've completed minting, the value you set for `royalties` will now be used to distribute royalties on each swap of your NFT.

