---
title: "Presto: How to get the length of an array"
date: 2020-08-24T17:13:11Z
tags: [
    'presto',
    'prestodb',
    'prestosql',
    'sql'
]
comments: false
---

# problem

I've been doing a lot of sql queries on PrestoDB recently and just ran into Arrays. For one query I needed to see if an array was empty or not. To do that I needed to find the length of the array and see if it was greater than 0.

So my question - How do I get the length of an array in PrestoDB / PrestoSQL?

# solution

In order to get the length or size of an array in Presto, you can use the `cardinality` function. `cardinality` takes in an array and will output the size or length of an array.

To solve my issue where I was looking for rows where the array field was non-empty, I could do something like this:

```
SELECT my_array
FROM my_table
WHERE CARDINALITY(my_array) > 0
```

Running this should give me all the rows in my table where `my_array` is a non-empty array.