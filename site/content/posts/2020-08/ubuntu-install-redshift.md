---
title: "Ubuntu: How to install Redshift on Ubuntu 18+"
date: 2020-08-17T03:15:14Z
tags: [
    'ubuntu',
    'redshift',
    'nightlight'
]
comments: false
---

# problem

I just booted up a new version of Ubuntu and would like to set up redshift so I'm not kept up by blue light all night.

# solution

In Ubuntu 18+, you don't event need to use a separate Redshift package - there's a built-in Night Light function that does Redshift for you.

To use it:

* Go to `Settings > Displays`
* Choose the `Night Light` section / tab
* Toggle it on and configure

Easy as that!