---
title: "Netlify: How to migrate sites to a new domain without losing PageRank / SEO"
date: 2020-08-10T11:20:03-04:00
tags: [
    'netlify',
    'domain',
    'migration',
    'seo'
]
comments: false
---

_EDIT: Since performing my domain migration and writing this post about the steps I took, [my online properties have taken a severe ranking and traffic hit](https://blog.hamy.xyz/2020-q3-review/#iamhamy). As such, I need to warn you that following this guide is **not** sufficient to migrate you domain without losing traffic. I'm still not sure what can be done to ensure a smooth migration without a traffic hit but I'll update here as / if I find out._

# problem

I host all my sites on Netlify (you can read about how I do this in [How I scale my sites to 500k* users per month for free*](https://labs.hamy.xyz/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free/)) and used to use the domain hamy.xyz but recently decided that I wanted to use hamy.xyz instead since it was shorter and more to the point. This meant that I needed to migrate 4 sites that I hosted on Netlify to a new domain:

* hamy.xyz -> hamy.xyz
* blog.hamy.xyz -> blog.hamy.xyz
* labs.hamy.xyz -> labs.hamy.xyz
* shop.hamy.xyz -> shop.hamy.xyz

Doing this naively is pretty simple, you would just redirect the hamy.xyz domains to point to the hamy.xyz domains and be done with it. But the naive way has some drawbacks - 1) it's not guaranteed that each url from the old site will correctly redirect to the new url on the new site and 2) many search engines will see that the old url doesn't exist since the old url goes to the new url and trash any SEO / PageRank you've built up over the years (this is how Google indexes your site and decides what to show users). 1) could break the site for users / leave them with a bad experience and 2) could keep users from finding your site in the first place. I've built up [an audience of around 5k users / month](https://blog.hamy.xyz/posts/2020-h1-review/#iamhamy) over a couple years, so these are things I'd like to avoid.

The solution to this problem is to use proper redirects. Redirects, when done correctly, allow you to map your old urls to the new urls so that you send users to the correct place and tell search engines that your content has moved so that they can transfer any PageRank / indexing over to your new content. In this post, I'll talk about how I migrated my Netlify sites without harming my PageRank / SEO.

# solution

## domain migration basics

The first thing to understand is the basics of SEO / PageRank as this is the mechanism by which search engines index what content you have, how authoritative it is, and ultimately decide what to show users. It's key to having a findable website. At its core it relies on two things:

* content on your site - to determine what each page has on it
* backlinks from other sites - as a means of understanding how popular / useful your pages are

So as we go through the migration it's important that we keep our old links alive and have them point to the new links where the content now lives so that search engines can see that the backlinks still work and we can maintain that 'popularity'.

The internet way of telling visitors that something has moved is to use a redirect http code. The most common ones are:

* 301 - content moved permanently
* 302 - content moved temporarily

_Here's [a list of all http codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)._

My migration from hamy.xyz -> hamy.xyz is meant to be permanent, so I went with 301. The way this works in practice is that you'd have all old urls return an http response with the redirect code and the url to the new page. So you could imagine that for my about page hamy.xyz/about, I'd return something like:

* http code: 301
* new url: hamy.xyz/about

There are multiple ways to do this but that's the gist of it.

Now that we understand the basics of SEO / PageRank and how to tell visitors that we've moved we can move onto implementing redirects with Netlify.

## domain redirects with Netlify

One of the [reasons I love Netlify](https://labs.hamy.xyz/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free/#my-host) is that it's simple and just works. Redirects are no different.

In a typical migration, you need to have 1) a mapping of old urls to new urls, 2) a way for old urls to return an http redirect to the new url, and 3) your new site up and running. Depending on what tools are at your disposal and your implementation stack, this could be as easy as a server config change or as tedious as needing to set up a whole new site just for redirects. 

The whole migration with Netlify biases towards the easy side requiring just a few domain configurations and a single redirect configuration code push. Here are the steps:

### 1. get your new domain

Now if you're migrating sites from one domain to another, you're going to need to own the domain that you're migrating to. I'll trust you to know or Google how to do this yourself.

### 2. Setup the new domain so that it points to your website

The Netlify method of migrating doesn't require setting up a new website. So the first step is to just make your domain point to the correct website.

There are two ways to do this 1) allow Netlify to do the domain configuration for you or 2) configure your domain manually. Both require at least a little bit of configuration so it's really up to you to decide how you want to go about this.

1) allow Netlify to do the domain configuration for you

* Go to Netlify
* Click the Domains tab
* Click Add or register domain
* Type your new domain and hit next
* Skip over the domain entries for now
* Find the Netlify domain name servers (should be visible on this page) and modify your domain registrar to use them (the exact way to do this will differ based on registrar)

At this point, Netlify will be able to control some domain configurations for you. Now we just need to tell Netlify what domains should go where. 

2) configure your domain manually

This one is a little less work but requires you to know a little bit more about domain configuration. We're going to assume that you're updating the domain configuration directly in your domain registrar here and note that each registrar may have slightly different ways to get to the domain configuration but the configuration itself will be largely the same regardless of what you're using.

For each site you want to point this domain to:

* Navigate to your site in the Netlify admin console
* Navigate to the `Domain settings` page
* Find the default Netlify entry in your `Custom domains` list. This is typically formed like random-words-123blah.netlify.app.
* In your registrars domain console, create an A record for an apex domain (like hamy.xyz) or a CNAME for a subdomain (like blog.hamy.xyz, www.hamy.xyz, or labs.hamy.xyz) and place the default netlify url in the value field. This effectively points this version of your domain to your Netlify site.
* Test and wait. DNS changes can take up to 24 hours to propagate but I'd expect it to take like 20-40 mins. You can test by going to your new url and ensuring it goes where you think it should.

Regardless of which choice you took, you should now have your domain set so that it's pointing to Netlify. There's a little more configuration we should do to get it properly pointed to each site (and so that netlify can configure https for us).

For each site you want to point the new domain to:

* Navigate to the project you want to the site you want to add the domain to
* Click the `Domain Settings` button
* Click `Add domain alias`
* Type in the domain you want to add (so if I wanted this one to be accessible via hamy.xyz, I'd type hamy.xyz but if I wanted it to be blog.hamy.xyz, I'd type blog.hamy.xyz) then hit `Next`
* This new domain should show up in your `Custom domains` list now and be greyed out which indicates it's not configured yet. 
* Test and wait. Domain changes can take up to 24 hours to propagate, though I'd expect it to be done within 20-40 minutes in most cases. Netlify will show an error message until it can confirm that the url is working and you can manually test that the url is pointing to the correct place by going to your domain. For example, if I was using hamy.xyz as my new url, I'd just type in hamy.xyz to the address bar every few minutes and see what happened.

_Read the official docs for [assigning a custom domain with Netlify](https://docs.netlify.com/domains-https/custom-domains/#definitions)_

Once you're able to get to your site via your new domain, we've completed this step!

### 3. Add a redirect entry to your Netlify repo

So at this point, we've got our new domains pointing to our sites. All that's left is to have our old domains point to our new domains and to tell search engines that we've done this permanently so they maintain our popularity (SEO / PageRank).

Netlify makes this _super_ easy with their [redirect options](https://docs.netlify.com/routing/redirects/). I went with the Netlify config option via `netlify.toml` because I didn't have an easy way to get the `_redirects` file into the top level of my publish directory so I'm only walking through that, but know that there are more options.

A `netlify.toml` is a configuration file that Netlify will read from to know how you want it to serve your sites. This is called configuration in code.

To add a redirect from your old domain to your new domain, you can use something like:

```
[[redirects]]
  from = "https://labs.hamy.xyz/*"
  to = "https://labs.hamy.xyz/:splat"
  status = 301
  force = true
```

The `from` field says that we are trying to redirect requests that come in on this domain. You can use relative domains as well (like `/blog/`) but since we're migrating to new domains in this post, I'm using absolute domain redirects.

You'll notice that I am using an `*` at the end of my url. This is a wildcard and in this case means that I want to match any requests that start with `https://labs.hamy.xyz/`.

The `to` field says where we want to send these requests. So if I have a request matching the `from` field, where do I want them to go. Since I'm migrating from labs.hamy.xyz to labs.hamy.xyz, I put labs.hamy.xyz in the to field.

You'll notice that I have the `:splat` denotation at the end of my `to` url. This means to take whatever was in the wildcard of the `from` field and place it in the `:splat` field. So if a request came in at labs.hamy.xyz/projects, this setup would make it redirect to labs.hamy.xyz/projects.

The `status` field is the http code you want to return. Recall that http code 301 means a permanent redirect. I'm moving my sites permanently so I want to let any visitors know that.

The `force` option is whether you want to make all requests that match the `from` field go to the `to` field or whether you only want to redirect those requests where no content is found in the `from` field first. Since we're trying to migrate domains, we want to send all requests to the new domain so this should be `true`. 

Once you've added and pushed your redirect entry to each of your sites, you should once again test that each one is going where you think it should by navigating to the old domain in your browser and verifying it sends you to the new domain and that you land on the right page.

The last thing to do is to go to each site's `domain settings` and to set your new domain as the primary domain. This step doesn't matter in most cases but is a nice cleanup thing and Netlify does do some default configuration for apex domains so it's nice to do. You can do this by clicking the `Options` dropdown and selecting `Set as primary domain`

# fin

That's it! You've now successfully migrated your site from one domain to another without affecting your SEO / PageRank!

I'll call out that you should likely hold onto your old domain for awhile and keep it configured as-is so that you continue to catch visitors that don't know about the new url and redirect them there. Personally, I plan to hold onto this old domain for at least a year to limit my exposure to this issue but it's really up to you.

Let me know how your migration goes and anything I can do to improve this guide!

Happy coding,

-HAMY.OUT