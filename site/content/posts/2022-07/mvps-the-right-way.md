---
title: "MVPs the Right Way (Minimum Viable Experiments)"
date: 2022-08-03T17:36:45Z
tags: [
    'business',
    'product',
    'creation-cycle'
]
comments: false
---

Startup heavyweights from Y Combinator to Tech Stars to 500 tout Minimum Viable Products (MVPs) as the golden standard for startup iteration. But the truth is most founders still get this wrong. They spend too much time, effort, and resources to create an MVP that eats up their runway and fails to move their startup forward.

In this post I'll introduce a philosophy and framework for building MVPs the right way. We'll solve for the most common pitfalls founders make so you don't spend months and thousands of dollars building something nobody wants.

# Table of Contents

* [What is an MVP?](#what-is-an-mvp)
* [Creating Effective MVPs](#creating-effective-mvps)
* [Building Businesses with MVPs](#building-businesses-with-mvps)

# What is an MVP?

MVPs (Minimum Viable Products) are a source of frequent confusion among founders - both new and experienced. Frequent questions include:

* What's a good amount of time / resources to spend on it?
* Do we need to build feature "x"?
* How do we know when it's complete?

It's this confusion that leads so many founders to spend months building something that nobody actually wants.

So how do we do better? How do we make MVPs that actually help our startup progress vs burning runway?

It all starts with why. But first, some declarations:

* Business - The goal of a Business is to create Impact (value, $$$, change, etc)
* Market - The Market is complex with lots of hidden incentives, opportunities, and motivations
* Startup - A Startup is a hypothesis for a sustainable business
* Product - A Product is a hypothesis for a specific opportunity of value creation

When we look at the markets like this, we notice that in order to create a Business, we basically need to verify a strategy that covers [Market x Product x Business](https://labs.hamy.xyz/posts/business-hypothesis/). A strategy is really a set of hypotheses based on the current state of the world or Market (if we do STRATEGY, then we'll get RESULTS).

So basically all of our Business plans are made of and based on hypotheses. So if we actually hope to fulfill these plans then a good first step is to actually verify these hypotheses. If any are proven false then our business plan must change in some way as they're based on incorrect data.

This is where MVPs come in. An MVP is really the Minimum Viable Experiment we can use to verify one or more of these hypotheses. So as you think about MVPs and what features you need to build (if any), you really need to ask yourself one question:

* Q: How can we test these hypotheses as accurately, quickly, and cheaply as possible?

The MVP is should be the answer to this question. Because of this, I actually believe that MVE (Minimum Viable Experiment) is a more apt name for this as it more clearly conveys its purpose.

# Creating Effective MVPs

Now that we understand why we're building the MVE (Minimum Viable Experiment), we can get to work doing it.

The first thing we need to understand is taht every business is based on a set of core assumptions. For our business to be successful, these core assumptions must be understood and verified. The faster we can verify these assumptions, the faster we can capitalize on a promising opportunity or pivot from a strategy based on false assumptions.

Common core assumptions that underlie most businesses:

* People have the Problem we're solving (Market)
* People will use the Product we make to solve this Problem (Market x Product)
* People will pay money for this Product (Market x Product x Business)

The hypothesis that underlies your business then is that all of these core assumptions are true. Thus each of these assumptions is a hypothesis underlying your business.

Now how do we actually go about testing this hypothesis?

We do it with some age-old, battle-hardened systems - the [Scientific Method](https://en.wikipedia.org/wiki/Scientific_method)! The scientific method has been used for centuries to rigorously test unknowns and it's equally applicable to products and startups where we're really trying to understand the market and how to impact it.

The basic Scientific Method is:

* Observation / Question
* Research the topic area
* Hypothesis
* Test with experiment
* Analyze data
* Report Conclusions
* (repeat)

This has been condensed and repurposed for the application of startups / products many times. For instance, [Eric Ries' The Lean Startup](https://amzn.to/3aOiCl5) introduces the Build - Measure - Learn framework:

* Build - Build an MVE
* Measure - Analyze the data of your MVE
* Learn - Create Validated Learnings

![The Creation Cycle](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/the-creation-cycle/applied-creation-cycle.png)

I created [The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle-2022-06/) which is a more generalized, streamlined approach specifically for building products:

* Observe - Create hypotheses for the state of the world and how we can impact it.
* Create - Create MVEs to test our hypotheses.
* Reflect - Compile Validated Learnings from this Cycle to improve the next Cycle.

The MVE fits directly in the experimentation phase. By using a hypothesis testing framework with an MVE we are able to create a cycle of continuous learning and iteration to make informed guesses about what might work and rigorously understand if it worked or not.

Thus the actual form of your MVE is whatever best helps you test the core assumptions / hypotheses that underlie your business / creation. This could be:

* A website
* A webform
* A mobile app
* A survey
* Demos with Customers
* A blog post

Once you have your Why in mind, it's much easier to determine what actually needs to go into your MVE.

# Building Businesses with MVPs

So now we understand what MVPs and MVEs are and how we can leverage the Scientific Method via [The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle-2022-06/) to iterate on products to test our hypotheses. Now I want to give some examples to show how we can leverage this process to actually build towards a sustainable business.

To do this, we're going to pretend like we're building a business and go through each of these steps. In particular, I'm going to call out the specific hypotheses we may have and how we can test them.

**Example business:** An app that displays happy hours in NYC

## Observe

Goal: Create hypotheses for the state of the world and how we can impact it.

* What are we trying to accomplish?
    * Build a sustainable business.
* How will we know we've accomplished it?
    * Profitable, making decent $ / hour
* What core assumptions are we making?
    * [Market] People care about finding happy hours in NYC
        * Implicit: People have a problem finding happy hours in NYC
    * [Market x Product]: People will use this product over Google Maps
        * Implicit: People are not satisfied with Google Maps for Happy Hours
    * [Market x Product x Business] There's a way to monetize this

## Create

Goal: Create MVEs to test our hypotheses.

* What are the most effective experiments we can create to test these hypotheses?
    * [Market] People care about finding happy hours in NYC
        * Implicit: People have a problem finding happy hours in NYC
            * Research and talk to people, see if we can verify problem
            * Write blog posts, see if we get traffic
            * Build landing page, see if we get sign ups
            * These also solve for: [Market x Product] People are not satisfied with Google Maps for Happy Hours if we see traffic
* Design it
* Build it

## Reflect

Goal: Compile Validated Learnings from this Cycle to improve the next Cycle.

* How does our data match up with our hypotheses?
    * If we wrote a blog post and got traffic
        * Seems like people are searching for this
            * May mean it's a real problem
            * May mean they're not satisfied with existing solutions
    * If we built a landing page but got no signups
        * Maybe people don't have this problem
        * Maybe people don't want to use this specific Product
        * Maybe people don't want to put a lot of effort in / the positioning is off
* Validated Learnings
    * Based on the experiments and results from above:
        * People do seem interested in this
        * People are not interested in the current Product we have
* Followups
    * Hypotheses for improving blog posts
        * More efficient (less cost)
        * More effective (higher returns)
        * Understand more about why this worked
    * Hypotheses for improving landing page / pivoting Product
        * Why do people like the blog posts but not the landing page?
        * What pivots can we experiment with and how does that impact our metrics

# Conclusion

Building startups can be a long, hard journey. Don't make it longer and harder by wasting resources on MVPs that don't actually test your business' core assumptions or move your startup forward.

Leverage MVEs and efficient frameworks for hypothesis testing like [The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle-2022-06/) for smooth, disciplined iterations. It won't guarantee startup success, but it will guarantee less wasted cycles.