---
title: "Affinity Designer: How to Rotate around Custom Rotation Center"
date: 2016-08-15T22:52:43.000Z
date_updated:   2016-08-15T22:52:43.000Z
comments: false
---


**Problem: **I’m using Affinity Designer and I’m trying to rotate an object around a custom rotation center I’ve set. However, whenever I use the rotation option under Transform, it turns the object in place, taking my custom rotation center with it. How do I get the object to rotate around the rotation center, not around its own center?

**Solution: **This is assuming you’ve already set the custom rotation center to the point of your choosing. To set the custom rotation center:

- Select the object you’d like to rotate
- Select the “Show rotation center” (looks like a target reticule) as shown below: <div class="wp-caption aligncenter" id="attachment_1806" style="width: 236px">![Screen Shot 2016-06-16 at 10.49.36 PM.png](http://i2.wp.com/www.sirhamy.com/blog/wp-content/uploads/2016/06/Screen-Shot-2016-06-16-at-10.49.36-PM.png?fit=226%2C70)The “show rotation center” option looks like a target reticule

</div>
- Move the rotation center to the point you’d like to rotate around

Now that your rotation center has been set, we can do the actual rotation. To rotate your object around the rotation center:

- Select the object you’re rotating
- Click and drag the grey handle attached to your object. This is the rotation handle. - This should rotate your object around your custom point

<div class="wp-caption aligncenter" id="attachment_1811" style="width: 438px">![Screen Shot 2016-06-16 at 10.51.24 PM.png](http://i1.wp.com/www.sirhamy.com/blog/wp-content/uploads/2016/06/Screen-Shot-2016-06-16-at-10.51.24-PM.png?fit=428%2C304)The grey handle is the rotation handle. Click and drag to rotate

</div>

