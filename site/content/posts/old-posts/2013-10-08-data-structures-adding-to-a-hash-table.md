---
title: "Data Structures: Adding to a Hash Table"
date: 2013-10-09T01:03:57.000Z
date_updated:   2013-10-09T01:03:57.000Z
comments: false
---


**Adding to a Hash Table: **Usually you add data of the form <K,V> (Key, Value pair) to a hash table.  In that case, you would pass the key value K into a hash function and then use the resulting hash value to determine which spot inside of the hash table to put the corresponding value V.  For the most part, you’ll take this hash value and take the modulus of that value over the table’s length (hashValue % hashTable.length) to ensure that regardless of the size of the table, the hash value will direct the datum into a valid slot.  A hash table usually has some system of collision handling to deal with situations when two pieces of data are hashed to the same value.



