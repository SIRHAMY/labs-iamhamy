---
title: "Calculus: Find the Distance from the Point to the Plane"
date: 2013-09-23T18:44:05.000Z
date_updated:   2013-09-23T18:44:05.000Z
comments: false
---


**Find the distance from the point  P(2,2,3) to the plane 2x + y + 2z = 4**

The formula for distance here is:

d = |projN(W)| = (W dot N)/|N|

N is the vector normal to the plane, N = (2,1,2)

W is the point P(2,2,3)

Find the distance by plugging in the values to the formula

d = (2*2 + 2*1 + 3*2 – 4)/ 3

**Final: d = 8/3**

 



