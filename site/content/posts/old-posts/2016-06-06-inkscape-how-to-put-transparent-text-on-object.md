---
title: "Inkscape: How to put Transparent Text on Object"
date: 2016-06-06T15:16:21.000Z
date_updated:   2016-06-06T15:16:21.000Z
comments: false
---


**Problem: **I have a sketch in Inkscape and I want to embed a transparent shape into another shape. Specifically, I have a red rectangle and I want the shape of the word ‘tranquil’ to be transparent inside that rectangle. That basically means there will be a rectangle with “holes” in it in the shape of the word. How can I make an object transparent based on the shape of another object?

![TranquilPathDifference](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2016/05/TranquilPathDifference.png?fit=262%2C133)

**Solution: **The easiest way to go about this is to use Inkscape’s built-in Path tools.

- Create your rectangle
- Create your text
- Position the text over the rectangle as you wish the transparency to be transferred
- Select both the text and the rectangle using Shift + selection
- Select Path > Difference. This will remove any overlap between the two objects

That’s it! You’ve successfully embedded transparency into an object using the shape of another object.



