---
title: "Eclipse: Open Existing Directory in Editor"
date: 2016-02-04T15:01:44.000Z
date_updated:   2016-02-04T15:01:44.000Z
comments: false
---


**Problem: **I have a project I’ve been working on in a folder on my computer. I now want to open that same folder in Eclipse, but it’s not an Eclipse Project. How do I get these files inside the Eclipse editor?

**Solution: **To open an existing file system in your Eclipse editor:

- Open Eclipse
- Got to **File > New > Project…**
- Select **General > Project**
- Enter a Project name
- **Deselect “Use Default Location”**
- Hit **Browse** and select the folder your files/project is in
- Hit finish



