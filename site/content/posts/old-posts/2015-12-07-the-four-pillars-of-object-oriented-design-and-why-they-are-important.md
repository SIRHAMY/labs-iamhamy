---
title: The Four Pillars of Object Oriented Design - And Why They Are Important
date: 2015-12-07T05:35:13.000Z
date_updated:   2015-12-07T05:35:13.000Z
comments: false
---


Object Oriented Programming is a booming design paradigm in the software industry. It helps keep code clean, efficient, and modular. This post details the four key pillars in the theory of OOP and why each pillar exists.

1. Abstraction  
**-Definition: **Exposing the essential features of an element, hiding irrelevant detail. Generalize functionality.  
 –**Why It’s Important: **What we’re essentially saying here is that we only expose that which outsiders *need *to see. This means we can keep all the internal logic and functions from outside eyes which means our code/functionality is a lot ***easier to read/understand***. Moreover, by separating our functionality from outside elements, we enable ourselves to more easily replace internal workings without affecting these external elements thus making our code base more ***modular*** and ***easier to modify/maintain*** in the long run. By abstracting away irrelevant details, we set ourselves up to more easily inherit/reuse this code later on.
2. Encapsulation  
 –**Definition: **Everything that involves the element is handled within the element  
 –**Why It’s Important: **This one feeds directly off the last one. By handling all functionality directly involved with the element within the element (think a class in Java) we are both hiding the internal workings and exposing only the essential features to the outside – as prescribed by the Abstraction feature. So, by encapsulating all functionality within each element, we achieve greater ***security*** (by preventing outside, unauthorized sources from accessing key internal functions, data), ***code readability*** (by having all pertinent functionality in one place), and ***modularity*** (all functionality handled internally, so it can be modified without affecting external connections)
3. Inheritance  
 –**Definition: **The ability to create an element from an existing element  
 –**Why It’s Important: **The ability to inherit attributes from an existing element means we have to ***write less code*** to achieve the same functionality as we don’t have to rewrite all existing methods and features. It also means we can ***reduce code complexity***, ***increase readability***, and ***increase maintainability*** (key here is that by changing an element upstream i.e. a superclass/parent, functionality trickles down to its children). With this pillar in place, we’ve paved the way for polymorphism.
4. Polymorphism  
 –**Definition: **The ability to take many forms  
 –**Why It’s Important: **At this point we’ve introduced all these attributes involving specializing functionality through the inheritance of base attributes. This is fine and dandy, but we now need some way for all these new slightly disparate elements to interact with each other.  This is where polymorphism comes to play – you can think of it as the grease for the gears or the glue for the chasis. It allows us to write functions that can accept elements of many different types and still accomplish the required work. It could be a square function to raise different types of numerical values to the power of two. Or a toString() function that works differently based on what it’s called on. This allows us to be true to the first three principles without having to write complex code later on to offset these inequities.

That’s a pretty good summary of the four principles. The key thing to keep in mind is that OOP aims to ***reduce code complexity, increase maintainability, and decrease the amount of code written.***

Here’s another [good post on the Four Pillars](http://theleancoder.net/index.php/2015/10/11/the-four-pillars-of-object-oriented-design/) complete with UML diagrams as examples.



