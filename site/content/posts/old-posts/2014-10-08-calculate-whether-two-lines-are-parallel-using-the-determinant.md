---
title: Calculate whether two Lines are Parallel using the Determinant
date: 2014-10-08T21:30:06.000Z
date_updated:   2014-10-08T21:30:06.000Z
comments: false
---


**Problem: **Figure out whether two lines are parallel using the determinant

**Solution: **If we have line AB and line CD (technically edges as lines continue on into infinity), then we can determine whether these two lines are parallel by taking the determinant of the two.

Computing P = AB | CD will give us the point at which the two lines intersect.  If the lines don’t intersect i.e. are parallel, then the result will be 0.



