---
title: "JavaScript: Format Integers with Commas"
date: 2016-05-16T10:53:16.000Z
date_updated:   2016-05-16T10:53:16.000Z
comments: false
---


**Problem: **I have a set of large integers I’m trying to display on a page. As they are, they’re just a long string of numbers which makes it hard to tell where the thousands, millions, billions, etc. places are. How can I format these integers with commas so that they are human-readable?

**Solution: **The easiest solution is to use the built-in toLocaleString() function. It’s not the fastest method, but it’ll get the job done. I’ve tested it in FireFox and Chrome and it’s worked fine. For more info, you can check out the original [StackOverflow post](http://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript).

> var num = 3000000;
> 
> console.log(num.toLocaleString()); //3,000,000



