---
title: "Troubleshoot: Can't See Std::Cout in Visual Studio 2015 C++ Project"
date: 2016-03-21T20:31:29.000Z
date_updated:   2016-03-21T20:31:29.000Z
comments: false
---


**Problem: **I’ve made a C++ project in Visual Studio 2015, but can’t see the std::cout console logs I’ve created.

**Solution:**


## *Look in the right place*

First, make sure you’re looking in the correct place. When you run your program, you should see a** black console window** appear. Your output will be inside of that.

If you don’t see a black console window, continue to the last bit.


## *Make sure you’re flushing your logs*

Sometimes console logs aren’t pushed unless you flush the stream. You can do this by appending **<< endl;** to the end of each of your logs.


## *Make sure you have a console app*

If you don’t have a console app, VS may not know what to do with your std::couts. You can ensure this by going to **Project > Properties > Linker > System > Subsystem** and choosing **“Console”**



