---
title: "Jiffy Time Tracker: How to Archive Projects"
date: 2016-06-20T22:07:52.000Z
date_updated:   2016-06-20T22:07:52.000Z
comments: false
---


**Problem: **I’m using the Jiffy Time Tracker to keep track of time spent on different activities. Over time, I’ve accrued a lot of different projects that are now crowding my dashboard pane. I want to remove the projects from the Dashboard, but keep all the data I’ve tracked. How can I do this?

**Solution: **You can remove a project from the Dashboard pane and keep its records by using the Archive feature. To archive a project:

- Click the hamburger button in the top left to pop out the menu
- Select Projects
- Select the project you’d like to archive
- Click the settings button in the top right (three vertical dots)
- Select Archive

Boom, you can still access the project records, but they won’t be shown in the Dashboard screen.



