---
title: "Troubleshoot: Can't Find Gear Fit Manager in Samsung Apps (GALAXY Apps)"
date: 2014-12-26T21:13:23.000Z
date_updated:   2014-12-26T21:13:23.000Z
comments: false
---


My mom recently got a Gear Fit watch so that she could have a running count of the steps she took as she went about her daily life.  We turned on the watch and were greeted with a simple list of instructions to set up the device.  Unfortunately, we were unable to complete the very first task: downloading Gear Fit Manager from Samsung.

After a long period of banging heads against walls, we were finally able to find the app in question.  Here’s what we did:

**How to Find Gear Fit Manager**

*Before we begin, make sure you have the app “GALAXY Apps” installed on your device.  If you don’t, it means you probably haven’t updated Samsung Apps yet.  Go to Samsung Apps and install the required update.*

- Go to GALAXY Apps and search for Gear Fit Manager in the search bar.  If you can’t find it, it means that the app isn’t compatible with the current state of your phone.  Don’t fret, there’s a quick fix.

**Ensuring your Phone is Compatible with Gear Fit Manager**

Assuming your phone model is compatible with the Gear Fit watch, follow the instructions below to update your phone to the required version.  A list of phones models that are *not *compatible with Gear Fit can be found on [Samsung’s website](http://www.samsung.com/us/support/gear/gear-fit-support/ "Gear Fit Support").

- GALAXY Apps only shows app results that are compatible with your phone.  It is therefore relevant to notice that Gear Fit Manager requires an Android version of 4.3 or higher.  You can check your current version by going to your phone’s settings and then scrolling down to the About Device option – usually located in the System menu.  Once there you can see your current Android version next to, you guessed it, “Android Version”.  If you’ve been having trouble finding the Gear Fit Manager app, odds are that your version is below 4.3.  In order to update your phone’s software, simply select the Software Update option and confirm the download/installation.



