---
title: "My SWE interview guide"
date: 2019-05-16T12:26:12-04:00
tags: [ "swe", "interview-guide" ]
comments: false
---

_Updates 2022.07.05: Most of the advice remains true in 2022. I've updated a few areas with newer platforms and perspectives to make this work for more senior engineers as well._

For those that don't know, [I've been working as a software engineer for the past several years](https://hamy.xyz/about). In this time (and [particularly during my time in college](https://medium.com/@SIRHAMY/in-review-my-fall-2016-1b4c24d2c6c7)), I've gone through several different interview phases consisting of studying for and then taking interviews at various companies around the US. Now I don't think I'm amazing at this as evidenced by not receiving offers from every company I interview at, but I like to think I'm not bad - at least good enough to prove to a few companies that I'm worth paying for.

In this post, I'll walk through some of the ways that I like to study, some things to keep in mind during this study, and some resources that will help you on your journey.

# what companies are looking for

Before we talk about preparations, it's first best to understand what it is that companies are actually trying to determine as this will serve as a north star for your preparations. In the most basic sense, they're trying to understand how you'll perform if you were at their company. There are infinite data points that could be used to determine this but the most common / generalizable ones seem to be:

* **How do you solve problems?** - In any job function, the most basic thing you are doing is solving problems. This is particularly true in software engineering where you're not only supposed to solve a problem in the real world, but solve it with a specific set of tools at your disposal (read: additional constraints). This is probably the most important indicator of any individual's success as if you can solve problems, everything else can be acquired from there.
* **Can you code?** - The second most important thing is whether or not you can code. Recall that we're being hired to write good, maintainable code. So it's great if you can solve the problem but if your code is spaghetti, we might want to just hire someone slightly worse at solving problems but that can write code that can be maintained long after you've left the company. So be sure you can write good, maintainable code and that you're comfortable in whatever programming language you choose.
* **How do you deal with feedback / work in a team?** - This one is more nebulous than the others and will rarely get you positive points but a red flag here can derail an entire application even if you excelled in the other areas. The keys are really to not be a jerk, listen to your interviewers and try to understand where they're coming from and why they might be suggesting / asking certain things, and to not lose your head. All interviews are really just get-to-know-you discussions. Both of you are humans so treat each other like it.
* **How do you identify and execute on large scale problems / impact effectively?** - This is more geared towards more senior roles where you're expected to do more than just build features but to really drive business impact. The more senior you are the more you are expected to be an impact entrepreneur - understanding the business' problems and opportunities, working with many stakeholders to plan how to tackle it, and executing on efficient iterations to verify these plans and produce the desired impact. The signal you provide here is largely used to level you (i.e. do you come in as a junior E4 or a staff E6) making this crucial to focus on after you pass the basic threshold of coding. Signal for this will typically be captured in your behavioral and system design interviews however your problem exploration in coding is also a signal for this. My general approach to these things in the real world is [The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle-2022-06/).

# points to keep in mind

Great, we now know how most companies are going to be grading us / through what lens they'll be viewing us, so what do we do about it.

* **Understand your tools** - The first thing we need to do is make sure that we understand the tools at our disposal to solve these problems. Without these tools, we'll either be unable to solve them or be solving them with magical ideas that an interviewer will disqualify. The tools at your disposal are thus the datastructures and algorithms that we all learned in school. There are numerous so I won't delineate them here but sticking with your basic / intermediate ones will usually be fine. That being said, you should know each of them inside and out and be able to talk about why each has particular peculiarities - so what's the run-time of them / their operations, how much space do they take, how does using one in a given situation stack up to another in the same situation, how would you implement them, etc. If you need to brush up on these, I'd say a quick read through the [getting started resources](#getting-started) resources is sufficient.
* **Solve quickly, simply, and efficiently** - Each interview is likely only going to last ~45 minutes and in this time many expect you to finish multiple problems / parts of problems. Thus it's useful to practice doing problems quickly and succinctly to demonstrate capability and also give yourself a fighting chance against other candidates that will surely be doing the same. A good rule of thumb is to be able to complete 2 "medium" questions or 1 "hard" question within 45 minutes. To practice these, I've listed some of my favorite [problem solving resources](#problem-solving) below.
* **Talk/think/solve out loud** - Recall that the number one thing people are looking for is to understand how you solve problems. The above two points will help you with problem solving fluency, but that's only half the battle. The interviewers will want to know what you're thinking, how you weigh different design / implementation decisions, and will often interject with questions of their own. As such, thinking out loud will both clue them in to what you're doing at every step of the process and also get you used to talking during your thinking process. This is not hard, but it's also not natural so practice in this area can go a long way to keeping you on track during an interview. A good way to get used to this is to use some of the [practice interviewing resources](#practice-interviewing) listed below.

# general problem solving technique

There are infinite ways to go about solving a given problem but these are some points I like to keep in mind because they've helped me in the past. Take them or leave them as you wish.

* **Consider the problem and ask clarifying questions** - The first step is to try and internalize the problem and clarify with the interviewer whether the problem you understand is the problem that they're trying to ask. This can save you from going down a completely wrong rabbit hole. You can do this by providing counter examples, asking about edge cases, thinking through constraints on inputs, etc.
* **Consider the best conceivable run-time** - This step isn't really necessary to solve the problem but I like to think about it at the beginning because it gives me a limit for how good my proposed algorithm _could_ be. It's possible that you won't be able to come up with an algorithm that's this good however it gives you some assurance that once you do, you no longer have to worry about trying to come up with an alg that's an entire order of magnitude better. So for instance, if you need to find the min/max of totals of 2 elements in an unsorted list, you know that the best conceivable runtime would be O(n) because in order to find all the answers from all the elements, you at least need to first look at all the elements.
* **Brainstorm ways to solve this** - I like to start with thinking through the super naive solution, then thinking about reasons its so naive and ways it could maybe be optimized (like are we comparing the same elements over and over, why is the runtime so high / how could we decrease it, why are we using so much space, etc.). Some people like to call this process as massaging the bottlenecks, where at every step you consider why the worst part of your algorithm is so bad and ways you could change it / other constraints to make it better. I like to keep doing this, writing out bulleted steps on how each would work and their corresponding run/space complexities, til I either come up with a satisfactory answer or begin to run out of time.
* **Code it up** - Now it's time to code. Remember to talk out loud and let the interviewer know if you're using and shorthands for things. I like to use shorthands for simple classes, helper methods, variable names, and even validation checks. This is always fine given I explain to the interviewer what it is, how it works, and why I'm doing it. They'll often ask me to go back and implement it later on if we have more time which is reasonable but it allows me to defer the simple stuff to prioritize the harder things that, I think, they care about more.

# resources

Here I've listed some resources that I like / that have been recommended to me. This is by no means an exhaustive list but it'll get you started.

## getting started

I think everyone who's getting back into the interviewing game and trying to brush off some of the rust should go through this

* [Cracking the coding interview](http://www.crackingthecodinginterview.com/) has been a staple of swe interview preparation for years now and I think it lives up to the hype. It's a big book and I think that some chapters are better than others. The book itself is expensive but there's almost always someone who's uploaded it to a github repo somewhere so a quick google search should net you a free copy. If you're trying to get the most bang for your buck, I'd recommend reading:
    * Essential: Chapters I, IV, V, VI, VII, IX ( 1 - Arrays and strings, 2 - linked lists, 3 - stacks and queues, 4 - trees and graphs, 8 - recursion and dynamic programming )
    * Optional (like if you just want to mix up questions down the road): 9 - system design and scalability, 16 - moderate questions, 17 - hard questions

## problem solving

Honestly there are tons of sites that all do just about the same thing. Here are some of my faves:

* [LeetCode](https://leetcode.com) - good questions, good submissions / answers to compare against, and they have cool sorting by companies and frequencies at which Qs are asked.
* [InterviewCake](https://www.interviewcake.com/) - Some Qs are good, some aren't so good but they do a really good job of walking you through some of the thought processes that happen in good problem solving.
* [TopCoder](https://www.topcoder.com/) - If you're looking for a real challenge, go to TopCoder or one of the actual programming competition sites and try some of their past competitions. It's very rare that you'll get a question that hard but if you can compete on these, you're going to do very, very well in the interviews.

## practice interviewing

As I said, problem solving / coding isn't everything. There's also an element of simply practicing solving in front of another human with a time constraint. Luckily, there are a ton of new sites popping up that help you do just that:

* [interviewing.io](https://interviewing.io/) - This site is really great and matches you up with real, trained interviewers from real companies. The questions are very "real" in their format and delivery and all the interviewers I've had were excellent. Would highly recommend.
* [pramp](https://www.pramp.com) - pramp allows you to schedule interviews with other people practicing for interviews themselves. It's a little more free-form so this might be a good place for you to start. In this mode, each participant takes a turn solving a problem and then you switch and give a different problem to the other person. I think it's good practice to be both the interviewee and the interviewer to better understand where each side is coming from and what they might be looking for. Because it's candidates to other candidates, there's not that much quality control so sometimes the Qs can be bad, people will leave, or the experience just isn't great overall. But you're learning and it's way better than nothing so would still recommend you plop this in your training regimen somewhere.

# my regimen

Honestly I didn't really stick to any one schedule while studying but a median schedule was to try to regularly complete this each day:

* 2 medium questions (< 20 mins each)
* 1 hard question (< 25 mins)
* 1 random question (in whatever time period equates to its difficulty)

No one can know what kind of preparation is right for you, but this worked for me so I kept with it.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on things I've built by [subscribing here](https://hamy.xyz/subscribe)