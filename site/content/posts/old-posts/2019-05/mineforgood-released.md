---
title: "Mine for Good now available in a browser near you!"
date: 2019-05-26T13:00:59-04:00
tags: [
    "mineforgood",
    "v0",
    "tikkunolam"
]
comments: false
---

For the past week or so I've been building a new app called [Mine for Good](/projects/mineforgood). It embodies a combination of my enjoyment of building shit and my will to [make the world a slightly better place](https://blog.hamy.xyz/posts/introducing-tikkun-olam-tuesdays/).

Today I'm releasing it to the world. Check it out at [mineforgood.xyz](https://mineforgood.xyz)

Most everything you need to know is available on the [Mine for Good project page](/projects/mineforgood) and that's where I'll be keeping things up-to-date so if you wanna know more go over there.

If you want updates on this, and other, projects of mine, you can get them by [subscribing here](https://hamy.xyz/subscribe).

Thanks for reading!

-HAMY.OUT