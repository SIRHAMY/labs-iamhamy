---
title: "Sorting: Boyer Moore"
date: 2013-11-25T02:11:59.000Z
date_updated:   2013-11-25T02:11:59.000Z
comments: false
---


**Big O: **O(nm)

Start by creating a pattern array containing each unique character that appears in the pattern.  Next giver each of these elements a weight congruent to the first instance of that character in the pattern, starting from the end of the pattern.  These weights will be used in the instance of a negative match between the pattern and provided text.

**Example: **Given a pattern: “kadabra”, the pattern array would look like

<table border="1"><tbody><tr><td>k</td><td>6</td></tr><tr><td>a</td><td>1</td></tr><tr><td>d</td><td>4</td></tr><tr><td>b</td><td>2</td></tr><tr><td>r</td><td>1</td></tr></tbody></table>Notice that the first and second (0 and 1 indices) from the end both receive weights of 1.

Start matching the pattern to the text, starting with the last element in the pattern.  If the last element matches, then continue on down the pattern until you either reach the end of the pattern or hit a negative match.  In the case of a negative match, move the pattern down the text string by spaces equal to that character’s weight.  If the text character being matched is not within the given alphabet, move the pattern down by the pattern’s length.



