---
title: "How to Add Google Analytics to Hugo Theme"
date: 2018-08-29T22:00:24-04:00
description: "About the page"
draft: false
---

**Problem:** I have a Hugo theme (currently using [manis-hugo-theme](https://github.com/yursan9/manis-hugo-theme)) and I want to use Google Analytics to keep track of my traffic. How do I do this?

**Solution:**

Hugo actually has a really awesome built-in solution that you can read about in their guide [Google Analytics with Hugo](https://gohugo.io/templates/internal/#google-analytics).

Basically, you can just add this line to your config.toml:
```
googleAnalytics = "my-google-analytics-code"
```
[See an example commit](https://gitlab.com/SIRHAMY/labs-iamhamy/commit/15756e11ff39aec4ab197c16861c883870835e92)

Then you can add the internal template that Hugo maintains by adding this line somewhere in your header partial (so that the Google Analytics JS payload is generated on each page):
```
{{ template "_internal/google_analytics_async.html" . }}
```
[See an example commit](https://github.com/SIRHAMY/manis-hugo-theme/commit/8422086881d718d354bdb2e6e691d862b3da712b)

Simple as that, your Hugo site is backed by Google Analytics.