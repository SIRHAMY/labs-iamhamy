---
title: "Mac: Chrome Refresh Hotkey"
date: 2016-08-08T10:55:44.000Z
date_updated:   2016-08-08T10:55:44.000Z
comments: false
---


**Problem: **I’m using Google Chrome on my Mac and have been trying to figure out how to refresh my Chrome tab using a keyboard shortcut. I know on PC it’s usually CTRL+F5, but that doesn’t seem to be working. How do I refresh my tab on Mac?

**Solution: **On Mac, the default refresh hotkey for Chrome is CMD+R



