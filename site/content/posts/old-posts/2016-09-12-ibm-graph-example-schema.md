---
title: "IBM Graph: Example Schema"
date: 2016-09-12T14:23:35.000Z
date_updated:   2016-09-12T14:23:35.000Z
comments: false
---


**Problem: **I’m using IBM Graph, but I can’t figure out how to make a schema that ‘works’. Are there any examples out there that I could use to build mine off of?

**Solution: **The IBM Graph docs have a pretty good example in the *[Interactive Guide](https://ibm-graph-docs.ng.bluemix.net/interactive-guide.html) *portion of their documentation.

Reproduced below:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/b3c5d411faf3ad730ca3ecd8d0ebc429.json"></div>[[https://gist.github.com/SIRHAMY/b3c5d411faf3ad730ca3ecd8d0ebc429](https://gist.github.com/SIRHAMY/b3c5d411faf3ad730ca3ecd8d0ebc429)]



