---
title: "Troubleshoot: AWS Elastic Beanstalk Returning Error Code 9 When Uploading PHP Files"
date: 2015-04-14T21:44:00.000Z
date_updated:   2015-04-14T21:44:00.000Z
comments: false
---


**Problem: **You’re trying to upload PHP files to your Elastic Beanstalk application server on Amazon Web Services (AWS), but you keep getting an error.  In most cases, you’ll notice that when accessing your app server from the web you’ll get the standard Sample Application instead of your updated code.  If this is the problem, you’ll see an Event similar to this appear:

> Command failed on instance. Return code: 9 Output: (TRUNCATED)…ive. unzip: cannot find zipfile directory in one of /opt/elasticbeanstalk/deploy/appsource/source_bundle or /opt/elasticbeanstalk/deploy/appsource/source_bundle.zip, and cannot find /opt/elasticbeanstalk/deploy/appsource/source_bundle.ZIP, period. Hook /opt/elasticbeanstalk/hooks/appdeploy/pre/01_unzip.sh failed. For more detail, check /var/log/eb-activity.log using console or EB CLI.

**Solution:** In this case, it means that you are trying to upload something that isn’t in a zip file.  As this [AWS doc](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_PHP_console.html) mentions, you need to upload your files in a zip file when going through the Elastic Beanstalk GUI.

1. Place your PHP files in a zip file.
2. Upload your file.
3. Configure the app server so it know what to do with them.



