---
title: "Splunk: Get fields from TimePicker Module in Sideview Utils"
date: 2014-06-04T18:04:09.000Z
date_updated:   2014-06-04T18:04:09.000Z
comments: false
---


**Problem: **I want to use the time range selected in the TimePicker Module for my search/HTMl box/etc.

**Solution: **

- Assuming the module you would like to use the time range in is a child of the TimePicker Module, you can denote the field by typing “$search.timeRange.earliest$” for the earliest time and “$search.timeRange.latest$” for the latest one.  The $ signs on each end of the variable tell Splunk that it is a variable and that it should look for the appropriate source.

*Note: Some modules automatically inherit the timeRange from their parent module.  Read the tooltips carefully (bottom right quadrant of Sideview Utils visual editor) so that you don’t interfere with any automatic mechanisms.*



