---
title: "Guide: Turn On Keyboard Backlight - Lenovo Think Pad T440s"
date: 2014-08-18T14:00:49.000Z
date_updated:   2014-08-18T14:00:49.000Z
comments: false
---


To turn on your Lenovo Think Pad’s keyboard backlight, press the following buttons:

Fn + Space Bar



