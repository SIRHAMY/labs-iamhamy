---
title: "C++: How to Add Two Eigen::Quaterniond Structures"
date: 2016-04-15T20:40:26.000Z
date_updated:   2016-04-15T20:40:26.000Z
comments: false
---


**Problem: **I’ve got two Eigen::Quaterniond values that I’m trying to add together. However, whenever I try to use the standard plus/addition (+) operator, it throws an error saying that function isn’t implemented. How can I add my two quaternion’s together?

**Solution: **Luckily, the solution is pretty simple, albeit tedious. Recall that an Eigen::Quaterniond’s value can’t be accessed directly. Instead, you have to use “getter” methods to access those values. For instance, you would use myQuaternion.w() to get the scalar and myQuaternion.vec() to get the vector portion of the quaternion.

So, to add two quaternions together, you would write code similar to this example:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/6d7a14a4dbeb3126ff89.json"></div>[[https://gist.github.com/SIRHAMY/6d7a14a4dbeb3126ff89](https://gist.github.com/SIRHAMY/6d7a14a4dbeb3126ff89)]



