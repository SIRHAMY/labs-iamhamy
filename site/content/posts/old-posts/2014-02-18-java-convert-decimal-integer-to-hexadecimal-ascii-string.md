---
title: "Java: Convert Decimal Integer to Hexadecimal ASCII String"
date: 2014-02-19T01:49:41.000Z
date_updated:   2014-02-19T01:49:41.000Z
comments: false
---


**Problem: **Convert a decimal integer to an ASCII string in hexadecimal without using built-in library functions.

As you should know, hexadecimal is base 16.  A simple guide to converting from decimal to hex can be found [here](http://www.webelfin.com/webelfindesign/dechex.html).

On to the code:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/9087140.json"></div>(https://gist.github.com/SIRHAMY/9087140)

I started by creating the result variable that will be used to store the returned value.

Next, I created an array to easily access the hexadecimal equivalent of each number (0-15).

I then created a loop that continually adds the remainder of the number remaining divided by 16 to the front of the result string.  After this action takes place, the number remaining is divided by 16.  This continues until the remaining total is less than 16, where that hex value is added to the beginning of the result string then returned.



