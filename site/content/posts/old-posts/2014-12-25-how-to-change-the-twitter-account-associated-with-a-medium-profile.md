---
title: How to Change the Twitter Account Associated with a Medium Profile
date: 2014-12-26T02:10:23.000Z
date_updated:   2014-12-26T02:10:23.000Z
comments: false
---


Over the past few months, I’ve changed Twitter accounts three or four times due to various reasons.  Because of this, I’ve had to go through several of my connected accounts in order to update the Twitter account I use to sign in.  This is usually a pretty easy process, but Medium stumped me a few times so I thought I’d share my knowledge.

One thing you should know about Medium is that you must always have at least one account – Facebook or Twitter – connected at all times.  This means you can’t simply disconnect your Twitter account and connect a new one, instead forcing you to use an interim Facebook alias during the switch.

Another thing to notice is that only one account may be connected to an email address at any one time.  So, if you have multiple accounts, you’re going to have to have a separate email address for each one.

**Switching Twitter (or Facebook) Accounts**

*If switching Facebook accounts, simply substitute Twitter with Facebook and vice versa in the following guide.*

1. The first thing you’re going to want to do is sign in to the Twitter account that you are disconnecting and the Facebook account that will hold the account during the switch.
2. Once signed in to your Twitter account, go to Medium and sign in with Twitter.  Make sure that the account has the email address you would like to use. This can be checked by navigating to the **Settings** page by clicking on your **Profile Picture** in the top right corner of the site then selecting the appropriate option from the drop-down menu  *Remember: Only one account may be connected to an email address at any time.*
3. Once on the Settings page, and after verifying your email, scroll down to the **Connect to Facebook** option.  Click the button next to it to connect your Facebook account to the Medium account.
4. Now, next to the **Connect to Twitter** option, click “**Disconnect**“.  This will disconnect the old Twitter account and allow us to use a different one.
5. Open up a new tab and sign out of the old Twitter account.  Sign in to the new one you’d like to use.
6. Back on the Settings page, click the **Connect to Twitter **option to connect the new Twitter account.
7. Disconnect the Facebook account (or leave it – your choice).

Happy writing and don’t forget to connect with me on [Medium](https://medium.com/@SIRHAMY "HAMY Medium Profile")!



