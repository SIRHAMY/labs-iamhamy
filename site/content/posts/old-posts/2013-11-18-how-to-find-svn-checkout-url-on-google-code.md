---
title: How to Find SVN Checkout URL on Google Code
date: 2013-11-18T14:24:21.000Z
date_updated:   2013-11-18T14:24:21.000Z
comments: false
---


Google Code isn’t the most friendly tool for beginners, which is why tutorials like this have to be made.  To find the SVN Checkout URL in your Google Code Project, follow these steps:

- Navigate to your **Google Code Project Page**.  (Go to code.google.com and type your project’s name into the Project Search bar)
- Click **Source -> Checkout**
- Your URL should be contained inside of a light grey box.
- If it’s a Read-only url, then you won’t be able to push back to the repository (it will say read-only in the URL).  If you are listed as a contributor of this project (if this is your project) make sure you’re signed in to your Google account before navigating to the Checkout page.



