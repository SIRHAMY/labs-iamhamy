---
title: "Calculus: Find the Local Maxima, Minima, and Saddle Points"
date: 2013-11-06T13:45:23.000Z
date_updated:   2013-11-06T13:45:23.000Z
comments: false
---


**Find the local maxima, minima, and saddle points for f(x,y) = x^2 – 2xy + 2y^2 – 2x + 2y + 1**

Start by taking the partial derivatives fx and fy

fx = 2x – 2y – 2

fy = -2x + 4y + 2

Because both functions exist at all vallues (x,y), a local extreme can only occur where

fx = 2x – 2y – 2 = 0 and fy = -2x + 4y + 2 = 0

Possibilities: (1,0)

At (1,0):

fxx = 2, fyy = 4, fxy^2 = 4, and fxxfyy – fxy^2 = 4

**Solution: By the [Second Derivative Test for Local Extremes](http://hamycodes.wordpress.com/2013/10/07/calculus-second-derivative-test-for-local-extreme-values/ "Calculus: Second Derivative Test for Local Extreme Values"), we find that there exists:**

**Local minima at (1,0) with a value of 0**



