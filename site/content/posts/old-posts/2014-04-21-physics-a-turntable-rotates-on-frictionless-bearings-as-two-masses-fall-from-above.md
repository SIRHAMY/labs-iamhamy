---
title: "Physics: A Turntable Rotates on Frictionless Bearings as Two Masses Fall from Above"
date: 2014-04-21T19:20:01.000Z
date_updated:   2014-04-21T19:20:01.000Z
comments: false
---


**Problem: **A 1.6 kg, 20-cm-diameter turntable rotates at 140 rpm on frictionless bearings.  Two 500 g blocks fall from above, hit the turntable simultaneously at opposite ends of a diagonal, and stick.  What is the turntable’s angular velocity, in rpm, just after this event?

**Solution: **

Always write down the information given to you:

M = 1.6 kg, R = 10 cm = 0.1 m, w(i) = 140 rpm, m = 500 g = 0.5 kg

First remember that the formula for angular momentum is L = Iw when rotating about a fixed axle/axis of symmetry.  Based on the law of conservation of momentum, we know that the momentum of the system in its initial state must equal the momentum of the system after the two blocks fall on the turntable.

L(i) = L(f)

Using resources readily available online, we know that the moment of inertia of a solid disk is I = .5 MR^2

So L(i) = .5MR²w(i)

Momentum in the final state will look something like this:

L(f) = .5MR²w(f) + m1R²w(f) + m2R²w(f) => .5MR²w(f) + 2mR²w(f)  *as m1 = m2 = m*

Solving for w(f), we get:

w(f) = (.5MR²w(i))/(.5M + 2m) = 62.22 rpm

**Answer: **w(f) = 62.22 rpm



