---
title: "Tutorial: Build an Angular Website Around a Wordpress Blog"
date: 2015-07-23T04:31:03.000Z
date_updated:   2015-07-23T04:31:03.000Z
comments: false
---


This tutorial will guide you through the process of building a website with Angular that utilizes a standard install of WordPress for blogging.  To put it more simply, following this tutorial will show you how you can build a custom site using all the cool things Angular provides while also using WordPress to take care of all the blogging stuff, albeit at a slightly different url (e.g. YOURDOMAIN.com/blog).

An example of a site with such a configuration would be [THIS SITE](http://sirhamy.com).  I have an Angular app that serves as my website on the base domain and a WordPress install sitting on my /blog/ route (you can check by looking at the address bar).  I wanted it this way so I could customize my site and landing pages as much as I wanted.  But I didn’t want to take the time to create a whole new blogging system when I had other projects I’d rather tackle.  So I’m using the Angular side as a custom portfolio of sorts for my projects and the blog to hold everything else I write.  Of course this isn’t the only use case for a setup such as this, just an example.

Let me know what other things you’ve been using this guide for in the comments below.

- - - - - -

**Installing WordPress**

The first thing we need to do is install WordPress.  Once we do this, we’ll have something we can build around and test against so we know if we break anything.

The exact steps for installing WordPress for your website vary depending on what hosting service you’re using (or if you’re hosting it yourself).  If you’re using BlueHost to host your site, I wrote [How to Install WordPress with BlueHost](http://www.sirhamy.com/blog/2015/07/guide-how-to-install-wordpress-with-bluehost/) to help guide you through the process.

If you’re using another hosting service, there are plenty of guides on the subject so Google around until you find one.  Most of the steps will probably be pretty similar so don’t worry too much if you can’t find one that uses the exact service you’ve chosen.  Just look at a different guide and you should be able to figure out how to solve any differences.

The most important thing when installing WordPress is to ensure that your install is not on the base url.  For example, if your url was mydomain.com (this is the base url), you want your blog on mydomain.com/blog or some other sub directory.  *Most sites that have a blog separate from the main site put it under the /blog directory, but feel free to get creative if that doesn’t suit your tastes.*

By doing this we ensure that your Angular app has control over routes and it’s not clashing with the built-in WordPress code.  Which is the whole point here.

*Steps for this section*

- Install WordPress on your site - Make sure you don’t install it on the base URL, instead put it in a sub directory like /blog

**Building an Angular App**

*If you’re using BlueHost to create this site, checkout [How to Create a Custom Site Using BlueHost](http://www.sirhamy.com/blog/2015/07/guide-how-to-create-a-custom-site-using-bluehost/) to help you get setup with an FTP client and configure your user settings so you can start uploading your files to the server hosting your domain.  *

From here on, I’m going to assume that you already know how to upload files to your server.

You don’t need to do anything special to accommodate the blog.  In fact, if you just create a normal Angular app, you should be good to go.  Here’s a template you can use:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/478a20ea8616622a64e4.json"></div>[https://gist.github.com/SIRHAMY/478a20ea8616622a64e4]

Using this as-is requires you to create a MainCtrl controller and an html file called main.html in a /views directory.  You’ll also need to add the [angular.min.js file](https://angularjs.org/) to a /lib directory.  If you remove the routing, however, you should be able to get by with just the Angular file.

Because Angular employs the pound/hash (#) sign for routing, you don’t have to worry about putting any exceptions in your routeProvider to allow access to the blog.  People will be able to access it as normal, by inputting the URL.

- - - - - -

 



