---
title: "Splunk: Convert a Simple XML Dashboard to Advanced XML"
date: 2014-06-04T09:28:01.000Z
date_updated:   2014-06-04T09:28:01.000Z
comments: false
---


**Problem: **I have a simple XML dashboard that I want to convert to advanced XML, but there aren’t any buttons to do so.

**Solution: **The solution is easy, albeit not quite straight forward.

1. First open the dashboard you’ve created in a browser (go to its url).
2. Append “?showsource=true” to the end of the URL (leave out the quotation marks).
3. Scroll to the bottom of the resulting page and copy the XML found there.
4. Create a new view and paste that XML there.
5. Check to ensure that both the new view and old view look and function the same.



