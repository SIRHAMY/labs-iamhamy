---
title: "Amazon Web Services: Allow Access to MYSQL RDBS Instance from Elastic Beanstalk App Server"
date: 2015-04-30T00:39:11.000Z
date_updated:   2015-04-30T00:39:11.000Z
comments: false
---


*This may work for other RDB instances as well as non-relational DB instances, but I haven’t tried it.  Best of luck!*

**Problem: **I have an app running on an AWS Elastic Beanstalk app server, but it doesn’t seem to have access to the MySQL DB I’ve created.  I know this because I can access a file on the app server that doesn’t require access to the DB, but whenever I try to access something that requires DB access, the app seems to hang.

**Solution: **All of this has to do with VPC access rights between the security group your app server is in and the security group your RDBS is a member of.  To allow access to your DB instance from your app server, follow these steps:

1. Navigate to your EC2 Dashboard.
2. Scroll down on the left pane to Security Groups, located inside of Network and Security.
3. Once there, copy the Group ID of the security group that your app server is part of.  This assumes that your account has both of these security groups.  If the app server security group is managed by another account, you will need to get this from them.
4. Select the security group of your RDB.
5. Click the Inbound tab
6. Click Edit then Add Rule
7. Specify the Type of connection, protocol, and port range you’d like to allow.
8. Now put the security group name you got earlier into the source field.
9. Save.  You should now be able to access your RDB from the security group you specified.



