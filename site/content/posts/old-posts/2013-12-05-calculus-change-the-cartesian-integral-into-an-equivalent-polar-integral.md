---
title: "Calculus: Change the Cartesian Integral into an Equivalent Polar Integral"
date: 2013-12-05T18:27:38.000Z
date_updated:   2013-12-05T18:27:38.000Z
comments: false
---


**Change the Cartesian Integral into an equivalent polar integral then evaluate the polar integral.**

Given Cartesian Integral: ∫0-1∫0-sqrt(1-y^2)  (x^2 + y^2) dxdy

Start by substituting x = rcosΘ and y = rsinΘ

Now find the polar limits of integration for the boundary of R (the area we’re trying to evaluate).  Start by sketching the region R and the bounding curve.

Find the R limits by drawing an imaginary ray from the origin through R.  Where the ray first enters R is the lower boundary and where the ray leaves R is the upper boundary.

Theta is found as the largest and smallest angle between the imaginary ray and the x-axis that the ray must make to encompass the entirety of region R.

Now replace dxdy with r drdΘ.  Note that this extra r will be multiplied by the original equation.

So, with 0 <= Θ <= pi/2 and 0<= r <= 1, we get the polar integral

∫0-pi/2∫0-1  ( (rcosΘ)^2 + (rsinΘ)^2 ) * r drdΘ

Simplifying this using the trigonometric identity sin(x)^2 + cos(x)^2 = 1, we get:

**Integral in Polar Form: **∫0-pi/2 ∫0-1  r^3 drdΘ

Evaluate the integral as follows:

∫0-pi/2 ( r^4)/4 |0-1 dΘ

∫0-pi/2  1/4  dΘ  ->  Θ/4 |0 – pi/2 = pi/8

**Answer: pi/8**



