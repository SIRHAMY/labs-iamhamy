---
title: "Splunk: Change Colors of a Chart with Multiple Inputs"
date: 2014-07-02T17:41:12.000Z
date_updated:   2014-07-02T17:41:12.000Z
comments: false
---


*For most of my Splunk endeavors, I use the Sideview Utils app to streamline my workflow.  If you don’t have Sideview Utils installed on your Splunk instance, that’s fine – although I’d highly recommend doing so.  The language in this post will be geared to Sideview users, although most solutions should still work (e.g. instead of creating a child, nest your module in advanced XML).*

**Problem: **I have a chart with multiple inputs and I want to be able to choose what color each series is.

**Solution:**

First of all, a series is defined as follows:

> Also referred to as a data series, this is a sequence of related data points that are plotted in a chart. For example you could have a chart that shows the sum of kilobytes processed by different client ip addresses over a given timerange. That is a series; the chart could be expressed as a column chart where each column shows the kb sum for a particular ip address.

Essentially, it is the defining attribute between the inputs.  For example, if I had two categories called “Normal Response” and “Slow Response”, each series would consist of a category (maybe the other way around).  Bear this in mind, as you will be attaching the desired colors to a series.

First of all, I’m going to assume you already have a search with a chart as a child (or nested within it).  Create a HiddenChartFormatter module as a child of the search and as a parent of the chart.  Add the parameters “charting.legends.labels” to identify which series you are targeting and “charting.seriesColors” to specify the colors you would like each series to be.

**Example:**

Back to the example I made above, if you have two categories (“Normal Response” and “Slow Response”) going into a chart, you’d put the following code inside the HiddenChartFormatter to make them green and blue respectively:

[“Normal Response”, “Slow Response (>3)][0x00CC23,0x2428CC]

Overall, your chart should look something like this:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/4da79cbeea0c6f86594d.json"></div>(https://gist.github.com/SIRHAMY/4da79cbeea0c6f86594d)



