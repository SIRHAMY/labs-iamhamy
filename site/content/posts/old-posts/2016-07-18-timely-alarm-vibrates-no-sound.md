---
title: "Timely: Alarm Vibrates - No Sound"
date: 2016-07-18T14:54:21.000Z
date_updated:   2016-07-18T14:54:21.000Z
comments: false
---


**Problem: **I use the Timely app as my everyday alarm, but the past few days I’ve slept through it. When I set an alarm to test it out, it vibrated instead of its usual melody. How do I get Timely to have an audible alarm sound again?

**Solution: **The most likely cause of this symptom is that your alarm volume is turned off. To turn on your alarm volume:

- Open the Timely app
- Click the menu button in the lower right corner (three stacked dots)
- Select Settings
- Select Alarm Volume
- Slide the slider somewhere to the right (anywhere not on the far left is fine)

To be sure that this remedied the problem, try setting a test alarm and checking if the sound is loud enough.

 



