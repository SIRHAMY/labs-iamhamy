---
title: "NPM: Run Node Module from Project without Installing Globally"
date: 2016-08-29T13:30:22.000Z
date_updated:   2016-08-29T13:30:22.000Z
comments: false
---


**Problem: **I’m using NPM as my package manager and have all my project’s dependencies installed via NPM. This means that they’re all sitting in the node_modules directory. However, whenever I try to run them, it says the command is not found. I know I can solve this by installing the package globally, but I don’t want to do that and the modules are already within my project. How can I just run them from the project node_modules directory?

**Solution: **NPM uses the package.json file to store customizable scripts that you can run.

Let’s say I have Webpack installed in my node_modules directory and I want to run it, but I don’t have it installed on my local machine.

- In my package.json, I declare the script “build”: “webpack” so my scripts object looks like: - > “scripts”: {  
>  “start”: “node app.js”,  
>  “build”: “webpack”  
>  },
- From my terminal, I can run `npm run build`. This will run my build script, pulling from the node_modules directory.



