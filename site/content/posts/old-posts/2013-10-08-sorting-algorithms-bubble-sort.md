---
title: "Sorting Algorithms: Bubble Sort"
date: 2013-10-09T02:04:23.000Z
date_updated:   2013-10-09T02:04:23.000Z
comments: false
---


**Bubble Sort**

Bubble sort moves through the data set, comparing each pair of adjacent elements.  If they are in the wrong order, it swaps them.  It iterates through the data “bubbling” the largest piece of data to the top.

****In-Place: yes  
 Online: No  
 Stable: Yes  
 Big O: Best O(n), Worst O(n^2)

Example:

<span style="text-decoration:underline;">5 3</span> 13 7 9 2 6  
 3 <span style="text-decoration:underline;">5 13</span> 7 9 2 6  
 3 5 <span style="text-decoration:underline;">13 7</span> 9 2 6  
 3 5 7 <span style="text-decoration:underline;">13 9</span> 2 6  
 3 5 7 9 <span style="text-decoration:underline;">13 2</span> 6  
 3 5 7 9 2 <span style="text-decoration:underline;">13 6</span>  
<span style="text-decoration:underline;">3 5</span> 7 9 2 6 | 13  
 3 <span style="text-decoration:underline;">5 7</span> 9 2 6 | 13  
 3 5 <span style="text-decoration:underline;">7 2</span> 9 6 | 13  
 3 5 2<span style="text-decoration:underline;"> 7 9</span> 6 | 13  
 3 5 2 7 <span style="text-decoration:underline;">9 6</span> | 13  
<span style="text-decoration:underline;">3 5</span> 2 7 6 | 9 13  
 3 <span style="text-decoration:underline;">5 2</span> 7 6 | 9 13  
 3 2 <span style="text-decoration:underline;">5 7</span> 6 | 9 13  
 3 2 5<span style="text-decoration:underline;"> 7 6</span> | 9 13  
<span style="text-decoration:underline;">3 2</span> 5 6 | 7 9 13  
 2 <span style="text-decoration:underline;">3 5</span> 6 | 7 9 13  
 2 3 <span style="text-decoration:underline;">5 6</span> | 7 9 13  
<span style="text-decoration:underline;">2 3</span> 5 | 6 7 9 13  
 2 <span style="text-decoration:underline;">3 5</span> | 6 7 9 13  
<span style="text-decoration:underline;">2 3</span> | 5 6 7 9 13  
 2 3 5 6 7 9 13



