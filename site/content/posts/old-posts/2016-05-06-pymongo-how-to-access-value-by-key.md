---
title: "PyMongo: How to Access Value by Key"
date: 2016-05-06T10:55:20.000Z
date_updated:   2016-05-06T10:55:20.000Z
comments: false
---


**Problem: **I’m trying to access a value by its key, but I can’t figure out how to do it in pymongo.

**Solution: **

To start with, you need to get a cursor to the piece of data you want. In this example, I’m going to assume your db only has one entry, so you’ll have to figure out how to sort the data yourself.

> example = db.collection.find()  
>  example[0][‘_id’]

Here, the first call gets a cursor object into the collection that fits the search params. I gave no search params, so it returned everything in the collection.

I then index into that cursor with the first [] param, giving me that ‘row’. Once I have that row, I can index into that using the key for the given data slot (like a column)



