---
title: "DotNet Core: Tuple could not be found"
date: 2019-03-20T21:58:10-04:00
tags: [ "dotnet", "core", "tuple", "tutorial" ]
comments: false
---

# Problem

I'm trying to use a Tuple in my dotnet core project but it keeps giving me the error `The type of namespace name 'Tuple<,>' could not be found (are you missing a using directive or an assembly reference?)`.

# Solution

In DotNet core, particularly if you're not using enterprise-grade tools like Visual Studio, using some of the core libraries takes extra steps.

First off, you need to install the `System.ValueTuple` package in your project so that you can actually use [Tuple](https://docs.microsoft.com/en-us/dotnet/csharp/tuples). You can do this via commandline with:

```
dotnet add package System.ValueTuple
```

Now that it's added in your project, you can use it in your C# file by adding to the top of your file:

```
using System;
...

namespace MyNamespace {
    ...
}
```

Note that we don't pull in `System.ValueTuple` cause that's a type, not the namespace that contains the type we want.