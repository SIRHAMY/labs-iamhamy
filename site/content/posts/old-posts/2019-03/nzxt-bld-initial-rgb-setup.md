---
title: "NZXT BLD: Controlling and customizing RGB LED lights and patterns"
date: 2019-03-18T00:00:12-05:00
tags: [ "nzxt-bld", "rgb", "led" ]
comments: false
---

# Problem

I just got my new NZXT BLD build in and it came equipped with the Hue Light 2 upgrade. However, I'm not sure how to program these rgbs to do anything other than how they came. How can I customize them?

# Solution

Depending on your setup, the answer to this question could differ quite a bit. That being said, the first thing you should do is load up the pre-installed [CAM software](https://www.nzxt.com/camapp) which provides pretty robust monitoring for your computer as well as an easy-to-use LED controller.

With this, you'll be able to modify things like which lights turn which colors and how each should behave (e.g. should they just stay a certain color or should they blink / change).

# Troubleshooting

## CAM doesn't control all of my lights with ASRock motherboard

CAM should get you pretty far but it's definitely possible it won't go the whole way. In my case, the lights directly installed to my Asrock motherboard and DRAM LEDs weren't recognized by CAM so I had to go another route to control them.

To solve this, I went and downloaded the official [Asrock Polychrome Sync](https://www.asrock.com/microsite/PolyChromeRGB/) application which has the ability to control these lights that CAM wasn't able to.

That being said, I'm not that knowledgeable in this space so there may be other, better options. If you are using a different motherboard entirely, it's also possible this won't work for you and you'll have to find a more fitting app.