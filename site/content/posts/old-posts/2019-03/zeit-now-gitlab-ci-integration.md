---
title: "Zeit Now: Gitlab CI Integration"
date: 2019-03-12T21:22:51-04:00
tags: [ "now", "gitlab", "ci", "cd", "serverless", "git", "tutorial" ]
comments: false
---

![willigetcancer deployment paradigm](/projects/willigetcancer/willigetcancer-zeitnow-deployment-paradigm.png)

For the past month or so, I've been working on a side project called [Will I get cancer?](/projects/willigetcancer) that, well, gives you info on your odds of getting cancer. When deciding what tools to use, I wanted to try out Serverless cause although [I've built sites on my own servers](/projects/iamhamy) before, it was a lot of work and I wanted to see if I could do it faster and cheaper cuse #optimization.

For this task, I chose the [Zeit Now](https://zeit.co/now) platform because it had a pretty good free tier and was in some of the [NextJS](https://nextjs.org/) docs and proved to be simple to use. I started manually deploying my project to Now and then aliasing it to my url but that soon became annoying so I decided to implement CI/CD to help automate my pipeline.

I switched over to using GitLab as my primary repo platform a few months ago and had already built out deploy pipelines leveraging [GitLab CI](https://about.gitlab.com/product/continuous-integration/) so it was a natural choice for me to 1) build on a platform that worked and 2) that I had strong domain knowledge of.

So that's where I'm coming from and here I'll detail how to hook up your GitLab repo so that it deploys your project to Now and then aliases it to your custom domain.

# GitLab CI and the Now CLI

Now before we can get into exactly how we do the integration, we need to go over a few pre-reqs about the individual tools we'll be using so we know what's going on in the final integration.

## GitLab CI

GitLab CI is a process built into the GitLab ecosystem that kicks off customizable build / deploy processes. 

Basically GitLab CI kicks off on commit, looks for a top-level file in the repo called `.gitlab-ci.yml`, and, if found, parses it to decide what it should do. This is where the CI customization comes in. If that file is found and in a proper format, GitLab will spin up a container image of your choice (from among some approved containers) and provide an interface to an in-container shell from which you can run arbitrary commands.

Through this simple functionality, you can actually get a ton of stuff done. Take for instance commands you might run from your shell locally that are now automatable via GitLab:

* `npm install`
* `git build`
* `docker run my-test-container`
* `now // this is the one  we really care about for this tutorial`

You can read more about this in [GitLab's official CI/CD docs](https://docs.gitlab.com/ee/ci/introduction/index.html).

## The Now cli

* what is now cli
* how to use it
* how to set up with your custom domain

The [now cli](https://zeit.co/docs/) provides an ergonomic wrapper around the now apis. For our purposes, we really only need to do two things 1) deploy our project to the now servers and 2) alias our domain to the most recent now deploy.

Running locally, we can do this with two now commands:

```
cd myProjectDirectory
now # this does the deploy
now alias mysupercoolsite.tld # this aliases your custom domain to the last deploy
```

_Read more about [how to set up a custom domain with now](https://zeit.co/docs/v2/domains-and-aliases/adding-a-domain)._

# GitLab CI integration with Now

Now that we know a little bit about the individual tools we'll be using, we can go on to the integration.

To do so, I'm going to just plop my existing GitLab CI script from [Will I get cancer?](/projects/willigetcancer) as-is and talk through what each of these things do.

```
# in GitLab repo .gitlab-ci.yml

# 1
image: node

# 2
stages:
  - deploy

# 3
build_and_deploy:
  stage: deploy
  script:
    - cd web # navigate to source directory
    - npm install
    - npm install -g --unsafe-perm now
    - now --token=$NOW_TOKEN
    - now alias willigetcancer.xyz --token=$NOW_TOKEN
  only:
    - master
```

_code pulled from [SIRHAMY/will-i-get-cancer](https://gitlab.com/SIRHAMY/will-i-get-cancer/blob/master/.gitlab-ci.yml), 2019.03.10_

* `image: node`

At the beginning of every GitLab CI file, we must tell it what base container image we want it to run using. It pulls from Docker Hub so if the image you want to use in your pipeline exists there, you shouldn't have a problem. Here I'm saying I want to use the official node image because I'm using npm for package management and want it all pre-installed/configured.

* stages

This is likely exactly what you've intuited. Here you say the stages taht your pipeline entails and the ordering in which you'd like them run. Later on when we get down to specific steps, you'll notice that we mark which stage the given step should run in and it'll run during that.

* steps

Here I've created a step called `build_and_deploy` and configured it inside my `deploy` step. What it does is navigates to the folder in which my source files are located, install all dependencies, install the `now` executable, run now using a token I created in now and [configured as a variable in GitLab CI](https://docs.gitlab.com/ee/ci/variables/), then aliased my new deploy with my custom domain.

At the bottom, I configure the deploy step to only happen when the master branch was updated to keep random feature branches from leaking out to prod.

# Fin

That's it! This post got a little wordy but hopefully it helps you build your own integration with Now / GitLab. Happy to answer any questions if you've got them!

If you want more content like this, I write about stuff I'm building pretty regularly on [HAMY.LABS](/) and you can [get updates via email by subscribing](https://hamy.xyz/subscribe).