---
title: "Asrock: Enable AMD-V virtualization on Windows 10"
date: 2019-03-11T23:50:53-05:00
tags: [ "bios", "asrock", "amd-v", "windows-10", "virtualbox" ]
comments: false
---

# Problem

I'm trying to spin up an Ubuntu machine with Virtual Box but it's throwing an error `AMD-V is disabled in the BIOS. (VERR_SVM_DISABLED)`, how do I fix this?

# Solution

1. First turn off your computer. We'll need to access the BIOS menus during restart.

2. Restart your computer

3. During boot you should see a prompt prompting you to hit `F2` or `DEL` to access configuration controls. Hit one of those a few times until you see the configuration settings. If you don't see these then you need to re-do 1-3 until you do. Note that it's really easy to miss this window so it's not that weird to have to retry it a few times.

4. Navigate to `Advanced/CPU Configuration`

5. Change `SVM Mode` to enabled

6. Go to `Exit` and hit `Save Changes and Exit`

Once Windows boots again, your BIOS setting should be reflected and VirtualBox should start up!