---
title: "Install Hugo on Windows Subsystem for Linux (WSL)"
date: 2019-03-04T22:20:58-05:00
tags: [ "hugo", "wsl" ]
comments: false
---

# Problem

I'm trying to install Hugo on Windows Subsystem for Linux (WSL) in order to use it for one of my sites hosted in my Windows user's Dropbox folder at WSL path `/mnt/c/Users/myuser/Dropbox`. How do I install Hugo such that it's available in that path?

# Solution

First off, I'd advise checking [the official Hugo install docs](https://gohugo.io/getting-started/installing) to see if any of those methods work for you. Those will almost certainly be better maintained than the answer I'm about to provide here. That being said, I did try to follow those docs for my own WSL installation of Hugo and found them to be lacking.

So if you tried to do the same without much success, then read on brave adventurer!

First of all, some context. The problem with many of the guides in the official docs is that they encourage downloading your executable to a local directory controlled by your user. In most cases, this makes sense. It gives your user control over your executable with protections against others messing with it or for your executable to be able to touch things outside of your bounds.

However, with WSL, the idea of user boundaries kinda breaks down as, if you're trying to modify folders in your Windows user's domain, you're already going to be outside of your WSL user's domain and thus will need to install all executables in a place where they can be accessed globally.

As such, the method in which we'll be installing hugo will allow your wsl command prompt to access the install regardless of where you are in the file structure. Alright, enough chit-chat.

From here on out, all directories will be within the WSL prompt.

Target directory: `/usr/local/bin`

1. Go to https://github.com/gohugoio/hugo/releases to see the list of latest releases and find the one for your target WSL install.

I chose Ubuntu for my WSL install so I picked `hugo_0.54.0_Linux-64bit.tar.gz`

2. Download that release. This will put the download in your Windows user's `Downloads/` folder. Note that, to access this folder via WSL, you will need to go to `/mnt/c/Users/YOURUSERHERE/Downloads/`

3. Extract the tarball by using `tar -xvzf /mnt/c/Users/YOURUSERHERE/Downloads/HUGOTARBALLNAME /usr/local/bin`. Note that you may need to prepend `sudo` to the beginning of that command to elevate to the appropriate permissions to access the `/usr/local/bin` directory.

After that, Hugo should be installed. To verify, try running `hugo version` from within WSL.

Happy coding!