---
title: "Windows Subsystem for Linux: Run Windows DotNet Core install from WSL"
date: 2019-03-25T13:04:01-05:00
tags: [ "wsl", "windows", "linux", "cli", "dotnet" ]
comments: false
---

# Problem

I installed .NET Core for Windows but am primarily using Windows Subsystem for Linux for development. It's annoying to have to have two different command lines open (one for Linux commands via WSL and one for Windows commands) just so I can run everything on my machine. How can I run the .NET Core Windows install from WSL?

# Solution

To run Windows tools from WSL, use `[binaryName].exe` syntax (see: https://docs.microsoft.com/en-us/windows/wsl/interop).

So for the command `dotnet` via Windows command line, you can run it in WSL with `dotnet.exe`