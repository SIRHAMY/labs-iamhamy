---
title: "Calculus: Find the Local Maxima, Local Minima, and Saddle Points"
date: 2013-11-06T13:03:43.000Z
date_updated:   2013-11-06T13:03:43.000Z
comments: false
---


**Find the local maxima, local minima, and saddle points of f(x,y) = x^3 + 3xy + y^3**

Start by finding the partial derivative fx and fy

fx = 3x^2 + 3y

fy = 3x + 3y^2

We can tell that both functions exist everywhere because at no (x,y) combination does the function not exist (should speak for itself).

Because bot functions exist everywhere, local extremes can only occur where:

fx = 3x^2 + 3y = 0 and fy = 3x + 3y^2 = 0

Possibilities: (0,0), (-1,-1)

Using the [Second Derivative Test for Local Extrema Values](http://hamycodes.wordpress.com/2013/10/07/calculus-second-derivative-test-for-local-extreme-values/ "Calculus: Second Derivative Test for Local Extreme Values"), we can determine what kind of critical points the found possibilities are.

fxx = 6x, fyy = 6y, fxy^2 = 3^2 = 9

@(0,0): fxxfyy – fxy^2 = 27 > 0 and fxx = -6 < 0

@(-1.-1): fxxfyy – fxy^2 = -9 < 0

**Solution: Saddle point at (0,0) with a value of 0  
**

**Local mxima at (-1,-1) with a value of 1**



