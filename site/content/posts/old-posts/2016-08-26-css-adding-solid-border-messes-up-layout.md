---
title: "CSS: Adding Solid Border Messes up Layout"
date: 2016-08-26T11:38:35.000Z
date_updated:   2016-08-26T11:38:35.000Z
comments: false
---


**Problem: **I want to create a border on the top bar of my site, but when I add it my whole column layout gets messed up. Why is this and how can I fix it?

**Solution: **Most likely, the problem is that your media queries aren’t accounting for the extra spaces the borders are taking up. If you want to have a border and not worry about the extra pixels, try using outline:

`outline: 1px solid black;`

**Source:**

- ****************[Why adding line border to a css messes up its correct displacement? [duplicate]](http://stackoverflow.com/questions/22233881/why-adding-line-border-to-a-css-messes-up-its-correct-displacement)  

#



