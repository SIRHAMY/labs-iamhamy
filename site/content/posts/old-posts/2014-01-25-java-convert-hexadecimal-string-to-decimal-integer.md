---
title: "Java: Convert Hexadecimal String to Decimal Integer"
date: 2014-01-25T21:33:58.000Z
date_updated:   2014-01-25T21:33:58.000Z
comments: false
---


**Objective: **Convert a string of hexadecimal ASCII characters to decimal integer.

My code:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/8627320.json"></div>(https://gist.github.com/SIRHAMY/8627320)

**Logic: **Go through the string, determining what value to subtract from the character’s value.  After reducing the character to its int value, determine the scalar to multiply it by to convert to decimal.  Add this number to the final value.



