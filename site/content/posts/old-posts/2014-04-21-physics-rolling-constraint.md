---
title: "Physics: Rolling Constraint"
date: 2014-04-21T19:03:42.000Z
date_updated:   2014-04-21T19:03:42.000Z
comments: false
---


The **Rolling Constraint** is the basic link between translation and rotation for objects that roll without slipping (*According to Knight, Physics for Scientists and Engineers)*

V<sub>cm</sub> = Rw = (2Pi/T)R

where V = velocity of the object’s center of mass (cm), R = radius, w = angular velocity, and T = period of rotation



