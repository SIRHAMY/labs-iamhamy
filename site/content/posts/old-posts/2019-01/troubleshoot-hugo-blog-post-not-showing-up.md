---
title: "Troubleshoot: Hugo blog post / content not showing up"
date: 2019-01-28T22:24:13-05:00
tags: [ "troubleshoot", "guide", "hugo", "blog", "static-site", "web-site", "front-matter" ]
comments: false
---

# Problem

So I've been running my [Hugo](https://gohugo.io/) blog(s) for a while now and during one particular deployment, I noticed that one of my posts wasn't appearing on my domain. Eventually I figured out that it was caused by my date being some time in the future and thus Hugo didn't build it into the resulting `public/` site.

This post serves as a checklist of common areas to look when your post isn't appearing when (you believe) it should.

# Solution

Before we get into anything specific, we're assuming that you have a Hugo blog up and in working condition. I gauge a "working" Hugo blog as one in which you've already created at least one piece of custom content in the given content section and it appears as you'd expect it to. If this condition is not met, then there are tons of other configuration issues that could be the cause of your post not showing up and the rest of this guide will likely not help you.

If your blog _is_ in working order outside of this pesky piece of content, then continue on and hopefully squash the bug.

## Test locally with hugo serve

Before you do anything, you should first load your site locally to determine whether the culprit is more likely in your blog's code or your deployment infrastructure. You can do this quickly by navigating to your site's directory and running `hugo serve` via command line.

Navigate to the `localhost:PORT` that hugo bound itself to via browser and see if your post is there.

If your post is there, then it's either a deploy problem or you fell into a local/production behavior pitfall.

If it's not there, then it's likely your post's configuration is wrong.

In either case, it'd probably be helpful to continue on, but keep this behavior in mind as we go through the steps to help pinpoint likely causes.

## Check the date

Hugo, by default, will only build content with a [Front Matter](https://gohugo.io/content-management/front-matter/) (metadata) key of date that is in the past. So the first thing that you should do is check that the date on the target content is sometime in the past.

Note that whether Hugo decides to build the content or not is solely driven by the clock it has available to it, so if your build machine's clock is off that could cause some unexpected behavior in the site generation process.

## Check the draft

Hugo supports a Front Matter key of `draft` which tells the generator that you don't want the piece of content to end up in the final build outputs. If you have `draft` set to true, then your piece of content won't end up in the generated output.

If you saw your piece of content when you served locally then this was probably not the cause, but worth a check anyway.

## Make sure the directory makes sense

Hugo offers a fair amount of flexibility wrt where you put your content but is very strict based on those rules. If you already have content working, make sure that your troublesome content is located in a directory that mirrors that of the working content. If not, try moving it there to see if that causes it to show up and work backwards from there to try and pinpoint the config mismatch.

## Redeploy

If everything looks good locally and you just can't figure out why it's not showing up in your remote, published environment, try kicking off the build pipeline again. This is the Hugo equivalent of turning your computer on/off again but we keep doing it cause sometimes it just works.
/shrug

# fin

Did this help? Have other ways to troubleshoot missing content? Let me know here [or otherwise](https://hamy.xyz/connect)

-HAMY.OUT