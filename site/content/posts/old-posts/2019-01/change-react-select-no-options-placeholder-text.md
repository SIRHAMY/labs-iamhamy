---
title: "React Select: how to change 'No options' placeholder text"
date: 2019-01-14T13:16:22-05:00
tags: [ "tutorial", "react", "react-select", "tutorial", "javascript" ]
comments: false
---

<blockquote class="imgur-embed-pub" lang="en" data-id="a/ppYNjYu" data-context="false"><a href="//imgur.com/ppYNjYu"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

# Problem

I’m using [React Select](https://github.com/JedWatson/react-select) but I’d like to change the text that appears ("No options") when I don’t have any options in the selector. How do I do this?

# Solution

The React Select component [has a prop](https://github.com/JedWatson/react-select) named `noOptionsMessage` that allows you to pipe in a default value when the selector doesn’t have any options.

Usage is like:

```
<AsyncSelect
      ...
      noOptionsMessage{() => resx.MyComponent_NoOptionsMessage} />
```

This is useful if you require custom resx (i8n strings) or even if your selector is performing operations under the hood that make the default not quite right. For instance, in one of our builds we only show options once the user has started typing as a way of naively limiting the returned results. As such “no options” isn’t really right because there could certainly be options that we just haven’t loaded yet. We instead changed it to something of the effect “no options loaded - start typing to see results” to be more transparent about what was happening under the hood.