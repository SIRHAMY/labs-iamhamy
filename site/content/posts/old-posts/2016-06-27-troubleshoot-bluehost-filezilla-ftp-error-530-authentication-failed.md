---
title: "Troubleshoot: Bluehost Filezilla FTP Error - 530 Authentication Failed"
date: 2016-06-27T10:31:18.000Z
date_updated:   2016-06-27T10:31:18.000Z
comments: false
---


**Problem: **I’m trying to connect to my Bluehost server using the Filezilla FTP client. However, whenever I input the configuration files as shown on the cPanel FTP page, the connection fails returning:

> Response: 530 Login authentication failed  
>  Error: Critical error: Could not connect to server

What am I doing wrong? How can I connect to my server via FTP using Filezilla?

**Solution: **There are many possible causes for this, but here are the most common factors you should look at:

In Filezilla’s Site Manager, make sure your configuration looks like this:

**Host: **YOURDOMAIN.com (assuming you have a .com domain)  
**Protocol: **FTP – File Transfer Protocol  
**Encryption: **Only use plain FTP (insecure) – by default Bluehost doesn’t allow for TLS. You can go into the advanced settings to set up ssh and sftp (more secure) connections, but for most this is fine.  
**Logon Type: **Normal  
**User: **USERNAME@YOURDOMAIN.com – For many connections, you may not need to add your host after your username. However, for some edge cases (e.g. if you’re hosting multiple domains under one Bluehost account), it makes things alot easier.  
**Password: **Whatever your password is

Most issues I’ve come across arose from improper username format and using an incompatible form of encryption to contact the server.



