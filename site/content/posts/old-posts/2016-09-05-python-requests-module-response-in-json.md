---
title: "Python: Requests Module Response in JSON"
date: 2016-09-05T15:36:34.000Z
date_updated:   2016-09-05T15:36:34.000Z
comments: false
---


**Problem: **I’m using the [Requests Module](https://requests.readthedocs.io/en/master/) for Python 3.5 to handle my HTTP get and post requests. I know I can get my response in JSON, but I’m having trouble accessing the keys on the response. When access a key that I know is in the response, it’s saying:

<span class="s1">`AttributeError: 'Response' object has no attribute 'keywords'`</span>

What am I doing wrong?

**Solution: **It looks like you’re trying to directly access the `requests` module’s `Response` object without first extracting the actual response JSON. The `Response` object has several built-in functions and parameters that you can use (refer to the documentation linked above for more about that).

To get to the actual JSON response from your request, first extract the JSON object out into a new variable using the module’s `.json()` method call then simply access that keyword within the created dictionary using `.get('key')`.

Here’s gist to get you started:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/20e6fc335191d0e45a7250786f3fe457.json"></div>[[https://gist.github.com/SIRHAMY/20e6fc335191d0e45a7250786f3fe457](https://gist.github.com/SIRHAMY/20e6fc335191d0e45a7250786f3fe457)]



