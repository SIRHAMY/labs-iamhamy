---
title: "Windows 10: How to Run Git Bash as Administrator"
date: 2016-03-07T21:05:26.000Z
date_updated:   2016-03-07T21:05:26.000Z
comments: false
---


**Problem: **I need to run Git Bash as admin as my NPM install won’t work otherwise. I know on Unix, I can usually just add sudo to the beginning, but how do I do something similar on Windows?

**Solution: **To run a Git Bash Terminal as Admin/root administrator:

- Go to your **Start menu**
- Search for Git Bash
- Right Click on the listing
- Select **“Run as Administrator”**
- Now just cd into your directory, and install away



