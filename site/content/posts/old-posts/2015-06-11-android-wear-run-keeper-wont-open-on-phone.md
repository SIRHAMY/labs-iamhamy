---
title: "Android Wear: Run Keeper won't Open on Phone"
date: 2015-06-11T21:26:32.000Z
date_updated:   2015-06-11T21:26:32.000Z
comments: false
---


**Problem:** When I select the option to open Run Keeper on my phone, nothing happens.

**Solution: **First of all, make sure that when you select that button your phone is unlocked.  This will allow your device to access your phone.  If you’ve already ensured your phone is unlocked, make sure the Run Keeper app is closed on that device.  For some reason, the app won’t open unless the watch/wearable is creating a new session of it.



