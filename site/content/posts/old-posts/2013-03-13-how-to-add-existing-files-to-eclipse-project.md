---
title: How to Add Existing Files to Eclipse Project
date: 2013-03-13T05:51:20.000Z
date_updated:   2013-03-13T05:51:20.000Z
comments: false
---


![](http://i1.wp.com/wwwbruegge.in.tum.de/lehrstuhl_1/images/teaching/eclipse.png?resize=175%2C175 "Eclipse")

**To add an existing file to a project in Eclipse:**

1. <span style="line-height:13px;">Select (cut, copy) files you wish to add to the project.</span>
2. Place them in the correct folder  
 -.java files go in the src folder  
 -.class files go in the bin folder  
 -Other files such as pictures can go in the project’s main folder
3. Go to File -> Refresh or press F5

The projects in the Package Explorer should be updated to show all files currently in each directory.  You can now access and save the files in the project, just as you would had you created the file yourself.



