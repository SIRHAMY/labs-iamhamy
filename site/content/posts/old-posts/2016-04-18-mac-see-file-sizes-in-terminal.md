---
title: "Mac: See File Sizes in Terminal"
date: 2016-04-18T19:30:55.000Z
date_updated:   2016-04-18T19:30:55.000Z
comments: false
---


**Problem: **I’ve got a terminal opened in a directory on my computer. I want to see a list of all the files in the directory along with their corresponding file sizes, in human-readable form. How can I do this?

![Screen Shot 2016-03-20 at 8.28.12 PM](http://i2.wp.com/www.sirhamy.com/blog/wp-content/uploads/2016/03/Screen-Shot-2016-03-20-at-8.28.12-PM.png?fit=548%2C364)

**Solution: **To see the file sizes of all the files in a given directory, simply type:

> du -h *

- du will list the memory consumption of the directory
- -h tells it to print out human-readable sizes, as in kilobytes, megabytes, gigabytes, etc.
- * Tells it you want to see all the individual items in the directory



