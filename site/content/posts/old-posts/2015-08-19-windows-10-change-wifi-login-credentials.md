---
title: "Windows 10: Change Wifi Login Credentials"
date: 2015-08-19T18:29:00.000Z
date_updated:   2015-08-19T18:29:00.000Z
comments: false
---


**How to Change your WiFi Login Credentials on a Device Running Windows 10**

**Problem: **I just changed my wifi login credentials, but my computer still remembers the old ones.  I need to change the credentials saved on my device.

**Solution: **

1. Open up the **Settings** program
2. Scroll down to “**Manage Wifi Settings**” and select it
3. Scroll down to “**Manage Known Networks**“.
4. Find the network you’d like to modify credentials for.  Select it then **hit “Forget”**
5. Go back to the previous screen and connect to the network using your new credentials



