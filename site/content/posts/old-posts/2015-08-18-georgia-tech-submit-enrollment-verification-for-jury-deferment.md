---
title: "Georgia Tech: Submit Enrollment Verification for Jury Deferment"
date: 2015-08-18T18:28:55.000Z
date_updated:   2015-08-18T18:28:55.000Z
comments: false
---


**Problem: **I go to the Georgia Institute of Technology and I need to send a verification of my full-time student status to my county courthouse in order to defer a current jury duty summons.

**Solution: **

*I do not guarantee that these steps will work for you.  This is simply a documentation of my own experience.  Follow them at your own risk.  *

Most of these instructions are available on the [Registrar’s website](http://www.registrar.gatech.edu/students/formlanding/enrollverification.php), but I’ve changed a few things to reflect how I did it.  Before you follow these steps, you should first verify that all you need to send them is an Enrollment Verification form.  You should also find out where you can fax this form to as this is where we’ll tell the Registrar’s office to send it.  I called the courthouse, but you might be able to find a fax number on the jury summons.  *They don’t currently have an option for email, so that information is useless unless you pick up the form in person and send it yourself.*

1. Log into Buzzport
2. Under “Registration and Student Services” click Registration – Oscar
3. Click Student Services & Financial Aid -> Student Records -> Verification Requests
4. At the bottom of the page there should be a form title “Order a Verification Below”.  Fill out this form then click submit.
5. Fill out the next form that appears and use the fax number given to you by your county courthouse.  It won’t let you input multiple destination addresses, so just fill out the fax section.  Submit.
6. You should be set.



