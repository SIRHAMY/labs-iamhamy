---
title: "Troubleshoot Mac: Can't Find usr/local in Finder"
date: 2016-01-31T01:51:57.000Z
date_updated:   2016-01-31T01:51:57.000Z
comments: false
---


<span style="font-weight: bold;">Problem: </span>I’m installing software that says it’s located in my usr/local directory. I’m trying to access a file that was supposed to be installed with it, but I can’t find the usr folder anywhere. When I open Finder and search for it, it just returns a bunch of partial matches with files in my Desktop or Documents. Where are these lower level folders located?

<span style="font-weight: bold;">Solution: </span>It appears that Macs want to hide any files/directories by default from access via Finder. Coming from Windows, this is a bit frustrating when you’re used to being able to see everything on your machine in an Explorer window. This isn’t to say that you can’t access these files at all, just that you’ll have to get your hands dirty with some terminal commands before you do.

To access the usr/local directory on your machine:

- Open a terminal. You can do this by opening Finder (ALT/OPTION + CMD + SPACE) and typing “terminal” in the search bar.
- Once in the terminal make sure you’re in the /Users/[YourUsername]/ directory. You can do this by typing “pwd” and hitting enter. If you aren’t, that’s okay – you might just have to run the next commands a few more times. The directory I gave you is a standard home directory, so most users will have tehir terminal open in that directory.
- To go up “one level” in your directory hierarchy, type “cd ..”. Because your usr/local directory is system-related, your machine has stored it in folders at a higher level than those you usually interact with when performing every-day activities.
- Type “cd ..” and hit enter. Now type “pwd” to see your current directory. Once your directory displays as “/”, you’ve hit the highest you can go and you should continue to the next step. Otherwise, iterate on this one.
- Type “ls” and hit enter to see the contents of the directory you’re currently in. You should see the usr folder listed.
- Type “cd usr” to enter the usr directory.
- Now type “cd local” to enter the usr/local directory

 

There you have it, you now have a terminal opened in the usr/local directory. The terminal is a powerful thing and can do a bunch of stuff, but that’s beyond the scope of this post. If you’re looking for ways to modify the usr/local directory or files therein, a Google search for the desired outcome should suit your needs.



