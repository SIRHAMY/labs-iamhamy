---
title: "Python: Convert Float in Scientific Notation to Int"
date: 2016-05-13T11:33:29.000Z
date_updated:   2016-05-13T11:33:29.000Z
comments: false
---


**Problem: **I’ve got a bunch of large integers stored in a DB (a bunch of Twitter IDs), but when I used Python to access them, it automatically converted them to scientific notation. How do I get them from scientific notation back to their original forms?

**Solution: **

In order to convert from the scientific notation, simply cast them to an integer.

> for scienceNum in scienceNums:  
>  print int(scienceNum)

This should convert the e+17, or whatever form it gives you, back to the original large integer.



