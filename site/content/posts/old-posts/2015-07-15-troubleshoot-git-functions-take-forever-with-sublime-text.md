---
title: "Troubleshoot: Git Functions Take Forever with Sublime Text"
date: 2015-07-15T20:37:43.000Z
date_updated:   2015-07-15T20:37:43.000Z
comments: false
---


**Problem: **Running git rebase -i and git checkout sometimes takes forever – as in never resolves/hangs/freezes – when I have a related file open in Sublime.

**Solution:**  The only solution I’ve found to this problem is simply closing Sublime and reopening it.  The act of closing the view, usually allows git to complete its operation.

Sometimes I can get away with just closing the tab rebase -i opened, but usually I have to close the entire window before it continues.

With git checkout, it’ll often hang after I submit the command.  In which case I’ll simply close the window and it will complete.

This problem seems to only occur on certain configurations and installs of git/Sublime as I have it running fine on another machine, although that machine is running a different version of Windows.  As such, these “solutions” are simply ways to fix the symptoms of the problem, but to fix it long-term you’ll probably have to reinstall or at the very least modify your installs of git and Sublime.



