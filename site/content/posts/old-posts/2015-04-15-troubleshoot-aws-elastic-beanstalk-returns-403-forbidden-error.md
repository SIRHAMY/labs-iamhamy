---
title: "Troubleshoot: AWS Elastic Beanstalk Returns 403 Forbidden Error"
date: 2015-04-15T22:11:00.000Z
date_updated:   2015-04-15T22:11:00.000Z
comments: false
---


**Problem: **When trying to access an application server hosted on AWS’ Elastic Beanstalk, you’re faced with a 403 error that looks something like this:

> Forbidden
> 
> You don’t have permission to access / on this server.

**Solution: **Many times this is caused by zipping your files in a nested folder.  In order to circumvent this, when zipping your files follow these instructions:

1. Navigate inside of the directory you want to zip, select all the files located there, then zip them into a folder.  This prevents a nested folder from being created inside the .zip.
2. Upload to your Elastic Beanstalk server and ensure it works.



