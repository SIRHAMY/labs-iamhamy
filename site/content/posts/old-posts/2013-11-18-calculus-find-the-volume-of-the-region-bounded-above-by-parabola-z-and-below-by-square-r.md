---
title: "Calculus: Find the Volume of the Region Bounded Above by Parabola Z and Below by Square R"
date: 2013-11-18T20:31:25.000Z
date_updated:   2013-11-18T20:31:25.000Z
comments: false
---


**Find the volume of the region bounded above by parabola Z and below by square R **

z = 16 – x^2 – y^2

R: 0 <= x <= 2,  0 <= y <= 2

Because this is a double integral over a general region problem, the equation is going to look like this:

∫R∫ 16 – x^2 – y^2 dA

Essentially,  we’re finding the integral of the region covered by parabola Z within the boundaries given by R.

Here we use the x, y bounds given in R to replace R in the above function.  For this problem, it doesn’t matter which order you choose to integrate, so long as the integration bounds match the integration variable (i.e. use the x bounds when integrating x).

∫0-2∫0-2   16 – x^2 – y^2 dxdy  =>  ∫0-2   16x – x^3 – xy^2|0-2  dy

Integrating x gives you:

∫0-2   88/3 – 2y^2 dy  =>  88y/3 + (-2y^3)/3 |0-2

Complete the double integral, you should get the answer:

**Answer: 160/3**



