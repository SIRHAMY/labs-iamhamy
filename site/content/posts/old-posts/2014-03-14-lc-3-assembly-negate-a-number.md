---
title: "LC-3 Assembly: Negate a Number"
date: 2014-03-15T01:41:44.000Z
date_updated:   2014-03-15T01:41:44.000Z
comments: false
---


**Problem: **Given a number in assembly, change its sign

Essentially, what we have to do is take a number and return the same number with the opposite sign (positive becomes negative and vice versa).

This is what my code looks like:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="anonymous/9502128.json"></div>First off, we set the registers to 0 then load R1 with the given number.  Next we figure out if the given number is positive or negative.  If it’s positive, we move to the POSLOOP in which we decrement R2.  If not, R2 is incremented.

We then store the sum of the given number and R2 in R3.  If that sum is equal to zero, then we know we have negated the number and we store that number in the address location of ANSWER.  If it’s not zero, then we continue to iterate through the given loop until it is.



