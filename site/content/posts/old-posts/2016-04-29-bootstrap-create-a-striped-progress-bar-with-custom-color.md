---
title: "Bootstrap: Create a Striped Progress Bar with Custom Color"
date: 2016-04-29T22:47:06.000Z
date_updated:   2016-04-29T22:47:06.000Z
comments: false
---


**Problem: **I want to create a progress bar with stripes of the color of my choice. How can I create a custom striped progress bar with Twitter Bootstrap?

**Solution:**

You can see my finished solution on [CodePen](http://codepen.io/SIRHAMY/pen/zqwEwv). Continue below for a walk-through of how everything works.

Here’s the Gist:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/3081d3e3920f46ad4c27.json"></div>[[https://gist.github.com/SIRHAMY/3081d3e3920f46ad4c27](https://gist.github.com/SIRHAMY/3081d3e3920f46ad4c27)]

**HTML**

In the HTML, you can see we’re using the built-in Bootstrap class “progress-bar”. We must also include the role=”progressbar” if we want it to function properly.

The “progress-bar-custom” and “progress-bar-striped-custom” classes are of course to link our progress bar with the custom CSS classes we created. A little more on that later.

For the display of the progress bar itself, we input aria-valuemin and aria-valuemax to declare the minimum and maximum values that the bar will be displaying.

Notice that we’ve put the width style equal to the aria-valuenow style to declare the current state of the progress bar – or how much we want filled in.

Lastly, you can see how the text “Progress Bar Text” is displayed on top of the progressbar.

**CSS**

To add custom colors and stripes, we created two CSS classes: “progress-bar-custom” and “progress-bar-striped-custom”.

*progress-bar-custom*

This class sets the color of the progress bar – in this case blue with value #497ad6

*progress-bar-striped-custom*

This class creates the pinkish striped pattern you see on top of the regular blue. Here, we’re setting a linear gradient as the background image for the progress bar, declaring which parts are transparent and which parts are colored, and tilting it -45 degrees.

Compare this with the standard bootstrap implementation and you can see that our custom stripes are going the opposite way of bootstrap’s and that the stripes appear to be larger. Well, that’s because they are.

I tilted the stripes in the opposite direction to show that it was possible. You could even change the angle at which the stripes are presented, although I’ve found 45 degrees to work best in most situations. To tilt the custom stripes like the standard implementation, simply remove the negative sign in front of the 45.

As for stripe size, you can change that by modifying the values of background-size. The larger they are, the larger your stripes will be. I’ve experimented with having the two values different sizes, but I’ve found that it works best if you input the same value twice.

As for the linear-gradient and the color itself, don’t worry too much about it. A simple copy and paste should do the trick – just be sure to change the color to the one you want.

Feel free to mess around with the Codepen or even fork it if you’re so inclined.

Cheers,  
 SIRHAMY



