---
title: Download Source Files from Read-Only Google Code Project
date: 2013-11-18T10:23:35.000Z
date_updated:   2013-11-18T10:23:35.000Z
comments: false
---


I was recently tasked with creating and executing a UI evaluation on someone’s coding project.  They hosted the project on Google Code and sent me the link to the Project Page.  the problem is that I wasn’t added as a contributor, so the only option available to me for SVN checkout was a read-only checkout.  While usually not a problem, SubClipse (my Eclipse SVN plugin of choice) doesn’t digest it very well.

There are a few simple solutions:

- Change the URL that normally looks something like this:***  http***://PROJECT.googlecode.com/svn/trunk/ PROJECT-read-only  to instead read like this ***http***://PROJECT.googlecode.com/svn/trunk/.  For the most part, this bypasses any difficulties your SVN may have pulling the files.  It won’t allow you to push to their repository, but you’ll at least be able to pull down their project.
- Use [DownloadSVN](http://hamycodes.wordpress.com/2013/11/18/use-downloadsvn-to-easily-download-subversion-and-git-repositories/ "Use DownloadSVN to Easily Download Subversion and GIT Repositories").  You might still have to modify the URL and there will be a couple extra steps between downloading the files to your computer and then ensuring that modification is reflected in your workspace, but it’s a pretty solid program.



