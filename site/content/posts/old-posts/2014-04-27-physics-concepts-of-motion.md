---
title: "Physics: Concepts of Motion"
date: 2014-04-27T22:52:12.000Z
date_updated:   2014-04-27T22:52:12.000Z
comments: false
---


**Problem: **<span style="color:#000000;">Two soccer players kick a soccer ball back and forth along a straight line. The first player kicks the ball 12</span><span style="color:#000000;"> m to the right to the second player. The second player kicks the ball to the left weakly; it only moves 2.5</span><span style="color:#000000;"> m before stopping. (Consider the right to be the positive direction. Where applicable, indicate the direction with the sign of your answer.)</span>

D1 = 12 m, D2 = -2.5 m

<span style="color:#000000;">**(a)** What is the total distance that the ball moved?</span>

**Solution: **Total distance is the sum of the magnitudes of the individual distances traveled.  Therefore, total distance can be computed as:

Dtot = |D1| + |D2| = 12 m + 2.5 m = 14.5 m

**Answer: **Dtot = 14.5 m

<span style="color:#000000;">**(b)** At the end, what is the displacement of the ball (from the first player)?</span>

**Solution: **We know that displacement is simply the distance from the origin to the final resting place of the object.  As the object is simply moving in two dimensions, all we have to worry about is conserving the negative sign.  We can find the displacement, S, by:

S = D1 + D2 = 12 m + -2.5 m = 9.5 m

**Answer: **S = 9.5 m



