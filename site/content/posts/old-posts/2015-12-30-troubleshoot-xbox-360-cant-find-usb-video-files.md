---
title: "Troubleshoot: Xbox 360 Can't Find USB Video Files"
date: 2015-12-30T05:36:21.000Z
date_updated:   2015-12-30T05:36:21.000Z
comments: false
---


**Problem: **I’m trying to play a video on my TV through my Xbox 360. I’ve loaded the video file onto my USB and connected it to the game console. However, when I open up the movie player app and navigate to the portable device section where my USB drive is connected, it shows no videos are present.

**Solution:**

First off, make sure your video is .avi or .mp4. While these aren’t the only file types the 360 *can *play, they’re the only ones supported natively through the built-in app created by Microsoft. To ensure we don’t run into unneeded difficulties, those file types are probably the way to go.

If your video is already in one of these file types, but the Xbox still can’t find it on the USB drive, follow these steps:

- Take the USB drive out of the Xbox and put it in your computer.
- Open the USB drive using your file explorer of choice.
- Make sure the video is actually there. If it isn’t, put it on the USB drive.
- Right click on the desired video and scroll down to Open With.
- Under this, click Windows Media Player. This tells whatever device is reading the file that the preferred program to consume it is Microsoft’s Windows Media Player.
- Now try it in the Xbox again.

Sometimes, the Xbox reads the preferred open program and then looks through its own system for a matching one. Having failed that, it simply doesn’t show the file as it doesn’t have anything it thinks can deal with it.

In my case, I have all MP4s default to VLC Player for viewing. When I stuck the USB drive into the Xbox 360, it didn’t show my video at all until I changed it back to Windows Media Player.

Hope this helps and happy watching!



