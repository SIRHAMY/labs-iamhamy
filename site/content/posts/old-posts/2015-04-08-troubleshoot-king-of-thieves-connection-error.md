---
title: "Troubleshoot: King of Thieves Connection Error"
date: 2015-04-08T22:31:06.000Z
date_updated:   2015-04-08T22:31:06.000Z
comments: false
---


> [King of Thieves: Connection Error](http://imgur.com/qCiBMdX)

<script async="" charset="utf-8" src="//s.imgur.com/min/embed.js"></script>

*King of Thieves is a game for mobile devices that allows you to create dungeons to guard your treasure and try your hand at stealing those of others.  It is currently available for Android and iOS.  *

If you’ve made it to this post, that means you’ve probably been experiencing some issues connecting to the game servers.  In most cases, you’ll see a message pop up that reads:

> Connection Error: Server unreachable.  Please check your internet connection and try again.

It seems that there’s a bug in the game in which you can only connect to the game’s servers through WiFi.  I say it’s a bug, but it’s also possible the developers intended the game to only be played through WiFi.  Either way, the game will display this error whenever you start it up without a current WiFi connection.

**Solution**

If you find yourself faced with the above issue, simply follow these steps:

1. Go to your task manager and **close the app**.
2. Go to your settings and **make sure WiFi is enabled**.
3. **Choose an available WiFi connection** and connect to it.
4. **Restart the app**.
5. Enjoy your game session.



