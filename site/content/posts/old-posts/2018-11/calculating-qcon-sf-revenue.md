---
title: "HAMQ: Calculating the revenue of Qcon SF 2018"
date: 2018-11-06T21:02:01-05:00
description: "About the page"
comments: false
---

_I oft find myself wondering about the value/magnitude/value of random things, sometimes carrying out the full investigations, sometimes publishing my findings, and sometimes doing none of the above. Recently, I've begun to think that it would be beneficial to publish these more frequently because 1) they could be interesting, 2) I may want to review my results in the future, and 3) I'd like to have an easy wa to share them with others to better ensure my findings aren't totally off-the-wall incorrect. To this end, I've decided to make a dumping ground for these one-off calculations so that they can be refined over time and easily shared. Depending on success, they may find themselves undergoing large transformations but we'll go down that path once we have evidence supporting its value proposition._ 

**Question:** How much money did QCon San Francisco make in 2018?

**Guess:**

With questions like these, sometimes we don't have much information to go off of. As you can see in the furnished code below, I had to guess about the number of attendees by multiplying the speakers by the supposed attendee:speaker ratio. I guessed at the number of attendees in each purchase group and also guessed at the fees each tier of sponsor paid, with the further assumption that each member of each tier paid the same amount which could totally be false.

So there's definitely some error here, but my hope is that this gets me a little closer to the truth.

So here are my best guesses:

* Total attendees ~= 1540 (1078 regular, 462 deluxe)
* Total revenue ~= $4,621,610 ($3,921,610 [~85%] from attendees, $700,000 [~15%] from sponsors)

**The Code:**

I like to provide the math behind the numbers for ease of review and recently came to the conclusion that code is way better for sharing things like this than plaintext algs because it's reproducible and code literacy is high enough that it doesn't feel like I'm sharing in the language of babel. So here it is:

[Read this code on GitLab](https://gitlab.com/SIRHAMY/junkyard-script-calculations/blob/master/2018/qcon-sf/qcon-sf.py)

```python
# HG: we don't have real numbers, so we estimate based on the frontpage that says
# * 140+ speakers
# * 11:1 attendee:speaker ratio
# source: https://qconsf.com/
def getTotalAttendees():
    totalSpeakers = 140
    attendeesPerSpeaker = 11

    return totalSpeakers * attendeesPerSpeaker

# def getAverageCostOfAttendeeBy

# **** BEGIN MAIN ****

print("I am running")

# It's a pay scale, so will need to bucket (maybe as bell curve? doesn't)
# really hold cause this isn't average but time series of sorts
totalAttendees = getTotalAttendees()
ratioOfAttendeesWhoDidWorkshops = 0.3
numberOfRegularAttendees = (1 - ratioOfAttendeesWhoDidWorkshops) * totalAttendees # check
costOfRegularAttendance = 2095 # todo: time series to take avg of all buckets
numberOfDeluxeAttendees = ratioOfAttendeesWhoDidWorkshops * totalAttendees # check
costOfDeluxeAttendance = 3600 # todo: time series w buckets

# base sponsor info from: https://qconsf.com/sponsors
# todo: find a table that shows sponsorship levels and their cost
numberOfPlatinumSponsors = 1
revenueFromPlatinumSponsors = 50000 # check
numberOfSilverSponsors = 22
revenueFromSilverSponsors = 25000 # check
numberOfBronzeSponsors = 10
revenueFromBronzeSponsors = 10000 # check

totalRevenue = (numberOfRegularAttendees * costOfRegularAttendance) \
    + (numberOfDeluxeAttendees * costOfDeluxeAttendance) \
    + (numberOfPlatinumSponsors * revenueFromPlatinumSponsors) \
    + (numberOfSilverSponsors * revenueFromSilverSponsors) \
    + (numberOfBronzeSponsors * revenueFromBronzeSponsors)

print("total revenue: ", totalRevenue)

# Estimate the cost
```

For further reading, you can find examples of previous calculations:

* [My Summer 2017 Q3 Reflection](https://medium.com/@SIRHAMY/in-review-my-summer-q3-2017-2a514f646316) in which I calculate the distance and time spent traveling in my first summer of "real" life
* [My Fall 2016 reflection](https://medium.com/@SIRHAMY/in-review-my-fall-2016-1b4c24d2c6c7) in which I enumerate the time costs of searching for a full-time job while in college