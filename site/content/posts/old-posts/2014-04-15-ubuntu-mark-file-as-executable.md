---
title: "Ubuntu: Mark File as Executable"
date: 2014-04-15T21:09:38.000Z
date_updated:   2014-04-15T21:09:38.000Z
comments: false
---


In some instances, running/compiling files will be blocked by the system to prevent unwanted executions.  The system will block executions of files not marked as “executable” and present the following error message when clicked:

*The file ‘DIRECTORY’ is not marked as executable.  If this was downloaded or copied from an untrusted source, it may be dangerous to run.  For more details, read about the executable bit.*

Here’s how to circumvent this warning:

1. Right click on desired file
2. Properties > Permissions
3. Check the box next to ‘Execute’

Double-clicking your file should run it without errors

*Note: This should only be done to files you are familiar with and trust.  Allowing untrusted sources to run may cause damage to your system/theft of personal information.*



