---
title: "Mac Troubleshoot: Window-Switch Shortcut not Working"
date: 2016-02-03T04:00:59.000Z
date_updated:   2016-02-03T04:00:59.000Z
comments: false
---


<span style="font-weight: bold;">Problem: </span>I know the standard key for switching between windows of the same application is CMD + `. However, I currently have two windows of the same application running and using that hotkey combo isn’t working. How can I get this to switch between my open windows?

<span style="font-weight: bold;">Solution: </span>Macs don’t seem to do fullscreen very well. Most likely, one or both of your windows are open in fullscreen. This blocks the standard command from working as you’d expect. So in order to get your CMD + ` working:

> Make sure both windows aren’t in fullscreen mode



