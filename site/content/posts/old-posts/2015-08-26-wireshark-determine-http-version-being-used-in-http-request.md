---
title: "WireShark: Determine HTTP Version Being Used in HTTP Request"
date: 2015-08-26T05:01:39.000Z
date_updated:   2015-08-26T05:01:39.000Z
comments: false
---


**Problem: **I’ve used WireShark to monitor my network packets in a HTTP request and now I want to figure out what version of HTTP is being used in the request.

**Solution: **

1. Find the HTTP request you’d like to check the version on
2. Select it.
3. In details, navigate to the Hypertext Transfer Protocol section
4. Next to the GET/POST should be something in the form of “HTTP/#.#\r\n” or similar where the #.# is the version number

or

1. Find HTTP request you’d like to check the version on
2. Simply look in the info for HTTP/ followed by the version number

**Example: **

![](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/08/WireSharkHTTPVers1.png?resize=730%2C177)



