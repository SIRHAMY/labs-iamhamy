---
title: "Troubleshoot: Running Processing 3 Opens Processing 2.2.1"
date: 2015-01-05T22:53:42.000Z
date_updated:   2015-01-05T22:53:42.000Z
comments: false
---


If you’ve just downloaded one of the alpha versions of Processing, you may have noticed that whenever you try to run the executable, the current stable version (2.2.1 at the time of this writing) opens instead.  Fear not, this isn’t you being crazy, it’s that Processing is actually opening the last stable version on your computer.

A quick solution to this problem is to simply delete that version from your computer so that you’re left with the alpha version.  Simply run the new version after you’ve deleted the old one and you should be good to go.



