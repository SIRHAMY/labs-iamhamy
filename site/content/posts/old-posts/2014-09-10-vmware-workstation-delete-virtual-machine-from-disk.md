---
title: "VMWare Workstation: Delete Virtual Machine from Disk"
date: 2014-09-10T16:51:19.000Z
date_updated:   2014-09-10T16:51:19.000Z
comments: false
---


In the process of using VMWare Workstation, you may have created sever virtual machines that you no longer have a need for.  This is how you delete them from both your workstation and computer’s hard drive.

1. Open VMWare Workstation
2. Select the virtual machine you wish to delete
3. Right-click on the virtual machine -> Manage -> Delete from Disk
4. A warning should pop up telling you the action is irreversible.  Click OK.  
5. Your VM should be deleted from the disk.



