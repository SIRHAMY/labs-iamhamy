---
title: "Physics: Simple Harmonic Motion"
date: 2014-04-22T19:13:22.000Z
date_updated:   2014-04-22T19:13:22.000Z
comments: false
---


**Problem: **<span style="color:#000000;">A mass is attached to the end of a spring and set into simple harmonic motion with an amplitude </span>*A*<span style="color:#000000;"> on a horizontal frictionless surface. Determine the following in terms of only the variable </span>*A*<span style="color:#000000;">.</span>

<span style="color:#000000;">**(a)** Magnitude of the position (in terms of </span>*A*<span style="color:#000000;">) of the oscillating mass when its speed is </span><span style="color:red;">50%</span><span style="color:#000000;"> of its maximum value.</span>

**Solution:**

Using the conservation of energy formula, solve for Vmax in terms of P, M, K, and A (where P is the ratio).

Etot = KE + PE = K(A²)/2 = MV²/2 + KX²/2

We know that KE is maximized when the velocity is at its maximum and that KE is at its maximum when KE = Etotal, and thus PE = 0.

Etot = KE = KA²/2 = MVmax²/2

Solving for Vmax, we get:

Vmax = sqrt(KA²/M)

To get Vmax down to 50% of its value, we simply refer to it as PVmax, where P = .50      *PVmax = P * sqrt(KA²/M)*

Now, plug the formula we just found for PVmax into the conservation of energy formula from above to get:

X² = A²(1-P²)

**Answer: X = 0.866A**

<span style="color:#000000;">**(b)** Magnitude of the position (in terms of </span>*A*<span style="color:#000000;">) of the oscillating mass when the elastic potential energy of the spring is </span><span style="color:red;">50%</span><span style="color:#000000;"> of the total energy of the oscillating system.</span>

**Solution: **

Similarly, here we just set PE = (P)Etot, where P is the given ratio

PE = (P)Etot = KX²/2 = PKA²/2

Solving for X, we get:

X² = PA²

**Answer: **X = 0.707A



