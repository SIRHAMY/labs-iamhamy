---
title: "Calculus: Find the Point in which the Line meets the Plane"
date: 2013-10-03T06:16:39.000Z
date_updated:   2013-10-03T06:16:39.000Z
comments: false
---


**Find the point in which the line L1: x = -1 + 3t, y = -2, z = 5t meets the plane 2x – 3z = 7**

Substitute the plane x,y,z values with the x,y,z values of the line

2(-1 + 3t) – 3(5t) = 7

Solve for t

t = -1

Plug the value of t into the parametric equation for the line

x = -1 + 3(-1), y = -2, z = 5(-1) => x = -4, y = -2, z = -5

**Solution: P=(-4, -2, -5)**



