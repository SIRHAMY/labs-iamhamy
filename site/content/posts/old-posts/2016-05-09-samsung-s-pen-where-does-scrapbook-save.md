---
title: "Samsung S Pen: Where does Scrapbook Save?"
date: 2016-05-09T14:48:14.000Z
date_updated:   2016-05-09T14:48:14.000Z
comments: false
---


**Problem: **I’m using a Samsung tablet equipped with the S Pen. I’ve used it’s functionality to select a graphic on the screen and save it to the Scrapbook. How do I access this image on the device?

**Solution:**

In order to access images in your scrapbook, say to insert them into another app for example, follow these steps:

- **Open the Scrapbook app** on your device
- Navigate to the correct Scrapbook folder by clicking the name in the top left corner
- **Select the scrap**(s) you’d like to move to the device’s image directory
- Select **More > Export file**
- You should now be able to find your file at this directory on your device: - **Root > Storage > Emulated > 0 > Scrapbook**



