---
title: "Ubuntu: Add a lockscreen screensaver with an image slideshow"
date: 2019-10-21T22:35:00-04:00
tags: [
    "ubuntu",
    "screensaver",
    "lockscreen",
    "slideshow"
]
comments: false
---

# problem

I'm running Ubuntu and have a directory of images I want to display as a screensaver. How can I do this?

# solution

Here I'll show you how to set up an image slideshow on Ubuntu using `Xscreensaver`. But before I do that, you'll need to install it. Vitux wrote a great guide on that ([How to replace Gnome Screensaver with Xscreensaver on Ubuntu](https://vitux.com/how-to-replace-gnome-screensaver-with-xscreensaver-on-ubuntu/)) so go ahead and install it and come back here.

Now that that's installed, let's get the slideshow running.

1. Create a directory with all the photos you want the slideshow to go through
2. Open the `screensaver` application that came with the Xscreensaver install. This allows you to modify its settings.
3. In the `Display Modes` tab, select the `Only One Screen Saver` mode from the dropdown, then select the `GLSlideshow` option from the scroll menu. This is the screensaver mode that will slideshow through the images
4. In the `Advanced` tab, find the `Image Manipulation` section and select `Choose Random Image` then click `Browse` and navigate to the folder you put your images in

That's it!

You can change other settings on the `Advanced` tab and if you go back to `Display Modes`, you can change the settings specifically for your slideshow by clicking `Settings` while `GLSlideshow` is selected.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).