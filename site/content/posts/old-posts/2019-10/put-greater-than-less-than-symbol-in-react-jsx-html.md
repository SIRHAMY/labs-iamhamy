---
title: "React/JSX/HTML: How to put a greater-than or less-than symbol in React/JSX/HTML"
date: 2019-10-28T17:19:58-04:00
tags: [
    "react",
    "jsx",
    "html",
    "tutorial"
]
comments: false
---

# problem

I'm building a web page in React / JSX and need to put a greater-than (`>`) and less-than (`<`) symbol in my page. How can I do this without the page breaking?

# solution

You can wrap your symbols in a code block and insert them as a string!

Example:

```
<div>{"<"} Less than, {">"} greater than </div>
```

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).