---
title: "Python: Remove duplicate list items with a set"
date: 2019-10-14T22:32:32-04:00
tags: [
    "python",
    "list",
    "duplicates",
    "set",
    "tutorial"
]
comments: false
---

# problem

In my Python program I have a list containing duplicate elements. How can I easily get rid of those duplicates?

# solution

There are tons of possible ways to do this, but one simple way to dedupe a `list` is to use an intermediate `set`. Here's an example:

```
list_with_duplicates = [ "a", "b", "a"]
set_without_duplicates = set(list_with_duplicates)
list_without_duplicates = list(set_without_duplicates)

print(list_without_duplicates)
```

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).