---
title: "WireShark: Find the TCP Port Numbers being Used for a Connection"
date: 2015-08-26T05:19:29.000Z
date_updated:   2015-08-26T05:19:29.000Z
comments: false
---


**Problem: **I’m using WireShark to monitor my network traffic and I want to see what port numbers are being used for a particular TCP connection I’ve captured.

**Solution:**

1. Find the desired connection packet in WireShark
2. In the details section at the bottom, find “Transmission Control Protocol”
3. Next to that, you’ll see the source and destination ports

or

1. Find the desired connection packet in the WireShark list view
2. Look in the info section
3. There should be two numbers of the form “####->#####”.  This indicates the source port number and the destination port number

**Example:**

![](http://i1.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/08/WireSharkPort.png?resize=527%2C198)



