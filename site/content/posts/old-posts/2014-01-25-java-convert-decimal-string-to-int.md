---
title: "Java: Convert ASCII Decimal String to Int"
date: 2014-01-25T19:33:00.000Z
date_updated:   2014-01-25T19:33:00.000Z
comments: false
---


**Objective: **Convert a string of positive ASCII decimals into the integer data type without using built-in Java library functions.

Here’s the code to my solution:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/8625995.json"></div>(https://gist.github.com/SIRHAMY/8625995)

**Logic: **The method takes in a string of positive integers that fit inside the constraints for Java’s int primitive data type.

Essentially, you’re taking each character in the string (starting from the right) and converting it to its ASCII code.  Because we know that the given string will only contain positive integers, we can subtract 48 from the given value to get the correct integer value.

We have a scalar value to ensure that the added value goes in the correct place.  Starting from the right of the given string, the value will be in the 1s, 10s, 100s, etc. place we can multiply each value by the scalar, re-calibrating the scalar by a multiple of ten after each iteration.



