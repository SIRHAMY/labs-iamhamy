---
title: "Guide: Create Looping Animated GIF with Moovly and Windows Movie Maker"
date: 2015-12-14T10:10:00.000Z
date_updated:   2015-12-14T10:10:00.000Z
comments: false
---


We’ve all seen those cool GIFs online that seamlessly loop their animations. This guide will walk you through the basics of creating your own.

In this guide, I use Moovly as an example for creating an animation, but you can use pretty much anything for this that can output the animation in a widely-used video file type. When I searched ‘create an animation online’ it was the first search result, so don’t put too much stock in this choice. The upsides of Moovly are that it’s free (with limited functionality), has a bunch of templates for quick animation-creation, and it’s web-based so you don’t have to install anything.

For video editing, I used Windows Movie Maker as it’s freely available, stupid easy to use, and was already installed on my machine. Again, don’t feel like you need to use the exact same program to finish this tutorial, the point was to show you that this can be done using very basic features.


## **The Guide**

![moov-full](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/12/moov-full.gif?fit=730%2C410)

This guide will give you step-by-step instructions to follow with examples of me creating my own GIF. My GIF is a map of the USA with imaginary data centers imposed over it. I have dots indicating data that will move in and out from these data centers. The finished product is below.

### Creating Your Animation

*In this section, we’ll create our “halfway” animations as shown below.*

![moov](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/12/moov.gif?fit=730%2C410)

- Head over to [Moovly](http://moovly.com) – or your favorite animation-creating software – to get started. If you do use Moovly, make sure you sign up with an accurate email address as this is where they’ll send your finished “moov”. If you aren’t using Moovly, feel free to skip to the next section.
- Create your animation. The site gives a pretty good intro for the interface. It’s a little blocky, but it does the trick. Here, you only have to create your animation up to the “halfway” point as shown above. Notice that the dots only move out, the move in functionality hasn’t been added yet.
- Once you’re satisfied with your animation, save it by hitting the save icon in the top left corner. Once it says “Save Successful”, you’re free to exit the window.
- Head on over to My Moovs, select the one you want to download, then click “Details”.
- This should bring you to a new page with a large preview of your animation. If you don’t see the animation right away, don’t freak out. It’s taken upwards of 15 seconds in my experience to load sometimes. Click the greyed-out “Download” button, then click “Download” in the drop down. This will bring you to a new page with upgrade options to get rid of the watermark and outro. Just continue through all this and the download should start.
- Once you’re finished with the extra options, you should be back at the Details page. If you click on “Download” again, it should say “In queue…”. Once the animation has finished processing they’ll send you an email – this has taken about 10 minutes in the past.
- Once you get your email, download the file and make sure it looks alright.
- Notice that the file you downloaded may be a bit too long and that there is an outro/intro with Moovly branding on it. Simply open up Movie Maker and trim the bits you don’t want in the finished GIF away.

### Completing Your Animation

*We have half an animation. Time to come full circle.*

![moov-rev](http://i1.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/12/moov-rev.gif?fit=730%2C410)

- In the previous steps, we created our animation – but only the first half. Now, we need to complete our loop by appending the second half to the first.
- We’re going to accomplish this, by uploading our animation clip to an online video reverser. This will give us the second half of the animation we can use to close our loop.
- I used [Video Reverser](http://www.videoreverser.com/), but feel free to use whatever you please.
- If you used something else to reverse your video, feel free to skip to the next section.
- On Video Reverser, select your file from your computer’s file system, select your output file type, then hit Start.
- Once it’s done, you should see a “Result” link. Click it to download your reversed animation.

### Combining the Pieces

*We now have two halves of a whole.*

- Open up your favorite video editor.  Again, I’ll be using Movie Maker for these steps.
- Add both your forward-running animation and your reversed animation.
- Place the reversed animation directly behind the forward-running one – or vice versa. If you play through the entire sequence, it should appear that the animation runs forward then reverses to its original state.
- Export this new video as a widely-used file type (.mp4, .avi, etc.)

### Creating the GIF

*We have the loop. Now to make it loop.*

- To create the GIF, I’m using [EZGIF](http://ezgif.com), but feel free to use whatever tool you like best.
- Choose the video file you just created then hit Upload.
- Play your GIF to the end, then select that time stamp as the End Time for best results.
- Keep the size original, boost the frame rate to 20 (or the highest you can), and keep the method as ffmpeg. Hit Convert to GIF.
- You should now be able to see a preview of your newly-created GIF.
- Edit it if you wish then download the result by clicking Save.

### You Did It!

![moov-full](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/12/moov-full.gif?fit=730%2C410)

There you have it, your very own animated, looping GIF. Now that you’ve gone through the process you’re much more prepared to create ever more complex animations. If you come up with anything cool, share it with us. Or don’t, whatever floats your boat.



