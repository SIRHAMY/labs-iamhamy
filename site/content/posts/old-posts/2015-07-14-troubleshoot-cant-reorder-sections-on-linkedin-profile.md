---
title: "Troubleshoot: Can't Reorder Sections on LinkedIn Profile"
date: 2015-07-14T21:16:54.000Z
date_updated:   2015-07-14T21:16:54.000Z
comments: false
---


**Problem: **I want to rearrange the sections in my LinkedIn Profile, but can’t seem to figure out how.  I’ve gone to my profile, but there isn’t a button that allows me to move the sections around.

**Solution: **The standard solution given by LinkedIn’s help articles state that to reorder the sections in your profile, you must:

1. Move your mouse up to the navigation bar where it reads “Profile”
2. Click “Edit Profile” in the drop down
3. Then you’re supposed to hover over the section you’d like to move and click the button with an up and down arrow.  This will allow you to rearrange the sections in your profile.

**Solution [Actual]: **However, if you’re here, you probably can’t find this button.  Apparently, the site doesn’t support reordering functionality in Chrome.  So, if you want to edit your profile, you’re going to have to edit it in Firefox or Opera.

- LinkedIn’s profile edit feature is built with FireFox and Opera in mind, so you won’t be able to see the reorder feature in Chrome.  Switch to one of these two browsers before you edit.



