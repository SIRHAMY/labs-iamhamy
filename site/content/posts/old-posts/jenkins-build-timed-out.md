---
title: "Jenkins Build Aborted due to Time Out"
date: 2018-09-19T09:08:22-04:00
description: "About the page"
draft: false
---

# Jenkins build timed out

**Problem:** My Jenkins build is failing with the error message:

```
Build timed out (after 161 minutes). Marking the build as failed.
Build was aborted
```

however I don't think that Jenkins has a built-in way to cause timeouts. If that's the case, why is this happening?

**Solution:** So it is true that (to my knowledge) Jenkins doesn't have a default build timeout, however there are plugins that add this functionality for you. This particular error message seems to come from the [Build-timeout Plugin](https://wiki.jenkins.io/display/JENKINS/Build-timeout+Plugin) for Jenkins.

To get rid of this error, you can either increase the build timeout (which brings with it its own risks) or configure the job to not timeout at all (likely even riskier).
