---
title: "Troubleshoot: 8Tracks Playlist Cover Image Displaying Black"
date: 2016-05-23T02:48:30.000Z
date_updated:   2016-05-23T02:48:30.000Z
comments: false
---


**Problem: **I’m trying to upload an image to use as a cover for my 8Tracks playlist, but it keeps displaying black. The image I’m using is a PNG and I’ve opened it in other programs to ensure that the file isn’t corrupted. Why is this displaying black and what can I do about it.

**Solution: **The problem seems to indicate that the image you’re uploading has no background color (it’s set to transparent) and the contents of your image are black. 8Tracks defaults to a black background if none is given, meaning that your image is black on black – thus why you don’t see anything.

To remedy this situation, and have your image display correctly on 8Tracks:

- Set a background color on your image
- Upload it again

For best results, give your image square dimensions (500 x 500, 750 x 750, etc.) as this is the default cover photo ratio.



