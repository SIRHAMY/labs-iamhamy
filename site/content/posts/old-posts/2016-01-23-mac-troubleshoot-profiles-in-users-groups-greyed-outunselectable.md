---
title: "Mac Troubleshoot: Profiles in Users & Groups Greyed Out/Unselectable"
date: 2016-01-23T16:39:57.000Z
date_updated:   2016-01-23T16:39:57.000Z
comments: false
---


**Problem: **I’m in the System Preferences > Users & Groups menu on my Mac and I’m trying to modify a profile’s user rights/privileges. However, the profile I’m trying to modify appears greyed out in the profile list which makes it unselectable. How can I select this profile?

**Solution: **The solution is pretty simple. It appears that you can’t be logged in to the user profile that you are modifying in System Preferences. So, in order to select the greyed out profile, log that profile out on your device. After logging out, you should be able to select it and modify its user privileges at will.



