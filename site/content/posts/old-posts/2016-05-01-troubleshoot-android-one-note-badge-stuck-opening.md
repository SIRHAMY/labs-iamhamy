---
title: "Troubleshoot: Android One Note Badge Stuck 'Opening...'"
date: 2016-05-02T00:31:19.000Z
date_updated:   2016-05-02T00:31:19.000Z
comments: false
---


**Problem: **I’m using the Microsoft One Note Badge on my Android Galaxy Tab A. The badge floats along the screen til I press it then it opens up One Note for me. I just opened the badge and told it to open One Note, but now it’s stuck with the message ‘Opening…’ It’s obviously stuck, but now I can’t touch any of the other buttons on the screen like Power Off or Restart because the badge has focus. How can I close the One Note Badge when it gets stuck like this?

**Solution: **The solution to this issue is pretty simple, albeit a bit obscure. Before you go ahead removing your battery or smashing your tablet, try this:

*Hit your Android’s back button.*

This should signal to the One Note badge that you want it to end whatever operation it’s currently performing – in this case opening a page. Plus, it does this without having to touch the screen which we already know is blocked.

Hope this saves a few tablet lives.



