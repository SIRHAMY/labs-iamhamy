---
title: "Angular: ng-show Change Leaves Container Element with Width and Height of 0"
date: 2015-07-31T03:27:00.000Z
date_updated:   2015-07-31T03:27:00.000Z
comments: false
---


**Problem: **I’m trying to show an image when a certain variable is equal to true, but the size of the image created is dependent on the size of the container element.  When my function is called that sets the visible variable to true and the image is created, the container variable comes back with a width=0 and height=0 which skews the appearance of my image.

**Solution: **I’m not really sure why this happens, but I’m assuming it’s because Angular doesn’t have time to propagate the new element attributes before the loading function runs.  This is an issue in JavaScript because much of it runs asynchronously, so you’ll find some functions running in slightly different orders than you may have assumed.

The solution I found was to ditch ng-show altogether and instead use ng-class to apply a CSS class that “hides” the element.  This way, the element is technically instantiated (has a width an height), but is also hidden for the user.

**Example: **

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/2eb1e77a4416c839f52e.json"></div>[[https://gist.github.com/SIRHAMY/2eb1e77a4416c839f52e](https://gist.github.com/SIRHAMY/2eb1e77a4416c839f52e)]

Now when isVisible = false, the img will be hidden with the CSS ‘hide’ class.



