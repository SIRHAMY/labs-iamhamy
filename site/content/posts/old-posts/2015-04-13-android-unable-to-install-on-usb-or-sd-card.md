---
title: "Android: Unable to Install on USB or SD Card"
date: 2015-04-13T22:32:06.000Z
date_updated:   2015-04-13T22:32:06.000Z
comments: false
---


[![USBInstallationErrorAndroid](http://i0.wp.com/hamycodes.files.wordpress.com/2015/04/usbinstallationerrorandroid.png?resize=169%2C300&ssl=1)](http://i0.wp.com/hamycodes.files.wordpress.com/2015/04/usbinstallationerrorandroid.png?ssl=1)

**Problem: **This problem usually occurs when attempting to download an app from the app store.  The app will finish downloading then a pop up will appear that says:

> Couldn’t install on USB storage or SD card.

**Solution: **If you’ve experienced this problem, do the following:

1. **Open the back of your phone **– or wherever the storage device is housed.
2. **Remove your SD card** or other storage device from your device.
3. **Re-insert storage device**.
4. **Restart downloads **that may have completed, but not installed.

If this doesn’t work, try restarting your phone after removing your storage device then inserting it after it powers back on.  Sometimes, the device just needs to reset.



