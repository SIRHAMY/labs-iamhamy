---
title: Watch TV Shows on your Phone for Free with iShows
date: 2015-07-09T05:07:00.000Z
date_updated:   2015-07-09T05:07:00.000Z
comments: false
---


*Disclaimer: Please note that this is for informational purposes only and that any actions you take based on the information contained therein must be made at your own discretion.  The act of streaming isn’t usually considered illegal, it’s the content you stream that causes problems.  As such it is up to you to inform yourself about the legality of these actions and whether or not you will undertake them.*

Now that the disclaimer is out of the way, I’ll show you how you can stream high quality TV shows straight to your mobile device for the unbeatable price of free.  iShows is a website with a great UI and a large selection of shows – Game of Thrones, Sherlock, House of Cards, etc. – with a very small amount of ads when compared to other services like it.

**Ads and General Warnings**

There aren’t any pop-ups, at least not to my knowledge, and most of the ads that are present are standard in-line images that are easy to distinguish from the files you actually came there to see.  This makes it alot less likely to be downloading adware/malware/badthings to your computer.  That or it’s more sly about it than any other site I’ve seen so take it with a grain of salt and be as careful as possible.

As a general rule, I tend to not click on any ads I see on sites I don’t completely trust and exit out of any popups that may inadvertently appear.  Keeping to this policy should protect you from the majority of dangers that could be caused by downloading things.

On that note, I *strongly *suggest that you don’t download anything from the site.  That’s, without a doubt, one of the most dangerous things you can do on the site.  To that end, try to always stream the content and exit out of anything that looks like it really wants you to download it – it doesn’t matter how many viruses the popup says you have on your phone.

**How to Stream with iShows**

*Note: Make sure you’re on a mobile device when you access the site, otherwise it will return a blank page with a server error message.*

1. Navigate to the site at isho [dot] ws.  I usually do this in Chrome’s Incognito mode, not because it’s more safe per se, but because it gives me a little more peace of mind.
2. Type the title of the show you want to watch into the search bar or browse through the titles present on the home page.
3. Select the one you want to be brought to the Show screen.
4. Here, you can choose which season you want to watch and browse through each episode of that season.
5. Find the episode you want, then choose which definition you want to watch it in (480P or 720P).
6. Now decide whether you want to stream or download the file.  As I previously mentioned, I highly recommend streaming the file rather than downloading anything from an unstrustworthy source.  But, in the end, the choice is yours.  Make it at your own peril.
7. Wait for the 30 second guest timer to expire, then enjoy your show.

*Note on Streaming*: Sometimes when you stop the stream for more than a few minutes at a time, the stream will break and you’ll no longer be able to resume from pause.  In that case, just reload the page, reselect the episode, wait for the timer to expire, then move the slider to the time where you left off.  Unlike other services, iShows seems to buffer the video extremely fast regardless of how far in you move the slider on initialization.  At most, this process usually only takes ~30 seconds.



