---
title: "Calculus: Find a Linear Equation of the Plane that Contains the Line and Passes through the Point"
date: 2013-10-03T06:54:08.000Z
date_updated:   2013-10-03T06:54:08.000Z
comments: false
---


**Find a linear equation of the plane that contains the line L1: x = -1 + 2t, y = 2 + t, z = 1 – t and passes through the point Q(1,1,2)**

Start by pulling point P out of the parametric equation of L1

P(-1, 2, 1)

Now find directional vector PQ

PQ = (2, -1, 1)

Pull the directional vector v from the parametric equation of L1 (coefficients of the line)

v = (2, 1, -1)

Take the cross product of the vectors PQ and V

PQ X V = (2, -1, 1) X (2, 1, -1) => 0i + 4j + 4k

Plug the values into the equation for a plane (use the cross product result as weights a,b,c and Q as x0,y0,z0)

0(x-1) + 4(y-1) + 4(z-2) = 0

**Solution: Simplify to get the equation of the plane y + z -3 = 0**



