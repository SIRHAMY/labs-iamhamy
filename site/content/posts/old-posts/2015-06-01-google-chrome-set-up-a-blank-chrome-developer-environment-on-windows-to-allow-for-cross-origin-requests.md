---
title: "Google Chrome: Set Up a Blank Chrome Developer Environment on Windows to Allow for Cross Origin Requests"
date: 2015-06-01T13:35:52.000Z
date_updated:   2015-06-01T13:35:52.000Z
comments: false
---


**Problem: **I’m developing a web app and testing locally through my Chrome browser.  However, Chrome blocks Cross origin requests/the injection of HTML locally.  I’m getting console errors through Chrome Dev Tools (CTRL + Shift + I) like:

> XMLHttpRequest cannot load *filePath* Cross origin requests are only supported for protocol schemes: http, data, chrome, chrome-extension, https, chrome-extension-resource.

and

> Error: Failed to execute ‘send’ on ‘XMLHttpRequest’: Failed to load

I would like an easy way to setup a Chrome instance to locally test my web app.  It must be able to allow Cross Origin requests while allowing me to have other personal Chrome instances up at the same time (I like listening to music).

**Solution: **In most cases, you won’t run into this problem if you were to load your app onto a server then have the server send you back the data – it’s the fact that you’re testing locally that is (most likely) the culprit of this issue.  The simplest way around this, that I’ve found, is to simply create a desktop shortcut that creates a new instance of Chrome with web security off.  For practical purposes, you probably shouldn’t use this shortcut for anything but testing your app locally, but it should do that just fine.

To create a shortcut on your desktop that opens up a Chrome instance that allows Cross Origin requests, do the following:

1. Go to your desktop and create a new shortcut by right-clicking, selecting New, and then Shortcut.
2. In the target field, put  
> “C:Program Files (x86)GoogleChromeApplicationchrome.exe” –user-data-dir=/dev/null –disable-web-security

Link to gist: https://gist.github.com/SIRHAMY/f2d5ffa5f5b8f09ea28f

Note: it’s possible that you have Chrome installed somewhere else on your computer.  If so, just replace the above path with one that matches your configuration.  *The user-data-dir attribute tells Chrome where to get the configuration data and the /dev/null configuration tells it to set up a blank environment.  Disable web security tells Chrome to disable all web security which, in turn, allows us to test Cross Origin requests locally.  Like I said earlier, you probably don’t want to use this instance for anything other than development.*
3. Name it something conspicuous so you don’t accidentally use it for non-dev purposes.
4. happy coding!



