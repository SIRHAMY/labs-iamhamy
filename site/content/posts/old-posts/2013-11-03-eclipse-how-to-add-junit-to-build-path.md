---
title: "Eclipse: How to Add JUnit to Build Path"
date: 2013-11-03T22:46:16.000Z
date_updated:   2013-11-03T22:46:16.000Z
comments: false
---


When you download and place a .java JUnit file into your project’s source folder, Eclipse doesn’t automatically add JUnit to the project’s build path.  In order to add the library to the build path, follow these steps.

1. Right click on your **Project folder** in the Package Explorer
2. Click** Build Path -> Configure Build Path…**
3. Click **Add Library**
4. Select **JUnit** then click Next
5. Choose your JUnit settings, then hit Finish



