---
title: "C++: Get Size of Eigen::Matrix"
date: 2016-04-11T20:35:40.000Z
date_updated:   2016-04-11T20:35:40.000Z
comments: false
---


**Problem: **I’m using the Eigen library in C++. I have a Matrix and want to check the size of it within my code. How can I do this?

**Solution: **The best way to go about this is to call the .rows() and .cols() functions on the Matrix. This will return the number of rows and columns in your matrix, respectively.

Here’s an example code snippet to show you how it might be implemented:

 

[https://gist.github.com/SIRHAMY/577f03949a048ede0fab]

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/577f03949a048ede0fab.json"></div>

