---
title: "Troubleshoot: Failed to Parse JSON - Trailing Comma"
date: 2016-03-01T01:51:56.000Z
date_updated:   2016-03-01T01:51:56.000Z
comments: false
---


**Problem: **My npm is throwing an error when I try to run “npm install”:

> Failed to parse json  
>  Trailing comma in object at 12:3  
>  },  
>  ^

It then says that it failed to parse package.json data. Package.json must be actual JSON, not just JavaScript.

**Solution: **The solution here, is to take out your extra comma. You may be looking at the comma there and saying something like “but you need a comma to separate different objects”. While this is true, you probably have an extra one within that object.

Take one of my mistakes for example:

[https://gist.github.com/SIRHAMY/b9a776484989ccd01983]

Can you find the error?

Following the given stack trace at the top, you’d assume it was the comma on line 12. However, as you said, we know that that comma is required to denote the end of the babel object. The real offending issue here is the line directly above it.

On line 11, we see “presets” set to a value and then a comma. However, that entry isn’t followed by another entry. Here, we’re parsing package.json, finding the comma that denotes another object, not finding it, and finding another comma instead – thus the error.

**TL;DR **Check the lines above the stack trace error to ensure you don’t have a comma denoting another object when there is none.

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/b9a776484989ccd01983.json"></div>

