---
title: "Lutris: Steam install - Sequence item X: expected str, NoneType found error"
date: 2019-07-22T12:23:46-04:00
tags: [
    "lutris",
    "steam",
    "troubleshoot"
]
comments: false
---

# problem

I'm trying to install a Steam game with Lutris on my Ubuntu Linux desktop. However, when I install I get an error of `Sequence item 0: expected str, NoneType found`. If I exit out of the error, the installer appears to keep going. However, it's been up for awhile and I'm starting to think it's just spinning.

What is this error and how do I fix it?

# solution

This error can happen for a bunch of different reasons. Really all it's saying is that it expected to find a value but found nothing instead. Here are some things you should check:

* Are all of the dependencies for Lutris correctly installed?

Lutris is typically good at letting you know if it expected certain dependencies to be present but it's having trouble finding them. If you don't see an explicit error about this on Lutris start up, then it's probably not this one.

* Is Steam open before you try installing?

Depending on the installer, sometimes they expect Steam to already be open. If it's not, it may throw a not found error. Make sure you've got Steam opened before initiating install.

* Are you logged into Steam?

Again, depending on the installer, they may assume that you're already logged into Steam. Log in before initiating install.

* Have you purchased the game / software?

Note that Lutris is just a runner, not a distribution engine. As such, it requires that you've already acquired the rights to the property (in this case your Steam game) from the chosen distribution at install. Some installers will throw nice messages and some won't. Double check that the game you're trying to install is already in your Steam library.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).