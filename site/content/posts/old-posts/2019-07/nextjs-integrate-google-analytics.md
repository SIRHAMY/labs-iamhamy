---
title: "NextJS: How to integrate Google Analytics into your app"
date: 2019-07-15T17:32:59-04:00
tags: [
    "nextjs",
    "npm",
    "google-analytics",
    "tutorial"
]
comments: false
---

# problem 

I'm building a NextJS app and I want to have stats on who's visiting my site and what parts of my site are being visited. I decided to use Google Analytics because it's free and powerful, but I'm not quite sure how I can get this integrated into my site. Any help?

# solution

So there are going to be lots and lots of ways to do this, but here's what I've done and found most useful in my own projects. Basically, I install the [`react-ga` `npm` module](https://www.npmjs.com/package/react-ga), create a wrapper utility `Analytics` for it that instantiates the module with my site's Google Analytics credentials, then have each of my `NextJS` pages consume that `Analytics` utility and log whatever data they want directly to Google.

It seems like a lot of steps and I'll admit that there is some initial setup, but once that's done every additional logging place just needs to import your `Analytics` utility and call it with whatever log you want to send. Easy and extensible.

Okay so the steps I listed above are:

* install `react-ga`
* create a wrapper utility `Analytics` to instantiate the module with your Google Analytics credentials
* import the `Analytics` utility into each page and log the page view

## install `react-ga`

To start, we'll be installing [`react-ga`](https://www.npmjs.com/package/react-ga). There are probably a bunch of other Google Analytics npm packages out there, but this one seems solid cause it has a lot of usage and support so it's good enough.

To install, just go to the root of your NextJS project and type:

```
npm install --save react-ga
```

## Create the `Analytics` utility

So in this step, we'll basically just be making a wrapper around the `react-ga` package we just installed that instantiates the package with the correct credentials for our site. The reasson this is useful is that we now don't have to instantiate the package each time we want to use it, we can just do a simple import and use (read: good for dev ergonomics and maintainability through abstraction).

My `Analytics` utility looks something like this, but feel free to deviate if you see fit:

```
import ReactGA from "react-ga";

const GoogleAnalyticsId = "YOURGOOGLEANALYTICSSITEIDHERE";

class AnalyticsInternal {
    constructor() {
        ReactGA.initialize(GoogleAnalyticsId);
    }

    logPageView(url) {
        ReactGA.pageview(url);
    }
}

export const Analytics = new AnalyticsInternal();
```

So now we've made it so no consumers need worry about what credentials we are using here. Also, down the road, if we ever wanted to utilize additional steps on each page view or maybe even switch off GoogleAnalytics to something else, we could just do that right here with little to no changes to our existing usages of this utility (yay!).

## import the `Analytics` utility into your pages and use

So now we have our utility, let's show an example usage of that on the default `index.js` page.

```
import { Analytics } from "../some/path/to/Analytics";

export default class Index extends React.Component {
  constructor(props) {
    super(props);

    Analytics.logPageView("/"); // or whatever other thing you wanted to log
  }
  ...
}
```

# fin

That's it, you should now have analytics baked into your app.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).