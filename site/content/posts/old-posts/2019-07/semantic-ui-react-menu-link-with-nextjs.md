---
title: "Semantic UI React: Using NextJS Links in a Semantic UI Menu"
date: 2019-07-01T01:51:46-04:00
tags: [
    "semantic-ui",
    "react",
    "tutorial",
    "nextjs"
]
comments: false
---

# problem

I'm building an app with NextJS and Semantic UI React. I am trying to create a header menu that links to individual NextJS pages, but can't figure out how to make the links work. How can I get the NextJS `Link` elements to work with the Semantic UI React `Menu` element?

# solution

Luckily the Semantic UI React `Menu` isn't very opinionated about what gets put inside of it. Really it's just there for styling purposes. As such, we can put anything in there, including the NextJS `Link` that's used for navigation.

An example of a NextJS `Link` (and a normal HTML link for reference) inside a Semantic UI React `Menu` would look something like the following:

```
<Menu>
    <Link href="/">
        <Menu.Item
            name="This is the NextJS Link">
        </Menu.Item>
    </Link>
    <Menu.Item position="right">
        <a href="https://hamy.xyz">
            This is the normal link
        </a>
    </Menu.Item>
</Menu>
```

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).