---
title: "Lutris: Skyrim Special Edition music and voices not working on prefix_64"
date: 2019-07-29T20:25:20-04:00
tags: [
    "lutris",
    "skyrim",
    "troubleshoot"
]
comments: false
---

# problem

I installed Skyrim Special Edition (via Steam)'s Lutris runners and while everything loads up and starts fine, the characters don't have any voices and there's no background music playing. It seems that the characters skip over all the dialog (I can see the subtitles but they go super fast), maybe related to the voices not working. How can I get this to work?

# solution

I got my version of Skyrim Special Edition to work within wine_64 (as denoted by prefix_64 in the file path) by using `winetricks` to install both `faudio` and `xact_64`. Doing this yourself (assuming you have a standard setup) is pretty straightforward:

* open Lutris
* select Skyrim SSE
* select winetricks
* select `select the default wineprefix`
* select `install a windows DLL or component`
* find and check the boxes next to `faudio` and `xact_64`
* click `ok` and it should start installing by itself.

Make sure you kill all wineprocesses so you get a fresh boot, then start up your game. With any luck, you've now got music and voices.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).