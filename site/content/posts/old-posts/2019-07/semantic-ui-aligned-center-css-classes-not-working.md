---
title: "Semantic UI: 'aligned center' css classes not working"
date: 2019-07-08T02:00:41-04:00
tags: [
    "semantic-ui"
]
comments: false
---

# problem

I'm using Semantic UI to help style my application, but I can't get the `aligned center` css classes to, well, align the items in an html element. What am I doing wrong.

# solution

To start, try using the correct ordering of `center aligned`. I know it's not the most idiomatic ordering, but that's what they made it as.

Note though that some elements will not be directly affected by some css classes and that adding these classes to the tops of certain elements won't necessarily affect their children. But try switching the ordering and see if that helps.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).