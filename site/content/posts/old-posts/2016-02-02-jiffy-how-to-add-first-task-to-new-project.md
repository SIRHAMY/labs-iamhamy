---
title: "Jiffy: How to Add First Task to New Project"
date: 2016-02-02T05:27:40.000Z
date_updated:   2016-02-02T05:27:40.000Z
comments: false
---


**Problem: **I’m using Jiffy to track the time I spend on different projects. I’ve just created a new project, but I can’t figure out how to add a new task to it. Tapping the project starts/stops the timer for the project as a whole and holding my finger on it brings up the screen to change the start time. How do I add an additional task to track its time separately?

**Solution: **To add the first task in a project:

- **Click the menu button** in the top left to open up the sidebar
- **Select Projects**
- Select the Project you would like to create the task under
- Hit the Tasks tab
- **Tap the screen** to add a new task



