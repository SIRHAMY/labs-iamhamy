---
title: "TCP: Closing Process Finite State Diagram"
date: 2015-10-06T20:51:41.000Z
date_updated:   2015-10-06T20:51:41.000Z
comments: false
---


This is a Finite State Diagram of two machines using the Transmission Control Protocol (TCP) to reach the CLOSED state.  It is based off information from [The TCP/IP Guide](http://www.tcpipguide.com/free/t_TCPOperationalOverviewandtheTCPFiniteStateMachineF-2.htm).

![](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/10/HW3TCPStateDiagram.png?resize=730%2C730)

**Sequence**

*Note: In this configuration, it doesn’t matter who the so-called “client” and “server” are.  The only thing that matters is who decides to initiate the closing sequence.*

1. Initiator: Wants to Close.  Send FIN to Responder. Move to FIN-WAIT-1 state.
2. Responder: Receive FIN.  Send ACK to Initiator.  Move to CLOSE-WAIT state.
3. Initiator: Receive ACK from Responder.  Move to CLOSING state.
4. Responder: Wait for application to close on host. On close, send FIN.  Move to LAST-ACK state.
5. Initiator: Receive FIN from Responder.  Send ACK. Move to TIME-WAIT state.  Start timer.
6. Responder: Recieve ACK from Initiator.  Move to CLOSED state.
7. Initiator: Upon timer expiration, move to CLOSED state.



