---
title: "Eclipse: Open Multiple Console Views at Once"
date: 2015-09-29T06:38:54.000Z
date_updated:   2015-09-29T06:38:54.000Z
comments: false
---


**Problem: **I have multiple programs running in Eclipse at once and want to be able to see the console outputs of each one at the same time.  How do I open multiple console windows so that I can see the output of each of these programs?

<div class="wp-caption alignnone" style="width: 823px">Click the “Open Console” button to create a new Console View

</div>**Solution: **Assuming you already have the default Console View open in your window, you can create a new console by:

*If you don’t have a Console View open, just go to Window -> Show View -> Console which should open a new view for you.*

- Select the **Console View**
- On the top right of the Console View, there should be a button called “**Open Console**“.  Click it and **choose which type of console you want to open** – Java Stack Trace Console, Maven Console, or New Console View.  I usually go with the Java Stack Trace Console, but choose whatever suits your needs best.
- You should now have another Console View to look at.

If you want to know how to change which program a console receives output from check out: “[Eclipse: Change which Program the Console Receives Output From](/posts/2015-09-29-eclipse-change-which-program-the-console-view-receives-output-from/)“



