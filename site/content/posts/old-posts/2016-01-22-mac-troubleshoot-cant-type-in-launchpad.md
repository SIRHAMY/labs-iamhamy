---
title: "Mac Troubleshoot: Can't Type in Launchpad"
date: 2016-01-22T21:08:09.000Z
date_updated:   2016-01-22T21:08:09.000Z
comments: false
---


**Problem: **Every time I open Launchpad to open an app, the search bar won’t accept any text I input. I’ve tried clicking the search bar to no avail.

**Solution:**

The accepted solution to this problem is to press tab alot. Sometimes Launchpad doesn’t give proper focus to the search bar, so your key inputs are lost somewhere in the background. Tab should either focus or de-focus the search bar. The general rule of thumb is if you can see the blinking character indicator, you should be able to type in the search bar.

Some remedies prescribe 20+ consecutive tab hits, but I’m not sure it matters once you get above 5 or 6.

*If you’ve tried the above step and it hasn’t worked…*

I tried the above and it didn’t work, so I tried something a little different. I logged out (I know, a pain) with the option to retain windows turned off. I then logged back in and repeated the above steps. Although it didn’t work for me the first time, the above worked flawlessly after logging out/in.

Your mileage may vary, but these steps are by far the most simple solution to this irritating problem.



