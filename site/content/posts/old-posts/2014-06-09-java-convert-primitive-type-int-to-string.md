---
title: "Java: Convert Primitive Type int to String"
date: 2014-06-09T13:16:14.000Z
date_updated:   2014-06-09T13:16:14.000Z
comments: false
---


**Problem: **I have a primitive int that I want to turn into a string

**Solution:**

Eclipse hates it when you try to pass in a primitive int as a String.  You’ll get warnings if you try to cast it to a string or call .toString() on it.

The easiest way to get around this is to simply concatenate “” at the end.  This’ll allow methods/variables to read the primitive int as a string.

Example:

int aNum = 9;

String bNum = aNum + “”;



