---
title: "IBM Bluemix: How to Manually Deploy Cloud Foundry App from Git Repo"
date: 2016-08-19T14:17:44.000Z
date_updated:   2016-08-19T14:17:44.000Z
comments: false
---


**Problem: **I’ve been using IBM Bluemix’s Cloud Foundry to host my NodeJS app, but I can’t get the code to redeploy. I’ve already attached the repo and it was automatically redeploying whenever I pushed a new commit. For some reason, it’s no longer deploying automatically – how do I manually tell Bluemix to deploy the current state of my repo?

**Solution: **

*Note: **I’m assuming that you’ve already connected a git repo to the Cloud Foundry instance on Bluemix. If you haven’t, check the top right corner of your app’s dashboard and select the “Add Git” button.*

To manually deploy your repo to the server:

- Go to your Bluemix Dashboard
- **Select your App** from the list of Applications. This will bring you to your app’s dashboard.
- From your App’s dashboard, **click your GIT URL** located in the top right corner. This will send you to your git repository.
- Ensure that this is actually the repo you want to deploy, then **select Build & Deploy** from the top right.
- This will send you to your App’s build Pipeline page. From here, you can modify the stages your app goes through during deployment.
- Select **‘Add Stage’**.
- Select the **‘Jobs’** tab.
- Click **Add Job**
- In the Job Type dropdown,** select Deploy**.
- **Select Save** to add that stage to your Pipeline.
- On the new deployment stage you just created, you should see a Play button [sideways triangle] in the upper right hand corner of the stage’s card. **Click the Play button to begin deployment** of your repository.
- A ‘Jobs’ section should appear on the card to show the status of deployment. When the job says ‘succeeded’, your app has been deployed successfully.



