---
title: "Angular: Select not Showing Options"
date: 2015-07-30T01:41:55.000Z
date_updated:   2015-07-30T01:41:55.000Z
comments: false
---


**Problem: **I’m trying to create a <select> element in my Angular app and I can see the select in the page, but none of the options appear inside of it.

**Solution: **Make sure you’ve added options to populate your selector and an ng-model variable to hold the selected option.  If you don’t have the options, there won’t be anything for the selector to pull in.  If you don’t have an ng-model, Angular won’t display anything inside of your selector.

Example:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/42b1111f2b4d09021a3a.json"></div>[[https://gist.github.com/SIRHAMY/42b1111f2b4d09021a3a](https://gist.github.com/SIRHAMY/42b1111f2b4d09021a3a)]

Remember, the options are usually pulled in from a $scope.variable created in a controller.  The ng-model variable will be accessible in any controller that has access to the surrounding select element.



