---
title: "Mac: Run Dropbox on Seagate External Drive"
date: 2016-07-04T10:40:37.000Z
date_updated:   2016-07-04T10:40:37.000Z
comments: false
---


**Problem: **I just bought a new Mac, but it only has 128GB of space. After installing all the base programs I’m going to use, I’ve only got 80 gigs left. I’m worried that I’ll soon run out of space. I have a Dropbox account that has 1 tb of storage, but those files have to be downloaded to my machine to be accessible which means I’m limited to the amount of space available on my machine. I have a 1 tb external that I could use to sync the Dropbox files down to, but I’m not sure how to use it. Is there an easy way to go about this?

**Solution: **I’m assuming you’ve already downloaded and installed the [required Mac Paragon driver](http://www.seagate.com/support/downloads/item/ntfs-driver-for-mac-os-master-dl/). This basically allows you to read and write files to and from the external drive.

To setup Dropbox to work with your drive:

- **Click the Dropbox icon** on the topbar
- **Click the gear** in the lower right corner of the dropdown
- **Select Preferences**
- On the General page, **deselect “Start Dropbox on system startup”**. This prevents issues where Dropbox may start before your external drive is connected. Worst case, this causes file deletion, but usually it would just create a bunch of warning popups.
- On the Account page, **change the location of the dropbox folder** by clicking the dropdown next to “Dropbox location”. - In the dropdown, click “Other” to browse for your external hard drive.

Once that is done, Dropbox should begin moving the Dropbox folder files into your hard drive.

*Note: You will now have to manually start Dropbox after your drive is connected*.

Don’t worry about your hard drive coming unconnected while you’re working and causing mass file deletions. If Dropbox can’t find its folder, it will issue a popup asking whether you want to relink a new folder or close the app. You should close the app then reconnect your external before restarting Dropbox. Once restarted, it will run as normal.



