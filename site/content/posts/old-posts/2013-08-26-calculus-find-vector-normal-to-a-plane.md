---
title: "Calculus: Find Vector Normal to a Plane"
date: 2013-08-26T05:40:16.000Z
date_updated:   2013-08-26T05:40:16.000Z
comments: false
---


To find a vector normal (perpendicular) to a plane, take the coefficients of the plane in equation form.

Ex: a plane is defined as 3x + 7y – 5z = 21

A vector normal to this plane is N = (3, 7, -5)



