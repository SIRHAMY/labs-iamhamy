---
title: "VS Code: `Select: Python Interpreter` Not Showing Anaconda Envs"
date: 2019-08-19T19:01:02-04:00
tags: [
    "python",
    "anaconda",
    "vs-code",
    "troubleshoot"
]
comments: false
---

# problem

I'm using anaconda to create Python virtual environments for cause #bestpractices. I created a new conda environment with `conda create -n MYENV` but when I go to the `Python: Select Interpreter` option from the command palette, MYENV isn't there. How do I get it to show up?

# solution

In all likelihood, the environment was successfully created and VSCode is just holding onto stale data. To resolve, you should be able to either close and re-open vscode or select the `Reload Window` option from the command palette.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).