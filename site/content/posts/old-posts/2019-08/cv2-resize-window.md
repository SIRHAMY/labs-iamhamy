---
title: "cv2: How to resize window"
date: 2019-08-26T03:12:25-04:00
tags: [
    "python",
    "opencv",
    "cv2",
    "window",
    "tutorial",
    "image"
]
comments: false
---

# problem

I'm using Python and the library `cv2` (`opencv-python`) to display an image in a window but my image is super big and going off the screen. How do I resize my window so the image stays on the screen without distorting it?

# solution

Here's an example:

```
window_width = 600
window_height = 600

cv2.namedWindow("my_window_name", cv2.WINDOW_NORMAL)
cv2.resizeWindow("my_window_name", window_width, window_height)
cv2.imshow("my_window_name", image_to_show)
```

Basically we're creating a `namedWindow` which allows us to identify and modify that window later. We then mutate it with `resizeWindow` and show our image in it with `imshow`.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).