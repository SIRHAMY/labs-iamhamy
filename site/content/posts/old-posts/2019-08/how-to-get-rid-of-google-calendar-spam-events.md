---
title: "Google Calendar: How to remove spam events (and keep them from appearing again)"
date: 2019-08-28T08:47:36-04:00
tags: [
    "google",
    "calendar",
    "spam",
    "security",
    "tutorial"
]
comments: false
---

# problem

I'm using Google Calendar for most of my calendaring needs but recently there have been a bunch of spam events popping up on it, touting deals for free iPhones and such. I really love Google Calendar so don't want to switch and I like that it's usually pretty good at guessing which events I'm going to based on my emails so preferably I'd like to keep that functionality as well. How can I remove these spam events (and keep them from happening) while also keeping all the functionality of Google Calendar that I love?

# solution

Like all spam, there's really no one size fits all solution to keep it out (except closing your address entirely). But there are a few things you can do to achieve both outcomes with a high success rate.

## how to remove the spam events (and keep them from appearing again)

* Go to Google Calendar
* Select the spam event
* Click the hamburger / options button (three dots) to show the options dropdown
* Select `Report as spam`

By doing this, you are able to remove the event (and all recurrences) as well as give Google data that events like that one are spam. This allows them to feed that new data back into their spam filters to better protect you (and everyone else!) against similar attacks.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).