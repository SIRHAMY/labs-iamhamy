---
title: "Kubernetes: Failed to pull container image from Gitlab registry"
date: 2019-08-12T19:58:21-04:00
tags: [
    "k8s",
    "GKE",
    "containers",
    "kubernetes",
    "gitlab"
]
comments: false
---

# problem

I'm running Kubernetes on Google's Kubernetes Engine (GKE) and I'm using Gitlab CI for, well, continuous integration. I just spun up a new service within GKE but when it tries to pull the image from the Gitlab repository, it's failing with `Failed to pull image "registry.gitlab.com/PATHTOREGISTRY:TAG": rpc error: code = Unknown desc = Error response from daemon: manifest for registry.gitlab.com/PATHTOREGISTRY:TAG not found`. I've removed the personal path to registry, but that's the path that I see from the repo's Packages > Container Registry screen.

# solution

So there are a ton of reasons why this could be happening but here are some things you should check first:

1. Is your repo public? If your repo is not public, did you give k8s the right credentials to be able to access it?

2. Are you _sure_ that the PATHTOREGISTRY:TAG combo are correct? To check this, you can go into your repo's `Packages > Container Registry` screen.

To see if it's the right PATHTOREGISTRY, there should be an icon to copy the url so just compare that to the PATHTOREGISTRY you have.

If that's right, click the dropdown to see all your containers and find the container with the TAG you have. If you can't find a container with that TAG then that's your problem.

If you did find it but k8s still can't find the container, try manually forcing a rolling update so k8s has to try again and if that fails re-check your repo's access permissions such that the container repository is available for external access via your repo's `Settings > General > Visibility, project features, permissions` tab.


# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).