---
title: "Troubleshoot: Pokemon Go on Android GPS Not Found Error"
date: 2016-07-08T08:11:50.000Z
date_updated:   2016-07-08T08:11:50.000Z
comments: false
---


**Problem: **I’m trying to play Pokemon Go, but when I open the app, a big red banner drops down saying ‘GPS Not Found’. I’m running the game on my Android phone and it seems like the GPS is getting through because my character seems to be standing in the correct location. While the banner is up, it looks like I can’t find Pokemon. I want Pokemon. How do I fix this?

**Solution: **Because you say it looks like your character is in the correct position, I’m going to assume that you have some sort of network connection. Before going through the rest of this troubleshooting guide, make sure you do have some sort of network connection. If not, fix that and see if you still have issues.

If you have ensured that you have a network connection, but are still receiving the error:

- Press the Home key to go back to your smartphone’s home screen
- Navigate to your apps and open Google Maps
- Once Google Maps is open, make sure that it can locate you accurately. You can do this by cross-referencing the blue dot location (indicating where it thinks you are) with where you think you are. If Maps isn’t locating you correctly, check your network connection.
- Kill Pokemon Go
- Restart Pokemon Go
- Your GPS issues should now be solved.

It seems that Pokemon Go piggy-backs off of some form of the Google Maps/Android GPS functionality. So if Maps is closed, it may have issues locating you.



