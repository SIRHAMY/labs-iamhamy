---
title: Change Georgia Tech Computer Science Concentration through Buzz Port
date: 2014-11-04T22:04:13.000Z
date_updated:   2014-11-04T22:04:13.000Z
comments: false
---


This is a quick guide to navigating your way through Buzz Port in order to change your Computer Science degree threads.  This may also work for other majors, but no guarantees.

- Log in to Buzz Port
- Click the **Registration – OSCAR** link
- Click **Student Services & Financial Aid**
- Click **Student Records** then **Change Program of Study**

From here, you should be able to select your major then change your thread combinations using a pull down menu.



