---
title: "Associated Credit Union: Guide to Filling out Deposit Slip"
date: 2014-07-21T13:21:24.000Z
date_updated:   2014-07-21T13:21:24.000Z
comments: false
---


*This guide will show you how to fill out the unreasonably complicated deposit slips provided by Associated Credit Union branches.*

Name: Put your name, or the name on the account.

Share Number: The account number you have – the preceding 1100000 indicate a checking account.

Member Signature: Sign

Date: The current date.

 

Now you’ll see two columns, one with a list of numbers and another with a list of categories followed by ID.

**Left Column**

In the left hand column, list the check numbers, then the dollar amount, and cent amount associated with each check.  Find the total of these checks and place them in the Sub Total field.  If you would like to pull money out, list that in the Less Cash Received field.  Now place the result of subtracting the amount in the Less Cash Received field from the Sub Total into the Total Deposit field.

**Right Column**

In this section, you will decide which parts of the account will get the money.  For instance, if I wanted everything in the checking account, I’d put my checking account number next to Checking ID, then put the total amount to the right of that section.  Split the money up as you wish.



