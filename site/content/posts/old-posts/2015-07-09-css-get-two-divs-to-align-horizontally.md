---
title: "CSS: Get Two Divs to Align Horizontally"
date: 2015-07-09T18:06:53.000Z
date_updated:   2015-07-09T18:06:53.000Z
comments: false
---


**Problem: **I want to align two of my divs horizontally (side by side), but I can’t figure out how.

**Solution: **Just add the style display: inline-block to your two divs.

In each div put:

> style=”display: inline-block;”

Source: http://stackoverflow.com/questions/4938716/align-div-elements-side-by-side



