---
title: "Calculus: Find the Equation for the Plane through Points (2, 4, 5), (1, 5, 7), and (-1, 6, 8)"
date: 2013-09-01T18:40:05.000Z
date_updated:   2013-09-01T18:40:05.000Z
comments: false
---


**Find the equation for the plane through points ( 2, 4, 5), (1, 5, 7), and (-1, 6, 8)**

P = (2, 4, 5), Q = (1, 5, 7), R = (-1, 6, 8)

Find the vectors PQ and PR by subtracting Q and R by P

PQ = (-1, 1, 2)                  PR = (-3, 2, 3)

Take the cross product of PQ and PR to get the weights a, b, and c for the equation for a plane: a(x-x0) + b(y-y0) + c(z-z0) = 0

PQxPR = -i – 3j + 1k

Plug in the weights to get

-1(x-2) – 3(y – 4) + 1(z-5) = 0

**Final: **x + 3y – z = 9



