---
title: "Ubuntu: Install open JDK 7"
date: 2014-04-15T21:02:38.000Z
date_updated:   2014-04-15T21:02:38.000Z
comments: false
---


Open a terminal and type:

sudo apt-get install openjdk-7-jdk

Follow terminal instructions if installation requests input/runs into a problem.

If installation completes correctly ‘done.’ should appear.

*If you just want to install JRE 7, replace ‘jdk’ with ‘jre’*



