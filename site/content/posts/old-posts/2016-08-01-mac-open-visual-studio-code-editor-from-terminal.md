---
title: "Mac: Open Visual Studio Code Editor from Terminal"
date: 2016-08-01T16:38:52.000Z
date_updated:   2016-08-01T16:38:52.000Z
comments: false
---


**Problem: **I use Visual Studio Code as my primary editor on my Mac. I’ve heard that there’s a way to open a directory in an editor through a Terminal command. How can I open a new editor window inside a directory from the command line?

**Solution: **

First, make sure that you already have the Visual Studio Code ‘code’ command installed in PATH. You can check this by:

- Open a terminal window
- Run ‘code -help’
- If you see a bunch of help printouts, it’s installed and you can skip to the last section.

If you did the above and saw something similar to ‘command code could not be found/resolved’, then code hasn’t been added to your path yet. To install it correctly:

- Open a Visual Studio Code window
- Open the Command Palette with CMD+SHIFT+P
- Type ‘Install’ and select “Shell command: Install ‘code’ command in PATH” - This will allow you to run the code command directly from terminal

Once you’ve finished the above, you’re now ready to open your first Visual Studio Code editor in a directory straight from the command line.

- Navigate to the directory you’d like to open an editor in
- Run ‘code .’ in Terminal

 



