---
title: "Physics: Calculate the Acceleration of a Sled Heading Down a Hill"
date: 2014-02-16T23:03:39.000Z
date_updated:   2014-02-16T23:03:39.000Z
comments: false
---


**Problem: Calculate the acceleration of a sled that is heading down a <span style="color:red;">40</span>° slope (one that makes an angle of <span style="color:red;">40</span>° with the horizontal). The coefficient of kinetic friction on ice is **μ<sub>k</sub> = 0.100.

First start by drawing a force diagram with positive X down the hill, parallel to the incline, and Y perpendicular to the surface.

Use the formulas Fk = force of kinetic friction =  μ<sub>k</sub>  * n  and Fnet = ma<sub>  
</sub>

Positive X: MGsin(40), Negative X: μ<sub>k</sub> * n

Positive Y: N, Negative Y: MGcos(40)

Now set Fnet = positive X – negative X

Fnet = MGsin(40) – μ<sub>k</sub> * n

Substituting MA for Fnet, we get:

Gsin(40) – μ<sub>k</sub> * Gcos(40) = A = 5.55 m/s^2

**Answer: A = 5.55 m/s^2**

 



