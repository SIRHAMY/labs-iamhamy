---
title: "Calculus: Compute a QR Factorization using the Householder Method"
date: 2013-11-11T22:37:22.000Z
date_updated:   2013-11-11T22:37:22.000Z
comments: false
---


**Compute a QR Factorization of A using the Householder Method.  Separately list all of the Householder reflections used along the way.**

A =

<table><tbody><tr><td>1</td><td>4</td><td>3</td></tr><tr><td>2</td><td>2</td><td>1</td></tr><tr><td>2</td><td>-4</td><td>1</td></tr></tbody></table>Start by finding the Householder reflection M with V1 = the first column of A

X = V1 = (1, 2, 2)

Y = ||X||e1 = (3, 0, 0)

U = (Y-X)/(||Y-X||) = (1/sqrt(3))(1, -1, -1)

M1 = I – 2 UU^t                                                           (^t denotes the matrix’s transpose)

M1 =

<table><tbody><tr><td>1/3</td><td>2/3</td><td>2/3</td></tr><tr><td>2/3</td><td>1/3</td><td>-2/3</td></tr><tr><td>2/3</td><td>-2/3</td><td>1/3</td></tr></tbody></table>**Solution: A = QR**

**R = M1A = **

<table><tbody><tr><td>3</td><td>0</td><td>7/3</td></tr><tr><td>0</td><td>6</td><td>5/3</td></tr><tr><td>0</td><td>0</td><td>5/3</td></tr></tbody></table>**Q = M1^t =**

<table><tbody><tr><td>1/3</td><td>2/3</td><td>2/3</td></tr><tr><td>2/3</td><td>1/3</td><td>-2/3</td></tr><tr><td>2/3</td><td>-2/3</td><td>1/3</td></tr></tbody></table>

