---
title: "Guide: How to Install Wordpress with BlueHost"
date: 2015-07-21T04:09:45.000Z
date_updated:   2015-07-21T04:09:45.000Z
comments: false
---


This post will guide you through installing WordPress on your site when using BlueHost as your hosting provider.  The steps in this post will probably be very similar to the steps required for other hosting services, but I’m limiting the scope to focus on BlueHost installs.

*Prerequisites*

Before going any further, make sure the following things are done.  If not, you will have to do them before you can complete this tutorial:

- Have a BlueHost account
- Have an active domain name
- Have an active hosting plan

*Installing WordPress*

- Login to your Bluehost account
- Navigate to **Hosting -> cPanel** in the navbar
- Under “Website Builders” click **Install WordPress**- This is going to bring you to another page where it’ll offer you several packages for installing WordPress.  We’re going to choose the FREE option here because I don’t want you spending any extra money and because I’m as much expertise as you need to accomplish this.

[![Install WordPress ](http://i2.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/07/BHWPInstall1-300x178.jpg?fit=300%2C178)](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/07/BHWPInstall1.jpg)

- Under “Do It Yourself (FREE)” click Install - This will bring you to the Mojo Marketplace which is where Bluehost does alot of its upselling on premium features.  You may find yourself here in the future if you’re looking for a premium WP theme or some other luxurious widget.
- Tell the system where to install your blog (I installed it in the /blog directory) - If you want your entire site to be a blog/managed by WordPress, you might want to leave the directory field blank.  This will make it so anyone putting your domain into the address bar will see your WordPress site immediately.
- If you’re building your own custom site and just want WordPress to take care of the blog, put something in the directory field.  This will tell Bluehost to install WP inside its own folder on the server, allowing it to be more or less independent of the rest of your site.  You might also be interested in my guide on How to Build an Angular Site Around a WordPress Blog.
- Install WP with Advanced Settings or standard settings. - Advanced settings let you create an administrator profile and change the name of the Blog before you log in, but you can always change this later.
- Click Install
- Wait for your Installation to complete.  There should be a big bar at the top of the screen showing progress.
- Once Installation is done, click on **Show Credentials**
- There will be an alert that says “Your WordPress install is finished!”.  **Click View**, which should be right next to this.
- On this page, it’ll have all of your credentials.  The URL is your blog’s web address where you can access it from anywhere on the internet – assuming you’ve made it publicly viewable.
- **Go to the Admin URL** to login using the username and password that was either provided or you set earlier.
- Enjoy your new, luxurious blogger lifestyle



