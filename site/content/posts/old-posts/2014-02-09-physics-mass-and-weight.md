---
title: "Physics: Mass and Weight"
date: 2014-02-10T00:46:04.000Z
date_updated:   2014-02-10T00:46:04.000Z
comments: false
---


**Problem: An astronaut weighing 193 lbs on Earth is on a mission to the moon and mars**

**A) **What would he weigh in newtons when he is on the Moon? The acceleration due to gravity on the Moon is one-sixth that on Earth.

First, we should convert the man’s mass to kg.  1 lb = 0.453592 kg, so we get the man’s mass as 87.543 kg

The formula for weight is w = mg, so we simply multiply the mass by gravity divided by 6

w = mg = (87.543 kg)(9.8 m/s^2 / 6) = 142.987 N

**Answer: **142.987 N

**B) **(b) How much would he weigh in newtons when he is on Mars, where the acceleration due to gravity is 0.38 times that on Earth?

Following the process above, we simply multiply the mass by 0.38 * gravity

w = mg = (87.543 kg)(9.8 m/s^2 * 0.38) = 326.010 N

**Answer: **326.010 N

**C) **What is his mass on Earth?

His mass on Earth is the same mass we’ve been using throughout the problem, so his mass = 87.543 kg

**Answer: **87.543 kg



