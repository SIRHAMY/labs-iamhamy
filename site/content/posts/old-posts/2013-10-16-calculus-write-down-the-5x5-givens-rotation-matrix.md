---
title: "Calculus: Write Down the 5x5 Givens Rotation Matrix"
date: 2013-10-16T06:04:15.000Z
date_updated:   2013-10-16T06:04:15.000Z
comments: false
---


**Write down the 5×5 Givens rotation matrix G(pi/3, 1, 4)**

**Solution:**

<table><tbody><tr><td>cos(pi/3)</td><td>0</td><td>0</td><td>-sin(pi/3)</td><td>0</td></tr><tr><td>0</td><td>1</td><td>0</td><td>0</td><td>0</td></tr><tr><td>0</td><td>0</td><td>1</td><td>0</td><td>0</td></tr><tr><td>sin(pi/3)</td><td>0</td><td>0</td><td>cos(pi/3)</td><td>0</td></tr><tr><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td></tr></tbody></table>

