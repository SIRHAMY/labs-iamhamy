---
title: "Calculus: Evaluate the Double Integral"
date: 2013-11-18T21:22:08.000Z
date_updated:   2013-11-18T21:22:08.000Z
comments: false
---


**Evaluate the integral ∫1-2 **∫y-y^2  dxdy **<**– That’s not a typo, there’s no given function to integrate

**Solution: **Start by integrating with x.  In this case, it basically means skipping integration and plugging in the bounds as if there were an x variable.

∫1-2  y^2 – y dy

Integrating with y gives us:

(y^3)/3 – (y^2)/2|1-2 => 8/3 – 4/2 – (1/3 – 1/2)

Simplify this to get:

**Answer: 5/6**



