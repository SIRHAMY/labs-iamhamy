---
title: "GIMP 2 Troubleshoot: Pasted Images Have Black Background"
date: 2016-01-21T02:06:32.000Z
date_updated:   2016-01-21T02:06:32.000Z
comments: false
---


**Problem: **I’m searching for images/icons on the internet to paste into one of my GIMP creations. However, every time I copy the image from my Chrome browser into the image, it fills in the background with black. The image looks to have a white or transparent background in the browser.

*Note: This solution is for Chrome browsers*

**Solution: **Unfortunately, it looks like Chrome browsers automatically fill in the background of images copied to the clipboard to black.

Here are two possible solutions:

1. Download the images to your computer before copying and pasting them into your GIMP creation
2. Use Firefox. *Caveat: it has been reported that Firefox users sometimes run into this issue as well and, even more flagrant, sometimes the background of images copied to the clipboard switches from black to white or vice versa.*

**Sources**

- [“Pasted images have black backgrounds”](http://gimpforums.com/thread-pasted-images-have-black-backgrounds)



