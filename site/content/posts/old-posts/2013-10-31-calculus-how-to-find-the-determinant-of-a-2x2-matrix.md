---
title: "Calculus: How to find the Determinant of a 2x2 Matrix"
date: 2013-10-31T04:59:35.000Z
date_updated:   2013-10-31T04:59:35.000Z
comments: false
---


**Find the Determinant of the Matrix A = **

<table><tbody><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></tbody></table>**Solution: ad – bc**



