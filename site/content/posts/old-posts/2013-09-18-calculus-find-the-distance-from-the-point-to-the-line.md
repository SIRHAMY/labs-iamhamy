---
title: "Calculus: Find the Distance from the Point to the Line"
date: 2013-09-19T03:11:42.000Z
date_updated:   2013-09-19T03:11:42.000Z
comments: false
---


**Find the distance from the point S(2,1,-1) to the line x = 2t, y = 1+2t, z = 2t**

We can tell from the equation of the line that L passes through P(0,1,0) parallel to v = 2i + 2j + 2k

Find the directional vector PS by subtracting P from S

PS = (2-0)i + (1-1)j + (-1 – 0)k = 2i – k

The equation for distance is d = (|PS x V|)/|V|

First we find the cross product of PS and V

PS x V = (2,0,-1) x (2,2,2)

We know cross products are evaluated as: |a b| = ad – bc  
 |c d|

=|0 -1|i – |2 -1|j + |2 0|k = 2i – 6j + 4k  
 |2  2|     |2  2|      |2 2|

PS x V = (2,-6,4)

V = (2,2,2)

d = (|PS x V|)/|V| = sqrt((2^2 + (-6)^2 + 4^2)/(2^2 + 2^2 + 2^2)) = sqrt(56/12)

**Final: Sqrt(14/3)**



