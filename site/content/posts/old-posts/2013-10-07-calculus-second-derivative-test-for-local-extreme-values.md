---
title: "Calculus: Second Derivative Test for Local Extreme Values"
date: 2013-10-07T05:40:49.000Z
date_updated:   2013-10-07T05:40:49.000Z
comments: false
---


For this test to work, your two-variable function f(x,y) and its first and second partial derivatives must be continuous throughout a disk centered at point (a,b).  Also, the partial derivative of x at (a,b), f’x(a,b) must equal the partial derivative of y at (a,b), both of which must equal 0.

So, basically f’x(a,b) = f’y(a,b) = 0

1. The function f has a local maximum at (a,b) if f’xx < 0 and f’xx * f’yy – (f’xy)^2 > 0 at (a,b)

2. Local minimum at (a,b) if f’xx>0 and f’xx*f’yy – (f’xy)^2 > 0 at (a,b)

3. Saddle point at (a,b) if f’xx*f’yy – (f’xy)^2 <0 at (a,b)

4. The test is inconclusive at (a,b) if f’xx*f’yy – (f’xy)^2 = 0 at (a,b)



