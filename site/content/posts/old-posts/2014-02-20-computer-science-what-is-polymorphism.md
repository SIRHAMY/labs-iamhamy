---
title: "Computer Science: What is Polymorphism?"
date: 2014-02-20T15:23:00.000Z
date_updated:   2014-02-20T15:23:00.000Z
comments: false
---


Polymorphism is, essentially, the attribute that allows subclasses to inherit functionality from their superclasses while retaining the ability to implement their own, unique behaviors.  This is important because it keeps you from having to recreate every single method in each instance of subclass, instead being able to reuse the functionality you’ve previously coded.  Polymorphism differs from inheritance in that it focuses on the ability for items to be inherited and how they’re inherited whereas inheritance is the actual action of inheriting behavior.

We can create an example of the polymorphism attribute by making an animal class and a mammal class that extends it.

Our animal class may have the methods:

getEnergy() and reproduce() because all animals have those same basic functions (with a few exceptions of course).

Now all mammals get energy in some ways and reproduce, but the way they do such things differs a bit from the rest of the animal kingdom.  For instance, we would probably override the reproduce() method and modify its contents so that the result is live young (versus eggs).  Moreover, we would add a method raiseYoung() because all mammals nurse their babies.

The mammal class inherits a bunch of functionality from the animal class, but it also modifies the existing content and adds additional features.



