---
title: "WireShark: Filter results by IP Address"
date: 2015-08-26T04:29:06.000Z
date_updated:   2015-08-26T04:29:06.000Z
comments: false
---


**Problem: **I’m using WireShark to monitor the packets going in and out of my device, but I only want to see the results from a specific IP Address.

**Solution: **

WireShark provides a bunch of [display filters](https://wiki.wireshark.org/DisplayFilters), but here are the two pertinent ones for filtering by IP:

To filter by source IP:

> ip.src==INSERTIPADDRHERE

To filter by destination IP:

> ip.dst==INSERTIPADDRHERE



