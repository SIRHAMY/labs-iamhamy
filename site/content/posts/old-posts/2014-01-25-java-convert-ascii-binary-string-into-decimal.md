---
title: "Java: Convert ASCII binary string into Decimal"
date: 2014-01-25T20:32:21.000Z
date_updated:   2014-01-25T20:32:21.000Z
comments: false
---


**Objective: **Convert a string of ASCII binary values into decimal value in integer data type.

Here’s the solution I concocted:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/8626697.json"></div>(https://gist.github.com/SIRHAMY/8626697)

**Logic: **Starting from the beginning of the string, we check to see if the ASCII character is equal to 1.  If it is, we determine how much value that one represents by updating the scalar to 2^(positions to the right of the character) then adding it to the result.  If the character happens to be the last character in the string, then we simply add one to the pending result.



