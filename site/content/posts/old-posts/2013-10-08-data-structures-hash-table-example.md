---
title: "Data Structures: Hash Table Example"
date: 2013-10-09T01:09:49.000Z
date_updated:   2013-10-09T01:09:49.000Z
comments: false
---


Collision Handling: External Chaining*  
 Max Load Factor: Not given**  
 Hash Function: Sum of individual digits  
 Data: 555, 2110, 0004, 1332, 2424, 839, 891, 033, 787, 3851

<span style="text-decoration:underline;">Data</span> -> <span style="text-decoration:underline;">Hash</span>

555 -> 15  
 2110 -> 4  
 0004 -> 4  
 1332 -> 9  
 2424 -> 12  
 839 -> 20  
 891 -> 18  
 033 -> 6  
 787 -> 22  
 3851 -> 17

Given Hash Table of length 10

HashTable[0] -> 839  
 HashTable[1] ->  
 HashTable[2] -> 2424 -> 787  
 HashTable[3] ->  
 HashTable[4] -> 2110 -> 0004  
 HashTable[5] -> 555  
 HashTable[6] -> 033  
 HashTable[7] -> 3851  
 HashTable[8] -> 891  
 HashTable[9] -> 1332

*****In external chaining you simply add the data that hashes to the same spot in the Hash Table to the array of values located there.  For instance, both 2424 and 787 hashed to slot 2, so 787 was added as the next value of 2424.

**It is not common for a hash table to have no maximum load factor.  In this example, the load factor isn’t necessarily needed because we are using external chaining and we have a rather small amount of data, but it is suggested your hash table always have a max load factor.



