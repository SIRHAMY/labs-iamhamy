---
title: N-Type Gates vs. P-Type Gates
date: 2014-02-17T00:40:02.000Z
date_updated:   2014-02-17T00:40:02.000Z
comments: false
---


Gates are a fundamental part of creating useful circuits.  These gates either allow or block voltage from flowing through them based on the input given them.   I won’t go into what makes each type of gate work, but I will detail how they work and ways to manipulate them to your will.

**N-Type**

N-Type gates are normally open, meaning no electricity gets through them.  When the gate is given a voltage, then the gate becomes closed (as in closing the circuit) and thus acts like a piece of wire, allowing voltage to flow freely.

**P-Type**

P-Type gates are complimentary to N-Type gates in that they perform the exact opposite function.  These gates are normally closed, meaning a P-Type gate acts like a piece of wire when supplied with 0 volts.  Giving the gate a voltage will cause it to be open, breaking the circuit and blocking energy flow.



