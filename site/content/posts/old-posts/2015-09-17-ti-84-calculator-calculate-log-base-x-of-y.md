---
title: "TI 84 Calculator: Calculate Log Base X of Y"
date: 2015-09-17T05:43:38.000Z
date_updated:   2015-09-17T05:43:38.000Z
comments: false
---


**Problem: **I have a graphing calculator and I want to calculate the log with base X of the number Y.  For instance, maybe I want to calculate the log with base 4 of 16.

**Solution: **According to [MathBits](http://mathbits.com/MathBits/TISection/Algebra2/logarithms.htm), in order to do this, all we have to do is use the Change of Base formula:

> Log of base X of Y = log(our number)/log(base) = log(Y)/log(X)

*As long as the bases are the same.*



