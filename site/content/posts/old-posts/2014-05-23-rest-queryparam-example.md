---
title: "REST: @QueryParam Example"
date: 2014-05-23T12:54:22.000Z
date_updated:   2014-05-23T12:54:22.000Z
comments: false
---


This is a simple @QueryParam that utilizes the REST/JAX-RS APIs.

Here’s the code:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/b54ed86b7f5c56df0ead.json"></div>(https://gist.github.com/SIRHAMY/b54ed86b7f5c56df0ead)

The @GET tag denotes which HTTP method should be used to access the Java method.

@Path tells the program how the method will be called.

@Produces tells it what kind of output will be produced.

The @QueryParam tells the program how the parameter will be passed in.  By giving it “name”, we tell it how that parameter will be referred to in the URI.

Assuming my project is called TestProject and my class name is Learning (their @Paths match their name), we can call the method and pass in a name as such:

localhost:NUM/TestProject/rs/Learning/getMyInfoXML?name=Hamilton

Your address will probably differ a bit as the /rs/ is defined in my XML file and the NUM should be replaced with the portnumber (I think?) that Tomcat is running from.  Of course, you may not even be using Tomcat so disregard that if you aren’t.



