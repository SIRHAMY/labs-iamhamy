---
title: Security vs. Accessibility
date: 2014-06-09T09:59:42.000Z
date_updated:   2014-06-09T09:59:42.000Z
comments: false
---


I write guides for myself all the time.  If I have to look something up or explain a process to someone, more often than not, I’m going to create a guide.  I use this tactic for content creation because it reveals an obvious, unsatisfied need.  If there’s demand, there’s an opportunity.

I currently work at a large corporation in IT and often come across problems that no one in my department has a solution for.  So, every time a solution is found, I document it to, hopefully, save the next guy a few hours of frustration.

Therein lies the issue.  I like to publish everything online, so it can be found by everyone who has experienced the same issue.  However, many of the tools we use have been tailored to our office use which means many of the work-arounds may not work.  Uploading the document to an internal wiki is inefficient as there are no tools that effectively search through all of our storage locations, and for good reason.  Being discreet is one of the best ways to protect sensitive information.  Just try googling a search term along with the term “classified” and tell me how many sensitive documents are returned.

Moreover, in a large corporation like this, there are literally thousands of different releases for any one piece of software/documentation.  If some pieces are left unindexed or others naturally have a higher spot in the results queue, you may end up with the wrong version, opening the door for mistargeted software.

I’ve already established that hosting guides on my own Google-indexed site is more efficient than an in-house server.  The problem, of course, is that everyone can get to it.  Being pretty new to the company and the Tech industry in general, it’s hard to tell what could be sensitive information.  Solving errors that appear in Eclipse probably isn’t such a big deal, but providing a guide on how to access the database (even without passwords) might give evil-doers insight into our server architecture and security systems.

Essentially, the internet is great and I do what I can to make it better.  However, the unparalleled accessibility it provides can also lead to bad, albeit unintentional, consequences.  Share your knowledge, but take precautions to keep it out of the hands of the malicious.



