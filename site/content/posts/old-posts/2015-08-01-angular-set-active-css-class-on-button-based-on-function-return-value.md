---
title: "Angular: Set Active CSS Class on Button Based on Function Return Value"
date: 2015-08-01T14:55:00.000Z
date_updated:   2015-08-01T14:55:00.000Z
comments: false
---


**Problem: **Buttons have a different aesthetic when they’re set to active.  Some may be colored blue and others look ‘depressed’ (pushed) to symbolize they’ve been activated. Dynamically allocating these CSS classes can be a pain – here are a few ways to go about it in AngularJS.

**Solution: **

*Using ng-model*

The first way to go about this is to give the button an AngularJS directive ng-model variable.  ng-model tells the element that its value will be equal to the value of the variable you assigned to it.  So you might have a variable that holds true or false based on whether or not the element should be active.

This can become unwieldy when you get to deeper application logic as you have to explicitly set that variable when you want the element to appear active.  When you try to scale this technique to multiple elements, you’re forced to keep up with the state of multiple variables to show a few elements on the screen.

*Using ng-class*

The AngularJS directive ng-class gives you the ability to add a CSS class to an element based on the value of a variable or function.  The key part here is that it can rely on the return value of a function to apply a CSS class whereas ng-model can only be bound to a variable.  This gives us a certain amount of abstraction from the variable juggling we had to do in the previous solution.  It also gives you more control over what stylings are added based on different values.

**Example: **

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/7916569982874029c28a.json"></div>[[https://gist.github.com/SIRHAMY/7916569982874029c28a](https://gist.github.com/SIRHAMY/7916569982874029c28a)]



