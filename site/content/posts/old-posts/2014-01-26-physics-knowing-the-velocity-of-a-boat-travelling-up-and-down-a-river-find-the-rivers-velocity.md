---
title: "Physics: Knowing the Velocity of a Boat Travelling Up and Down a River, Find the River's Velocity"
date: 2014-01-26T15:21:11.000Z
date_updated:   2014-01-26T15:21:11.000Z
comments: false
---


**Question: **A boat takes 4 hours to travel 30 km down a river, then 9.5 hours to return.  How fast is the river flowing?

Dx = 30 km  <- this is the distance the boat travels each time  
 T1 = 4 hrs <- Time for first trip  
 T2 = 9.5 hrs <- Time for second trip

We can assume that the boat’s velocity is constant as no other information is given.

First, we must determine the velocities for trip one and two so that we can compare them.  Remember, velocity is equal to distance over time.

V1 = 30km/4hrs = 7.5km/hr

V2 = 30km/9.5 hrs = 3.158 km/hr

We know that V1 = Vb + Vr = (velocity of the boat) + (velocity of the river), and that V2 = Vb – Vr, so the velocity of the boat (Vb) can be determined as such: Vb = V1 – Vr or Vb = V2 + Vr.

Solving for the inequality, we get: V1 – Vr = V2 + Vr  
 V1 – V2 = 2Vr  
 (7.5km/hr – 3.158km/hr)/2 = Vr

**Answer: **2.171 km/hr

 



