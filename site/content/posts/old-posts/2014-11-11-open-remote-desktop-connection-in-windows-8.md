---
title: Open Remote Desktop Connection in Windows 8
date: 2014-11-12T00:41:27.000Z
date_updated:   2014-11-12T00:41:27.000Z
comments: false
---


Remote Desktop Connection is a Windows (also available for mac) program that allows you to remotely control another system, provided that you have the correct credentials and address for said system.

In most Windows environments, you can find the program by following this chain of actions:

**Start -> Accessories -> Communications -> Remote Desktop Connection **

In most Windows 8 environments, there is no Accessories option.  **So, in order to access Remote Desktop Connection, simply press start and go to your apps page.  Click the search bar and type in “Remote Desktop Connection”**.  At the top of the list of returned results, should be the app you are looking for.  Happy remoting!



