---
title: "Mac: Keep Computer from Falling Asleep with Caffeinate"
date: 2016-04-22T12:24:37.000Z
date_updated:   2016-04-22T12:24:37.000Z
comments: false
---


**Problem: **I have some long-term processes that I want to run for an extended period of time. However, this time frame is longer than my Mac’s set sleep time based on inactivity. How can I ensure that my processes continue to run and that my Mac doesn’t become dormant?

**Solution: **Luckily, most Macs come pre-installed with caffeinate, a command line tool that keeps your computer from falling asleep. This means that you can turn off the screen or even close the laptop lid and your computer will continue to run normally.

In order to enable caffeinate, open a new terminal window and type:

> caffeinate

This will run the caffeinate tool in this terminal, keeping your computer awake.



