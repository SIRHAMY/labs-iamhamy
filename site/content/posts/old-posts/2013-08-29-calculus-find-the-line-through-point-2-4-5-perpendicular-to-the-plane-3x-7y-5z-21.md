---
title: "Calculus: Find the line through point (2, 4, 5), perpendicular to the plane 3x + 7y - 5z = 21"
date: 2013-08-29T19:50:00.000Z
date_updated:   2013-08-29T19:50:00.000Z
comments: false
---


**Find the line through point (2, 4, 5), perpendicular to the plane 3x + 7y – 5z = 21**

Find the normal vector of the the plane by taking the coefficients of the equation for the plane

Normal vector = (3, 7, -5)

Because the normal vector is perpendicular to the plane, we can use it as the direction vector for the line.  Using the given point and the normal vector, we can find the equation for the line.

L: (2, 4, 5) + t(3, 7, -5)

In parametric form:

x = 2 + 3t  
 y = 4 + 7t  
 z = 5 – 5t



