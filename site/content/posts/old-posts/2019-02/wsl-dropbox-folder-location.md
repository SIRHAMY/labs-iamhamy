---
title: "Windows Subsystem for Linux: Dropbox Folder Location"
date: 2019-02-25T22:08:22-05:00
tags: [ "dropbox", "wsl", "linux" ]
comments: false
---

# Problem

I'm using [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/faq) for dev purposes and am trying to access my Windows-installed Dropbox folder. Dropbox is installed on my C:/ drive but I don't know how to get to it from the Ubuntu prompt. `ls ~` returns with nothing so I'm not exactly sure where it would be.

# Solution

All Windows drives will be mounted (read: available) in WSL's `/mnt` directory. So the `c` drive will be available at `/mnt/c` and the `f` drive will be available at `/mnt/f`. 

A typical Dropbox install will go to `C:/Users/USERNAME/Dropbox` so you should be able to access it via WSL with `cd /mnt/c/Users/USERNAME/Dropbox`