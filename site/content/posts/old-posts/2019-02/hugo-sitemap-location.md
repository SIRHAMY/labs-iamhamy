---
title: "Hugo: Where is my sitemap located?"
date: 2019-02-18T22:42:09-05:00
tags: [ "hugo", "sitemap", "tutorial", "troubleshoot" ]
comments: false
---

# Problem

I'm generating my blog with [Hugo (the static site generator)](https://gohugo.io/) and want to take a look at my google search rankings via the [Google Search Console](https://search.google.com/search-console/about). Where does my sitemap.xml get generated to?

# Solution

Hugo is highly customizable (one of its virtues!) so the answer really depends on how your site (and in particular the theme you're building with) is configured. [You can check the Hugo docs to learn more.](https://gohugo.io/templates/sitemap-template/) That being said, the majority of sites will have their sitemaps generated to

```
SITEBASEURL/sitemap.xml
```

For example, I currently run https://labs.hamy.xyz and its sitemap is located at https://labs.hamy.xyz/sitemap.xml. Go ahead, take a look!

If you can't find it in one of those locations, your best bet is to go searching for `sitemap` within your configured theme to see if you can't figure out where they re-routed the output.

Happy hunting!

-HAMY.OUT
