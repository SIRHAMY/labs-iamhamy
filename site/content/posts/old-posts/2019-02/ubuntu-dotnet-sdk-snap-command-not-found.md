---
title: "Ubuntu: resolve dotnet-sdk snap `command 'dotnet' not found`"
date: 2019-02-11T13:36:27-05:00
tags: [ "ubuntu", "tutorial", "dotnet-sdk", "dotnet-core", "snap", "troubleshoot" ]
comments: false
---

# Problem

I'm trying to install and run `dotnet-sdk` on Ubuntu but after I run `sudo snap install dotnet-sdk`, I keep getting the error `command 'dotnet' not found`.

I've tried searching around and the best answers have told me to symlink the dotnet binaries / add to path but those haven't worked. I sanity checked and closed my terminals / restarted my computer but that didn't work either. How can I resolve this?

# Solution

Believe it or not, this is actually expected default behavior when `dotnet-sdk` is installed via snap ([see discussion](https://github.com/dotnet/core-setup/issues/4230#issuecomment-446579045)). The reason for this is that snap tries to ensure that all its packages and their respective functions are properly namespaced. So the `dotnet` command you're seeing used around the web, will actually be namespaced to `PACKAGE.COMMAND` or `dotnet-sdk.dotnet`. If you run that, you should get the expected command line output.

If you want to make it so that you can call `dotnet-sdk.dotnet` without the full namespace for ergonomics / consistency with code you'd find online, you can alias it to `dotnet` with `sudo snap alias dotnet-sdk.dotnet dotnet`