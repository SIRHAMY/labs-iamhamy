---
title: "Troubleshoot: JavaScript Uncaught TypeError: object is not a function"
date: 2016-01-12T18:53:24.000Z
date_updated:   2016-01-12T18:53:24.000Z
comments: false
---


**Problem: **I’m trying to run my JavaScript file, but I keep getting an “Uncaught TypeError: object is not a function” error being thrown. I’ve looked through the code and it looks fine to be. I’m setting a var equal to a function and calling that var with the correct parameters.

**Solution: **Check the var declaration that you used to set it equal to the function you’re calling. If you accidentally placed new in front of the function declaration, this error will be thrown.

For example, your code should look like this:

> var foo = function() {
> 
> }

Not this:

> var foo = new function() {
> 
> }

Happy coding!



