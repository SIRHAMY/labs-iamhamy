---
title: "Caches: Calculate the Number of Tag Bits"
date: 2014-11-05T01:23:34.000Z
date_updated:   2014-11-05T01:23:34.000Z
comments: false
---


Tag bits are left over bits you need to store with the block of data in order to differentiate that block from other blocks of data that could be stored in the same place.  You will always need tag bits with each block.

**Example: **Continuing from where the previous examples left off (click the links in the answer), we are given an 8-way set-associative cache, with 4 byte blocks, 1 kb total data, and addresses of 32 bits.

# tag bits = 32 – [# index bits](http://hamycodes.com/2014/11/05/caches-calculate-the-number-of-index-bits/ "Caches: Calculate the Number of Index Bits") – [# offset bits](http://hamycodes.com/2014/11/05/caches-calculate-the-number-of-offset-bits/ "Caches: Calculate the Number of Offset Bits") = 32 – 5 – 2 = 25 tag bits for each block



