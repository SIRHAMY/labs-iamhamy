---
title: "Dropbox Linux Application Broken"
date: 2018-09-04T21:26:45-04:00
description: "About the page"
draft: false
---

**Problem:** I use DropBox's desktop client for Linux to help keep all of my files backed up and synced across devices (and I'd highly recommend everyone do this, too!). This is awesome when it works, but for the past few weeks, every time I boot up the DropBox client, I just get a red DropBox logo with a warning sign on it saying that it can't sync my files. What gives? How do I fix this?

**Solution:** It's likely that your DropBox process/client doesn't have the requisite permissions to execute the synchronizer that, well, syncs all your files to their servers. Luckily, this is super easy to fix.

* If installed normally, you should have a DropBox icon in your system tray, click that to pull up an options menu
* If the issue is the DropBox process not having requisite permissions, there should be a button saying something like "The process cannot access the DropBox folder, click to fix"
* Fill in your administrator credentials so that DropBox can run as you and access all the files it needs