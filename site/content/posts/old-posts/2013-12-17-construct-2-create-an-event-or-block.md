---
title: "Construct 2: Create an Event 'Or' Block"
date: 2013-12-18T00:16:44.000Z
date_updated:   2013-12-18T00:16:44.000Z
comments: false
---


Construct 2 is a program that allows you to create your own applications by dragging and dropping objects and events.  Although it streamlines many processes, specifically bypassing the need for writing any code, some common functions can be hard to find.

In order to create an Event ‘Or’ Block:

1. Create the first condition (mouse clicked, touch on object, etc.)
2. Right click the Event block directly below the green arrow.  Don’t click anywhere else or the option won’t appear.
3. Click the “Make ‘Or’ Block” option
4. Right click on the event block
5. Click “Add Another Condition”
6. The next condition you add to the block should be connected via an or conditional to the original.



