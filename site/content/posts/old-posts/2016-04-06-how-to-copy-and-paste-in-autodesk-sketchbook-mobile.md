---
title: How to Copy and Paste in Autodesk Sketchbook Mobile
date: 2016-04-06T17:27:49.000Z
date_updated:   2016-04-06T17:27:49.000Z
comments: false
---


**Problem: **I’ve downloaded Autodesk Sketchbook app for my tablet, but I can’t figure out how to Copy + Paste. I would’ve figured this was a standard function in any digital art platform. How do I do it?

**Solution: **In order to copy and paste, you need to be able to select items – unless you’re going for entire layers.

Here’s a step-by-step guide to Copying and Pasting inside the app:

1. Choose the Selection tool from the toolbar!
2. Select the section of image you want to copy 
3. Tap on the layer in which the selection is held. One tap is sufficient to open the layer menu. Here, you see a variety of options. Click Copy on the layer with the selection, then navigate to the layer you want to paste into and repeat the process using Paste. 
4. Once pasted, a new action window will appear, allowing you to modify your newly-pasted layer. 
5. Copy and Paste to your heart’s content. 



