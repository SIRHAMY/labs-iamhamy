---
title: "Physics: A Mass is Attached to a Spring and set into Oscillation"
date: 2014-04-22T19:49:45.000Z
date_updated:   2014-04-22T19:49:45.000Z
comments: false
---


**Problem: **<span style="color:#000000;">A </span><span class="nobr" style="color:#000000;"><span style="color:red;">0.54</span> kg</span><span style="color:#000000;"> mass is attached to a light spring with a force constant of </span><span class="nobr" style="color:#000000;"><span style="color:red;">32.9</span> N/m</span><span style="color:#000000;"> and set into oscillation on a horizontal frictionless surface. If the spring is stretched </span><span class="nobr" style="color:#000000;">5.0 cm</span><span style="color:#000000;"> and released from rest, determine the following.</span>

M = 0.54 kg  
 K = 32.9 N/m  
 X = 5.0 cm = 0.05 m

<span style="color:#000000;">**(a)** maximum speed of the oscillating mass</span>

**Solution: **

We know that the conservation of mass equation is:

Etot =  KE + PE = MVmax²/2       *PE is 0 when KE is maximized, thus the right side of the equation*

Setting KE = 0 and, thus, maximizing PE, set PE = MVmax²/2

MVmax²/2 = KX²/2

Solve for Vmax to get:

**Answer: Vmax = 0.39 m/s**

<span style="color:#000000;">**(b)** speed of the oscillating mass when the spring is compressed 1.5 cm</span>

**Solution: **

X = 1.5 cm = 0.015 m

Use the conservation of mass formula from above, plugging in the respective values that we have found.  Solve for V (not Vmax) to get:

V = sqrt((m(Vmax²) – KX²)/M)

**Answer: V = 0.372 m/s**

<span style="color:#000000;">**(c)** speed of the oscillating mass as it passes the point 1.5 cm from the equilibrium position</span>

**Answer: Same as (b)**

<span style="color:#000000;">**(d)** value of </span>*x*<span style="color:#000000;"> at which the speed of the oscillating mass is equal to one-half the maximum value</span>

**Solution: **

Use the conservation of energy equation from above, substituting V for Vmax/2 and then solving for X

X = sqrt((M(Vmax²) – M(Vmax/2)²)/K)

**Answer: X = 0.043 m**



