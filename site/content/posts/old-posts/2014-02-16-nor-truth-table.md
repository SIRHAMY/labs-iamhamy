---
title: NOR Truth Table
date: 2014-02-17T00:46:38.000Z
date_updated:   2014-02-17T00:46:38.000Z
comments: false
---


Here’s a quick truth table for the NOR function.  NOR is a combination of NOT and OR, which, therefore, means that the output of such a function equals 1 only when OR would equal 0, thus NOT-OR.

 

<table style="width:300px;"><tbody><tr><td>A</td><td>B</td><td>OUT</td></tr><tr><td>0</td><td>0</td><td>1</td></tr><tr><td>0</td><td>1</td><td>0</td></tr><tr><td>1</td><td>0</td><td>0</td></tr><tr><td>1</td><td>1</td><td>0</td></tr></tbody></table>

