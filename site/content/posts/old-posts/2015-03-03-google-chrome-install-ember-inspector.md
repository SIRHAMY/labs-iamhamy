---
title: "Google Chrome: Install Ember Inspector"
date: 2015-03-04T01:33:08.000Z
date_updated:   2015-03-04T01:33:08.000Z
comments: false
---


This post will walk you through the short installation process of the Ember Inspector Extension on Google Chrome.

1. Navigate to [Ember Inspector’s page](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi) in the Chrome Web Store
2. **Click the blue button** that says “Free” to begin installation.
3. Once it’s installed, navigate to the **Chrome menu** (three bars in the top right), then click **Settings**.
4. Click on **Extensions** in the menu bar to the left.
5. Scroll down to Ember Inspector and **check the box** next to “Allow access to File URLs”.  This will ensure that Ember Inspector can work to its full potential



