---
title: "Complx: Installation Troubleshooting"
date: 2015-12-18T05:49:44.000Z
date_updated:   2015-12-18T05:49:44.000Z
comments: false
---


First off, you should download the Complx zip file from Github here: https://github.com/TricksterGuy/complx

Now that you’ve downloaded the files and put them some place logical in your file directories, open up the README.txt and follow the directions.  The creator has done a good job making the process as simple as possible.  Download, install, and input the commands he recommends.  Most everything will be to your benefit later on.

**Problems**

- Command not found when using “sudo ./install.sh”

First of all, you should make sure you’re in the correct directory (if you downloaded the Nautilus terminal and restarted your computer then opened it inside your complx directory, you should be fine).  To ensure you are in the correct directory, you can either look at the line of text before your input indicator and check that the last entry is “complx-master” or type “ls” and see if the file “install.sh” is present.  If you have made sure you’re in the correct directory, try inputting “sudo bash install.sh”.  This should override any problems and install the application.



