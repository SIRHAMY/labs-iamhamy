---
title: "Calculus: Find the Equation for the Plane through (1, -1, 3), Parallel to the Plane 3x + y + z = 7"
date: 2013-09-01T18:30:40.000Z
date_updated:   2013-09-01T18:30:40.000Z
comments: false
---


**Find the equation for the plane through point (1, -1, 3), parallel to the plane 3x + y + z = 7**

Plane P: 3x + y + z = 7

Normal N = (3, 1, 1)

We know that the Normal N is perpendicular to any direction on plane P

(x-x0) perpendicular to N

Therefore, (x-x0) times N = 0

Equation form of a plane: a(x-x0) + b(y-y0) + c(z-z0) = 0

Take N and multiply it by (x-x0, y-y0, z-z0) to get the equation for the plane parallel to plane P

(3, 1, 1) times (x-1, y+1, z-3) = 3x – 3 + y + 1 + z -3

**Final: **3x + y + z = 5



