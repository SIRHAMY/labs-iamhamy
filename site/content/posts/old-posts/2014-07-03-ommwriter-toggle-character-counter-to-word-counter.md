---
title: "OmmWriter: Toggle Character Counter to Word Counter"
date: 2014-07-03T18:29:43.000Z
date_updated:   2014-07-03T18:29:43.000Z
comments: false
---


You can toggle OmmWriter’s counter from counting characters to counting words, and vice versa, by simply clicking the counter itself.



