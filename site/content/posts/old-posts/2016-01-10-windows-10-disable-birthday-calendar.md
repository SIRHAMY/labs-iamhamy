---
title: "Windows 10: Disable Birthday Calendar"
date: 2016-01-10T17:32:37.000Z
date_updated:   2016-01-10T17:32:37.000Z
comments: false
---


**Problem: **I’m using Windows 10 and every day I get two or three notifications popping up telling me its someone’s birthday. Sometimes I know them, sometimes I don’t. Either way, it’s annoying and I either don’t care about the notification or have stored this information elsewhere – and more conveniently to boot. How do I turn off the Birthday Calendar/notifications?

**Solution: **To change this, you need to head over to outlook.com.

- Go to outlook.com
- Sign in to your account
- **Click the button in the top left** to bring down a list of apps to “Preview”. **Select the Calendar app.**
- **Click the Gear Symbol** in the top right to open a Settings drop down.
- **Click the Options button** in the drop down. This opens the Option menu.
- In the Options menu, **expand Calendar options** by clicking the arrow next to it.
- Select Birthday Calendar
- Select the “Turn off the birthday calendar” option
- Hit Save

Boom, no more pesky notifications for that guy-you-met-that-one-time’s birthday.



