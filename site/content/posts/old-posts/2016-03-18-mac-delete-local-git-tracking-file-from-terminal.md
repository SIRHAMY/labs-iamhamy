---
title: "Mac: Delete local git tracking file from terminal"
date: 2016-03-18T13:00:49.000Z
date_updated:   2016-03-18T13:00:49.000Z
comments: false
---


**Problem: **I just downloaded a seed/boilerplate repo from GitHub, but now I have the history of that entire repo inside my own project. How do I delete the local .git folder in my project directory so that I can essentially begin anew?

**Solution:**

- Open up your terminal
- Navigate to your project folder
- Input “ls -a” to ensure your .git file is in that directory (take it as a sanity check). The -a modifier at the end of ls allows us to see hidden files.
- If you’re sure you want to completely trash the current git tracking, type - rm -rf .git
- This should delete the .git folder and all of its contents
- To ensure your operation worked correctly: - “ls -a” again to see if the .git folder is still there
- “git status” – if it returns an error that says “fatal: NOt a git repository (or any of the parent directories): .git” then you’ve successfully deleted the git tracking for the project. *Note: this assumes that your project isn’t nested inside another directory that has git tracking enabled.*

Enjoy your clean slate.



