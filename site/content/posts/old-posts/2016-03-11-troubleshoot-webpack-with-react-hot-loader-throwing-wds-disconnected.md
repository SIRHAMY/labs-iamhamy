---
title: "Troubleshoot: Webpack with react-hot-loader Throwing [WDS] Disconnected!"
date: 2016-03-11T14:22:43.000Z
date_updated:   2016-03-11T14:22:43.000Z
comments: false
---


**Problem: **I’m using Webpack with react-hot-loader enabled to hot load all of my React components. However, when I hit my webpack-dev-server, I don’t get any hot reloads. It shows my app, but I have to manually refresh the page to see any changes. In the console it spits out a ‘[WDS] Disconnected!’ error.

**Solution: **Because you can see the site, but hot reload doesn’t work, I’d imagine it has something to do with your webpack.config.js file being configured incorrectly.

Double check your module.exports “entry” property for misspellings. If you’re having continued issues, try hosting the project on the default localhost port:

entry: [  
 ‘webpack-dev-server/client?http://localhost:8080’,  
 ‘webpack/hot/only-dev-server’,  
 ‘./src/index.jsx’  
 ],

Of course, your own file of entry (the third element) is probably different.



