---
title: "MS Word: Create a Line Across the Page"
date: 2016-01-21T11:06:17.000Z
date_updated:   2016-01-21T11:06:17.000Z
comments: false
---


**Problem: **I’m making my resume and have filled out all the text, but now I need to focus on the formatting. I want to put a line all the way across the page underneath each of my headings. Underlining doesn’t really work as it only underlines the word. How do I get a line that stretches across the page under my headings?

**Solution: **The solution lies within Microsoft Word’s Borders function. To add a line all the way across the page:

1. Select the text under which you’d like to place a line
2. Go to the “Home” tab
3. Click the dropdown next to Borders. This button looks like a square divided into four quadrants with the bottom line solid.
4. Select the kind of border you’d like to add

Happy formatting!



