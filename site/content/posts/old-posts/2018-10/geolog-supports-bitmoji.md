---
title: "Geolog gains Bitmoji support"
date: 2018-10-17T01:11:13-04:00
description: "About the page"
draft: false
---

Throughout the process of building [Geolog](http://labs.hamy.xyz/projects/geolog/), I've been attempting to reach feature parity with [a one-off visualization of my travels](https://www.instagram.com/p/Ba9R_7unkpv/?taken-by=sir.hamy) I created early last year. After several months of (extremely light) work, I've finally gotten there. It's rough to be sure and the code is certainly not my best, but it's something and it works.

Take a look:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v6KNf4qrgvc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

While I could've created this one-off by manually downloading each of the BitMojis I wanted, I decided to try and make my effort at least partially leverageable in the future. To do this, I studied the BitMoji APIs while heavily referencing [Matthew Nau's libmoji repo](https://github.com/matthewnau/libmoji). What I found is that you can actually identify a comic given a) the id of the comic you want and b) the id of the avatar you want inside it. This lends itself well to reusability because it means I can substitute my avatar for anyone else's should they want to use a similar visual scheme!

As part of this effort, I produced [Moji-Grabber](https://gitlab.com/SIRHAMY/moji-grabber) - a simple web scraper (kinda) that takes in a config consisting of the desired avatar's ID and a map of "emotions" to comicIds and downloads the corresponding BitMojis of each.

If you're interested in creating a Geolog with your own adventures (and now your own BitMojis!), let me know on [the Geolog project page](http://labs.hamy.xyz/projects/geolog/).

As for next steps, I'll be looking for ways to help streamline the creation process (right now many steps are manual) and to polish some of the animations/interactions so it reaches "purchase quality" (i.e. seems cool enough to actually buy).