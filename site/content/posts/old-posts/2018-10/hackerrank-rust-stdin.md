---
title: "Rust: Grab standard input on HackerRank"
date: 2018-10-24T21:54:24-04:00
description: "About the page"
draft: false
---

**Problem:** I'm trying to do a Rustlang problem on HackerRank but can't figure out how to read in the input (provided via stdin), how do I do this?

**Solution:** Use the std::io crate!

Here's an example:

<script src="https://gitlab.com/snippets/1768670.js"></script>

Or, if the embed's not working, the current state (as of writeTime):

```rust

use std::io;

fn main() {
    // Create a mutable String read_line can "read" into
    let mut input = String::new();
    // Read the stdin into our input variable
    io::stdin().read_line(&mut input).expect("something went wrong");
    
    // Many HackerRank inputs come in delimited by spaces, this is how you might
    // make that usable
    let iterable_input = input.split(" ");
}

```