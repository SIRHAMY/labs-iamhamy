---
title: "Calculus: Find the 5x5 Givens Rotation Matrix"
date: 2013-10-17T20:41:19.000Z
date_updated:   2013-10-17T20:41:19.000Z
comments: false
---


**Find the 5×5 Givens rotation matrix**

G(Pi/4, 2, 3)

**Solution:**

<table><tbody><tr><td>1</td><td>0</td><td>0</td><td>0</td><td>0</td></tr><tr><td>0</td><td>cos(pi/4)</td><td>0</td><td>-sin(pi/4)</td><td>0</td></tr><tr><td>0</td><td>0</td><td>1</td><td>0</td><td>0</td></tr><tr><td>0</td><td>sin(pi/4)</td><td>0</td><td>cos(pi/4)</td><td>0</td></tr><tr><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td></tr></tbody></table>

