---
title: "Ubuntu: Use Nautilus-Open-Terminal to Open a Terminal in a Directory"
date: 2014-09-07T21:54:43.000Z
date_updated:   2014-09-07T21:54:43.000Z
comments: false
---


Nautilus-open-terminal allows you to navigate to a directory in your file navigator and open a terminal there through your right-click menu.  This means no more searching through directories only to have to navigate there through another terminal as you can simply open one in the chosen directory through two simple clicks.

**How to Install**

Open a terminal and paste the following command:

> sudo apt-get install nautilus-open-terminal

The terminal will probably ask for your password.  Enter it then press enter.  Once nautilus-open-terminal is installed, restart you instance of Ubuntu to see the option to open a terminal in your right-click menu drop down.



