---
title: "Splunk: Sort by Column Title"
date: 2014-06-03T13:08:23.000Z
date_updated:   2014-06-03T13:08:23.000Z
comments: false
---


**Problem: **So you’ve already got your search returning the data you want.  The problem is, it’s all stuffed into a table in random order.

**Solution: **The solution’s pretty simple.  Splunk has a function called sort that sorts your data.  So, if you have a table with a column called: “Avg Response Time”, you can have Splunk sort by that column (assuming it’s filled with integers or something else with a standard hierarchy) by calling this sort function at the end of the search.

For example, you might have:

index=app sourcetype=server (hamycodes/getAwesomeStuff) earliest=-24h | eval response_time_s=response_time/1000 | convert ctime(_time) as time | stats first(time)  
 sparkline count avg(response_time_s) as “Avg Response Time (s)”, max(response_time_s) as “Max Response Time (s)” by host | sort “Avg Response Time (s)”

This would return a sparkline table that looks something like this:

[![Sample Sparkline](http://i2.wp.com/hamycodes.files.wordpress.com/2014/06/sample-sparkline.png?resize=510%2C92&ssl=1)](http://i2.wp.com/hamycodes.files.wordpress.com/2014/06/sample-sparkline.png?ssl=1)

As you can see, the table has been sorted by the “Avg Response Time (s)” column by adding ‘… | sort “Avg Response Time (s)” ‘ to the end of the search query.

 

 

 



