---
title: "Sorting Algorithms: Classifications"
date: 2013-10-09T01:47:51.000Z
date_updated:   2013-10-09T01:47:51.000Z
comments: false
---


**Stability: **An algorithm is considered stable if elements of equal value retain the order they were originally in.  For example if 3x and 3y are both in the data set, at the end of sorting, the order should still be 3x, 3y.

**In-Place:** An algorithm is in-place if it doesn’t require an external data structure to work effectively.

**Online: **An algorithm is online if you can add additional data to the data set during sorting without messing up the algorithm.

**Big O: **The efficiency of the algorithm with respect to the data set size.



