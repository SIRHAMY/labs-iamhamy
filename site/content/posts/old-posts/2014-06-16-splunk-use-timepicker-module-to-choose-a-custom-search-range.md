---
title: "Splunk: Use TimePicker Module to Choose a Custom Search Range"
date: 2014-06-16T15:46:12.000Z
date_updated:   2014-06-16T15:46:12.000Z
comments: false
---


*This guide assumes that you already have Sideview Utils installed in your Splunk instance.  If not, you definitely should.  Go to their website and download their free-for-internal-use application.  It’ll save you loads of time, I promise.*

**Problem: **I want to be able to choose custom start and end times for my search from the dashboard.

**Solution: **If you haven’t started using Sideview Utils yet, I’d strongly encourage you to do so.  In fact, if you aren’t using it then this guide probably won’t help you.

To use custom absolute time ranges in your search, simply add a timePicker module to your view and give it a search module as a child.  This will allow the search to inherit the time range from its parent timePicker module.

You’re done.  All the functionality required to choose a beginning and ending time is already wrapped up in the timePicker module.  Of course, there are a lot of customization options such as what the module defaults to and what options you’d like to be displayed, but you can play around with that on your own time.

To access the custom time functionality:

1. Go to your dashboard
2. Click the timePicker pull-down
3. Click on Custom Time
4. Input your beginning and ending values
5. Sit back and enjoy your search



