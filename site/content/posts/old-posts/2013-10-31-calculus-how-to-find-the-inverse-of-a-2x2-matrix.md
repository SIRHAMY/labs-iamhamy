---
title: "Calculus: How to Find the Inverse of a 2x2 Matrix"
date: 2013-10-31T04:56:18.000Z
date_updated:   2013-10-31T04:56:18.000Z
comments: false
---


**Find the inverse of the matrix A = **

<table><tbody><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></tbody></table>The formula to find the inverse of a 2×2 matrix is

1/det(A) * matrix

<table><tbody><tr><td>d</td><td>-b</td></tr><tr><td>-c</td><td>a</td></tr></tbody></table>

