---
title: "Physics: 2D Motion"
date: 2014-02-10T01:42:14.000Z
date_updated:   2014-02-10T01:42:14.000Z
comments: false
---


**Problem: **Older televisions display a picture using a device called a cathode ray tube, where electrons are emitted at high speed and collide with a phosphorescent surface, causing light to be emitted. The paths of the electrons are altered by magnetic fields. Consider one such electron that is emitted with an initial velocity of <span style="color:red;">2.20 ![](http://i2.wp.com/www.webassign.net/images/multiply.gif?w=730) 10<sup>7</sup></span> m/s in the horizontal direction when magnetic forces deflect the electron with a vertically upward acceleration of <span style="color:red;">5.00 ![](http://i2.wp.com/www.webassign.net/images/multiply.gif?w=730) 10<sup>15</sup></span> m/s<sup>2</sup>. The phosphorescent screen is a horizontal distance of <span style="color:red;">7.4</span> cm away from the point where the electron is emitted.

**A) ***How much time does the electron take to travel from the emission point to the screen?*

We’ll be using the following formula: Xf = Xi + VixT + 1/2Ax(T^2)

For this first part, we set Xf = 0.074 m and Vix = 2.20E7 m/s.  We can set Ax and Xi equal to 0 as their values aren’t given.  This gives us:

0.074 m = 0 + 2.20E7 m/s * T + (1/2)(0)(T^2) = T (2.20E7 m/s)

Solving for T, we get T = 3.364E-9 s

**Answer: **T = 3.364E-9 s

**B) ***How far does the electron travel vertically before it hits the screen?*

For this second part, we will use the same formula, just substitute Y values in for X.  By using the same time T from the previous problem, we can find the distance the particle traveled vertically before hitting the screen at Xf.

Yf = Yi + ViyT + 1/2 Ay(T^2) = 0 + 0T + (1/2)(5.00E15 m/s^2)(3.364E-9 s)^2 = 0.028 m -> 2.8 cm

**Answer: **Yf = 2.8 cm



