---
title: "Troubleshoot: Can't Git Commit with Atom Text Editor"
date: 2016-01-30T01:41:53.000Z
date_updated:   2016-01-30T01:41:53.000Z
comments: false
---


<span style="font-weight: bold;">Problem: </span>I’ve set the Atom text editor as my default git editor. When I run “git commit” it opens the editor in a new window. Now when I type in the commit message, save, and close the tab nothing happens. The terminal just sits there as if it’s still waiting for something to happen. How can I get the commit to complete successfully using Atom?

<span style="font-weight: bold;">Solution: </span>The issue here is that while Atom is waiting on something to close, it’s not waiting on your tab to close. In fact, when Atom opens, you should pay special attention to the fact that it’s opening the file in an entirely new window. So to successfully commit a message using Atom as your editor of choice:

-  Run “git commit” with files staged. This will open the editor asking for a commit message.
- Type in your commit message
- Save your message
- Close you tab
- Close the created Atom window

That should do it. The git process was waiting on the close of the entire window, not just the tab in which you edited your commit message.



