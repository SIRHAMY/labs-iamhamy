---
title: "Graph Search: Adjacency List"
date: 2013-11-25T12:36:58.000Z
date_updated:   2013-11-25T12:36:58.000Z
comments: false
---


![](https://lh3.googleusercontent.com/QuC-1rIAKv3gT1LwFDgjMlCzEUoNDrWFcJyK0vCLUTCOTSboZ1pMPcJlBZeFRkewY7ZoGw5SrHV8Kf_bJ7hA4kyXBJrypT3j8qmp3d0kN8FDPmJDe0W-OTL7mg)

An Adjacency list is, simply, a list of each node and the nodes that it is connected to.  Here’s an example:

**Adjacency List**

1. 1,2,5
2. 1,5,3
3. 2,4
4. 3,5,6
5. 1,2,4
6. 4



