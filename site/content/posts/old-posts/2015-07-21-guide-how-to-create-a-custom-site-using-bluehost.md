---
title: "Guide: How to Create a Custom Site Using Bluehost"
date: 2015-07-21T17:46:12.000Z
date_updated:   2015-07-21T17:46:12.000Z
comments: false
---


This tutorial will show you how to get setup to begin creating a custom website hosted by Bluehost.

*Prerequisites*

This is a list of things that should be done before starting this tutorial.

- Have an active domain name.
- Have an active hosting plan with Bluehost (this is paid so Bluehost actually hosts something for you).
- Have configured domain name and hosting account so you can access the site through the internet. - You can check this by navigating to your URL.  If it’s configured correctly, Bluehost will have a banner at the top advertising a few features.  If it isn’t, you’ll probably see a 404 not found or other error page.

**Building Your Custom Site**

There are a few steps we have to go through before we can begin uploading HTML/CSS/Javascript files to the server and have them reflected on your domain.  First, we have to install an FTP client which will allow you to upload the files.  Second, we’ll need to create an account through your Bluehost cPanel to allow the FTP client to modify the files on your server.  Third, we’ll connect to the server through FTP and upload your first files.

*Installing an FTP client*

There are alot of FTP clients out there, but my research has led me to download and use FileZilla.  It’s free and does a good job of letting you perform all the functions you need to create a custom website.

You can do your own research and pick a different one if you wish, but I’ll be doing the rest of the tutorial using FileZilla.  If you do decide to go with FileZilla, I’d recommend downloading it through [NiNite](https://ninite.com/filezilla/), so you don’t have to worry about accidentally installing bloatware (NiNite takes care of it for you).

Now that you’ve got your FTP client installed, we need to configure the server to allow us to access it.

*Configure server to allow access through FTP*

[![Bluehost FTP](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/07/BHFTP2-300x160.jpg?fit=300%2C160)](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/07/BHFTP2.jpg)

- Navigate to Hosting > FTP as seen in the picture above

Here, we’ll have to create a user account which we’ll use to login through our FTP client.  Input a username and password, then set the directory as public_html.  This will give your FTP user access to the public_html directory, which is where the server displays files to the public.

- Create user
- Set directory to public_html

*Upload files to the server using FTP*

- Open up FileZilla (or your other FTP client)
- Click on “Open the Site Manager” in the upper left corner
- Under Host, put your domain name as YOURDOMAIN.com. - For example, mine would be sirhamy.com
- Make sure the protocol is set to FTP
- With Bluehost, I’ve had the most success using plain FTP under Encryption, but I understand some people may want to be more secure.  I’m not transferring anything sensitive, but if you feel you need more protection, feel free to mess around with this.
- Under User, put the username you created @YOURDOMAIN.com - For example, if I created a user called ftpex, then I would put ftpex@sirhamy.com next to the User field.
- Click Connect
- Enter your password

If you’ve done everything correctly, you should now see a list of files that are currently hosted on your server.  You can delete most of these, but I’d leave cgi-bin, .htaccess, and ssv3_directory in case you want to use them later.

[![FileZilla File Explorer](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/07/BHFTP21-300x204.jpg?fit=300%2C204)](http://i2.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/07/BHFTP21.jpg)

Create an index.html folder with hello world somewhere on your computer then navigate to it using the file explorer located in the middle row on the left side of your FileZilla.  Once you’ve found it, just right click on it and hit upload.  This will upload a copy of that file to the public_html folder on your server

If you’ve done everything else correctly, you should be able to see your Hello World file when you visit your website.  If so, you can use the same process to upload just about anything on there.



