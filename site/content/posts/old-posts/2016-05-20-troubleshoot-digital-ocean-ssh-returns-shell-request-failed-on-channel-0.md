---
title: "Troubleshoot: Digital Ocean SSH Returns 'shell request failed on channel 0'"
date: 2016-05-20T10:52:52.000Z
date_updated:   2016-05-20T10:52:52.000Z
comments: false
---


**Problem: **I’m trying to ssh into my Digital Ocean Ubuntu droplet. However, when I get to the password section, it returns “shell request failed on channel 0”. I’m pretty sure my password is correct and that it would return “permission denied” if it wasn’t, so what’s happening and how do I fix it?

**Solution: **

I’m going to be upfront with this and tell you I’m not really sure what’s causing this issue. I searched around and was unable to find much, but I was able to circumvent the problem doing the following:

After attempting to ssh in to my box and receiving “shell request failed on channel 0”

- Logged into Digital Ocean
- Went to my droplet screen - Made sure the droplet wasn’t broken by looking at the usage graphs/status
- Opened the console for that droplet
- Typed some random commands in there to make sure it could take things
- Went back to my terminal and tried to ssh in again - It worked

I’m not sure if this is a fluke or if it’s reproducible, but that’s how I got around the problem. I want to say that maybe opening the console from the website triggered something that was breaking, but I have no idea.

Good luck and please let me know if you find a better answer.



