---
title: "Hello, world!"
date: 2018-07-25T08:29:31-04:00
description: "About the page"
draft: false
---

Yo, this is my new site. It's still pretty rough around the edges, but it'll get shit done and that's what's important.

Labs is intended to be a sandbox for experimentation and sharing, so expect a lot of technical learnings and tracer bullets. 

If you want to stay up-to-date, I recommend signing up for [my email list](http://blog.hamy.xyz/subscribe) (which isn't solely scoped to creations from this part of the Hamniverse, but will include it) and/or following the [hamy.labs Instagram](https://www.instagram.com/hamy.labs/).
