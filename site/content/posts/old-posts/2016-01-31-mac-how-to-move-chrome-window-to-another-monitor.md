---
title: "Mac: How to Move Chrome Window to another Monitor"
date: 2016-02-01T04:08:59.000Z
date_updated:   2016-02-01T04:08:59.000Z
comments: false
---


<span style="font-weight: bold;">Problem: </span>I’ve attached my laptop to another monitor and extended my display. I have a Chrome window open on my laptop and want to move it to my extended display, however clicking and dragging the title bar does nothing (in Windows this would move the whole window). How do I get my Chrome window to my other display?

<span style="font-weight: bold;">Solution: </span>To move your Chrome window – or any window for that matter – to another display:

- EITHER click the mission control button – it looks like three different-sized rectangles arranged around the key. On my Mac it’s on F3, but it might be different for your machine.
- OR swipe up on the track pad with three or more fingers.
- After doing one of the above, you should see all of your open windows on each screen arranged around the monitor. To move a window to the other monitor, simply click and drag that window over to the desired screen.
- To exit this mode, double click on a window, press the mission control button, or swipe up with three or more fingers again.



