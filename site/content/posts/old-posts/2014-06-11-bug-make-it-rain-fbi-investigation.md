---
title: "Bug: Make it Rain - FBI Investigation"
date: 2014-06-11T09:32:34.000Z
date_updated:   2014-06-11T09:32:34.000Z
comments: false
---


*This post is based off of the Make it Rain app for Android.  The bug may be present in iOS versions, but the extent of such a problem has yet to be investigated.*

**Bug: **Every time there’s an FBI investigation, the spinner lands on the same result.  Regardless of what the result is, that result will show up every time you are investigated.

**Solution:**

So far, no update has been released to remedy the issue and I’ve found no hacks to circumvent the issue.  If you do find that your FBI investigations keep landing on guilty, you may want to delete and reinstall the app.  This should randomize the investigations (at least initially) when you play again.  Of course, if you’ve gotten pretty far in the game, it may not be worth it to start over completely – weathering the storm being the best option.

I’ll keep a look out for any fixes and update this post with them.



