---
title: "Troubleshoot Mac: Seagate Drive Write-protected"
date: 2016-08-22T10:17:44.000Z
date_updated:   2016-08-22T10:17:44.000Z
comments: false
---


**Problem: **I plugged in my Seagate external hard drive into my new Mac, but it’s not working as expected. I have a file on my computer’s hard drive that I want to move to my external, but I can’t paste the folder onto the Seagate drive and attempting to move it shows a cancel icon near my cursor. When I try to save a file directly to the drive, it says the “Seagate Drive is Write-protected”. What am I doing wrong/how can I make my Seagate Drive usable on my new Mac?

**Solution: **The crux of the problem seems to be that many Seagate drives are setup to work with Windows. If your drive has been formatted to work with Windows, then you must take a few extra steps before you can use it on your Mac.

To make your Seagate External Hard drive work with your Mac:

- Your Seagate External Drive comes pre-loaded with a few files. **Locate “Set_Up.dmg” on your drive and run it**.
- Once run, **it will open up a browser to the URL [https://lyve.seagate.com/?sn=YOURSERIALNUMBER]**. *If you know your device’s serial number, you can simply follow that URL and replace ‘YOURSERIALNUMBER’ with the serial number of your drive.*
- Once you follow the URL, you will land on a registration page that will prompt you to ‘Register your Product’.
- You can r**egister your drive now or skip it by clicking on the arrow button titled “Mac Users”**. *Clicking this should lead you to the URL: [https://lyve.seagate.com/paragon].*- Note: The website requires you to have a valid serial number to reach the Paragon page, so simply navigating to the above URL probably won’t work.
- You should now see a page titled “Configure your Drive” that explains what the Paragon software does. Essentially it allows your Mac to read and write to your drive as you’d expect (at least from the user’s end). **Click the Download button.**
- Once downloaded, **find the downloaded dmg and run the executable.** Mine looked something like “NTFS_for_Mac_14.0.456.dmg”. Yours may be different.
- **Zip through the install wizard.**
- After installing, it will prompt you to Restart your computer.
- **Restart your computer.**

Boom, you should now have a usable Seagate External Hard drive. Happy saving!



