---
title: "Netlify: How to configure build for a Hugo site in a nested folder"
date: 2019-12-23T23:09:25-05:00
tags: [
    "hugo",
    "netlify"
]
comments: false
---

# problem

I'm trying to move my Hugo site to Netlify but the site itself is located in a subdirectory of my code repo. How can I configure Netlify to navigate to the correct subdirectory for my site code and build my site?

# solution

I just [recently moved over to Netlify](/posts/i-moved-to-netlify-from-gke-and-saved-60-per-month/) as well and had the same issue! For context, I like to put my hugo site code in a `site/` directory within my repo, so that the root directory can hold files like build configs, notes, and other project-level resources. I don't know if this is the best strategy, but it's worked for me and helped keep my many sites organized.

Anyway, if you have your site in a nested folder like I do, then you can configure Netlify like so:

## build command

The build command can be thought of as just a command that would be run in a terminal from your project's source directory on a computer it lives on. So in order to get this to work, we just need to get the command(s) required for our site to run and get them on one line. We can link multiple commands in terminal with `&&` so we'll use that to both navigate to our subdirectory and run our build command `hugo`.

Here's what my build command looks like:

`cd site && hugo`

This says to

1. Navigate to the subdirectory (we start out in our repo's root directory) where my site code is located
2. run `hugo` in order to build the site

Of course if your build is more complicated (maybe you have prefetching or installing or something) you can still add those into your build command using the same method:

`cd SUBDIRECTORY && COMMAND1 && COMMAND2`

At some level of complexity, you'll likely want to look into Netlify's build configuration via files but this works for simple things.

## publish directory

The publish directory is just the file path to the directory in which the final files that need be served for your site are located. In the case of Hugo - a static site generator - this is the directory where all your html, css, and other resources that make up your site reside.

Hugo, by default, will output its built files to the `public/` directory. So for my site that lives in the `site/` directory of my code repo, I set it to:

`site/public`

Note that it's possible to configure Hugo to not output to the `public/` directory and if you've done that then your configuration will be a little different. 

But in general it will always be something like

`SUBDIRECTORY1/SUB2/BUILDDIRECTORY`