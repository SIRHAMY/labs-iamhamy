---
title: "Netlify: How to configure subdomains for sites"
date: 2019-12-30T14:52:17-05:00
tags: [
    "netlify",
    "subdomain"
]
comments: false
---

# problem

I'd been looking at Netlify as a possible way to host my sites (and recently I decided that it was a good move and [migrated all my sites to Netlify](/posts/i-moved-to-netlify-from-gke-and-saved-60-per-month/)) but one of the things that held me back was not knowing how or if I could use subdomains.

My sites are set up as follows:

* hamy.xyz - the front page of my internet presence
* blog.hamy.xyz - where I write about personal / non-tech stuff
* labs.hamy.xyz - where I write about my tech stuff and post [my projects](/projects/)
* art.hamy.xyz - where I host my art creations

The question then was - how do I configure Netlify to use subdomains in the same manner I had been doing in the past?

# solution

The answer is yes you can use subdomains with Netlify.

As I mentioned, [I just moved to Netlify](/posts/i-moved-to-netlify-from-gke-and-saved-60-per-month/) myself and figured out how to do this in the process. I was running on Kubernetes before which used an Nginx reverse proxy for routing, but I've found Netlify's method way simpler.

At a high level, Netlify deals with routing to subdomains by using the domain's DNS configurations to map each subdomain directly to the Netlify project url that hosts the corresponding project. In order for this to work, you really only need to do a few things (assuming you've already set up your Netlify project and gotten it to work by itself):

1. Go to your domain's DNS configuration (like in NameCheap, or Digital Ocean, or whatever service you use to manage your domains)
2. Add a CNAME record, set host to be the subdomain path (so for `blog.hamy.xyz` I would set this to `blog`), set the alias / value to be your project url (for Netlify these are in the form of `MYPROJECTKEY.netlify.com` and the url can be found in your project overview page), and set a TTL (I like setting 3600 seconds while I'm trying things out and 43200 seconds when I've gotten it how I like it)
3. Go to your Netlify project and go to "set up custom domain" then input the url you want to map to it - so in my case for my `blog` project, I would put in `blog.hamy.xyz`
4. wait

If you have additional subdomains to add to other projects, just keep repeating the steps above for each.

If you've done this correctly, then these configuration changes should be reflected in a few hours. Once Netlify has verified that it's set up correctly, they'll also go ahead and get an SSL certificate for your domain so that people can access using `https`. That being said, there's a lot of caching that goes on in DNS land so it may take up to 24 hours for your changes to fully propagate (but I've only ever had to wait 3 hours so ymmv).

[From their docs](https://docs.netlify.com/domains-https/troubleshooting-tips/#certificates-and-https), if you want to check whether your domain is configured correctly yet, you can run (with YOURDOMAIN replaced with your custom domain):

`curl -s -v http://YOURDOMAIN 2>&1 | grep Server`

If it says `Server: Netlify` then you're gucci. If not, you may need to wait longer (and / or double check your DNS configuration).

For apex domains (i.e. not a subdomain, so in my case it would be hamy.xyz), read the [Netlify docs](https://docs.netlify.com/domains-https/custom-domains/configure-external-dns/#configure-an-apex-domain) - they did a good job of describing how to do it and special steps you need to take.