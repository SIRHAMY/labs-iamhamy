---
title: "UnrealEnginePython: `ERROR: Unable to instantiate module 'UnrealEnginePython': System.Exception: Unable to find Python includes, please add a search path to linuxKnownIncludesPaths`"
date: 2019-12-09T22:49:31-05:00
tags: [
    "unreal-engine",
    "python"
]
comments: false
---

# problem

I'm trying to use Python on in my Unreal Engine project using [UnrealEnginePython](https://github.com/20tab/UnrealEnginePython). However, when I try to load my project and build the plugin, it throws `ERROR: Unable to instantiate module 'UnrealEnginePython': System.Exception: Unable to find Python includes, please add a search path to linuxKnownIncludesPaths`. How can I fix this?

# solution

As part of UnrealEnginePython's build process, it goes through a set list of paths in search of the host machine's Python install location. Because not all machines will be setup the same, it's possible that your Python install location will be in a location not currently listed. If you're seeing this error, then that's probably the case.

To fix this, you can go to the file where these locations are listed `UnrealEnginePython.Build.cs` and modify the `pythonHome` variable with your machine's Python install location ([see the line on GitHub](https://github.com/20tab/UnrealEnginePython/blob/038192d7814d1aee182766028640d238204fefa4/Source/UnrealEnginePython/UnrealEnginePython.Build.cs#L11)).

## steps to fix

### Find your Python install location

Find your Python install location by typing into the terminal:

```
python
import sys
sys.executable
```

This will print out the install location of your system's Python.

### Tell UnrealEnginePython to look in that location for Python

Navigate to the `UnrealEnginePython.Build.cs` file located in your projects Plugins folder like `Plugins/UnrealEnginePython/Source/UnrealEnginePython/UnrealEnginePython.Build.cs`.

Set the `pythonHome` variable to point to your Python install location.