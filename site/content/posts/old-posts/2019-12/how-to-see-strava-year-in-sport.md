---
title: "How to see your Strava year in sport"
date: 2019-12-21T13:26:34-05:00
tags: [
    "strava",
    "troubleshoot",
    "year-in-sport"
]
comments: false
---

# problem

I got a notification to view my Strava year in sport on my phone, but dismissed the notification to look at it on my desktop browser. But now I can't seem to find it anywhere, how do I get to my Strava year in sport?

# solution

Based on Strava's 2018 year in sport being located at `2018.strava.com`, we can infer that the 2019 one will be located at https://2019.strava.com.

To see your year in sport:

* Go to https://2019.strava.com
* Login
* Browse through your year in sport

Future ones will likely be at `YEAR.strava.com`.