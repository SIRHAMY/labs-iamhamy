---
title: "Comparing Printful base shirt offerings - Gildan 64000 vs American Apparel 2001 vs American Apparel BB401 vs Next Level 3600 vs Next Level 4600 vs Champion"
date: 2020-01-01T11:55:44-05:00
tags: [
    "printful"
]
comments: false
---

I run [a shop](https://shop.hamy.xyz) that sells, among other things, shirts. I use [Printful](https://www.printful.com/a/1451646:3fdb65bd8e059459e1de1fb1e27475f9) to deal with the creation and shipping of these shirts on-demand so that I can spend my time, energy, and focus on more important things (like making more designs).

Printful gives you a wide array of different base shirts to pick from which is great but the review system isn't all that helpful at telling you what shirts are good and bad and why. So to make sure I was picking the best materials for my goods (you know, so I'm not sending my customers suboptimal products) I decided to test different base shirt offerings against each other.

In this post, I'll review 6 different shirts provided as base options for Printful's print service. The shirts (using the names and model numbers provided by Printful):

* Unisex Basic Softstyle T-Shirt | Gildan 64000
* Unisex Jersey T-Shirt | American Apparel 2001
* Unisex Crew Neck Tee | American Apparel BB401
* Men's Fitted T-Shirt | Next Level 3600
* Unisex Eco Tee | Next Level 4600
* Men's Champion T-Shirt

To try and keep this test as fair as possible, I set down a few ground rules to help eliminate stray variables.

1. All shirts would be the same color, size, and have the same print so we could more easily compare fit and print quality
2. All shirts would use the available custom label (preferring inside if available) as that's what all of my current shirt prints do
3. All shirts would go through one wash and subsequent dry cycle before review so we could see how the shirts stood up to "normal" conditions

On to the review!

# The reviews

Here's a video of me trying on each shirt. I'll link to the respective sections for each shirt below as well.

<iframe width="560" height="315" src="https://www.youtube.com/embed/qYCwqvzic00" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Gildan 64000

price: $10.53

Somehow we forgot to film this one, but this wasn't my favorite shirt so probs not a big deal.

* thoughts - feels fine, cottony. neck a little snug, slightly tight on arms. Not super long, not as tight on chest as other ones. I like this shirt, think it's a step up from original shirts (Bella + Canvas 3001) I got in fit, at least for my body type.
* fit - neck a little snug, slightly tight on arms, not as tight around chest as my original choice
* feel - cottony, not high end

# American Apparel 2001

price: $16.44

<iframe width="560" height="315" src="https://www.youtube.com/embed/qYCwqvzic00?start=12" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* thoughts - Feels like a very normal shirt you'd get at a store. A little tight on chest, neck line and arm holes standard
* fit - really standard fit, maybe a little tight on chest
* feel - Not too cottony, feels like a normal tshirt

# American Apparel BB401

price: $19.99

<iframe width="560" height="315" src="https://www.youtube.com/embed/qYCwqvzic00?start=25" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* thoughts - Feels soft and a little thin, though not bad quality. Not too long, shoulders fit right and a little tight on chest but thinness doesn't make it super apparent. This one is much like the Gildan 64000 but I think it feels just a bit better.
* fit - Fits well, again a little tight on chest
* feel - soft, a little thin but still feels high quality

# Next Level 3600

price: $17.26

<iframe width="560" height="315" src="https://www.youtube.com/embed/qYCwqvzic00?start=38" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* thoughts - feels a lot like original. Tight around chest, arms, and neck and cottony. Not too long though.
* fit - tight on arms, not on chest
* feel - feels cottony, not super nice

# Next Level 4600

price: $19.44

<iframe width="560" height="315" src="https://www.youtube.com/embed/qYCwqvzic00?start=53" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* thoughts - 4600 has a lot more space in the neck, chest, and arms than the 3600. Shirt bottom seems to go a tad too long. Feels a little less cottony than the 3600. Think it's a slight upgrade from 3600, at least for my body type.
* fit - standard fit
* feel - better than the 3600, not cottony but not soft

# Men's Champion T-Shirt

price: $20.99

<iframe width="560" height="315" src="https://www.youtube.com/embed/qYCwqvzic00?start=66" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* thoughts - Feel like I'm in a bag. V baggy, neckline is up. I like the canvas feel of this but the fit seems off. I think the fit oddness overshadows the nice feel of the material.
* fit - I'm in a bag with holes in it
* feel - The bag is made out of canvas

# Ranking

## 1. AA 2001 

With affordable price and standard sizing this is a good shirt.

Overall I think the American Apparel 2001 provides the best fit, feel, and price of the six types I reviewed. It is a little on the expensive side (50% more than Gildan) but I think it's important to find a balance between what works financially and what works as a product. If your product isn't good enough for people to buy (and keep buying) then it doesn't matter whether you saved 33% or not - you're not making any money.

## 2. AA BB401

A little on the expensive side but a pretty good shirt all around.

I like this one a little bit better than the Gildan but less than the AA 2001. The fit just isn't quite right for me like the 2001 is and the feel just seems a little off - it's soft and thin which is great but I keep getting this feeling of plastic-ness. Might just be me. As one of the highest prices of the bunch, this would've had to be better in almost every respect than the other options to come out on top and it just didn't for me.

## 3. Next level 4600

It's better than the 3600 and eeks out the Gildan 64000 on fit for me, though if we valued price more then this could easily swap with the Gildan.

The 4600 loses the tightness around the arms and relative looseness around the chest. It also gains some feel points by feeling less cottony and a little thicker / heavier. It does look like it runs a little long and I don't love the fit as a whole aesthetically on me but other body types may have different experiences. At $19.44, it's a solid shirt but not sure it's the winner.

## 4. Gildan 64000

v cheap and a very average shirt - good bang for your buck.

Overall I think this provides a better fit for my body type than my original shirts (Bella + Canvas 3001) at around the same price (I think within $1). It's a little less soft though so if texture is a thing for you then maybe this isn't a good tradeoff. For a slimmer frame I think this might be a good go-to shirt, but I'm not that so will likely pass.

## 5. Next level 3600

It's worse than the 4600 and for the extra price, I don't think it beats out the Gildan 64000.

This fits a lot like my original shirts (Bella + Canvas 3001) - tight on arms and shoulders - but a little less tight on the chest. I find it a little weird to have it tight on the arms and not in the chest as it feels like the shirt is drooping in the front, though that isn't really visible. At $17 I would've expected this to fit a lot better and the feel is pretty cottony so they can't even play that off as a reason to up the price. Not my fave, but slimmer-armed people may enjoy this.

## 6. Champion

At the highest price and oddest fit, just not sure who would really go for this.

I don't really know what to say. I feel like I'm in a bag. Like someone had a bag laying around and I needed a shirt so they cut holes in the bag to help me out. The feel is odd, it feels like a high quality material as it's thick but it's rough like canvas. The fit is just so baggy. For the price of $20.99 I'm really not sure who's buying this shirt.

# my final thoughts

This was a great exercise for me to help determine what I actually like in shirts and decide what it is I actually want to be selling. There were obvs some areas that still need to be investigated - like what kinds of shirts other people with other bodies like, different print formats / setups, etc. - but this was a start.

Moving forward, I realized that I really like the logo being on the back of the shirt so I'm going to make that standard and I'll be switching over all of my shirts to use the AA 2001.

You can take a look at the products I ended up producing (and get some yourself) at [my shop](https://shop.hamy.xyz/collections/all).

Thanks for reading!

-HAMY.OUT