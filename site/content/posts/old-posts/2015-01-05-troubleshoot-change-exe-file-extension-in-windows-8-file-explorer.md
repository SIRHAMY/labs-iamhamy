---
title: "Troubleshoot: Change .exe file extension in Windows 8 File Explorer"
date: 2015-01-05T19:10:55.000Z
date_updated:   2015-01-05T19:10:55.000Z
comments: false
---


*Note: Changing the extension of a file may affect its functionality and, thus, its usability.  Follow these directions at your own risk.*

Changing the extension of a file is pretty easy.  Here’s what you do:

1. Find the file in your File Explorer
2. If you can see the extension (i.e. the extension is in the name field), continue to #4
3. If you can’t see the extension, click View at the top of the explorer and check the “File Extensions” box.  You should now be able to see file extensions.
4. Select the desired file, right click, then select “Rename”.  Change the file extension to whatever your heart desires.



