---
title: "Calculus: Show that the Lines are Skew"
date: 2013-10-03T05:23:35.000Z
date_updated:   2013-10-03T05:23:35.000Z
comments: false
---


**Show that the lines L1: x= -1 + 2t, y = 2 + t, z = 1 – t and L2: x = 1 – 4s, y = 1 + s, z = 2 – 2s are skew**

Start by setting the variable values equal to each other

Equations:

1. 1-4s = -1 + 2t

2. 1 + s = 2 + t

3. 2 – 2s = 1 – t

Solve for t using the first equation

1-4s = -1 + 2t => t = 1 – 2s

Plug the found value for t into the second equation and solve for s

1+s = 2 + (1 – 2s) => s = 2/3

Now plug the found value for s back into equation 2 to solve for t

1 + (2/3) = 2 + t => t = -1/3

Plug (s,t) = (2/3, -1,3) into equation 3 to determine if the equivalency holds

**Solution: 2 – 2(2/3) = 1 – (-1/3) => 2/3 != 4/3**

**Because the equivalency does not hold, the lines are skew.**



