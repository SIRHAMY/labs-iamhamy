---
title: "Kdenlive: How to Change Clip Position"
date: 2018-08-05T08:57:52-04:00
description: "About the page"
draft: false
---

**Problem:** I've got a really long clip in my [Kdenlive](https://kdenlive.org/en/) workspace and I've just cut and deleted the first half of it. This means that there's about 2 minutes of empty space between the remaining clip and where I want it to go. I know you can drag the clip, but kdenlive's drag speed only goes at around 5s at a time, meaning I'm spendin about 30s just dragging my clip where I want it to go. Is there a way to have the clip snap to the time position I want?

**Solution:** Kdenlive allows you to quickly change the Position, Crop Start, Duration, and Crop End directly from the Duration pop-up. To use:

* Double click the clip in your workspace you'd like to modify
* Change the values accordingly
* Hit Ok