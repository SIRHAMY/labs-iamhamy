---
title: January 2016 Interview Questions
date: 2016-02-01T05:19:35.000Z
date_updated:   2016-02-01T05:19:35.000Z
comments: false
---


This post is part of my [Interview Guide](http://www.sirhamy.com/blog/interview-guide/) series where I log my journey towards Software Engineer-hood.

1. **WhatsWrong – Debugging/Code Tracing**

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/e5cc41aa71aa95eb767e.json"></div>(https://gist.github.com/SIRHAMY/e5cc41aa71aa95eb767e)

- What’s wrong with the above code? What changes would you make (performance, style, etc.)?
- Will it compile?

**Part B:**

Now assume this function is called when an object (say a vehicle) in a game collides with an obstacle (say a tree). This function returns the number of points you get depending on what kind of vehicle you are e.g. a car hitting the tree might give 3 points.

How would you write code that has the same effect, but does it better? Take into account best code practices, readability, ease of extensibility, etc.

2. **LongestCharSequence – Algorithms**

Create a function that takes in a String and returns the largest consecutive sequence of characters greater than 1 in that String. A consecutive sequence is one where each character comes after the character that came before it in the string in the alphabet e.g. “abc”.

Sample cases:

- “abxy” -> “ab”
- “abc” -> “abc”
- “a” -> “”
- “abdef” -> “def”

**My Solution: [InterviewQ: LongestCharSeq Solution](http://www.sirhamy.com/blog/2016/02/interviewq-longestcharseq-solution)**

3. **MyWorld – OOP**

Design a program (think OOP) that implements the following elements:

- World: Contains Countries, States, Cities, and Districts
- Country: Contains States, Cities, and Districts – has a name
- State: Contains Cities and Districts – has a name
- City: Contains a population – has a name
- District: Contains a population – has a name

For each of these entities (besides World), the name of the entity is declared in the first constructor argument and the entities within it are declared in the following arguments. You can assume that there won’t be any duplicate entities.

World should utilize a constructor that looks like this, don’t change it:

> public world(Country… entitites) {
> 
> }

Here is an example of how the World will be populated through constructor calls:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/2a0e8d175ddc14010f48.json"></div>([https://gist.github.com/SIRHAMY/2a0e8d175ddc14010f48](https://gist.github.com/SIRHAMY/2a0e8d175ddc14010f48))

As you can see at the bottom of this gist, there are two calls you must implement in World:

- **printPopulation():** This tallies up all of the populations inside the world. You can assume there won’t be any duplicate entities, so simply adding all the populations created will suffice.
- **printWorld():** This prints out each of the entity names in the order in which they were declared. So yes, Countries, States, Cities, and Districts will be all jumbled together, but that’s by design. Just print out the names.

**My Solution: [InterviewQ: MyWorld Solution](http://www.sirhamy.com/blog/2016/01/interviewq-myworld)**

4. **ZeroesToTheRight – Algorithms**

Write a function that takes an integer array and moves all the zeroes in it to the right of the array. Implement this in-place. It doesn’t matter if the order of the other integers is changed, just that they are still present.

Example:

[1,0,7,0,0,6,0] -> [1,7,6,0,0,0,0]

**My Solution: [InterviewQ: ZeroesToTheRight Solution](http://www.sirhamy.com/blog/2016/01/interviewq-zeroestotheright-solution)**

5. **BreezePassTesting**** – SW Testing Concepts**

On some toll roads, there are a set number of lanes designated for special vehicles and subscribers to special tickets that allow you to skip the tolls. Your company has created the software that runs these cameras and are now entering the testing stage. How would you test this software?

Think about things like edge cases, environmental factors, vehicle compatibility, etc.

6. **ElevatorController – Product Design**

There’s a 100-story building with two elevators in it. You’ve been hired to create software that optimizes the operations of these two elevators so that the wait time (amount of time waiting for and riding the elevators) is minimized.

You don’t have to write this code so much as come up with design specs that you could pass of to a dev to implement.



