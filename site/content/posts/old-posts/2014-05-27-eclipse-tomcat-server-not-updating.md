---
title: "Eclipse: TomCat Server Not Updating"
date: 2014-05-27T16:38:43.000Z
date_updated:   2014-05-27T16:38:43.000Z
comments: false
---


I’ve been creating a web service recently using the Tomcat plugins for Eclipse.  I got two methods working then everything else I wrote wouldn’t show up.  Eventually I was able to narrow down the problem to the server not updating/building a new .war file each time.

**Problem: **Tomcat server not updating with new content

**Solution:**

- First make sure you’ve saved all of your project files are saved.  Navigate to the server menu, right-click on the server and restart it.
- If that doesn’t work, try cleaning both your project files and your server.  You can clean your project by going to Project -> Clean in the Menu bar
- If that fails, right-click on the server -> Add/Remove projects and remove your project.  Once it’s done, re-add your project to the server and try again.
- If even that fails, delete the server and replace it with a new one.



