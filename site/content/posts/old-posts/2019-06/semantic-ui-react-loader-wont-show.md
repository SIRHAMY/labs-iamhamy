---
title: "Semantic UI React: Loader won't show up"
date: 2019-06-10T11:25:31-04:00
tags: [
    "semantic-ui",
    "react",
    "loader",
    "troubleshoot",
    "javascript",
    "jsx"
]
comments: false
---

# problem

I'm trying to use Semantic UI React components to build out my app but the `Loader` component won't show up. I've tried giving it the `inverted` class name which typically flips the element's content color to see if maybe it was just blending into the background, but that didn't help. How do I use the `Loader`?

# solution

The Semantic UI React `Loader` component is kind of confusing it's typical to have some issues when first using it. Note from [the official docs](https://semantic-ui.com/elements/loader.html) that `Loader`s are "hidden unless active or inside an active dimmer".

So this just means that you've got to either set your `Loader` to `active` (via component prop) or wrap it in a `Dimmer` that is set to `active`.

This'll look something like the following `jsx`:

```
<Dimmer active inverted>
    <Loader>Loading</Loader>
</Dimmer>
```

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on things I've built by [subscribing here](https://hamy.xyz/subscribe)