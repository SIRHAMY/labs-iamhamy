---
title: "Semantic UI React: How to center forms"
date: 2019-06-24T01:41:48-04:00
tags: [
    "semantic-ui",
    "react",
    "form",
    "tutorial"
]
comments: false
---

# problem

I have a Semantic UI React `Form.Group` that contains radio buttons. I want those buttons to be centered, but the usual `center aligned` css classes don't seem to be doing the trick. How can I center this form?

# solution

You can center this `Form.Group` (or even the entire `Form` itself) by wrapping the `Form` in a Semantic UI React `Grid` and then adding the `center aligned` classes to it.

So a centered `Form` might look something like:

```
<Grid className="center aligned">
    <Form>
        <Form.Group>
            ...
        </Form.Group>
        <Form.Group>
            ...
        </Form.Group>
    </Form>
</Grid>
```

# source

* [Semantic UI React - Center form in segment?](https://stackoverflow.com/questions/45111701/semantic-ui-react-center-form-in-segment)

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).