---
title: "JavaScript: Sort an array of objects by a string property"
date: 2019-06-03T12:07:14-04:00
tags: [
    "javascript",
    "array",
    "sort",
    "tutorial"
]
comments: false
---

# problem

I've got an array of objects that I'd like to be sorted by a particular property. How do I do this?

# solution

Let's say that you have an array of objects with structure like:

```
{
    text: string;
    value: string;
}
```

And let's say that you would like all of these objects sorted in the resulting array by text, not by value. You do this by:

```
let myArray = [];
myArray.push(
    {
        text: "1",
        value: "IShouldBeFirst"
    }
);
myArray.push(
    {
        text: "3",
        value: "IShouldBeThird"
    }
);
myArray.push(
    {
        text: "2",
        value: "IShouldBeSecond"
    }
);

// this is the sorting stuff
myArray.sort(
    (a, b) =>
    a.text === b.text
        ? 0
        : a.text > b.text
            ? 1
            : -1);

for(let element of myArray) {
    console.log(element.value);
}
/*
    Expected result:
    * "IShouldBeFirst"
    * "IShouldBeSecond"
    * "IShouldBeThird"
*/
```

The way this works is that `Array.sort` takes in two elements `a` and `b` and expects you to write a comparator with return values either negative, positive, or zero in order to determine which element goes first. In order to sort by our property, we simply pass in a comparator that only compares that property.

Read more about `Array.sort` on [GeeksforGeeks](https://www.geeksforgeeks.org/javascript-array-sort/)

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on things I've built by [subscribing here](https://hamy.xyz/subscribe)