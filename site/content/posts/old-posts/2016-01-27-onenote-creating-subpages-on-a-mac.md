---
title: "OneNote: Creating Subpages on a Mac"
date: 2016-01-28T03:18:49.000Z
date_updated:   2016-01-28T03:18:49.000Z
comments: false
---


<span style="font-weight: bold;">Problem: </span>I use OneNote a lot to take notes. Because of this, I have a lot of notes on my machine, which can become quite a hassle to look through without some sort of organization structure. To accomplish this, I use OneNote’s subpages feature to give a hierarchy to the pages within a folder.

However, having just started using a Mac, I’ve been unable to find this feature. Any time I create a new page or move a page around in the page directory side bar, that page defaults to the highest hierarchy with no way to modify it. How do I use the subpages feature on mac?

<span style="font-weight: bold;">Solution: </span>Unfortunately, it’s not quite as easy to create subpages on Mac as it is on Windows – clicking and dragging. Luckily, it’s not at all impossible – as many of the forums would have you believe.

*To make a page a subpage:*

- CTRL + Click the page you want to be a Subpage
- Select “Make Subpage”

Or use the shortcut: ALT/OPTION + CMD + ]

*You can also create subpages underneath an existing page. To do this:*

-  CTRL + Click the page you want to create Subpages beneath
- Select “New Subpage Below”

Or use the shortcut: ALT/OPTION + SHIFT + CMD + N



