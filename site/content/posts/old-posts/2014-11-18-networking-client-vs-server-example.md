---
title: "Networking: Client vs. Server Example"
date: 2014-11-19T00:47:27.000Z
date_updated:   2014-11-19T00:47:27.000Z
comments: false
---


A quick definition and example of what the difference between a client and server is in the computer networking context.

<span style="text-decoration:underline;">Client</span> – The client is the machine asking another machine to do something.  For example, if you were to go to Google and conduct a search, your machine would be the client.

<span style="text-decoration:underline;">Server</span> – The server is the machine that fulfills a request.  For example, a machine that fulfills the search request previously conducted by the client would be considered a server.

The names themselves are pretty straightforward.  A client requests something and a server completes the request.



