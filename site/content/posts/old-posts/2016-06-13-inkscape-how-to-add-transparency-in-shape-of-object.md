---
title: "Inkscape: How to Add Transparency in Shape of Object"
date: 2016-06-13T15:31:09.000Z
date_updated:   2016-06-13T15:31:09.000Z
comments: false
---


**Problem: **I have a shape in Inkscape that I want to make transparent inside of another shape. For instance, I’ve got a box, but I want a circular, transparent whole in the middle. How do I go about embedding a transparent shape inside of another shape?

![BoxWCircleHole](http://i0.wp.com/www.sirhamy.com/blog/wp-content/uploads/2016/05/BoxWCircleHole.png?fit=289%2C261)

**Solution: **The easiest way to go about this is using the built-in Path tools.

- Create your base shape and the shape you would like to make the transparency
- Position the shape you’d like to be transparent over the base shape in the way you want it to appear.
- Select both objects using the shift + selection key
- Select Path > Difference or Path > Exclusion. Depending on your object type, one may not work. If it doesn’t try the other.
- Your object should now be transparent.

 



