---
title: "Mac: Remove Last Line in File from Terminal"
date: 2016-05-18T13:17:43.000Z
date_updated:   2016-05-18T13:17:43.000Z
comments: false
---


**Problem: **I have a really huge file and I want to remove the last line in it. Opening the file in a text editor causes it to crash, so I can’t do it manually. Is there a way I can delete the last line from the terminal, so I don’t have to open it?

**Solution: **Here’s the command to delete the last line in a file:

> sed -i ” -e ‘$ d’ FILENAME.txt

The extension doesn’t have to be .txt, but beware that the command will remove the last line in the file, not the last object. This is significant if you’re storing some sort of file containing hierarchies, like JSON, as removing the final brace could break the file.

*This answer was found on [Stack Overflow](http://stackoverflow.com/questions/4881930/bash-remove-the-last-line-from-a-file) from user [mklement0](http://stackoverflow.com/users/45375/mklement0)**.*



