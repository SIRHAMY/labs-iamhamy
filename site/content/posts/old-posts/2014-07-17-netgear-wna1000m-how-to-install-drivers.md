---
title: "NetGear WNA1000M: How to Install Drivers"
date: 2014-07-17T14:24:03.000Z
date_updated:   2014-07-17T14:24:03.000Z
comments: false
---


When you buy a NetGear model WNA1000M wireless WiFi USB Adapter, you should receive a CD that you can run in your optical drive.  This will, obviously, install the requisite drivers needed to use the device or install an outdated version in which case you’ll be prompted to update your driver.  But what if you don’t have an optical drive?

**Problem: **I have purchased a NetGear WNA1000M WiFi USB Dongle, but don’t have an optical drive to install the required drivers with.

**Solution:**

1. Navigate to the [NetGear WNA1000M Wireless Adapter Support Page](http://support.netgear.com/product/WNA1000M#wrapper).
2. Click on the Firmware/Software link.  At this point, the drivers should support pretty much every operating system, bar linux/unix configurations.
3. Open the download, extract the files, and run the executable/application.
4. You’re done.  Open the NetGear Genie and connect your device to a wireless connection.



