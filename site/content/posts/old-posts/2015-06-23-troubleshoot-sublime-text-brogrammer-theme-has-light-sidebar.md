---
title: "Troubleshoot: Sublime Text Brogrammer Theme has Light Sidebar"
date: 2015-06-23T09:47:33.000Z
date_updated:   2015-06-23T09:47:33.000Z
comments: false
---


**Problem: **You’ve installed the Brogrammer theme on your Sublime Text editor, but the sidebar is light-colored and out of theme.

**Solution: **All you have to do is restart Sublime Text.  Once you restart your text editor, your sidebar should have reset to the installed theme’s colors.



