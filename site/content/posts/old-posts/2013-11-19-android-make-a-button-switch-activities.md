---
title: "Android: Make a Button Switch Activities"
date: 2013-11-19T14:55:38.000Z
date_updated:   2013-11-19T14:55:38.000Z
comments: false
---


This is a short tutorial on how to give a button the functionality to switch from one activity to another when it is pressed in Android.  For this tutorial, we assume you have already created the button and that you are coding in Java.  *The example below was tested in Eclipse, many factors such as alternative environments and Android OS levels may effect the success of implementation.*

Here’s a code snippet:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/7551444.json"></div>(https://gist.github.com/SIRHAMY/7551444)

Text after link.



