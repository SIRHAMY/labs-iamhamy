---
title: "Troubleshoot: Sublime Text 3 Tabs Running Slowly after Installing GitGutter"
date: 2015-06-23T10:03:29.000Z
date_updated:   2015-06-23T10:03:29.000Z
comments: false
---


**Problem: **I found that my Sublime Text editor was running slowly after I installed the GitGutter package.  By slow, I mean switching between and moving tabs caused the editor to freeze for a second or so before performing the action.

**Solution: **I removed the GitGutter package from Sublime Text.  While it’s pretty useful, the lag’s impact on productivity wasn’t worth it.  I’ll probably try it again in the future to see if it’s been fixed.



