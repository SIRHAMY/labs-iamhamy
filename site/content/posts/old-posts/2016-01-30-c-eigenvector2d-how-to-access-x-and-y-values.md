---
title: "C++: Eigen::Vector2d - How to Access X and Y Values"
date: 2016-01-30T17:43:39.000Z
date_updated:   2016-01-30T17:43:39.000Z
comments: false
---


**Problem: **I have a std::vector<Eigen::Vector2d> variable storing a bunch of points. How do I access the X and Y values of each of these points?

**Solution: **Vector2d takes in a binary value and returns the X or Y value respectively.

> double x = aVector(0);
> 
> double y = aVector(1);

Similarly, in order to change these values you can:

> aVector(0) += 50;
> 
> aVector(1) -= 100;



