---
title: "Troubleshoot: Battery Cannot Supply Enough Power for Dell Docking Station"
date: 2015-06-26T15:35:15.000Z
date_updated:   2015-06-26T15:35:15.000Z
comments: false
---


**Problem: **I tried docking my Dell laptop to its docking station, but was greeted with an error message that read:

> The battery currently attached to your system cannot supply enough power to your docking station for it to operate normally.  Attach a supported power adapter to the docking station.

**Solution: **In my case, I found that the power adapter that was hooked into the docking station from the outlet was burnt out.  So to fix it, all I did was switch out that power adapter with a new one that I had already tested for functionality.  Once I’d done that, the docking station worked as usual.



