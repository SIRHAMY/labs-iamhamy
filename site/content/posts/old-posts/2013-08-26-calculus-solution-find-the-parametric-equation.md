---
title: "Calculus Solution: Find the Parametric Equation"
date: 2013-08-26T05:29:09.000Z
date_updated:   2013-08-26T05:29:09.000Z
comments: false
---


**Find the parametric equation for the line through the point (3, -2, 1) parallel to the line: x = 1 + 2t, y = 2 – t, z = 3t**

Starting with the basics, we know we can define a line using a point on that line and adding a directional vector.  We are given a point on the line, but not the corresponding directional vector.  However, because the line is parallel to the line whose parametric equations are given to us, we can take the directional vector from the given line and add it to the given point in order to come up with the correct parametric equations.

Take apart the given parametric equation to get:

(x2, y2, z2) = (1, 2, 0) + t(2, -1, 3)

As such, the directional vector = t(2, -1, 3)

Add this to the given point resulting in:

0p + d = (x, y, z) = (3, -2, 1) + t(2, -1, 3)

Convert this to parametric form:

**Final: x = 3 + 2t, y = -2 – t, z = 1 + 3t**



