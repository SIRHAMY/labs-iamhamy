---
title: "Combinatorics: Number of Ternary Strings of Length 2n with Zeroes in Odd-numbered Positions"
date: 2014-09-11T21:45:12.000Z
date_updated:   2014-09-11T21:45:12.000Z
comments: false
---


**Problem: **How many ternary strings of length 2n are there in which zeroes appear only in odd-numbered positions?

**Solution: **Based on the problem, we know that odd-numbered positions will have 3 different possible values (0, 1, 2) and even positions will have 2 (1, 2).  We also know that, given a string of length 2n, there will be n odd-numbered positions and n even-numbered positions.

We can therefore calculate the total number of possibilities as the number of possibilities for even positions to the power of the number of even positions times the number of possibilities for odd positions to the power of the number of odd positions:

**Answer: **3^n * 2^n = 6^n

**Example:** We can also draw this out pretty easily

n = 1    n=2    n=3

2 * 3  |  2 * 3 | 2 * 3

6           36         216

Figure out the correlation between the value of n and the number of possibilities to get 6^n.



