---
title: "Windows 8 Holiday App Challenge: Spanish - English Matching Game"
date: 2013-12-18T01:32:46.000Z
date_updated:   2013-12-18T01:32:46.000Z
comments: false
---


Microsoft released a promotion for the holidays to help boost the Windows App Store’s offerings for Windows 8 enabled devices.  Essentially the promotion went as follows: create an app that works and isn’t terrible then submit the app’s Windows App Store link, get $50.  If that app happens to receive 100 or more downloads, resubmit the link to get another $100.  Here’s a link to the promotion’s official site, assuming it’s still up: [http://www.windows8holidaychallenge.com/](http://www.windows8holidaychallenge.com/).

The whole promotion sounds like a pretty good way to make some easy money, so I went with their matching game template and began tweaking their program to create a Spanish – English matching game playable on Windows 8 devices.  While the app is still in production, be on the look out for tips on the development process and any other complications I find myself faced with.

You can keep up to date with its progress on GitHub ([https://github.com/MRhamiltong/SpanishMatch](https://github.com/MRhamiltong/SpanishMatch)).  In order to interact with the project, you need to [download and install Construct 2](https://www.scirra.com/ "Construct 2") from Scirra’s website.  The free version will suffice.



