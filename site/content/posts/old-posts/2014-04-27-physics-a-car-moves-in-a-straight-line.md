---
title: "Physics: A Car Moves in a Straight Line"
date: 2014-04-28T00:38:40.000Z
date_updated:   2014-04-28T00:38:40.000Z
comments: false
---


**Problem: **<span style="color:#000000;">A car moves in a straight line at 16</span><span style="color:#000000;"> m/s for 9</span><span style="color:#000000;"> miles, then at 30.0 m/s for another 9</span><span style="color:#000000;"> miles.</span>

V1 = 16 m/s, D1 = 9 miles = 14,484.06 m, V2 = 30 m/s, D2 = 9 miles = 14,484.06 m

<span style="color:#000000;">**(a)** Is the car’s average speed less than, greater than, or equal to 23</span><span style="color:#000000;"> m/s?</span>

<span style="color:#000000;">**(b)** Calculate the car’s average speed.</span>

**Solution: **In order to solve **a)**, we must first solve for **b)**.

Find the time it takes to travel each interval.

T1 = D1 / V1 = 905.25 s

T2 = D2 / V2 = 482.802 s

Now that we have the time during each interval, we can find the total time by adding them up:

Ttot = T1 + T2 = 1388.06 s

Now, to get the average speed, we just have to divide the total distance by the total time:

Vavg = (D1 + D2) / Ttot = 20.87 m/s

**Answer: **Vavg = 20.87 m/s *which is less than 23 m/s*



