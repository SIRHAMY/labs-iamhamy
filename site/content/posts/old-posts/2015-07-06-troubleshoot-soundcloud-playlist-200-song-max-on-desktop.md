---
title: "Troubleshoot: SoundCloud Playlist 200 Song Max on Desktop"
date: 2015-07-06T23:21:21.000Z
date_updated:   2015-07-06T23:21:21.000Z
comments: false
---


**Problem: **I have a playlist on Sound Cloud that has more than 200 songs in it, but only 200 show up when I scroll down, making it impossible to listen/interact with them.

**Solution: **While on the playlist page, reload your browser window.  This should allow you to access the other songs in your playlist.



