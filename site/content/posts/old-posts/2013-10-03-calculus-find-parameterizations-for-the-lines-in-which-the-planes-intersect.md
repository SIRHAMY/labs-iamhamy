---
title: "Calculus: Find Parameterizations for the Lines in which the Planes Intersect"
date: 2013-10-03T06:32:56.000Z
date_updated:   2013-10-03T06:32:56.000Z
comments: false
---


**Find the parameterizations for the lines in which the planes P1: 5x -2y = 11 and P2: 4y – 5z = -17 intersect**

Take the cross product of the lines normal to each plane to get a vector normal to both normals, parallel to the line of intersection of the planes (the directional vector we’ll use for the parametric equation of the line)

N(P1) X N(P2) = (5, -2, 0) X (0, 4, -5)

=10i + 25j + 20k => (10, 25, 20)

Now we have to find a point on the line to complete the parametric equation (we found the directional vector above)

Set x =1 in P1 and solve for y

5(1) – 2y = 11 => y = -3

Plug the value of y into P2 and solve for z

4(-3) – 5z = -17 => z = 1

Therefore the point of intersection = (1, -3, 1)

**Solution: Parametric equation:**

**x = 1 + 10t  
 y = -3 + 25t  
 z = 1+ 20t**



