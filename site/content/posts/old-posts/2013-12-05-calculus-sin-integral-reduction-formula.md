---
title: "Calculus: Sine Integral Reduction Formula"
date: 2013-12-05T15:10:19.000Z
date_updated:   2013-12-05T15:10:19.000Z
comments: false
---


This formula is used when you’re attempting to find the integral of the sin(x) function raised to a large power.

∫sin(x)^m = -(cos(x)sin(x)^(m-1))/m + ((m-1)/m)∫sin(x)^(m-2)



