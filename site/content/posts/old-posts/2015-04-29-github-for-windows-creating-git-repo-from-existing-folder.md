---
title: "GitHub for Windows: Creating Git Repo from Existing Folder"
date: 2015-04-29T23:06:12.000Z
date_updated:   2015-04-29T23:06:12.000Z
comments: false
---


**Problem: **You already have a directory with your project files in it and would simply like to start a GitHub repository in the existing folder.

**Solution: **There are multiple ways to go about this.  The first, and probably simplest, is to open up your command prompt and follow the instructions through the command line found on [this Stack Overflow post](http://stackoverflow.com/questions/3311774/how-to-convert-existing-non-empty-directory-into-a-git-working-directory-and-pus "CLI for Git Repo Creation").

However, if you can’t follow those instructions or would rather use the GitHub for Windows GUI, follow the instructions below.

1. **Open GitHub** for Windows
2. **Click on the plus sign** in the top left corner
3. **Click Create**, **name the repo something random** (it doesn’t matter here what you name it), then place the **desired directory’s path as the Local Path**.
4. **Click Create Repository**
5. You will now have a folder named whatever you wrote in instruction 3 in the directory you’re trying to turn into a repo.  **Go inside this folder and copy the contents** (should be .git files) and **paste them inside your desired directory** (this should be the directory directly outside of this new folder if you followed my local path instructions in #3).
6. Once you’ve pasted the .git files, **delete the created folder**.
7. Go back to GitHub for Windows and **delete the temporary repo** you created.
8. Click on the plus in the top left corner again and **click Add**.
9. Place the directory of the project you’re trying to repo into the Local Path.
10. **You should now have a repo** with the name of your project’s root folder in GitHub for Windows.



