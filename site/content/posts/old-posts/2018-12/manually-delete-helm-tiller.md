---
title: "HowTo: Manually delete Helm's Tiller"
date: 2018-12-26T18:21:33-05:00
tags: []
comments: false
---

# Problem

I was attempting to install Helm (and its user agent Tiller) onto my k8s cluster, but ran into an issue where Tiller wouldn't deploy its pod. Believing this to be a configuration issue, my first action was to try a reinstall to see if it would right itself. However, running the standard reinstall commands `helm reset` and `helm reset --force` resulted in the error:

`Error: could not find tiller`

The problem is that no Tiller being found is the root problem I'm trying to solve, so seems like I'm at a bit of a deadlock.

# Solution

As part of the `helm reset` command, Helm does a quick sanity check to determine whether or not tiller exists and, thus, whether or not it should look for it. To both get rid of Tiller and remove the remnants of Tiller that might trick Helm into thinking it exists, you must delete both the tiller deployment and the tiller service.

You can do this using the following:

```
# delete the deployment
kubectl delete deployment tiller-deploy --namespace kube-system

# delete the service
kubectl delete service tiller-deploy -n kube-system

helm init
```

This effectively performs helm reset, but disregards the sanity checks from helm reset (because we know we're doing a full refresh of these two resources) and thus is less prone to becoming stuck.

Sources: 
* https://github.com/helm/helm/blob/4204f69cd24c0dbe80c3045f89c6f315daa4055a/docs/install.md#installing-tiller-canary-builds