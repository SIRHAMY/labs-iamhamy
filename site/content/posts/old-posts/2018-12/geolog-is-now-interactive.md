---
title: "Geolog is now interactive!"
date: 2018-12-22T08:14:51-05:00
tags: [ "geolog", "hamy", "hamy-labs", "map", "timelapse" ]
comments: false
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/AnYlr8gjOLo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[About a year ago](https://medium.com/@SIRHAMY/in-review-my-summer-q3-2017-2a514f646316) I created my first iteration of Geolog by manually placing Bitmojis on top of a map png I had downloaded. It was a very manual process, but the outputs got some good feedback. Fast forward to today and many sections of my reflection (see: [my review of 2018 Q2](https://blog.hamy.xyz/posts/2018-q2-in-review/)) now include a Geolog.

This is great, but the process of creating them was still very manual and rigid. My original solution would only display images that were published in the repository which made further configuration require significant code changes.

For a time this was fine. I was the only user and I had direct access to the code/repo. However as my use cases for Geolog grew ever more diverse, I realized I needed to build something a bit more flexible/scalable.

To do so, I took the perspective of a basic user that has no previous Geolog knowledge and tried to understand what their use case would be. I came up with something like "I have some locations and I want to make a timelapse out of it". From there, I listed out all the things I would need to change in order to make using my tool as easy as possible.

In the end, the majority of the engineering was building scaffolding around user interactions to upload appropriate data and transform it into something that Geolog can use. Not the most exciting thing to work on, but it is a vast improvement over needing to manually git clone and configure the repo.

Once I had the scaffolding in place to enable Geolog to be more generalizable, I figured it was in a good spot for wider release. So, today, I'm deploying Geolog along with the rest of my site so that people can create their own geographic timelapses with their own data.

Interested? [Try Geolog now!](/projects/geolog)