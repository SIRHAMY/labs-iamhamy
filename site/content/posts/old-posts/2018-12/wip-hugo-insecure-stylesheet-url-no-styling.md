---
title: "Hugo Insecure Stylesheet Url No Styling"
date: 2018-12-13T19:27:33-05:00
tags: []
comments: false
draft: true
---

Error

Mixed Content: The page at '<URL>' was loaded over HTTPS, but requested an insecure stylesheet '<URL>'. This request has been blocked; the content must be served over HTTPS.
(index):1 Mixed Content: The page at 'https://blog.hamy.xyz/' was loaded over HTTPS, but requested an insecure script 'http://blog.hamy.xyz/js/main.js'. This request has been blocked; the content must be served over HTTPS.
(index):1 Mixed Content: The page at 'https://blog.hamy.xyz/' was loaded over HTTPS, but requested an insecure script 'http://blog.hamy.xyz/js/load-photoswipe.js'. This request has been blocked; the content must be served over HTTPS.
/favicon.ico:1 Failed to load resource: the server responded with a status of 404 ()

Fixed by changing config.toml to use new base url