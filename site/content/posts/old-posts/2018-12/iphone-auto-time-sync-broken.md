---
title: "iPhone: How to force your phone to re-sync its automatically-set timezone"
date: 2018-12-31T15:25:39-05:00
tags: []
comments: false
---

# Problem

I like to leave my phone with **Date & Time** > **Set Automatically** set to true. This lets my phone do the heavy lifting of switching time zones (and inherently deciding what the correct time zone is) without me having to lift a thumb. However, I've noticed that sometimes the timezone switch never triggers. How can I force my phone to update its time zone without setting it manually?

# Solution

It seems that there are two simple ways to solve this.

1. Turn your phone off and then back on. This will force your phone to retrieve **Date & Time** info as part of the startup process and thus it will fetch the new info
2. Navigate to the option (**General** > **Date & Time** > **Set Automatically**), disable it, then re-enable it. Similarly, this will cause your phone to re-fetch the relevant data, thus setting itself to the correct time zone.