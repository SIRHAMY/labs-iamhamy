---
title: "C++: Print Eigen::Quaterniond Values to Console"
date: 2016-04-01T19:12:49.000Z
date_updated:   2016-04-01T19:12:49.000Z
comments: false
---


**Problem: **I have a project where I’m using the Eigen library’s Quaterniond structure. I’ve initialized the values, but now I want to see what those values are by printing them to the console using std::cout. However, everytime I try something like this

> std::cout << “Debug: ” << “myQuaternion = ” << myQuaternion << std::endl;

I get an error like this:

> <span style="font-weight: bold; color: #c33720;">error: </span><span style="font-weight: bold;">invalid </span><span style="font-weight: bold;">operands to binary expression (‘basic_ostream<char, </span><span style="font-weight: bold;">std::__1::char_traits<char> >’ and ‘Eigen::Quaterniond’ (aka</span><span style="font-weight: bold;"> ‘Quaternion<double>’))</span>

How do I print my Eigen::Quaterniond’s values to the console?

**Solution: **To print the Quaternion’s values to the console, you have to print each value individually. Recall that you can access the Quaternion’s scalar with myQuaternion.w() and the vector with myQuaternion.vec().

Thus, you just have to print out these vals individually like so:

 

[[https://gist.github.com/SIRHAMY/ab7763cda2b842131de4](https://gist.github.com/SIRHAMY/ab7763cda2b842131de4)]

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/ab7763cda2b842131de4.json"></div>

