---
title: "Eclipse: Change which Program the Console View Receives Output From"
date: 2015-09-29T06:46:57.000Z
date_updated:   2015-09-29T06:46:57.000Z
comments: false
---


**Problem: **I have multiple console views, but they aren’t receiving output from the correct program.  How do I tell the Console View which program’s output I want it to display?

**Solution: **

<div class="wp-caption alignnone" style="width: 825px">Click “Display Selected Console” to choose which program the current console displays the output from

</div>*If you don’t know how to create multiple console views, check out [Eclipse: Open Multiple Console Views at Once](/posts/2015-09-29-eclipse-open-multiple-console-views-at-once/).*

To change which program the Console View receives output from:

- Select the desired Console View
- Click Display Selected Console in the top right corner
- Select which program you want to receive output from
- You should now see the ouput from the selected program in your current Console View



