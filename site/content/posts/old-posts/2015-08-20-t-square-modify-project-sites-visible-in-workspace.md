---
title: "T-Square: Modify Project Sites Visible in Workspace"
date: 2015-08-20T18:41:00.000Z
date_updated:   2015-08-20T18:41:00.000Z
comments: false
---


**Problem: **My T-Square workspace is cluttered with project sites that I don’t use anymore.  I want to get rid of them and only have the sites that I’m currently using visible on the main page.

**Solution: **

1. Login to your T-Square
2. Under the My Workspace tab, **click Preferences**
3. Change which sites are visible by selecting a site and clicking the arrows to move it from the “My Active Sites” to “My Hidden Sites” or vice versa.



