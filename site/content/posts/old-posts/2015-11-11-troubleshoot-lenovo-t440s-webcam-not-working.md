---
title: "Troubleshoot: Lenovo T440s Webcam Not Working"
date: 2015-11-12T02:24:38.000Z
date_updated:   2015-11-12T02:24:38.000Z
comments: false
---


**Problem: **I’m trying to use my Lenovo T440s built-in web cam, but it’s not working. When I turn it on, I can see the green light that indicates the webcam is in use. However, when I look at the application that’s using the webcam, it says there is no picture. More Info: Running Windows 10.

**Solution: **If the “On” light is on, but you’re still not seeing any video, try this:

- **Open Lenovo Settings** – you may have to update your software
- Once the app is running, **look at the bar of icons located at the top of the window**. At the very least, there should be a microphone, sun, and camera.
- If the camera has a red slash through it, it means that function has been turned off.
- **Click on the camera icon** to remove the slash and enable your webcam.



