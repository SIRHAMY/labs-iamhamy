---
title: "How to Restart Ubuntu via Terminal"
date: 2018-08-15T23:20:14-04:00
description: "About the page"
draft: false
---

**Problem:** I'm running my websites on a Digital Ocean droplet and every time I remote in via ssh, it says that I need to restart Ubuntu in order to complete an update. How do I restart Ubuntu?

**Solution:** Simple, remote into your machine and type:

`sudo shutdown -r now`

* `sudo` - tells terminal you want to run with admin permissions
* `shutdown` - this is what we're trying to accomplish
* `-r` - this flag tells Ubuntu we want it to restart after shutdown
* `now` - when we want the execution to occur

**Sources:**

* "How to reboot droplet through the command line / restart apache server" (https://www.digitalocean.com/community/questions/how-to-reboot-droplet-through-the-command-line-restart-apache-server)