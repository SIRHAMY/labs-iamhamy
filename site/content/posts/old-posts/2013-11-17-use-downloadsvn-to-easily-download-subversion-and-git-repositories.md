---
title: Use DownloadSVN to Easily Download Subversion and GIT Repositories
date: 2013-11-18T01:22:55.000Z
date_updated:   2013-11-18T01:22:55.000Z
comments: false
---


DownloadSVN isn’t the SVN handler of choice, but it does the job when all you’re looking to do is complete a quick and dirty pull.  The program comes in a lightweight package and requires <10 seconds set up time.  You can get the latest version of DownloadSVN here: [https://downloadsvn.codeplex.com/](https://downloadsvn.codeplex.com/)

<span style="text-decoration:underline;">How to use DownloadSVN:</span>

- Double Click **DownloadSVN** Application File
- Plug in Repository **Source URL**
- Select **Target Folder** to put downloaded files inside of
- Hit **Start**



