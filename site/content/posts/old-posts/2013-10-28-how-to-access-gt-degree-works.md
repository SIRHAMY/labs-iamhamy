---
title: How to Access GT Degree Works
date: 2013-10-28T18:11:21.000Z
date_updated:   2013-10-28T18:11:21.000Z
comments: false
---


This is a quick guide detailing how to access Georgia Tech’s Degree Works.

1. First, log in to your GT account at **buzzport.gatech.edu**
2. Click the **Registration – OSCAR** link
3. Click **Student Services & Financial Aid**
4. Click **Student Records**
5. Scroll down, then click on **Degree Works**
6. Click** Log In** in the upper right-hand corner of the page



