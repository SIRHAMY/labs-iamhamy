---
title: "Jersey: java.lang.ClassNotFoundException: com.sun.jersey.server.impl.container.servlet.ServletAdaptor"
date: 2014-05-23T12:02:20.000Z
date_updated:   2014-05-23T12:02:20.000Z
comments: false
---


**Problem: **java.lang.ClassNotFoundException: com.sun.jersey.server.impl.container.servlet.ServletAdaptor thrown when you try to run a Dynamic Web Project in Eclipse.  This happened while using Apache/Tomcat 6.0.37, so if you’re using something else, these steps might not resolve the problem.

**Solution: **

- Make sure you’ve added all the required JAR files to the project in the WEB-INF folder and that you’ve added them to the build path.  Once you’ve added them you can add them to the build path by right clicking on them and selecting Build Path -> Add to Build Path
- If you’ve done this and you’re still receiving the aforementioned error, navigate to the WEB-INF folder in your file explorer and copy the JAR files into the lib folder under WEB-INF.



