---
title: "C++: Implement Quaternion Multiplication with Eigen Library"
date: 2016-04-08T20:05:10.000Z
date_updated:   2016-04-08T20:05:10.000Z
comments: false
---


**Problem: **I’m using Eigen::Quaterniond, the built in Quaternion structure for the Eigen library. Unfortunately, it looks like the standard * operator performs normal multiplication, not the special quaternion multiplication required by an actual quaternion. How can I implement the special function to multiply a quaternion by a quaternion?

**Solution: **First, notice that your quaternion is composed of two parts – a scalar and a vector. With Eigen::Quaterniond, you can access these parts with myQuaternion.w() and myQuaternion.vec() respectively.

According to [Wikipedia](https://en.wikipedia.org/wiki/Quaternion), the formula for multiplying a quaternion by a quaternion is as follows:

> ![(r_1,\ \vec{v}_1) (r_2,\ \vec{v}_2) = (r_1 r_2 - \vec{v}_1\cdot\vec{v}_2, r_1\vec{v}_2+r_2\vec{v}_1 + \vec{v}_1\times\vec{v}_2)](http://i2.wp.com/upload.wikimedia.org/math/6/8/7/687f7d10c9876d3194930482368f8df8.png?w=730&ssl=1)

So, to implement this in C++, you might write something like this:

 

[[https://gist.github.com/SIRHAMY/9767ed75bddf0b87b929](https://gist.github.com/SIRHAMY/9767ed75bddf0b87b929)]

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/9767ed75bddf0b87b929.json"></div>

