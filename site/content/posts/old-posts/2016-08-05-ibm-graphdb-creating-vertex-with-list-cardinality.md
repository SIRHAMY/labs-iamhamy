---
title: "IBM GraphDB: Create a Vertex with LIST Cardinality"
date: 2016-08-05T16:52:36.000Z
date_updated:   2016-08-05T16:52:36.000Z
comments: false
---


**PROBLEM: **I’m using IBM’s Graph DB on Blue Mix, but I’ve been running into some issues. I’ve created a vertex called `tags` that has `LIST` cardinality, meaning it should be able to hold multiple values (including duplicates). However, when I try to POST a vertex with multiple values, I keep getting the error:

`{"code":"BadRequestError","message":"property 'tags' with meta properties need to have a 'val'"}`

What gives? How do I get my vertex with LIST to hold multiple values?

**Solution: **This solution applies to vertices of cardinality LIST and SET. Essentially, the error is occurring because [currently], GraphDB doesn’t support mapping lists to LIST properties using the API.

So, using the API you should:

- Create a vertex, without the LIST property
- Update the vertex with a value under the LIST property - Iterate until you have posted all of the values that are associated with the LIST property

GIST

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/316faa9bd51d9b2d34cedf65ffb57f44.json"></div>[[https://gist.github.com/SIRHAMY/316faa9bd51d9b2d34cedf65ffb57f44](https://gist.github.com/SIRHAMY/316faa9bd51d9b2d34cedf65ffb57f44)]

**Sources:**

- ********[What is the proper way to create a vertex with a set property in Bluemix Graph DB?](http://stackoverflow.com/questions/36024416/what-is-the-proper-way-to-create-a-vertex-with-a-set-property-in-bluemix-graph-d)



