---
title: "HTML: <TD> Tags Not Vertically Aligned"
date: 2016-04-25T14:06:20.000Z
date_updated:   2016-04-25T14:06:20.000Z
comments: false
---


**Problem: **I have an HTML table with rows containing two <td> tags. For some reason, the text within these tags is not vertically aligned, causing a small vertical shift. How can I fix this?

Example:

![Screen Shot 2016-03-23 at 2.43.25 PM](http://i1.wp.com/www.sirhamy.com/blog/wp-content/uploads/2016/03/Screen-Shot-2016-03-23-at-2.43.25-PM.png?fit=181%2C131)

Hard to see, but if you look really close, you can see that the results on the right are positioned slightly higher than the fields on the left.

**Solution: **The solution is the valign style.

In your CSS, just add:

> td {  
>  vertical-align: bottom;  
>  }

This will cause the vertical-align: bottom style to be added to all of your td tags, so all the content is aligned to the bottom of the “cell”. This will change the above example to look something like:

![Screen Shot 2016-03-23 at 3.05.18 PM](http://i2.wp.com/www.sirhamy.com/blog/wp-content/uploads/2016/03/Screen-Shot-2016-03-23-at-3.05.18-PM.png?fit=173%2C136)



