---
title: "Troubleshoot: Pandas Label not in Index"
date: 2016-03-14T22:30:45.000Z
date_updated:   2016-03-14T22:30:45.000Z
comments: false
---


**Problem: **I’m using Pandas to access values in a CSV file, but I keep getting this error:

> <span class="s1">KeyError: ‘the label [26] is not in the [index]’</span>

Can anyone point me in the right direction?

**Solution: **

First and foremost, you should take a look at your CSV file and ensure that index is actually there. Remember, the index that Pandas is using is the index that’s actually stored inside the CSV file. If you’ve opened your CSV in Excel, you’ll see that Excel automatically overlays row numbers on the spreadsheet which can mislead you to think that those indices exist in the file.

To make sure that your indices exist and that there are no breaks (skipped numbers), you can add:

> df = df.reset_index(); #df is a dataframe

**TL;DR **Make sure the index actually exists in your CSV file – reset_index to be safe



