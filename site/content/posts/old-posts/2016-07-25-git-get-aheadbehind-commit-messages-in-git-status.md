---
title: "Git: Get Ahead/Behind Commit Messages in Git Status"
date: 2016-07-25T21:50:10.000Z
date_updated:   2016-07-25T21:50:10.000Z
comments: false
---


**Problem: **I’m using Git to version control my project. Currently, ‘git status’ returns the number of commits ahead and behind I am for origin/master. However, I want git status to return the number of commits ahead and behind I am for a different branch. How do I do this?

**Solution: **With git 1.8 and later, the syntax is as follows:

While on the branch you’d like to track the difference between, run:

> git branch –set-upstream-to *remotename*/*branchname*

Now when you run git status, you’ll see the ahead/behind commit differences for the new branch you just set.

**Sources**

- [Git doesn’t show how many commits ahead of origin I am, and I want it to](http://stackoverflow.com/questions/5341077/git-doesnt-show-how-many-commits-ahead-of-origin-i-am-and-i-want-it-to)



