---
title: "Physics: Find the RPM Required for a Space Station to Produce Artificial Gravity"
date: 2014-03-03T15:32:14.000Z
date_updated:   2014-03-03T15:32:14.000Z
comments: false
---


**Problem: **A rotating space station is said to create “artificial gravity”—a loosely-defined term used for an acceleration that would be crudely similar to gravity. The outer wall of the rotating space station would become a floor for the astronauts, and centripetal acceleration supplied by the floor would allow astronauts to exercise and maintain muscle and bone strength more naturally than in non-rotating space environments. If the space station is <span style="color:red;">160</span> m in diameter, what angular velocity would produce an “artificial gravity” of 9.80 m/s<sup>2</sup> at the rim?

**Solution:**

We know that centripetal acceleration is Ar = V^2/R = W^2 * R, or centripetal acceleration is equal to angular velocity squared divided by the radius.  We will use this formula to solve the problem

Given: R = 80 m, Ar = 9.80 m/s^2

Find: W, convert to rpm

So, W = sqrt(Ar/R) = 0.35 rad/s

Now, to convert to rpm, we perform the following calculation:

(0.35 rad/1 s) * (60 s / 1 min) * (1 revolution / 2Pi radians) = 3.34 rpm

**Answer: 3.34 rpm**



