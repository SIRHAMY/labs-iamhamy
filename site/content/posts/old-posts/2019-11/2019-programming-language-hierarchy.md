---
title: "My 2019 programming language hierarchy"
date: 2019-11-28T13:19:47-05:00
tags: [
    "programming",
    "code",
    "language"
]
comments: false
---

Earlier today I was considering which language I should use for an upcoming project and realized that 1) I had a good amount of experiences / context to argue for each side and 2) that I'd had similar internal arguments in the past for many other projects so I thought this might make good content for a post. In this post I'll try to delineate my current internal hierarchy for languages, why I have them in this order, and any other relevant info that comes to mind. I have a feeling this will be extremely controversial (how do you compare two languages?) but by doing this, I hope to 1) know what I think by getting these thoughts outside of my brain and on DOM, 2) get some feedback on where my thoughts disagree with those of others, and 3) archive this so I can revisit it in future years.

Not every language that exists is listed here cause I don't actually consider every language known to man to build my projects in. Hopefully that needs no further explanation. Also I'll probably be wrong about a lot of things I declare in this post, that's part of the reason (#2) that I'm posting this in the first place.

Onwards.

# top languages

These are the languages that I have front of mind to tackle a project with.

## python

Until recently (really [when I joined Instagram](https://blog.hamy.xyz/posts/2019-h1-in-review/) where they use Python a fair amount), I had kind of written Python off as a programming language. Before IG, I had really only used Python in side projects in high school (omg so long ago!) and for academic projects (you know, those half-baked, built-for-a-5-minute-demo code blobs) so I'm assuming that lack of ecosystem exposure is really what stunted this relationship for so long.

Anyway, Python is now my go-to language for pretty much any project where performance is not paramount. It's simple (almost beautifully simple without sacrificing code ergonomics), #justworks pretty much everywhere, and has a huge, active community of people making it better and creating projects for almost any goal. This means that I can get up-and-running very fast and the internals are so good that I could reasonably create a robust system for almost any scale.

The only downside I've seen is really just performance and that only seems to matter when you need to do things extremely fast and / or at really high load (when small performance losses really start to add up). For most of my projects this is a fine trade off.

In fact, the only time I've really run into an issue is for complex / media-intensive projects. For instance some of my [#webz](/projects/webz/) outputs (example below) take several hours to complete where in a faster language it may be a magnitude smaller (though I'd bet much of that is just due to my own code inefficency, lul).

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/B5XxA3JFR1-/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B5XxA3JFR1-/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/B5XxA3JFR1-/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by HAMY (@hamy.art)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-11-27T13:56:32+00:00">Nov 27, 2019 at 5:56am PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

_A #webz output_

Some things I was pleasantly surprised to find well-supported in the language when I dove back in a few months ago (they may have been there all along and I just didn't find them /shrug):

* unit testing
* mocks
* async
* typing

### python pros

* simple
* fast enough for almost everything
* great, huge, and active community
* a package for almost anything you're trying to do
* just works
* just works everywhere

### python cons

* not great for super intensive jobs (thinking live video stuff, big / complex image manipulations, etc.)
* at high scale you may also feel burden of relative slowness (like startup times on lambdas that get hit a ton)

### python conclusion

For almost every project, I first see if Python makes sense. It just works and can get up-and-running super quick. That makes it my top choice.

## cSharp

Before [my first gig out of college (at APT)](https://www.linkedin.com/in/hamiltongreene/), I had never written a line of C#. My idea of it was that it was a monolithic, legacy language that only old, crufty enterprises used in the same realm as COBOL, Visual Basic, and PHP (lol).

To some extent, I feel like that hunch wasn't totally off - I don't see _that_ many startups / people writing their stuff in C#, but that doesn't mean that they don't exist (they do). However after really diving in (mostly writing in it for ~2 years), I've found it to be a really well designed, fast, and safe language that, again, just works and is getting constant updates from Microsoft (and after open sourcing, I'd assume many others as well). I often announce my like for C# with "C# is basically a better version of Java". #fightme

On top of C#'s speed and ergonomics (big fan of Linq - it's functional-ish syntax - as well as its normal syntax), C# has been widely used in the industry for quite some time so, similar to Python, has a devoted community with many packages and docs for much of your needs. Also it's the primary citizen of Unity which I've been eyeing for a while for future, more complex (think 3d!) iterations of audio visualizers, like [moon-eye](https://labs.hamy.xyz/projects/moon-eye/):

<iframe width="560" height="315" src="https://www.youtube.com/embed/MRTzmAWrixM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Some downsides of C# is that I feel like there is some friction in just getting started (there are a good amount of dependencies you need to DL), I'm not 100% confident in the cross-platform experience (though anecdotally I run Linux and C# has worked fine and they're def working on it), and I feel like the build process and common libs like web frameworks etc. just aren't as simple to use as Python (though if you compare to many other languages, they're still far simpler imo).

### c# pros

* pretty darn fast
* big community
* been around for awhile so lots of docs and battle-tested libs
* works everywhere, mostly
* a well-designed language

### c# cons

* slower than Python to get up-and-running
* I still don't feel that cross-platform is a first-class citizen in the ecosystem, even if the standard language is 100% cross-platform rn
* I don't think that C# is a first-class citizen in many cloud platforms (for instance [Google Cloud doesn't currently support C# for its version of lambda](https://cloud.google.com/functions/docs/writing/), though [AWS does](https://aws.amazon.com/lambda/faqs/))

### c# conclusion

If Python just isn't fast enough, I'll see if I can use C#. C# doesn't seem to have quite as many libraries for #everything like Python, but is much faster, works cross-platform for the most part, and feels v nice to write in.

## Rust

I'll be honest, I'm more of a Rust fan-boy than an actual Rust developer but it's still one of the top (3rd) languages I usually think about when considering what language to build a project in. The main reason for not yet taking the plunge is it's kinda hard.

Don't get me wrong, I think that it's an extremely well-designed language and it seems like the internet agrees. But I've read [the book](https://doc.rust-lang.org/book/) (great book btw!) and tried a few times to get simple programs working as expected but each time was like "ugh, f this I could've written this 5 different ways in Python, stood up a website to interact / show off / ingest it, and written a launch post by now" and then stopped and written it in Python.

I think what gets me is the necessity to think about how your program is handling memory, both from the ownership aspect and size / location aspect. Of course you've had to do this in other languages like C and C++ for instance, but that's one of the reasons both of those are near the bottom of my hierarchy. It's important because that's what all these languages are really doing behind the scenes - altering data - but I just don't think it's necessary for me to really need to think about this - Python and C# seem to agree.

But I think it's something that is good to know and that's one of the reasons why, despite this feature (not a flaw), that Rust is still in my top 3. I just need to find a problem that could benefit from its speed enough to warrant me taking the leap (and / or just take a month and make it happen).

Okay so that was a lot of barf all about why Rust is bad so why's it good? Well it's supposed to be super duper fast, it's a language that is said to be most "correct" in that if you can get it to compile then that thing will likely run 5ever unless there's like catastrophic interference (though this adds to the learning curve cause you've gotta fight the compiler to make it pass), it's got like every modern feature and is actually pretty ergonomic / pretty to write in minus maybe all the memory Boxing / Unboxing #magic, plus it's cool and new.

Like I bet if I rewrote [#webz](/projects/webz/) in Rust, it would be ~10x faster than the Python implementation (though also think it would take me 10+x longer to write than the Python one).

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/B4CsYfYlO15/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B4CsYfYlO15/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/B4CsYfYlO15/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by HAMY (@hamy.art)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-10-25T13:00:37+00:00">Oct 25, 2019 at 6:00am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

_A #webz output_

### Rust pros

* v fast
* has all ze modern features
* v type safe / strong emphasis on correctness
* a pretty language, minus all the memory things
* extremely active community

### Rust cons

* memory (Ahhh!)
* high learning curve due to focus on "correctness"
* new-ish and community isn't quite as big as others so less things written / documented

### Rust conclusion

Rust is a language that's basically on my "wish list". I've never written anything substantial in it but it just seems super cool and if I can ever get over my fear of memory management maybe I'll be more than a #fanboi. #swoon

# middle languages

These are languages that I'll use if it's really the right tool for the job (like if a framework / lib / assignment / project / domain requirement is that I write in it) but that I don't default to if I don't have to.

## javascript

Oh Javascript. Honestly Javascript has come a long way since I first started using it (or more accurately, I've probably come a long way since misusing it in blobs of nested JQuery callback hell), but it's not my favorite language. Like if I'm writing a server thing I'm almost definitely gonna choose !Javascript. But if it's something that I want visitable by a browser then probably Javascript (though [wasm](https://webassembly.org/) may change that (and entice me to use Rust!!1)).

Javascript is cool cause it's a first-class citizen (really primary) of web and web is accessible like everywhere and thus it's a great and flexible tool. It's pretty simple and when you add that to its inherent domain reach, you get a very large and active community of devs creating lots of packages and docs for you to use. Like any large and active community there's gonna be some good stuff and some maybe-not-so-good stuff (like I always try to prefer [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) over [W3schools](https://www.w3schools.com/jsref/jsref_splice.asp) but if W3schools is the only one that has it then fine) but this is almost totally a plus.

Javascript just isn't suuupppeeerr fast though browsers have done a great job making optimizations to make it run v fast. It also isn't that cross-platform. Like I'd say Javascript runs on the web, but wouldn't really say it runs on Windows, Mac, or Linux though you could certainly get it to.

One thing that Javascript is super good at (partially due to the maturity of the web as a platform, the maturity of this particular usecase, and its near-total-coupling with HTML / CSS (though you could argue some frameworks like React get it away from this)) is UI creation, design, and interaction. So while I might not use Javascript to do my heavy application lifting, I probably would consider it for creating a frontend for that application lifting (even over system-native UI frameworks / libs, mostly due to low cross-platform capability #buildonceruneverywhere).

### javascript pros

* primary web citizen (for now) so it basically runs everywhere
* huge, active community so lots of docs and libs
* simple
* v easy to get up-and-running (open in browser)
* really great UI ecosystem

### javascript cons

* not really cross platform but kinda
* not suuuppppeeeerr fast

### javascript conclusion

I use Javascript for web things. If it's not a web thing (like gets hit directly by a browser) or have a UI, then JS probably isn't involved cause it's not super fast, isn't as easy as Python, and not _really_ cross platform.

## go

Some people love Go. I am not one of those people. But I think I understand why.

Go is pretty fast, like 2/3 the speed of C in my mental model. It's pretty simple with simple syntax and setup. It works everywhere (I think). It has a strong community.

But I think it's ugly and it feels so simple that it's almost tedious.

That's really my only complaint with the language, but that's enough to get it down to #5.

### go pros

* fast
* simple
* good standard library
* good community

### go cons

* I think it's ugly in that it's too simple
* I also don't like the whole organization structure they force you into, though maybe that's changed since last I touched it (and I know there are workarounds but I am #lazy)

### go conclusion

I'll use Go if I'm contributing to a project in Go or someone wants to build in Go or I need to build in Go, but I'm not really ever thrilled to write in Go.

## java

Java is there. It's been there. It'll continue to be there.

It works well, has a huge, mature community, and is continually being updated. But I think C# is a better version of it. So I'll just use C#.

### java pros

* fast
* works everywhere (I think)
* big, active community

### java cons

* I think C# is better
* seems kinda old and outdated, idk

### java conclusion

Java will do what you need it to do but I prefer C#.

# bottom languages

These are languages that I'm going to go out of my way to avoid.

## c / c++

People will probably rage at me for slapping these bad boys together. But those people are likely already raging at me for other things I've said so what's a little more #rage?

These languages are fast and feel like they've been around forever. They kinda feel like the ancient ones that are all powerful but have huge downsides. You know like Zeus and all his random courtships that somehow end in tragedy.

Of course some people love them but I think those are the people that are willing to take the time to really get to know them through many hours of tracking down memory leaks. I'm not one of those people (right now) as I've spoken to in my section on [Rust](#rust). So for the time being, I'm trying to give these languages some space.

Also I wrote a few C++ programs for a graphics class in college and I didn't really understand the math nor the language so think I may still be scarred from that experience.

### c / c++ pros

* mature codebase and community, so lots of docs and lots of libs
* v fast - I'd almost say that they're the gold standard for performance in the programming industry but I don't really know anything about that
* I hear they're getting more modern! but also that some internal structures are making them hard to modernize but idrk

### c / c++ cons

* ew, memory
* They don't seem very fun
* Rust is shinier

## PHP

I've only written in PHP once in college and I've only dabbled in [Hack](https://hacklang.org/) (Facebook's PHP fork) at work a bit so I don't have much real experience to back up these opinions with. Of course, I don't have a lot of real experience to back up many of the preceding opinions so I'm not going to let that stop me now. Onwards.

The internet doesn't seem to look fondly on PHP. But also a ton of people use PHP. Mixing that in with my own experiences, I'd say that PHP works. Though it might have some serious downsides. But it'll probably work for you if you want to use it.

But you don't just have to work, you also have to be better than Python for me to want to use you all the time. I currently don't think it's achieved that thus I mostly won't use it.

### PHP pros

* A lot of people use it, so probably lots of docs / libs
* Can be pretty fast (I think)

### PHP cons

* not better than Python imo
* internet seems to have some bad feelings towards it, but I didn't really do my research so idk

## Swift

I don't really like Apple things. Swift is an Apple thing. But it's also one of the Apple things I like most, so this whole paragraph is really nil in the grand scheme of judging Swift.

I don't usually like Apple things cause they often make it very closed-system. My only evidence is that I have a Pixel and I can't do those funny reactions that iMessage users love so much and everybody complains when I turn a group green. QQ

But all that baggage, again, isn't really Swift's fault. Everyone I know that uses Swift for iOS development (and even some server-side!) says they like the language. But it seems (from like 5 mins of reading) that it's not super easy for Android. Maybe more of Apple's closed-system things, idk.

Anywho mostly I don't use Swift cause I've never had a good enough reason to try it out. Again, if it's not better than Python for my use case then probably not gonna use it. I don't do much mobile dev so that's probably a big factor in this scenario but there you have it.

I'm skipping pros and cons cause I don't really know that much about Swift.

### Swift conclusion

I don't do much mobile dev and I haven't heard much about Swift being a lot better than Python for non mobile so I haven't tried it and likely will continue not to.

## Assembly

Too low-level. V verbose. Kinda confusing. I choose Python.

# fin

Thanks for listening to my long-winded programming language rant! That's all I've got for you (for now). If you would like to discuss a point I've made in this post / provide feedback / give suggestions / just rage you can [contact me via methods listed in my contact page](https://hamy.xyz/connect/) or just comment below. If you want to read more rants from me, consider [subscribing](https://hamy.xyz/subscribe/).

Always building / ranting / posting

-HAMY.OUT


