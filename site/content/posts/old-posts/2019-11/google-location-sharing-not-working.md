---
title: "Google Maps: Location sharing not working"
date: 2019-11-11T22:03:11-04:00
tags: [
    "google",
    "maps",
    "location"
]
comments: false
---

# problem

I'm using Google Maps to location share with close friends, but ever since I changed phones people can no longer see my location with the error "XXX is offline". How can I fix this?

# solution

To fix:

* Open up Google Maps
* Click the hamburger button to reveal extra optionss
* Click `Location Sharing`
* If the problem is with your device not sending location updates to Google, there will be a banner saying "This device is not sending location updates". Click the `Fix` button in the banner.
* A pop-up will appear saying "Maps needs your location", click `go to settings`
* Flip the setting `Turn location history on`

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).