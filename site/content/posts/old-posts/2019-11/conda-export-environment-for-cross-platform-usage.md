---
title: "Anaconda: Export your environment for cross-platform usage"
date: 2019-11-04T22:55:14-04:00
tags: [
    "python",
    "conda",
    "cross-platform",
    "mac",
    "linux",
    "osx"
]
comments: false
---

# problem

I'm using Anaconda for my Python virtual environment needs. I usually develop on my Linux desktop but, on the road, will develop on my Mac laptop. When I try to install the dependencies for my project which are stored in an `environment.yml` file with `conda env create --name MYENVIRONMENT --file environment.yml`, conda keeps crashing saying it could not complete the install. How can I make it so that the dependency file works across platforms?

# solution

The problem here is that many Python packages are going to be pre-built with specific systems in mind, so you could imagine there might be a `packagex` distribution for Windows, Mac, and possibly several flavors of Linux as well. Thus you have to specify to conda during the export that you just want `packagex` specified and not an OS-specific distro like `packagex-windows`.

I should point out that just because you can specify a non-OS-specific distro, that doesn't automatically mean that requirement list will work cross-platform straight out of the box. If `packagex` didn't have a Linux-specific distro, then it's possible it just won't work on Linux. All that being said, most of the common Python packages will likely have distributions for every major OS so you should be good to go.

To export your conda environments package requirements to `envrionment.yml` such that an install from it will work cross-platform, you can use the `--no-builds` flag:

```
conda env export --no-builds > environment.yml
```

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).
