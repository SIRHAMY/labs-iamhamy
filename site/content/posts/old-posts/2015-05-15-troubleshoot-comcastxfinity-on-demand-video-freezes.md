---
title: "Troubleshoot: Comcast/XFinity On Demand Video Freezes"
date: 2015-05-16T00:09:58.000Z
date_updated:   2015-05-16T00:09:58.000Z
comments: false
---


**Problem: **While playing a video (show, movie, etc.) the video gets stuck during a commercial.  Rewinding and fastforwarding the video still get stuck at the same point.

**Solution: **In order to get past the freeze point in an on demand video, do the following:

- **Stop the video** by pressing the button with a square on your remote.
- This will pop up a list of videos that you have watched recently.  **Select the video you were watching** from this list.
- Select the option to **continue where you left off**.
- This should reset the commercial break and you should be able to get back to your show.



