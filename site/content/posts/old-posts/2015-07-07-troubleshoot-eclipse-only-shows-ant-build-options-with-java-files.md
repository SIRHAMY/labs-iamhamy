---
title: "Troubleshoot: Eclipse only Shows Ant Build Options with Java Files"
date: 2015-07-08T03:03:22.000Z
date_updated:   2015-07-08T03:03:22.000Z
comments: false
---


**Problem: **I’m trying to run some Java files in Eclipse, but the only options are to run an Ant build and everytime I do that I get an error “Ant can’t be found”.  How do I run my Java files in Eclipse?

**Solution: **Basically, all you have to do is put these files in an Eclipse project.  Follow these steps:

1. **Create a new Project in your workspace.**  This is where you’ll put your Java files.
2. Go to **File -> Import -> File System**
3. **Locate the directory** where your files are stored.
4. Select them then select the new project you just created as the destination folder.
5. Hit Next.
6. Now **make sure your files are in the src folder** of the project, if you selected the option to hold files in the src folder.
7. You should now be able to run your Java files.



