---
title: "Troubleshoot: Adobe Photoshop Text Chars not Showing Up"
date: 2016-05-27T14:17:37.000Z
date_updated:   2016-05-27T14:17:37.000Z
comments: false
---


**Problem: **I’m trying to add text to my image via Adobe Photoshop, however when I type, it just adds underlines instead of the actual text. What am I doing wrong? How do I get my characters to show up?

**Solution: **The underlines are there to show you that the program is typing something. In this case, it’s very likely the color of the font is the same as the color of the background. Change the font color and you should be able to see your text once again.



