---
title: "Calculus: Find the Gradient of the Function at the Given Point"
date: 2013-10-02T20:33:14.000Z
date_updated:   2013-10-02T20:33:14.000Z
comments: false
---


**Find the gradient of the function g(x,y) = (x^2)/2 – (y^2)/2 at the point (sqrt(2),1)**

Start by taking the partial derivative of the function with respect to x and y

Partial derivative w.r.t. x = x

Partial derivative w.r.t. y = -y

Therefore the Gradient g(x,y) = (x,-y)

Plug the original (x,y) values into the value given by the Gradient g(x,y) to get

**Solution: Gradient g(Sqrt(2),1) = (sqrt(2), -1)**



