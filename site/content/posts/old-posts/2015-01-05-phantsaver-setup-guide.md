---
title: PhantSaver Setup Guide
date: 2015-01-06T00:51:17.000Z
date_updated:   2015-01-06T00:51:17.000Z
comments: false
---


*Note that there are some compatibility issues with Windows 8.  Let me know if you run into any others.*

PhantSaver is the new screen saver I coded over the winter break.  This is a short guide to get you up and running with it.

1. Go to the <span style="text-decoration:underline;">[PhantSaver page](http://hamycodes.com/products/phantsaver/)</span> and download the correct version for your system.
2. Once you’ve downloaded it, unzip the files into a directory on your hard drive.  *Note: **If you move the files after you install the screen saver, the computer will uninstall the screen saver and you’ll have to re-install it.*
3. Go into the file you just placed and right-click on “PhantSaver.scr” (might be “ssPhantSaver.scr”)
4. Click “Install” to install it.
5. Choose your screen saver options.
6. Enjoy!



