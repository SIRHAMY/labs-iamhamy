---
title: "Mac: Force Outlook calendar to update"
date: 2019-09-16T22:50:52-04:00
tags: [
    "mac",
    "outlook",
    "tutorial",
    "calendar"
]
comments: false
---

# problem

I'm using Outlook on Mac and I find that often my calendar isn't up-to-date with meeting requests I get. How can I force my calendar to update?

# solution

Unfortunately there's no good way to always ensure that your calendar is up to date. What you can do is manually force it to update whenever you want to look at it. 

To force your Mac calendar to update, you can go to the inbox view (where you see your mail) and, in the top bar, click the `Send & Receive` button. This will tell Outlook to go look for new information and send any information that's pending. In the process, this will also update any calendar information.

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).