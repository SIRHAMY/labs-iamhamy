---
title: "Python: ImportError: No module named asyncio"
date: 2019-09-23T23:33:16-04:00
tags: [
    "python",
    "asyncio",
    "import-error",
    "troubleshoot",
    "tutorial"
]
comments: false
---

# problem

I'm trying to use `asyncio` in my Python program but it keeps failing with `ImportError: No module named asyncio`. `asyncio` is supposed to be a built-in library so why is this failing?

Here's some example code I was able to reproduce it on:

```
import asyncio

async def main():
    print("running")

asyncio.run(main())
```

# solution

You're write, `asyncio` is a built-in library for Python versions 3.7+. So if you're getting an error with the import, check your python version with `python --version` and make sure that you're running at least version 3.7. 

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).