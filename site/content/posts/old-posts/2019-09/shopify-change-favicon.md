---
title: "Shopify: Change site favicon"
date: 2019-09-09T05:53:06-04:00
tags: [
    "shopify",
    "favicon",
    "troubleshoot",
    "tutorial"
]
comments: false
---

# problem

I'm using Shopify to build my online store and am in the midst of customizing it for my brand. I can't figure out how to change the favicon though (the picture that's next to the website name in the browser tab). How do I do this?

# solution

You can change the favicon through the Shopify editor. Do this by:

* Go to Shopify and login to your store
* Click `Online Store > Themes` from the sidebar
* Next to `Live Theme`, click `Customize` to pull up the visual editor
* Now click the `Theme settings` tab and then select `Favicon`

You should now be able to upload a custom favicon to your site.

# source

* As of writing, I use Shopify for [my own store](https://shop.hamy.xyz)

# did this help?

I regularly post about tech topics I run into. You can get a periodic email containing updates on new posts and things I've built by [subscribing here](https://hamy.xyz/subscribe).