---
title: "Windows Subsystem for Linux: How to edit dotfiles with Visual Studio Code"
date: 2019-04-15T14:15:27-05:00
tags: [ "wsl", "cli", "vscode", "dotfile" ]
comments: false
---

# Problem

I use VS Code for most of my editing, so naturally I tried to edit my WSL `.gitconfig` file in code using `code ~/.gitconfig`. Now everything else I've opened (non dotfiles that is) with code has worked as expected. I get my normal Windows install of code to open with the directory and saving works as normal. However when I try to open my dotfiles like `.gitconfig` or `.bashrc`, it seems like I'm opening up two different files completely. What gives?

# Solution

As touched upon in [Dave Rupert's intro to developing on Windows with WSL](https://daverupert.com/2018/04/developing-on-windows-with-wsl-and-visual-studio-code/) your WSL home directory (accessible at `~` from within WSL cli) is actually a special folder and should not be accessed by any non-wsl apps. So attempting to open one of these configs with your Windows version of code just won't work.

In my case, what happened when I tried to open my `.gitconfig` with `code ~/.gitconfig` is that code actually created an identical path within my `C:/` drive of `C:/~/.gitconfig` and had me editing that file. This was confusing as everytime I opened the file I would get my previously-saved version from the Windows file store whereas all my WSL apps were reading from the WSL file store rendering my changes to `.gitconfig` ineffective.

In order to change dotfiles (or really anything in your WSL-controlled home directory) you'll need to open it with a non-Windows app (think `vim` or `nano`).