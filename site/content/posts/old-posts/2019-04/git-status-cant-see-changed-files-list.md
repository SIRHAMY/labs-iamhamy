---
title: "Git: Can't see list of changed files when running `git status`"
date: 2019-04-22T14:36:06-05:00
tags: [ "git", "config", "troubleshoot" ]
comments: false
---

# Problem

I've changed some files in my directory and `git status` shows the tell-tale `Changes not staged for commit:` message which implies that git knows there are changed files in my directory. However, I can't see any of the file names that are listed below it. How can I fix this?

# Solution

Because git seems to know that there are files that require staging, it's unlikely that anything is actually wrong with git. Instead, it's more likely that somehow the color configuration of your git outputs match the background of your terminal and thus that git is outputting the file list just that you can't see them.

You can change these configurations via your `.gitconfig` file located at `~/.gitconfig` on Linux and Windows Subsystem for Linux.

To manually change these, open the file in your favorite editor and add something like:

```
[color "status"]
  added = green
  changed = red bold
  untracked = magenta bold
```
[source](https://unix.stackexchange.com/a/44283)

_Note: If you're using WSL, make sure you open your `~/.gitconfig` with a WSL-native app like vim or nano to avoid [Windows creating a shadow-copy of your dotfile when opening with a Windows app](/posts/opening-dotfile-with-vs-code-in-wsl-returning-wrong-contents)_

Change the color of the output message you can't see and that should do the trick!