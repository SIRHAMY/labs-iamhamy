---
title: "Windows Subsystem for Linux: Remove WSL command line's directory highlighting"
date: 2019-04-08T13:24:37-05:00
tags: [ "wsl", "cli" ]
comments: false
---

# Problem

The default editor theme for WSL is awful. Every directory name gets highlighted with a funky green highlight that makes it super hard to read what the name of the directory is in the first place. How do I remove that highlighting?

# Solution

According to an issue in the `vscode` repo (https://github.com/Microsoft/vscode/issues/7556), the themes of the WSL command line are settable just as you'd set anything else, via `.bashrc`.

As such, you can get rid of the highlighting by running these commands in your WSL CLI (as supplied by GitHub user [karlti](https://github.com/karlti)):

```
LS_COLORS='ow=01;36;40'
export LS_COLORS
```

# Bonus

If you're looking to get your WSL command line to look like that of Ubuntu's, this is a great guide: https://medium.com/@jgarijogarde/make-bash-on-ubuntu-on-windows-10-look-like-the-ubuntu-terminal-f7566008c5c2 