---
title: "Python: How to Append Key to JSON Record"
date: 2016-05-04T23:02:11.000Z
date_updated:   2016-05-04T23:02:11.000Z
comments: false
---


**Problem: **I have a JSON file consisting of a single JSON object per line. I’m parsing through it and trying to add a single key, value pair to each of these records. How can I add this key-value pair to the current JSON object?

**Solution: **The solution I’m about to give you is *not *in-place, meaning it involves writing the entire thing out to another file.

Here’s a gist:

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/0d0c112118a8e07f8a5ca367bf6cd587.json"></div>[[https://gist.github.com/SIRHAMY/0d0c112118a8e07f8a5ca367bf6cd587](https://gist.github.com/SIRHAMY/0d0c112118a8e07f8a5ca367bf6cd587)]

We’re reading in the json line by line and modifying that line with our new input then writing it to a new file.



