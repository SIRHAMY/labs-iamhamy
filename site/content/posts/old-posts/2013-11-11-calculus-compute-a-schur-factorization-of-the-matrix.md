---
title: "Calculus: Compute a Schur Factorization of the Matrix"
date: 2013-11-12T01:21:07.000Z
date_updated:   2013-11-12T01:21:07.000Z
comments: false
---


**Compute a Schur Factorization of the matrix A = **

<table><tbody><tr><td>-3</td><td>4</td></tr><tr><td>-6</td><td>7</td></tr></tbody></table>Because A(1, 1) = (1,1), we know that (1,1) is an eigenvector of A.

By normalizing this eigenvector, we get U1 = (1/sqrt(2))(1,1)

From U1, we get U2 = vector perpendicular to U1 = (1/sqrt(2))(-1,1)

Q = (U1, U2) =

<table><tbody><tr><td>1/sqrt(2)</td><td>-1/sqrt(2)</td></tr><tr><td>1/sqrt(2)</td><td>1/sqrt(2)</td></tr></tbody></table>and Q^-1 =

<table><tbody><tr><td>1/sqrt(2)</td><td>1/sqrt(2)</td></tr><tr><td>-1/sqrt(2)</td><td>1/sqrt(2)</td></tr></tbody></table>**Solution: T = (Q^-1)AQ is a Schur Factorization of A**

**T = **

<table><tbody><tr><td>1</td><td>10</td></tr><tr><td>0</td><td>3</td></tr></tbody></table>**Q = **

<table><tbody><tr><td>1/sqrt(2)</td><td>-1/sqrt(2)</td></tr><tr><td>1/sqrt(2)</td><td>1/sqrt(2)</td></tr></tbody></table>

