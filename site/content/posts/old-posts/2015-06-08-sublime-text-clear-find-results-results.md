---
title: "Sublime Text: Clear 'Find Results' Results"
date: 2015-06-08T10:28:43.000Z
date_updated:   2015-06-08T10:28:43.000Z
comments: false
---


**Problem: **I’ve made a few searches which has left my Find Results tab pretty full and slow.  I need to erase this data so that I can use the tab easier.

**Solution: **Like any other tab in Sublime, you can edit the Find Results page.  So, if you want to clear it, simply select everything you want to delete and delete it.  Simple as that.



