---
title: "Tiny Tower: Elevator Stars"
date: 2014-06-23T09:39:18.000Z
date_updated:   2014-06-23T09:39:18.000Z
comments: false
---


In your Tiny Tower, you may have noticed some stars appearing inside your elevator shaft.  You’re probably wondering what they are for.

**Observation: **There are stars in my Tiny Tower elevator shaft.

**Purpose: **The stars represent the amount of Bitizens located on the corresponding level that are performing their dream job.  More stars means more Bitizens doing what they love.



