---
title: Checkout Source Code from Google Code using SubClipse and Eclipse
date: 2013-11-18T00:05:26.000Z
date_updated:   2013-11-18T00:05:26.000Z
comments: false
---


This tutorial will guide you through the process of checking out source code from a Google Code repository using SVN into an [Eclipse](http://www.eclipse.org "Eclipse (software)") workspace.  Before this guide will be of any use to you, you’re going to want to first install SVN onto your computer. * You can find up-to-date versions at [http://subversion.apache.org/](http://subversion.apache.org/).  Subclipse is a good alternative that interfaces directly with the Eclipse environment – you can download that here: [http://subclipse.tigris.org/](http://subclipse.tigris.org/).*

- Create a project in Eclipse where you’ll store the checked out files locally
- Right-click on the project you just created and click **Team -> Share Project…**
- Assuming you have the SubClipse plugin successfully installed, you should see an **SVN** option.  Select it and hit next.
- In the URL field provided, put your Google Code project’s **SVN checkout URL **(You can find this URL by navigating to the **Source -> Checkout ** page)



