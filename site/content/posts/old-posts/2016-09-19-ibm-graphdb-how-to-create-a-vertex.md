---
title: "IBM GraphDB: How to Create a Vertex"
date: 2016-09-19T16:56:38.000Z
date_updated:   2016-09-19T16:56:38.000Z
comments: false
---


**Problem: **I’m using the IBM Blue Mix GraphDB. How do I add a vertex?

**Solution: **

Hit the API with an HTTP request:

- HTTP: POST
- URL: https://ibmgraph-alpha.ng.bluemix.net/$APIURL/$GRAPHNAME/vertices
- BODY - The things you want your vertex to hold

<style>.gist table { margin-bottom: 0; }</style><div class="gist-oembed" data-gist="SIRHAMY/0f1a0ed740ba6543ab103c031f795fe7.json"></div>[[https://gist.github.com/SIRHAMY/0f1a0ed740ba6543ab103c031f795fe7](https://gist.github.com/SIRHAMY/0f1a0ed740ba6543ab103c031f795fe7)]



