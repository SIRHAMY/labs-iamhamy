---
title: "HowTo: Reset Kensington Portable Combination Lock"
date: 2018-08-25T08:33:08-04:00
description: "About the page"
draft: false
---

**Problem:** I've got a Kensington Portable Combination lock but am unsure how to set it. Right now all the numbers are set to `0000` and it unlocks but I really don't want to leave it at that combo (cause | security |), how do I reset the combination?

**Solution:** The portable combination lock has a reset apparatus on the side of the lock opposite the locking mechanism. This guide will walk through how to use it, assuming that you are resetting from factory defaults. If not, then you'll need to replace the `0000` combination with whatever combination it's set to.

To reset your Kensington Portable Combination Lock pass code:

* Put the correct combination into the indicated area
* Grab a small tool (paperclip, flathead screwdriver, etc) and push in the reset apparatus, rotating it 90 degrees
* Set the desired combination in the indicated area
* Twist the reset apparatus back 90 degrees and allow to push out again.

Combination set.

**Sources:**

* [Kensington Combo Lock docs](https://www.kensington.com/us/us/s/k64673am/combination-laptop-lock-k64673am)
