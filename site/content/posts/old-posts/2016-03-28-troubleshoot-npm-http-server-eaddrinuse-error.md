---
title: "Troubleshoot: NPM HTTP-Server EADDRINUSE Error"
date: 2016-03-28T23:26:48.000Z
date_updated:   2016-03-28T23:26:48.000Z
comments: false
---


**Problem: **I’m trying to use the NPM module HTTP-Server to run a localhost, but it keeps throwing an error like so:

> events.js:141
> 
>       throw er; // Unhandled 'error' event
> 
>       ^
> 
> Error: listen EADDRINUSE 0.0.0.0:8080
> 
>     at Object.exports._errnoException (util.js:870:11)
> 
>     at exports._exceptionWithHostPort (util.js:893:20)
> 
>     at Server._listen2 (net.js:1237:14)
> 
>     at listen (net.js:1273:10)
> 
>     at net.js:1382:9
> 
>     at nextTickCallbackWith3Args (node.js:452:9)
> 
>     at process._tickCallback (node.js:358:17)
> 
>     at Function.Module.runMain (module.js:444:11)
> 
>     at startup (node.js:136:18)
> 
>  at node.js:966:3

How can I get past this and run my server?

**Solution: **Basically, this error is telling you that the port number you’re trying to use is already in use. Therefore, you can’t use HTTP-Server on it. Instead, you should choose a different port number. You can do this by typing:

> http-server -p [INSERTPORTNUMBER]

Simply choose a portnumber that’s not in use, and you should be good to go.



