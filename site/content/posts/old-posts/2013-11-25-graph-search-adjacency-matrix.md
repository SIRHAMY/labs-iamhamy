---
title: "Graph Search: Adjacency Matrix"
date: 2013-11-25T12:49:25.000Z
date_updated:   2013-11-25T12:49:25.000Z
comments: false
---


![](https://lh3.googleusercontent.com/QuC-1rIAKv3gT1LwFDgjMlCzEUoNDrWFcJyK0vCLUTCOTSboZ1pMPcJlBZeFRkewY7ZoGw5SrHV8Kf_bJ7hA4kyXBJrypT3j8qmp3d0kN8FDPmJDe0W-OTL7mg)

For each coordinate in the matrix, place a 1 if the nodes are connected and a 0 if they aren’t.  In the case of a weighted graph, replace the one with the respective weight.

**Adjacency Matrix**

<table border="1"><tr><td>Node</td><td>1</td><td> 2</td><td> 3</td><td>4 </td><td>5 </td><td> 6</td></tr><tr><td> 1</td><td>1</td><td> 1</td><td> 0</td><td> 0</td><td> 1</td><td> 0</td></tr><tr><td> 2</td><td>1</td><td> 0</td><td> 1</td><td> 0</td><td> 1</td><td> 0</td></tr><tr><td> 3</td><td>0</td><td>1 </td><td>0 </td><td>1 </td><td>0 </td><td>0 </td></tr><tr><td> 4</td><td>0</td><td>0 </td><td>1 </td><td>0 </td><td>1 </td><td>1 </td></tr><tr><td> 5</td><td>1</td><td>1 </td><td>0 </td><td>1 </td><td>0 </td><td>0 </td></tr><tr><td> 6</td><td>0</td><td>0 </td><td>0 </td><td>1 </td><td>0 </td><td>0 </td></tr></table>

