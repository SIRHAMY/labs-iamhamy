---
title: "Troubleshoot: Syncing Todoist with Google Calendar Events Using Zapier"
date: 2016-01-03T06:17:32.000Z
date_updated:   2016-01-03T06:17:32.000Z
comments: false
---


**Problem: **I’m using Todoist to keep track of my tasks for the day. I want to sync the tasks with set due dates with my Google Calendar for easier viewing. To do this, I set up a Zap using Zapier’s connection service so that every time I create a task with a due date in Todoist, it creates an event in my Google Calendar at that time. However, every time I create one of these tasks, the Calendar event is wrong. How do I sync the services so that the event times match up?

**Solution: **This tutorial assumes you’ve already created your zap and that events are being synced from Todoist to Calendar correctly in every way except the time at which they occur.

The first thing you should do, is go to your Zapier, Todoist, and Google Calendar settings and make sure the time zones are in sync. If one is off, that’s probably why your events are being set wrong. If they are in sync and you’re still experiencing this problem, continue on.

- Go to your **Zapier Dashboard** and select the Zap you created
- Click the dropdown directly under Google Calendar. **Set this drop down to “New Detailed Event”**. This will help us manually set the time offset. *Note: I’ve found that simply changing this to New Detailed Event instead of Quick Event fixed my problem, but we’ll still set up the offset just in case. It’s really easy to revert back to normal, so don’t worry about breaking anything.*
- Scroll down to “**Match up Todoist Incomplete Task with Google Calendar Detailed Event**“
- Next to the **“Summary” field**, click Insert Fields, then **select Content**. You can also add other fields if you so desire, but I’m just going to walk you through the bare minimum.
- In the **“Start Date & Time”** field, click Insert Fields, and **select Due Date**. If you’ve noticed that all your events are being set to five hours after you set the due date, then we’ll put -5h after “Due Date”. This tells Calendar that we want the due date to be set five hours before the time Todoist sent.
- In the **“End Date & Time”** field, click Insert Fields, and select **Due Date**. Now, you’re going to want to set the end date and time to be after the start date and time. I have all my events default to an hour long, so if you did end up putting the -5h offset in the start date and time, you’d set this offset to -4h.
- Scroll down to the bottom and **click “Turn Zap On”**. This will initialize our new settings.
- **Test this new zap** by creating a new task in Todoist with a due date and time. Wait a few minutes so that Zap has a chance to sync with Calendar (under some plans this can take up to 15 minutes). Check Calendar to see if the event created is at the time you input on the Todoist task. If it is, you’re good to go! If not, keep tinkering around with the offset until you get it right.

Cheers to a productive 2016!



