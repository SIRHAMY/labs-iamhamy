---
title: "Troubleshoot: Running Processing Executable Leads to Infinite Blank Screen"
date: 2015-01-05T23:10:57.000Z
date_updated:   2015-01-05T23:10:57.000Z
comments: false
---


You may have run into an issue when creating programs with Processing where you export your working sketch into an exe, run the file, and are then presented with a blank screen that goes on indefinitely.  Usually, you should have your console open so that you can see any error messages that may appear, but assuming you wish to forego that, try checking these two common issues.

1. Make sure all resources required by the program are placed inside the created executable’s folder.  This means sounds, fonts, images, and any other external materials are placed in the same directory as the application.  If they aren’t, then your program is probably looping due to a null pointer exception.
2. Make sure that you export java with your program.  While this probably won’t make a difference on your machine, it could ensure many of your users aren’t running into issues.



