---
title: "Webpack: Change webpack-dev-server Port"
date: 2016-03-25T14:14:43.000Z
date_updated:   2016-03-25T14:14:43.000Z
comments: false
---


**Problem: **I’m using webpack-dev-server to host my files locally. It’s defaulting to port 8080, but I want to use it on port  number 5000. How can I make webpack-dev-server serve files to ‘http://localhost:5000’?

**Solution: **To change the port webpack-dev-server serves on, use the –port flag.

For example, to have it serve on port 5000 run the following in your project directory:

> webpack-dev-server –port 5000

Source: [Webpack Documentation](https://webpack.github.io/docs/webpack-dev-server.html)



