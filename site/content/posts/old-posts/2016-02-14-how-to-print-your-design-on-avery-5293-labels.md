---
title: How to Print your Design on Avery 5293 Labels
date: 2016-02-15T02:27:48.000Z
date_updated:   2016-02-15T02:27:48.000Z
comments: false
---


You’ve got Avery 5293 label sheets and a sweet design to put on them, but how do you go about doing it?

This tutorial is going to walk you through getting your design onto a template and printing them on Avery 5293 labels. So creating the design itself is on you.

- Download the [Avery 5293 template for Microsoft Word](http://www.avery.com/avery/en_us/Templates-%26-Software/Templates/Labels/Round-Labels/Round-Label-24-per-sheet_Microsoft-Word.htm?N=0&refchannel=c042fd03ab30a110VgnVCM1000002118140aRCRD). The rest of this tutorial assumes you’re using this. If you don’t have Word, I’m told there are PDF templates around, but I haven’t used any myself. - When downloading this, make sure you grab the “Template Only” option so you don’t install any unwanted adware.
- Open your template in Word
- Convert your Word doc to the newest format. You can do this by going to File > Convert
- Now copy your design (preferably with a transparent background) and paste it into the first circle.
- Arrange your design within this circle how you wish. This is the template we’ll use for the rest of the sheet.
- Now this next part is a bit odd, so pay close attention. You need to select both your design and the circle in which it resides. To do this, LEFT CLICK on your design then SHIFT + LEFT CLICK on the circle its in. *Holding shift lets you select multiple objects.*
- Copy the selection (the circle and the design) using CTRL + C
- Left click the next circle so that you can see the control points (should be gray squares) around it. Paste the design using CTRL + V. You should see the design centered in the circle that was already there and the circle you just pasted should be off-center. Select that circle and delete it. - It may seem more intuitive to just copy the design in the previous steps and paste that into the middle of the circle, but in my experience that leaves the design off-center. The fastest way to get identical designs is to paste both the circle and the design then delete the superfluous circle.
- Repeat these steps til you’ve filled each spot.
- Now go back through and delete any circle outlines that remain. If you don’t these <span style="text-decoration: underline;">will </span>appear on the final print outs.
- All that’s left is feeding the labels into your printer and sending the print job.

 



