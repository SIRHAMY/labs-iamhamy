---
title: "Calculus: Find the Eigenvalues, Eigenvectors, and Orthogonal Matrix that Diagonalizes the 2x2 Symmetric Matrix"
date: 2013-10-17T19:46:48.000Z
date_updated:   2013-10-17T19:46:48.000Z
comments: false
---


**Find the eigenvalues, eigenvectors, and orthogonal matrix that diagonalizes the 2×2 symmetric matrix A**

A =

<table><tbody><tr><td>1</td><td>2</td></tr><tr><td>2</td><td>4</td></tr></tbody></table>Because A is a 2×2 symmetric matrix, we can find the eigenvalues using the following formulas:

μ+ = (a+d)/2 + sqrt(b^2 + ((a-d)/2)^2) and μ- = (a+d)/2 – sqrt(b^2 + ((a-d)/2)^2)  
 μ+ = (1+4)/2 + sqrt(2^2 + ((1-4)/2)^2) and μ- = (1+4)/2 – sqrt(2^2 + ((1-4)/2)^2)  
 μ+ = 5/2 + 5/2 and μ- = 5/2 – 5/2  
 μ+ = 5 and μ- = 0

Now that we have the eigenvalues, we can find the eigenvectors of A.  Because A is a 2×2 matrix, we can find these eigenvectors using the following formulas:

U1 = (1/|r1|) * vector orthogonal to r1  
 U2 = vector orthogonal to U1

where

A – μ+I =

<table><tbody><tr><td>r1</td></tr><tr><td>r2</td></tr></tbody></table>First we find r1 by subtracting μ+ times the identity matrix from A.

A – μ+I =

<table><tbody><tr><td>-4</td><td>2</td></tr><tr><td>2</td><td>-1</td></tr></tbody></table>which means r1 = (-4, 2)

Now we find the first eigenvector (U1) by multiplying the vector orthogonal to r1 by 1 over the dot product of r1

1/|r1| = 1/sqrt((-4)^2 + 2^2) -> 1/(2*sqrt(5))

The vector orthogonal to r1 can be found by flipping the vector values of r1 then multiplying the top one by -1.

Vector orthogonal to r1 = (-2, -4)

U1 = (1/|r1|) * vector orthogonal to r1 = (1/(2*sqrt(5)) * (-2,-4) -> (1/sqrt(5))*(-1,-2)  
 U2 = vector orthogonal to U1 = (1/sqrt(5))*(2,-1)

The orthogonal matrix that diagonalizes A can be found by:

U = {U1, U2}

U = (1/sqrt(5))*

<table><tbody><tr><td>-1</td><td>2</td></tr><tr><td>-2</td><td>-1</td></tr></tbody></table>**Solution: Eigenvalues: μ+ = 5 and μ- = 0  
 Eigenvectors: U1 = (1/sqrt(5))*(-1,-2) and U2 = (1/sqrt(5))*(2,-1)  
 Orthogonal matrix that diagonalizes A: U = (1/sqrt(5))***

<table><tbody><tr><td>-1</td><td>2</td></tr><tr><td>-2</td><td>-1</td></tr></tbody></table> 



