---
title: "Troubleshoot Mac: Eclipse Installation Asks for Java 1.6"
date: 2016-01-29T03:22:15.000Z
date_updated:   2016-01-29T03:22:15.000Z
comments: false
---


**Problem: ** I was trying to install Eclipse, but my machine didn’t have Java installed so, naturally, I was prompted to install Java. However, in the popup window, it suggested I install Java 1.6 when I knew that (at the time) the current version was 1.8. So smart-me went to Oracle’s site and downloaded and installed the [latest version of Java](http://www.oracle.com/technetwork/java/javase/downloads/index.html).

After completing the installation and attempting to install Eclipse once more, I got the same pop up saying I needed to install Java 1.6. What’s the deal here? I already have Java installed at a version higher than the requested version, shouldn’t that mean Eclipse will just *work*?

**Solution: **It seems Macs are weird. In order to make the later versions of Java work, it looks like you may have to first install a deprecated version. Here’s how I finally got Eclipse installed:

- I followed the link to [the Java 1.6 download](https://support.apple.com/kb/dl1572?locale=en_US) in the help pop-up  that appeared when trying to install Eclipse
- I then installed the downloaded Java configuration.
- After installation, I attempted to install Eclipse. When I did that, it said Eclipse required Java 1.7 or higher – which I thought was odd considering it didn’t work when I had 1.8 installed.
- So I went to Oracle’s site and found [the latest Java download](http://www.oracle.com/technetwork/java/javase/downloads/index.html), downloaded it, and installed it.
- After the installation of the latest Java download, I once again opened the Eclipse install and… Voila! Eclipse is installed.



