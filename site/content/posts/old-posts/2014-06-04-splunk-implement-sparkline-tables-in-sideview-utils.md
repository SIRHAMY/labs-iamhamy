---
title: "Splunk: Implement Sparkline Tables in Sideview Utils"
date: 2014-06-04T16:24:32.000Z
date_updated:   2014-06-04T16:24:32.000Z
comments: false
---


Converting from Splunk’s usual simple XML to Sideview Util’s editor can be quite daunting at first.  Everything’s moved and looks different.  Here’s how to create a table with sparkline graphs inset using sideview Util’s visual editor.

**Problem: **I want sparklines to show up in table format from my search.

**Solution:**

- ** **First of all, make sure that your search is formatted correctly (this includes using the “sparkline” term with the correct syntax).  It may be easier to create a new view using simple XML and try out your search there so that it shows up correctly.
- Add a Search module in your view.  This is where your search will go.
- Underneath this module (i.e. the search module you just created will be its parent), place a SimpleResultsTable module.  This will show the search in table format exactly how it appears in Splunk’s simple XML views.

*Note: Do not use a Table module to show sparklines.  The table will receive the correct data, but it won’t know how to use it – instead printing the data to the screen rather than creating sparkline visualizations.*



