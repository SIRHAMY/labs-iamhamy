---
title: "Troubleshoot: T-Mobile Samsung Galaxy S6 Edge Not Receiving Text Messages"
date: 2015-12-16T07:02:17.000Z
date_updated:   2015-12-16T07:02:17.000Z
comments: false
---


**Problem: **Recently bought an unlocked Samsung Galaxy S6 Edge. In order to switch from my old Galaxy S5, I had to go to the T-Mobile store to transfer my active SIM card into a nano-SIM. Everything worked fine, however any text messages destined for my phone got caught in the network. The only way to retrieve these messages was to make an outgoing call. This new connection to the network seemed to push all of the messages through to my phone as everytime made a call, I received multiple text messages.

I was able to send texts, make calls, and receive texts – but the texts could only be retrieved after I initiated an outgoing call.

**Solution: **I searched all over the internet, but was unable to find any guides that fixed my specific problem. Basically the phone/SIM wasn’t registered with the network correctly, which made most of the troubleshooting guides online obsolete. So here’s what I did:

- Go to T-Mobile’s Support Website
- Click [Contact Us](https://support.t-mobile.com/community/contact-us)
- Either call or initiate a chat with the support personnel
- They will guide you through a few preliminary troubleshooting steps like clearing your message cache and data.
- After that, they will ask you to turn off your phone and then they will reset your SIMs connection to the network.
- They will then ask you to restart your phone and look for a test message they’re sending to you.
- If you receive the message, you’re good to go.

*Alternatively…*

- Reset your phone’s connection to the network by simply turning it off, waiting five seconds, and turning it back on again. Don’t use your phone’s restart function as it doesn’t disconnect the phone from the network, which is the effect we’re trying to accomplish.

Believe it or not, tech support can do some miraculous things.



