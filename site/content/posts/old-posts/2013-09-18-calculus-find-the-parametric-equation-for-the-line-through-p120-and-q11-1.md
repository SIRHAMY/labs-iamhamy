---
title: "Calculus: Find the Parametric Equation for the Line through P(1,2,0) and Q(1,1,-1)"
date: 2013-09-19T03:08:16.000Z
date_updated:   2013-09-19T03:08:16.000Z
comments: false
---


**Find the parametric equation for the line through P(1,2,0) and Q(1,1,-1)**

PQ is the directional vector of line L

d = PQ

Find d by subtracting P from Q

d = Q-P = (1,1,-1) – (1,2,0) = (0,-1,-1)

We know that the parametric form is (where O is the origin):

(x,y,z) = P + t*d = OP + t*PQ = (1,2,0) + t(0,-1,-1)

**Final: Parametric equations:**  
**x = 1**  
**y = 2 – t**  
**z = -t**



