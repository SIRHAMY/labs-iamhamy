---
title: "Caches: Calculate the Number of Offset Bits"
date: 2014-11-05T00:35:05.000Z
date_updated:   2014-11-05T00:35:05.000Z
comments: false
---


The number of offset bits required in a cache is dependent on the number of addressable locations in one data block.  If there are 2^n locations in a block, then you need n offset bits to identify all of the locations inside the given block.

**Example: **Given a cache where the size of a data block is 4 bytes and the architecture is byte-addressable, you must have enough offset bits to address 4 different locations.  4 = 2^2, therefore you need 2 offset bits to address all locations in a block.



