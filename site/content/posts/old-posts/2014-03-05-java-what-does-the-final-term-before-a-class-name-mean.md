---
title: "Java: What does the Final Term before a Class Name Mean?"
date: 2014-03-05T17:07:15.000Z
date_updated:   2014-03-05T17:07:15.000Z
comments: false
---


Placing the final denomination before a class body means that the class can not be extended.  In other words, that is the last installment in that branch of the class tree.  No class can extend or be a subclass of it.

This Stack Overflow thread goes deeper into the fundamentals of what final means and why you would ever want to make a class final: http://stackoverflow.com/questions/5181578/use-of-final-class-in-java



