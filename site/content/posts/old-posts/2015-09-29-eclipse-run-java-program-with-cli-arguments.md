---
title: "Eclipse: Run Java Program with CLI Arguments"
date: 2015-09-29T06:58:06.000Z
date_updated:   2015-09-29T06:58:06.000Z
comments: false
---


**Problem: **I have a Java program that takes in command line arguments, but I want to compile and run it using Eclipse.  How do I run it with command line arguments from within Eclipse?

![](http://i1.wp.com/www.sirhamy.com/blog/wp-content/uploads/2015/09/EclipseProgramArgs.png?resize=730%2C655)

**Solution: **Using Eclipse, you can modify the arguments being passed into your program by configuring the Run option for each file in your project.  To modify the Run configuration for a program:

- Click **Run > Run Configurations…**
- On the left side, **find and select the program you want to add arguments for**.
- Click on the **Arguments tab**.
- In the **Program Arguments** field, type in the arguments exactly as they would be input into the command line (exclude the javac/java commands that would normally accompany a Java program call from the CLI).
- **Click Run**.  This will compile and run the current program with the given args.  After running the program, this configuration will be saved for future runs of the same program, so be sure to change it if you need to.



