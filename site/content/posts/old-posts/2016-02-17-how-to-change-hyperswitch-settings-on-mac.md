---
title: How to Change HyperSwitch Settings on Mac
date: 2016-02-17T15:13:29.000Z
date_updated:   2016-02-17T15:13:29.000Z
comments: false
---


HyperSwitch is a productivity plugin for Macs that overrides the ALT + TAB functionality to make it more like Windows. Where Mac usually tabs between apps, Windows usually tabs between open windows. Thus with Hyperswitch, you can now tab through all the open windows on your machine.

Of course there are a bunch of preferences you can set, such as:

- Window switcher and app switcher hot keys
- Which screen to show the switcher on
- Whether you should only be able to switch between windows on the current screen or not
- And many more

 

The preference window opens up automatically right after you install it, but with all these different settings, you’re bound to want to change some of them in the future – how do you do it?

<span style="font-weight: bold;">Problem: </span>I have HyperSwitch downloaded and running, but want to change one of its settings. How do I open the Preference menu?

<span style="font-weight: bold;">Solution: </span>To open the HyperSwitch Preference menu:

- CTRL + CLICK on the app icon on your dock
- Select Preferences
- Change your preferences to your heart’s content



