---
title: "7 ways to fight Climate Change as a Software Engineer"
date: 2021-11-07T22:02:33Z
tags: [
    'climate-change',
    'hamforgood',
    'software-engineer'
]
comments: false
---

# Overview

Humanity is hurtling towards a cacophony of global catastrophes in the next century brought about by climate change. It's going to take a lot of people doing a lot of things for us to change course and avert the worst of these disasters.

In this post I'll share some ways you can contribute to fighting climate change today, particularly from the lens of a software engineer (the lens I'm most familiar with). A lot of this post will also likely apply to other product-related roles.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xRHZoobgOy0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Climate Change

I want to start off with a brief intro to climate change - where we are and where we're heading - to provide some shared context from which to base the rest of this post.

Climate change is a huge problem affecting the world today and it looks like it's only going to get worse unless we make some big changes. Many experts posit that it's actually Earth's greatest threat - [to security](https://www.un.org/en/chronicle/article/greatest-threat-global-security-climate-change-not-merely-environmental-problem), [to public health](https://www.npr.org/2021/09/07/1034670549/climate-change-is-the-greatest-threat-to-public-health-top-medical-journals-warn), and even [to humanity](https://www.who.int/publications/i/item/cop26-special-report).

Right now we're on track to hit a 1.5 C average global temperature increase (as compared to pre-industrial levels) [by 2030](https://www.ipcc.ch/sr15/chapter/spm/). The effects of such an increase are wide-ranging and disastrous (see [2021 IPCC Report](https://www.ipcc.ch/sr15/)). Some examples:

* Increased risk of extreme droughts, water stress (lack of availability), and exceptional heatwaves (unsuitable for humans)
* Huge destruction of natural habitats -> 50% habitat loss for 6% of insects, 8% of plants, 4% vertebrates
* 70-90% death of existing warm water coral reefs

Even worse, if we are unable to stop ourselves at 1.5 C it's likely we may run over into 2 C or greater territory where negative effects tend to compound exponentially:

* Increase in water stress (lack of availability) by 50% compared to 1.5 C
* Additional destruction of natural habitats -> 50% habitat loss for 18% of insects, 16% of plants, 8% of vertebrates
* 7-10% loss of livestock rangeland, affecting global food supplies

And these are just a few of the effects we know about if we reach 1.5 C. You can read more about these in the full [2021 IPCC Report](https://www.ipcc.ch/sr15/).

So if we want to conserve the planet as we know it today - and skip all the disastrous consequences of extreme warming - we need to start working on solutions now. 

The daunting news: The scale of the problem is global - it's going to take a lot of us doing a lot of things in a very short amount of time to avert.

The good news: There are many ways to help out - large and small - that you can start today.

# Ways to fight climate change

In the rest of this post I'm going to go through some ways you can start fighting climate change today. I'll include some examples to try and make it easier to see how each of these methods could be applied in the real world - though note these are by no means exhaustive. I've listed the methods in a rough ordering of highest impact and lowest effort to minimize friction and maximize action.

Let's get started.

## 1. Vote

* Impact: Medium - High
* Effort: Low

Surprise! One of the most effective ways to fight climate change as a software engineer doesn't even depend on you being a sofware engineer.

Voting is a low effort activity that can have wide-ranging, outsized impacts on climate change across various cuts of society - geographies, demograhics, industries, etc. It can set policies, choose leadership, direct funds, regulate industries, create laws, and more that all contribute to the power and impact of this action.

Thus it's first on the list due to its high potential impact and relatively low effort requirement to take. (Though def take some time to research your options and advocate to others to maximize its impact!)

Examples:

* Regulations to limit carbon output and fines for those that break them
* Policies to invest in renewable energies like wind and solar
* Local building codes that enforce new construction to meet certain sustainability requirements

## 2. Work on projects / companies that directly fight climate change

* Impact: Medium - High
* Effort: High -> Often part of full-time job

One of the most impactful things you can do is to apply your skills and effort towards projects and companies that are directly working to fight climate change. This is because you'll be directly impacting efforts that will themselves have direct sizable, immediate impacts to help conserve the planet.

As a software engineer, you're going to be in a prime position to influence a project's chance for success.

* You're in product and organization meetings
* You're able to improve the efficiency and effectiveness of existing processes through code
* You're able to enable new development pathways by building new features and tools

Even if you aren't working directly on a project that fights climate change - just being a part of an organization that is and helping that organization to succeed can also provide high impact. By improving the effectiveness of the org in unrelated areas, the org is often able to free up more of its resources - some of which may be reinvested into areas directly fighting climate change.

Examples:

* Direct impact projects
    * Solar panel firmware and management software
    * Analysis software to track and identify deforestation, regulation violations, and the impact of climate change on public health (a la [ProPublica's pollution-cancer heatmap](https://projects.propublica.org/toxmap/))
* Indirect impact projects (at orgs directly fighting climate change)
    * Example orgs directly fighting climate change: [350](https://350.org/), [Sierra Foundation](https://www.sierraclubfoundation.org/)
    * Website to inform constituents and take donations
    * Improving finance, HR, legal, planning software and workflows to be more effective

## 3. Influence project / company to take climate-aware actions

* Impact: Medium -> Dilluted due to distance from direct projects, depends on project
* Effort: High -> often part of full-time job

Most of us probably aren't working on projects or at companies that are directly fighting climate change. This is for a variety of reasons:

* Only so many projects / companies in the world directly fighting climate change
* We're not necessarily passionate about the day-to-day work of the projects that are (like coding low-level firmware for solar panels)
* May not pay as well as other positions (e.g. FAANG)
* Geography / open positions / visas / skill alignment / et al

But that doesn't mean we can't have substantial impact from within our current orgs. Just that a bit of our impact will be dilluted by our distance from the source of direct impact.

There's a lot of opportunity here. 

* Most companies on Earth do not work directly on climate change - so a lot of money and power is available to steer towards the support of planetary conservation
* Many companies are responsible for resources and planetary footprints far exceeding that of any individual -  so even small movements here can have outsized effects when compared to initiatives you could participate in individually.
* Many companies have many vendors and customers, so initiatives implemented here have the opportunity to influence many more actors in their networks than you might have individually

As software engineers, we're directly involved in lots of product and direction meetings. We know how to go from idea to implementation so must be included in these meetings to help inform what is / isn't possible and how to actually get there.

We can leverage this seat at the table to push initiatives that help fight climate change - even if it doesn't involve writing a single line of code.

* Speak up in team meetings
* Ask questions to leadership
* Propose new projects and initiatives
* Point out potential project risks and propose climate-aware solutions
* Influence buying solutions (vendors / technologies / etc) that are climate-aware

Of course, you can also achieve impact through direct means like pushing new features and products. Examples below.

Examples:

* Build features that make it easy for users to contribute to initiatives that fight climate change - like [Stripe Climate](https://stripe.com/climate) which directs a % of sales towards climate change initiatives
* Raise awareness for climate change and its impacts - like [Sims' Eco Lifestyle pack](https://www.ea.com/games/the-sims/the-sims-4/packs/expansion-packs/the-sims-4-eco-lifestyle) which lets player actions (like type of energy source) change the in-game world's pollution levels (though do make sure your company is living these values, not just performing them)
* Push your company to switch to renewable energies - like [Facebook, Microsoft, and Google](https://www.cbsnews.com/news/facebook-renewable-energy-commitment-100-percent-milestone/)
* Allow users to show their support for climate change initiatives - through social media posts, stickers, badges, etc or in-game / in-profile cosmetics
* Direct your company's charitable arm to give funds to direct climate action organizations

## 4. Be involved in your community

* Impact: Small-medium -> depends on the opportunity and power of the community
* Effort: Small-medium -> Depends on involvement, but can typically be done part-time (1 / month, 1 / week)

Building and being active in your community can be an impact multiplier for each item on this list. Another pro is that it can typically be done with small amounts of regular investment - as opposed to some of the items that require large effort outputs.

With a community of people, you can often achieve outcomes that would be hard to do alone:

* Brainstom and research climate-aware initiatives to implement in your industry
* Organize and spread awareness for these initiatives across adjacent industries
* Vote and advocate for these initiatives and against climate-destructive ones within your varying realms of influence

As a software engineer, you have direct access to several communities that can have outsized effects on many industries when working together. Some examples:

* Software Engineering forums - a great way to get news out to other engineers and product roles who have a direct say in their own company's project decisions
    * Actions: Post about climate-related initiatives that relate to software engineering / product development (e.g. a more efficient technology, bad company practices, etc)
    * Hacker News
    * Dev [dot] to
    * Reddit
    * Tech Meetups and conferences
* Product User Groups - As a software engineer, you have a large say in product decisions. One big product decision is what software and tools to buy (and which to pass on). As such, the companies selling this software have a huge incentive to actively listen to their community of users. An impact multiplier here is that tech is a huge industry so lots of money opportunity to redirect and 'vote' with.
    * Action: Ask about and push for sustainable practices by software vendors
    * Product-specific forums
    * Voting directly on the vendor's roadmap / sprint boards
* Internal Org Groups - Of course at your own company you also have a big say. Since many companies have a much smaller employee-base than a typical user-base, your voice here will typically represent a larger slice of the pie. Build and leverage these communities to influence decisions within your own company.
    * Vote on internal initiatives -> like committing to 100% renewable energy by 2030
    * Steer product and company decisions to be climate-aware
* Local community - Your local communities outside of your org and industry is also a great place to cross societal boundaries and learn about what other pain points and opportunities are out there. This is a great one to leverage and influence towards another item on our list -> go vote!

## 5. Choose better technologies

* Impact: Small-Medium -> Dependent on your project footprint
* Effort: Small-High -> Dependent on complexity

As software engineers, we're familiar with the idea of tradeoffs. Some technologies are exponentially better at some things than others. Though usually not better at _all_ things.

If we start (or hopefully continue) thinking of resource consumption - like machines and electricity - as a project constraint and metric to be optimized for, we can start making tradeoffs for it that directly affect our project's resource footprint and by extension its effect on the environment.

According to the [EPA](https://www.epa.gov/ghgemissions/sources-greenhouse-gas-emissions), about 25% of all greenhouse gas emissions were emitted for the production of energy in 2019. Less energy for our project means less energy needed from the grid means less energy needed to be produced. Similar logic for reduction in resources for required machines.

As a software engineer you are uniquely positioned to make these calls. You're quite literally the expert on these implementation details!

Now the impact typically isn't as high as other methods on this list because it really is dependent on your own project's footprint. Most projects will not have a planet or even company scale footprint - though some do.

But I think this is still a worthwhile thing to pursue because it does add up and it is something you should be doing anyway for your practice and your company. Less machines and energy means less money spent on the project.

You can couple this with sharing learnings to your communities to increase the reach and impact beyond your project.

Examples:

* Programming languages: C# handles more processing per server than Python or Rails by about 22x (so about 22x less energy and machines required for the same workloads) - see [2021 TechEmpower Benchmarks](https://www.techempower.com/benchmarks/#section=data-r20&hw=ph&test=composite)
* Technology Implementations: Proof of Stake blockchains like Tezos use about 1,000,000 times less energy per transaction than Proof of Work blockchains like Ethereum 1.0 or Bitcoin - see [Selling NFTs - Tezos vs Ethereum](https://labs.hamy.xyz/posts/why-i-sell-nfts-on-hic-et-nunc/)
* Hosting: Serverless vs always-on, dedicated machines. If you can free up compute resources when you don't need them, those resources can go to others that do and you save money. - see [How I lowered my project costs from $50 / month to $10 / month](https://labs.hamy.xyz/posts/hosting-on-full-stack-apps-on-google-cloud-for-less-than-10-per-month/)

## 6. Make existing software more efficient

* Impact: Small -> Typically less impact than technology decisions
* Effort: Small-Medium

This is the classic O(n) vs O(n^2) discussion.

Typically the decision to use different technologies will result in a larger one-time shift due to the absolute footprint and ongoing efficiency impact of those technologies on your system than changes within the existing system will. But even within a system, small changes can have substantial impacts on its effectiveness and indirectly its resource footprint.

Though the absolute impact of each of these changes is often smaller than technology changes (rare to see 1,000,000x or even 22x differences), they're also typically less complex to make which means it may be easier to stack these up to achieve larger impact. Also typically easier to actually push these changes as don't need buy-in from as many vendors, finance, legal, stakeholders, etc.

Examples:

* Use caching in hot paths
* Don't do unnecessary work
* Don't over or under fetch
* From [my own career](https://www.linkedin.com/in/hamiltongreene/):
    * Improved the scalability of a core infra service by 10x by optimizing a bottleneck code path - this meant we could scale our processing to more workloads without having to run more or bigger machines
    * Landed small code improvements over 2 years netting over $1M / year savings in machine and energy costs - Each of these was a relatively small lift with a relatively modest return but in aggregate added up to quite a bit of savings!

## 7. Donate to orgs

* Impact: Small
* Effort: Small

Last we have donating to orgs. In this section I'm mostly talking about money because if you're donating another resource like time and effort it may fall into another bucket.

The impact here will typically be pretty small as we typically don't have control over as much capital or resources individually as say a community, company, or government might. That is unless you're a Bezos, Musk, or Zuck in which case I'm flattered you found my post.

But donating is still a very easy way to provide a bit of regular support to initiatives that fight climate change. While it's at the bottom of the list, it is certainly impactful as every org I've seen seems to need resources to run.

Examples:

* I donate monthly to [350](https://350.org/) and regularly to other orgs based on my [personal Earth Tax](https://blog.hamy.xyz/pages/principles/earth-tax/)
    * Do your research and consider giving today

# Take Action

Okay that's a lot of words from me. Now it's time to take action.

I've provided a bunch of examples and potential ways to get started but your implementations of each will certianly look a little different based on the situations you face. Never fear - there's no 'right' answer here, just a bunch of us trying to take baby steps in the right direction.

If you've got questions about any of this and / or examples of climate-aware initiatives you've seen please reach out! I'd love to learn more about your situation and how we can do better.

As Captain Planet said:

"The Power is yours!"

Let's get started.

-HAMY.OUT