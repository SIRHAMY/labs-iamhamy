---
title: "Create High Quality Images with AI: Night Cafe"
date: 2022-09-28T22:33:05Z
tags: [
    'artificial-intelligence',
    'art',
    'text-to-image'
]
comments: false
---

New advances in Artificial Intelligence have enabled the translation of text descriptions into full blown images. The problem is that while it's easy to create an image, it's hard to create a high quality image you'd be willing to share.

In this post I'm going to share strategies I use to produce high quality, shareable images using [Night Cafe](https://nightcafe.studio/) - a leading Text-to-Image service.

# Table of Contents

* [Text-to-Image AI](#text-to-image-ai)
* [Explore](#explore)
* [Enhance](#enhance)

# Text-to-Image AI

_Note: This post will not cover how to use Night Cafe to produce images. For that, check out [Generate art with text using AI](https://labs.hamy.xyz/posts/generate-art-with-text) then come back here for advanced tips._

The first thing we need to do is understand how these Text-to-Image AIs work so we know how to work with them.

Essentially:

* AI is trained on millions of images.
* Each image has associated metadata describing what's in the image and how it might relate to external trends - like type of photo, style of painting, etc.
* When we input text, the AI tries to produce a new image that it thinks best matches this text

![AI - Human Misunderstanding](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/high-quality-text-to-image-night-cafe/ai-human-text-to-image-misunderstanding-2.jpg)

The problem is that often times the images we get out are not the exact images we want and oftentimes won't look very good.

* Want: pictures of people with hats
* Input: "A man with a hat"
* Output: Will only be pictures of men wearing a hat

If I actually wanted someone holding a hat, I'd have to be more specific: "A man holding a hat".

Just as misunderstandings happen when we're talking to another person, so too do we need to be very clear with prompts to the AI.

# Explore

The first thing we want to do is understand how the AI thinks about the world and in particular how it visualizes the kind of things you want your image to contain.

## Experiment with prompts

The vehicle to understanding will be experimentation (just as it is for [The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle/)).

Let's say we're interested in creating an image about a vending machine on another planet, we could do a series of prompts to understand what the AI knows:

* `vending machine` - does it know what a vending machine is?
* `vending machine on mars` - does it know what mars looks like? Does it know how to place a vending machine on mars?
* `banana vending machine on mars` - Does it know that vending machines sell stuff - can it represent that?
* `blue vending machine on mars selling bananas` - Can it add additional attributes to the vending machine?

Each prompt can be used as an experiment into what the AI knows and how to get certain types of results with it. This will help us iterate towards the image we are trying to create.

![Batch Jobs with Night Cafe](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/high-quality-text-to-image-night-cafe/night-cafe-batch-jobs.jpg)

## Batch Jobs

Iteration can get expensive. Modern Text-to-Image models will require a GPU to run which is not cheap. Thus most services, Night Cafe included, require money / credits for each output image.

By batch creating images, we can maximize our understanding of the AI while minimizing the iteration cost in terms of money and time.

Night Cafe offers discounts for batching images together:

* 1 image, 1 credit = 1 image / credit
* 4 images, 2 credits = 2 images / credit
* 9 images, 4 credits = 2.25 images / credit
* 16 images, 7 credits = 2.28 images / credit

By batching images we also get a better understanding of the trends of a given prompt - something we wouldn't get with a single image / data point. This gives us a better read on whether this prompt has opportunity or not.

## Explore the prompts of others

The [Night Cafe Explore Page](https://creator.nightcafe.studio/explore) will allow you to scroll through the top images people have created including many of the settings that were used during their creation. You can use this to find images fitting the style you are trying to create and taking parts of their prompts / settings to use for your own.

Fair warning that these prompts are highly contextual so if the subject of your image is different from the subject of their image, it's likely many of the modifiers will not have the same exact effect. That said, it's still a good place to look to for inspiration on new pathways to try.

## Recommended Exploration Settings

* **Aspect Ratio** - Choose the aspect ratio you'll use for your output image. Because us humans tend to have biases for the kinds of images we put in different aspect ratios, so too will the images produced by the AI.
* **Number of images:** 4 or 9 - this is a good balance between cost efficiency / image and cost efficiency per prompt. Typically we won't need more than 9 to understand if this prompt is useful or not.
* **Output Resolution:** Thumbnail - you really want to use the smallest resolution possible for cost and time efficiency

# Enhance

In the Explore phase you are trying to understand the AI and find prompts that create the kind of image you are looking for. In the Enhance phase we are trying to tweak our settings to get a high quality final version.

## Use Style Modifiers

Style modifiers have a huge impact on the kind of images that are created - from watercolor to oil painting to 3d models. Becoming comfortable with these will greatly increase your ability to get the type of image you want.

You've likely already used the preset style modifiers provided by Night Cafe. These are great but there's a whole world of other modifiers available to you within the `advanced options` pane.

To view these:

* Go to the `Create` page
* Toggle `Show advanced options` 
* Click `Add Modifiers` under the text prompt
* Browse the available modifiers

Because Text-to-Image is so contextual, some modifiers will have greater / different effects on some prompts than others. It's a good idea to experiment with several of these as you iterate towards your final image.

![Reroll promising prompts](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/high-quality-text-to-image-night-cafe/night-cafe-reroll-promising-prompts.jpg)

## Reroll promising prompts

Often times you'll be getting the kinds of images you want but it's just not quite right. Never fear, you can get way more output out of your near-perfect prompt!

By rerolling prompts, we can explore the billions of different variations of images available to us from a specific prompt. 

To reroll an image:

* Navigate to the image within Night Cafe
* Click `Duplicate`
* Change the `Seed` -> This is important! This is how we ensure we get a different image!

This will run the exact same prompt with the exact same settings, just with a different seed which will result in different output images.

![Upscale with Enhance](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/high-quality-text-to-image-night-cafe/night-cafe-upscale-with-enhance.jpg)

## Upscale your final Image

At this point we've got an understanding of the AI and have found a good image and style we'd like to share out. The problem is that it's still at the thumbnail resolution which will be too blurry / pixellated to look good.

We can use Night Cafe's `Enhance` feature to turn these images into much sharper images. This will pass your image into an Upscale AI which tries to determine what your image would've looked like had it been higher resolution and outputs that. This allows us to get a high quality final image without pixellation.

# A Conversation

The final thing to understand is that this interaction is more like a conversation than a deterministic transaction. There will be a lot of iterations and clarifications required to get a high quality final output.

* Bad: It is hard to get exactly what you want
* Good: The AI will often surprise you

# Further Reading

* [Generate art with text using AI](https://labs.hamy.xyz/posts/generate-art-with-text)
* My latest AI art project: [ARTIFICIAL_DREAMS](https://labs.hamy.xyz/projects/artificial-dreams/)