---
title: "Create art from the Best Text-to-Image AI for Creators"
date: 2022-09-21T14:02:08Z
tags: [
    'artificial-intelligence',
    'art',
    'text-to-image'
]
comments: false
---

I've created hundreds of images generated from half-a-dozen AI text-to-image services. In this post I'm going to share my pick for the best AI for creators and tips for using it to create high quality, share-worthy images.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kvnIldLPM9g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Table of Contents


![We input text, AI outputs image](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/best-text-to-image-ai-for-creators/how-it-works-text-to-image.jpg)

# Text-to-Image AI

Before we start going into how to use this AI I think it's important to understand how it works at a high level so we can build intuition about how different inputs may lead to different outputs.

At the most basic level:

* Input: Text like "Purple blossom made of crystal"
* Output: An image that matches this

In an ideal world each text prompt will give us an exact match that is beautiful. But we don't live in an ideal world. Typically finding the perfect image will take many iterations of trial-and-error - inputting new prompts, seeing what the AI outputs, and crafting a new prompt to get closer to your ideal image.

![Iterating on text-to-image prompts](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/best-text-to-image-ai-for-creators/iterating-text-to-image.jpg)

In order to create the final image shown above, I had to go through several prompt iterations to get closer to what I wanted.

* "purple rose made of diamond"
* "purple rose made of crystal"
* "purple blossom made of crystal"

It's important to understand this UX paradigm as it will inform what features an AI service needs to have in order to support this iterative process.

For this post, we are going to be using [Midjourney AI](https://www.midjourney.com/home/) which is my pick for best text-to-image AI for creators.

I like Midjourney because it's:

* **High Quality** - The median output image you get from Midjourney is way more aesthetic and has higher semantic integrity than almost any mainstream AI except maybe DALL-E 2
* **Good Tooling** - Midjourney makes it super easy to explore and iterate on prompts and modifiers which is crucial for getting the most out of an AI system like this, beating out DALL-E 2 in tooling breadth and [Night Cafe](https://labs.hamy.xyz/posts/generate-art-with-text/) in iteration speed.
* **Great Enhancement / Remastering** - Only a few tools have built-in enhancement and remastering tooling which allows you to upscale from the raw small resolution image produced by the AI into an HD quality fit for internet consumption. Midjourney has feature parity with the best AI services available and the features themselves have excellent quality, far outpacing those with similar offerings.

All of these make Midjourney the best text-to-image AI for creators, giving you higher quality images for less time investment.

My only qualm with the service is that the UI can get a bit clunky as it's all Discord-based. This has both pros and cons as we'll see in the Explore and Generate sections so it's not a dealbreaker.

![Midjourney AI - Explore Top Generations](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/best-text-to-image-ai-for-creators/midjourney-top-generations.jpg)

# Explore

Every AI service needs a way to explore its outputs. This is because each AI is trained in a different way, so what works for input<>output in one AI will not necessarily work the same in another AI.

The best of these services will provide a way to explore outputs to see what's possible. The very best will also provide insight into the inputs that were used to create the output - this will allow you to reverse engineer how some styles were achieved and use them in your own prompts.

Midjourney allows you to explore its top generations via the [Midjourney Community Feed](https://www.midjourney.com/app/feed/all)

What's really cool about this Explore functionality vs ones available in other AIs is that it shows you the exact prompt that was used to create a given image. So if you like a style or subject, you can copy and paste the exact wording to start iterating in that direction.

Another way to explore is via the Generation channels in Midjouney's discord. we'll go into this more in the Generate phase but it's unique in that it allows you to explore other people's creations in real-time.

# Generate

Once you've signed up for Midjourney, you'll be equipped with a limited number of free generations to play around with.

Midjourney uses Discord for generation rather than having a dedicated web UI like most other AIs. When you send a message with the `imagine` command, a bot will take the message text and start generating images based on it. This can feel a bit clunky at first but offers some unique perks like quick iteration and passive exploration of prompts created by others.

To create using Midjourney:

* Access the Midjourney Discord (linked from the home page)
* Go to a generation channel (`newbie*` if you are on free tier, `general*` if you have a subscription)
* Send a message with the `imagine` command to your channel
    * For example, you might type `/imagine purple blossom made of crystal` to generate images for the prompt "purple blossom made of crystal"
* A bot should see your message and display a job status as it generates your images. This may take a minute or so to complete.
* When the image is complete, the bot will tag you in a new message that includes your image

You've generated your first image with Midjourney AI!

After doing this a few times, you're probably annoyed by scrolling through everyone else's generations to find the ones you created. Though we can't generate our images in private, we can create a separate chat so that we only see our messages and the bot's messages. This makes generation a lot more streamlined with a lot less scrolling.

To create a separate chat with you and the bot, we'll be utilizing Discord Threads. This isn't private in that other people could look at / join this thread later but typically you'll have the thread to yourself.

To create a Discord thread:

* Go to the Midjourney Discord and the Channel you want to message in.
* Click the "Threads" icon in the top right.
* Click `Create`
* Name your Thread then start messaging in it

Now you'll have a thread that just you and the bot are using which will make generation much less of a hassle.

# Export

Now that you have all the tools you need to iterate you can work towards your perfect final image. But what do you do when you have it? The picture is still very small and blurry and not fit for sharing.

All images in Discord are just images, so you can just click on the image and copy / save it for sharing. But the problem is that these will all be thumbnail-sized resolutions output by the AI.

Midjourney provides a few options for enhancing your images for sharing.

By default, Midjourney will produce 4 images each with a corresponding index:

* Top Left = 1
* Top Right = 2
* Bot Left = 3
* Bot Right = 4

Under each generation it will offer a few capabilities:

* U* - This will upscale the image at the respective index - making it larger with higher definition
* V* - This will create several variations of the prompt that look similar to this image - perfect if you have something you like but not quite perfect
* Redo - This will rerun the prompt, giving you four new images

The first thing we'll want to do is upscale the image we like to max. To do this:

* Click U* where * is the index of the image you want to upscale
* When the job is done, the output image will not be max upscaled. Keep upscaling til we've hit Max.

Finally I'd highly encourage you to use the `Remaster` functionality on your final image. This is like the Variation functionality but has much higher quality outputs. So if you really like an image, it's useful to Remaster it to see if you can get an even better variation of it.

You can Remaster an image by:

* Find the image you want to Remaster
* Click the `Remaster` button below it

# Further Reading

* My latest AI Generated art project: [ARTIFICIAL_DREAMS](https://labs.hamy.xyz/projects/artificial-dreams/)
* [Generate art with text-to-image AI Night Cafe](https://labs.hamy.xyz/posts/generate-art-with-text/)