---
title: "Docker: Change dotnet core default listening port"
date: 2021-01-25T19:17:28Z
tags: [
    'docker',
    'dotnet-core',
    'csharp'
]
comments: false
---

# Overview

I'm using Google App Engine to host my dockerized dotnet core app. App Engine expects all communications to go through port 8080 so I need to change the port my app is listening on. How do I do that? 

# Solution

There are several ways to go about this as laid out in [5 ways to set the URLs for an ASP.NET Core app](https://andrewlock.net/5-ways-to-set-the-urls-for-an-aspnetcore-app/) however I was only able to get one to work successfully.

That one is to add a `Urls` node inside the `appsettings.json`.

Here's an example of it in action:

```
{
  "Logging": {
    "LogLevel": {
      "Default": "Debug",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "Urls": "http://host.docker.internal:8080"
}
```
