---
title: "Ubuntu Gaming: Dragon Age Origins on Proton"
date: 2021-01-18T18:32:21Z
tags: [
    'ubuntu',
    'gaming',
    'proton'
]
comments: false
---

In this post I'll be sharing why I game on Ubuntu and how [Dragon Age: Origins](https://amzn.to/3qBALF5) plays on Ubuntu using Steam's Proton compatibility software.

You can find my video walkthrough here:

<iframe width="560" height="315" src="https://www.youtube.com/embed/6KoqKEJREkw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Gaming on Ubuntu

Before I get into the experience of gaming on Ubuntu I first wanted to share a bit about why I even game on Ubuntu vs another system like Windows, Xbox, etc.

Really it just comes down to preference. I love being on my computer and Ubuntu is my operating system of choice. It's free and open source, is extremely stable and flexible (and runs the majority of online servers), and has a great ecosystem for software tools and development.

When it comes to gaming - figured I'd try to support the system I love: Ubuntu.

## Proton

Now I'll note that not everything works on Ubuntu out of the box and gaming is one of those areas that suffers from this. Ubuntu has some games that were built for it but not nearly as many as were built for other platforms like Windows and Mac. 

For the most part it's because developers choose to focus more on Windows and Mac development than Ubuntu - not because Ubuntu is any less featured than the others (in fact I'd argue the opposite is true). A good analogy is how some games only run on Xbox or PlayStation or how some apps are only available on Android or iOS. It's not cause there's anything wrong with a particular system, the developers just chose to only support it on one.

[Proton](https://github.com/ValveSoftware/Proton) is a compatibility tool for Steam that helps games originally built for Windows to also be playable on Ubuntu. When there's a game I want to play that doesn't work out of the box on Ubuntu, I'll go check if it's working well via Proton. If so, it should work fine for me.

There's a great website [ProtonDB](https://www.protondb.com/) which tracks how well games are working via Proton, so you can do some research for your game before buying.

# Dragon Age: Origins on Steam Play and Proton

Alright, so now we know why Ubuntu and how we're running Dragon Age: Origins on it. Let's dive into how it plays.

In this post I'll be focusing on how Dragon Age: Origins plays with Proton, not how it plays as a game. So keep in mind that this was released in 2009 so some artifacts may not be due to Proton itself, but possibly just from being an older game running on newer hardware.

At time of playing (December 2020) [Dragon Age: Origins had a Gold ProtonDB score](https://www.protondb.com/app/47810) meaning it's highly playable but there may be a few noticeable glitches / issues. I used Proton 5.13-5 for my playthrough.

## Issues

Issues effecting game play

### Windowing

The biggest issues I had while playing Dragon Age: Origins on Proton was with windowing. When you unfocus the game screen (by ALT + TAB or clicking to another monitor or something similar) the game instantly crashes. This is a problem I've seen in many games using a compatibility layer so it's not just isolated to this game.

While a bit annoying, if you save before you switch screens then you can just reboot the game real fast right where you left off.

### Random Crashes

I experienced 3 random crashes in my 60+ hours of game play. That's pretty low from my perspective considering the game's age and it was running in a compatibility layer.

Save often and this won't be a problem.

### Cutscreen Shaking

In some cutscreens, the character models appear to 'shake' a bit. This is more a nuisance than anything else but some could find this is detrimental to their game play.

## Quirks

Things I saw but are pretty irrelevant to game play.

### Audio

In my install the audio was defaulted to off. I had to navigate the audio menus a bit to turn audio on. Everything worked fine after that.

### Video

In my Proton installs the screen dimensions don't seem to be set correctly, so I have to modify the settings to fit my screen. I expect many people will do this anyway for other settings regardless of whether it's native or not so I consider this a quirk and not an issue.

### Save Games

This isn't really an issue but definitely a bug of some sort. The previews for saved games are very glitchy. An interesting visual effect but definitely not supposed to happen. 

The saved games are fine so just enjoy the extra glitch art in your gameplay.

# Conclusion

Dragon Age: Origins works well on Ubuntu with Proton. There are some minor issues but nothing that significantly detracts from the gameplay experience.

If you haven't tried Ubuntu gaming yet, I'd highly rec.