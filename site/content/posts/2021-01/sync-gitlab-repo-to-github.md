---
title: "Sync your GitLab code repository with GitHub"
date: 2021-01-18T15:08:10Z
tags: [
    'gitlab',
    'github',
    'repository'
]
comments: false
---

# Overview

I'm a huge fan of GitLab. It has solid tech, is run by a great company, and has tons of features in its free tier. I use it as [my primary code repo service](https://gitlab.com/SIRHAMY).

However, GitHub is still the default player in the space and other tools I use - namely DigitalOcean's App Platform - only integrate with GitHub right now. This means that I need a way to mirror my code from GitLab repositories to GitHub repositories in an efficient manner.

In this post I'll share how I've set this up.

# Mirroring a GitLab repo with GitHub

The solution I chose was to just mirror my GitLab repo with a GitHub repo. I like this because there's really no abstraction - it's essentially the same exact repo, just hosted on another service. This means I can interact with it in all the same ways meaning I have a veritable backup and a close secondary if I ever decide to switch between the two.

GitLab offers the ability to [push mirror from GitLab to GitHub](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#setting-up-a-push-mirror-from-gitlab-to-github) in its free tier. Here are the steps we'll take to set this up:

* Setup GitHub repo to sync with
* Create GitHub personal access token
* Setup GitLab code mirroring

## Setup GitHub repo to sync with

The first thing we need to do is setup a GitHub repo that we can sync our code with. I'm going to assume that you've already setup a GitLab repo - which is why you wanted to sync in the first place.

* Go to GitHub profile
* Click the 'plus' icon and select `New Repository`
* Fill out the repo creation flow

Personally I like to name the repo the same thing as it's named on GitLab. This makes organization easier for me but if you want to name it something else - that's your prerogative.

## Create GitHub personal access token

Next we'll create a PAT (personal access token) which is basically a private key / password. This will allow GitLab to authenticate with GitHub so it can sync the repository.

* Click on your profile button
* Select `Settings`
* Select `Developer Settings`
* Select `Personal access tokens`
* Select `Generate new token`
* Fill in `Note` with a repo identifier
    * You'll likely want to create a new PAT for each repo so I think it'd be good to use a repo identifier here so you can more granularly add and revoke access as necessary.
* Select Scopes for your repo
    * If you're syncing a public repo, you can just select `public_repo` under the `repo` section
    * If you're syncing a private repo, you'll need to select the entire `repo` section
* Select `Generate token`
* Copy / take note of your new token
    * Note: Don't leave this page! You won't be able to see the token again if you navigate away and we'll need to give this to GitLab so they can access our repo!

## Setup GitLab code mirroring

Okay now we have a GitHub repo to sync with and we've generated a PAT token so that GitLab can authenticate with GitLab. All that's left is to setup code mirroring.

We'll do this from within the repository we'd like to mirror:

* Navigate to the GitLab repository you'd like to mirror
* Select `Settings > Repository`
* Find and expand the `Mirroring repositories` section
* Input the `Git repository URL`
    * It's in the form of `https://<GITHUB_USERNAME>@github.com/<GITHUB_GROUP>/<REPO_NAME>.git`
    * So let's say I have a repo for my [GitHub user SIRHAMY](https://github.com/SIRHAMY) called `example-repo`. I would input `https://SIRHAMY@github.com/SIRHAMY/example-repo.git`. You can test your link out by navigating to it in a browser. If it resolves to your desired repo that's a good sign.
* Set `Authentication Method` to `Password`
* Fill in `Password` with the PAT token you created in the last step
* Click `Mirror repository`

You should now see an entry in the `Mirrored repositories` list with your mirror settings. Click on the `circle icon` to force a push. Refresh the page to see if it worked. If not, it'll have an error message that you can click into to get more info. If it did, you should see `Last successful update` filled in.

Happy mirroring!

-HAMY.OUT

# References

* [Creating a personal access token](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/creating-a-personal-access-token) - GitHub
* [Setting up a push mirror from GitLab to GitHub](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#setting-up-a-push-mirror-from-gitlab-to-github)