---
title: "OBS Studio: How to remux mkv to mp4"
date: 2020-06-01T13:59:02Z
tags: [
    "obs",
    "obsstudio",
    "mkv",
    "mp4"
]
comments: false
---

# problem

I use OBS Studio a lot to record demos of [my projects](/projects) and videos of my audio visualizations like [monoform](/projects/monoform) and [maudern](/projects/maudern). I like it because it's free, powerful, works anywhere, and is pretty straightforward to use.

One thing I kept running into though was that OBS Studio recommends you use mkv video format as apparently that format is better for when streams could end abruptly. mp4 has a constraint where if you don't finish up encoding the entire encoding is corrupted.

This is fine encoding-wise as mkv is still playable in many places and it has similar video quality. But it's not playable in all places (like Instagram currently) so when it comes to distribution, mp4 is the better choice.

So how do I remux the mkv videos that OBS studio outputs into an mp4 that I can share anywhere?

# solution

You can actually do this right within OBS Studio. To do so:

* Click `File > Remux Recordings` from the topbar menu
* Select a row in the `OBS Recordings` column and then click the button with an ellipsis - `...`
* Select the mkv file you want to convert and click `Open`
* Select `Remux`

OBS Studio will then start converting your mkv to mp4!