---
title: "Python: Run a simple http server with Python 3"
date: 2020-06-08T10:27:06-04:00
tags: [
    "python",
    "dev",
    "http",
    "server"
]
comments: false
---

# problem

A lot of my projects just need a simple http server for development. I use conda a lot for my Python projects so Python is readily available on my machine. How do I run a simple http server with Python 3 to serve a given directory on localhost?

# solution

You can run a simple http server on localhost with Python 3 by running this command in your terminal:

`python -m http.server [port]`

So, for instance, if I wanted to bind my server to localhost:1228, I'd do:

`python -m http.server 1228`

By default this will serve the directory your terminal is currently in. You can change the directory being served by adding flag `--directory YOURDIRECTORY`.