---
title: "Python: Read secrets / api keys from toml"
date: 2020-06-15T10:38:37-04:00
tags: [
    "python",
    "secrets",
    "apikey",
    "toml"
]
comments: false
---

# problem

I work on a lot of Python projects that touch APIs ([webz](/projects/webz), blinder, and portal all hit the Unsplah API). Most APIs require an api key to help track usage and defend against malicious actors.

These keys work as a way of both indicating who you are and validating you are who you say you are - kind of a username and password mashup. As such, it's important to keep this key safe so that others don't impersonate you and do bad things like steal your usage quota.

This means that I'm often asking the question - How do I use secrets and api keys in my Python projects that is both secure and easy?

# solution

I like to use [toml](https://github.com/toml-lang/toml) because it's a clean, simple format but this general strategy could be used with other formats like yaml if you so desire.

I usually name the file containing all my secret keys `secrets.toml` which looks something like:

secrets.toml
```
google_api_key = "GOOGLEKEYHERE"
```

To read toml in Python, I like using the [toml Python library](https://pypi.org/project/toml/). You can install it with:

`pip install toml`

Then to use it, you can write:

main.py
```
import toml

parsed_toml = toml.load("./secrets.toml")
print(parsed_toml)

google_api_key: str = parsed_toml['google_api_key']
```

This will import the toml library, use the toml library to load the data from the secrets.toml file we created, and then pull out the value of `google_api_key` into a variable for ust to whatever with.

So that gets us a way to store and use these values but how about keeping it out of version control so that people can't just read my secrets? For this I just use git's `.gitignore` file to specify the `secrets.toml` file as something that shouldn't be stored in version control. This looks like:

.gitignore
```
secrets.toml
```

Now this doesn't keep people from seeing your keys if they already have the secrets.toml file loaded on their machine, but it does prevent you from uploading it to version control where everyone would get access to it. This works for my smaller projects that have simple deployments.

So that's how I deal with secrets and api keys in Python!