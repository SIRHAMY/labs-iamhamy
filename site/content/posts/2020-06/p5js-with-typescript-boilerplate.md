---
title: "P5js: My P5js x Typescript development environment"
date: 2020-06-22T18:02:15-04:00
tags: [
    'p5js',
    'typescript',
    'javascript'
]
comments: false
---

# problem

I've been building a lot of projects with p5js (the javascript port of the Processing creative coding library, see: [prickly](/projects/prickly) and [vicidual](/projects/vicidual)) lately and wanted to start using Typescript so that I could get the benefits of static typing in [my projects](/projects).

So I started gathering info and trying out what other people were doing for similar problems in the modern web dev ecosystem and this is what I came up with.

# solution

The solution I found to be easiest to get up and running with and to be extensible with other P5 libraries (like p5.sound for instance) without additional modifications was by cloning [ardelta's p5-typescript-starter repo](https://github.com/arcdelta/p5-typescript-starter).

This project runs with [parceljs](https://parceljs.org/getting_started.html) which bundles up the typescript files you have into javascript that you can then use anywhere. 

Reasons I like it:

* it just works, run `npm start`
* easy to port to production anywhere - `npm build` then take the output artifacts and use them anywhere
* it uses a modern javascript bundler which means there's a bunch of support / documentation if you ever run into problems
* dev mode is fast and features hot reloading right out of the box

Overall, it's just an extremely practical way to get coding with p5js and Typescript. 

If you have a different workflow / project setup, I'd love to hear about it! I'm always in the market for a better dev experience so lmk below or [shoot me a message](https://hamy.xyz/connect).