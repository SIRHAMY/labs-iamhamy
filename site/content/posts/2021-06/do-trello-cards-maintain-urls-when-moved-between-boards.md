---
title: "Do Trello Cards Maintain Urls When Moved Between Boards"
date: 2021-06-02T15:55:13Z
tags: [
    'trello',
    'tools'
]
comments: false
---

# Question

I use Trello as a backbone for many of my productivity systems. I was considering a new paradigm that would require me to move cards between boards while also maintaining url links to them from outside the platform.

Thus I needed to know whether Trello cards maintain their url links when they're moved from board to board.

# Answer

The answer is yes.

I experimented with this by:

* Created a Test Card in Board A and copied its URL
* Move Test Card from Board A to Board B
* Verify URL leads me to Test Car (it did!)
* Repeat process moving Test Card to Board C and Board D

In each case, the same URL was able to route me to the correct card! 

Mischief managed.