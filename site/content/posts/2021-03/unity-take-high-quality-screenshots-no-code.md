---
title: "Unity: Take High Quality Screenshots in Unity with No Code"
date: 2021-03-19T01:08:37Z
tags: [
    'unity',
    'screenshot'
]
comments: false
---

In this guide I'll show you how I take high quality screenshots in Unity3D with no code. 

To do this we'll be using the official Unity Recorder package. I like it because it's:

* Feature rich
* Officially supported
* Free
* No code
* Just works

All of these lead me to believe that this will be a good screen capture method now and in the future.

# Taking a screenshot with Unity Recorder

Here's how to take a screenshot using Unity's official Unity Recorder package.

## Installing Unity Recorder

First let's install Unity Recorder so we can use it.

* Window > Packge Manager
* Inside Package Manager, change the Package type to 'Unity Registry'. 
* Search for 'record' in the search box
* Select `Unity Recorder` then click `Install`

This will begin installation of the Unity Recorder package for use in your project.

## Making Unity Recorder visible in the Unity UI

Next we need to make the Unity Recorder tools visible so we can use them.

Open the Unity Recorder window by:

* `Window > General > Recorder > Recorder Window`
* Now drag and drop the Unity Recorder window wherever you want - just as you would any other tool window.

## Taking a screenshot

Unity Recorder is pretty feature packed, allowing you to take many different kinds of recordings - timed recordings, frame-based recordings, constant / variable rates, etc. But for this post we're just trying to take a screenshot.

* First we need to add a Recorder. To take a screenshot, we'll want to add `Image Sequence`.
* Once added, you should see the red play button colorize to indicate it's now ready to record. Tweak the settings like `Source` (what the recorder will record), `Media File Format`, and Output Resolution to your liking.
* In the top Recorder pane change `Recording Mode` to `Single Frame` and set `Target Frame` to the frame you want to capture (I usually use 0 so that my recorder gets the first frame after clicking the button).

To take a screenshot, simply get your video source looking at the things you want to capture and click the red button or `START RECORDING` to take a screenshot!

# Fin

I've been working a lot more in Unity this year so have been putting out semi-regular tips and tricks for how I get the most out of the environment. If you're interested in getting updates, consider [subscribing to my posts](https://hamy.xyz/subscribe) and/or [subscribing to my videos](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw).

Happy developing!

