---
title: "Hugo: How to get the tags set on the current page"
date: 2020-01-08T19:37:52-05:00
tags: [
    "hugo"
]
comments: false
---

# problem

I use Hugo to statically-generate my sites. In one of my templates, I want to be able to get the tags set on a current page. How can I get those?

# solution

Here's how you can get the tags of the current page:

```
{{ .Param "tags" }}
```

This will display the tags set on the current page (if they exist). Seriously, you can just pop that into a template and they'll appear.