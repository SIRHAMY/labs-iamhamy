---
title: "In 2019, the average value of one of my posts was $0.50"
date: 2020-01-07T21:49:05-05:00
tags: [
    "iamhamy"
]
comments: false
---

Recently I've been thinking a lot about making sure that I'm choosing valuable things to use my time on and backing up those decisions with data. This is one of the reasons why [I started tracking the number of visitors to my projects in 2019](https://blog.hamy.xyz/posts/2019-in-review/#iamhamy).

But number of visitors isn't _really_ value. I mean I like it when people read my posts but views alone won't pay the bills.

So to get at a metric that more closely tracks with inherent value - and that could be compared across project types, not just websites - I chose $$$. You know, that thing you pay the tax man with.

My idea is that by knowing the converted monetary cost of an action (and the expected return of said action), I would be able to more easily compare apples to oranges. Now obvs this isn't a perfect system, but I think it's a step in the right direction.

How this relates to this post is that I was wondering just how much value I got out of each of my posts to better determine how much time I should really be putting in - or at least the cost and returns of doing so.

# calculating the average value of a post

In 2019, [my sites were visited by 21,439 people](https://blog.hamy.xyz/posts/2019-in-review/#iamhamy). While that was a lot for me (a 2700% increase in views year over year!), I know that that's still small-time in the grand scheme of things.

I made $15.47 from ads on my sites and wrote 102 posts that year. Now if I leave it at this and assume that all of the views I got were from posts I wrote this year (which isn't true, but I'm trying to boost my numbers and make this calc easy) then each post was worth ~$0.15.

```
total_revenue_dollars: float = 15.47
total_posts: int = 102

average_revenue_per_post: float = total_revenue_dollars / total_posts
average_revenue_per_post
```

Not nothing, but certainly not a lot.

It does get a bit better though as I only ran ads on my [technical site](https://labs.hamy.xyz) and I only published 66 posts in 2019. If we run those numbers:

```
total_labs_posts: int = 66
average_revenue_per_labs_post: float = total_revenue_dollars / total_labs_posts

average_revenue_per_labs_post
```

Then the value of my average labs post is $0.23. Better, but still abysmal.

But I didn't have ads the entire year. My first day serving ads was actually December 19th, 2019. So we have to calculate just the number of views I got from December 19th to the end of the year.

I know this seems like a _huge_ change, but I think around half of my views were before and after that date as [I had a post hit the front page of Hacker News on the 24th](https://blog.hamy.xyz/posts/2019-in-review/#iamhamy) and totally disrupt my stats.

From 2019.01.01 to 2019.12.19, I had 9,366 visitors and from 2019.12.19 to 2020.01.01 I had 10,089 so that Dec 19th date was pretty close to the midpoint wrt visitors. Also, I know that those numbers don't add up to 21,439 but I'm not going to investigate why that is /shrug.

So if we weight the returns by the number of visitors that happened after the monetization date and project that over the entire year, then we'll have a better idea of the value of the average monetized post.

```
total_visitors: int = 21439
total_visitors_after_monetization: int = 10089

total_projected_revenue_dollars_for_year: float = (total_visitors / total_visitors_after_monetization) * total_revenue_dollars

average_projected_revenue_per_labs_post: float = total_projected_revenue_dollars_for_year / total_labs_posts
average_projected_revenue_per_labs_post
```

Which brings us up to ~$0.50 per post.

So, yeah, I make around $0.50 / post here on Labs and like nothing on my other sites. It's a lot less than I thought but that often happens when you don't take the time to sit down and measure things.