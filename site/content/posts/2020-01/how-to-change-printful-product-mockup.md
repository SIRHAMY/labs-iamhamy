---
title: "Printful: How to change product mockups"
date: 2020-01-13T16:53:19-05:00
tags: [
    "printful"
]
comments: false
---

# problem

I use Printful to create prints for [my shop](https://shop.hamy.xyz). Some of my prints have multiple variants and I want to modify the mockup for one of the existing variants in my store. The edit feature lets me modify the product and design, but there's no option to change the mockups.

# solution

Here's how you can change the mockups for one of your products:

* Navigate to your Printful store and click on the product you want to edit (you can also click the `Edit` button on the target product which will bring you to the same page)
* Click the checkbox next to the variant whose mockup you want to change - A row of actions should appear at the top of the variants list
* Click `Change Print File` - this should pop up an edit modal
* Click `Proceed to Mockups` at the bottom of the modal - this will bring you to a modal where you can change the mockups for your variant

Edit at will!