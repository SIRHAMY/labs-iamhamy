---
title: "Hacker News: How to submit an `Ask HN:`"
date: 2020-01-06T10:30:26-05:00
tags: [
    "hacker-news"
]
comments: false
---

To submit an `Ask HN` to [Hacker News](https://news.ycombinator.com/):

* Log in to Hacker News, if you aren't already
* Go to [the submit page](https://news.ycombinator.com/submit)
* In the `title` section, type your question with `Ask HN:` prepended to the question (e.g. "Ask HN: How do I submit an `Ask HN:`?")
* Leave the `url` section blank
* Optional: Fill out extra details in the `text` section if you think your submission requires extra explanation / clarification / context / etc.