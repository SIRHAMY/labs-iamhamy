---
title: "Hugo: How to see draft posts in localhost"
date: 2020-01-22T19:48:44-05:00
tags: [
    "hugo"
]
comments: false
---

# problem

I use Hugo to statically-generate my sites and while writing / developing, it's often useful to be able to see my posts even while they're in draft mode (i.e. they have `draft: true` in their frontmatter). How can I get those draft posts to show up while I'm serving my site on localhost?

# solution

To see these draft posts, we have to tell Hugo that we want it to output them. To do that, we can pass hugo the `-D` flag which tells it to render posts even if they're drafts.

So to render your draft posts in hugo, you can run:

`hugo serve -D`