---
title: "P5 Python: Create a Color with hexadecimal"
date: 2020-01-17T21:38:01-08:00
tags: [
    "p5",
    "p5py",
    "python"
]
comments: false
---

# problem

I'm using the P5 Python library and want to create a color with hexadecimal, how can I do this?

# solution

To create a color with a hex code, you can do:

`Color("#000000")`

For example, if I wanted to create a big red circle, I could use the following sketch:

```
from p5 import *

def setup():
    size(640, 640)

def draw():
    hex_color: str = "#be403b"
    stroke(hex_color)
    fill(hex_color)
    circle((width / 2, height / 2), height/2)

run()
```

which would create a big circle like so:

![A big red circle created using the above p5 sketch](https://storage.googleapis.com/iamhamy-static/labs/posts/one-off/p5-py-hexadecimal-circle-example.png)_A big red circle created using the above p5 sketch_