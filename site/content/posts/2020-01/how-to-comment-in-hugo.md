---
title: "Hugo: How to write a comment in Hugo code"
date: 2020-01-20T19:07:17-05:00
tags: [
    "hugo"
]
comments: false
---

# problem

I'm using the static site generator Hugo to build my sites but sometimes the templating can get kinda complex. On these occasions it'd be nice to be able to comment my code so I could more easily remember what parts were trying to do what. How do I write comments with Hugo?

# solution

To write comments in Hugo, you can simply use HTML comments. For example:

```
<!-- I am a Hugo comment -->
```

HTML comments are stripped by default (though [don't go adding Go templates to them](https://gohugo.io/templates/introduction/#html-comments-containing-go-templates) as they'll still be evaluated).

Here's another example of a comment in Hugo that (at time of writing) I have live on [my blog](https://blog.hamy.xyz):

```
<!-- if we didn't find any relevant content in tags, we look in kind -->
{{ if not ($scratch.Get "relatedContent") }}
    {{ $scratch.Set "relatedContent" (where .Site.RegularPages "Type" .Section) }}
{{ end }}
```

I've confirmed that this comment doesn't appear in the output html that's served to visitor's browsers on `Hugo v0.59.1`.