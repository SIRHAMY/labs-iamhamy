---
title: "P5 Python: Could not import backend `Glfw` on Mac"
date: 2020-01-14T17:43:48-05:00
tags: [
    "p5",
    "python",
    "glfw",
    "mac"
]
comments: false
---

# problem

I installed Python's port of P5 (the Processing library) and attempted to run the example code that's provided. However when I do so I get an error on line `from p5 import *` that says `RuntimeError: Could not import backend "Glfw": GLFW library not found` - how can I get P5 working on my Mac?

# solution

GLFW is an open source OpenGL library. Depending on your system, you might need to install it yourself. Since you're here, this is probably the case for you.

There are two primary ways to install this - either with homebrew or manually.

Personally, I recommend Homebrew. It just works and is super easy. To install Glfw with homebrew, simply run in a terminal:

```
brew install glfw
```

If you prefer to go the manual route, you can find instructions on [the official GLFW site](https://www.glfw.org/).

Once installed, you should get past the "GLFW library not found" runtime error.