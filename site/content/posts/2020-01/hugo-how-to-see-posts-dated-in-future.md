---
title: "Hugo: How to see posts dated in the future from localhost"
date: 2020-01-15T19:43:44-05:00
tags: [
    "hugo"
]
comments: false
---

# problem

I'm using Hugo to statically-generate my sites. During development (and while writing), I'd find it useful to be able to see my posts that are scheduled to release in the future (i.e. that have their `date` field set for some time in the future). How can I see those posts?

# solution

To tell Hugo that you'd like to render pages even though their `date` hasn't passed (which is the default behavior), you can add the `-F` flag to your command. So to run hugo on localhost while displaying posts dated in the future, you could run (via commandline): `hugo serve -F`