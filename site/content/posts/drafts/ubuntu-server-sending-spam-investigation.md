---
title: "Hello World"
date: 2018-07-25T08:29:31-04:00
description: "About the page"
draft: true
---

Problem: My fucking server is sending fucking spam emails

# Figure out what's sending

* Check mail logs in /var/log/maillog
** Apparently should see if it's an open relay (?)
https://serverfault.com/questions/117098/how-to-check-my-linux-server-isnt-spamming
** Result
*** Don't see a maillog

* Look into access logs to see if bad/other actors touching
** Result
*** Nothing in logs, access report using 'last' looks fine

* Look into the running processes to see if anything is talking to port 25
** watch 'netstat -na | grep :25'

* Install some antimalware shit
** wget "http://www.rfxn.com/downloads/maldetect-current.tar.gz"
** tar -xvzf maldetect-current.tar.gz
** cd maldetect-1.6.2/
** sudo ./install.sh
** sudo /usr/local/sbin/maldet -a /
** Result
*** Said it's all clear...

* See if .xyz does just have a weird open security hole
** Result: Doesn't appear so

* See if maybe it's cause my nginx config doesn't have a default? Or could be caused by bad proxy
** Result: Nothing coming up??? 

* Look at the email thing for clues
** Result: Watched port, but nothing happening...

* Maybe see if can look it up via user/sender

Othershit
* See if any of my docker containers are compromised? 
* git-all seemed like it installed a bunch of random shit, mybe remove?
** Removed

Conclusion: 
* Can't find what caused this...
** Nuking and rebuilding with just what I need (also on 16.04)

# Remove

* Nuke - restart server, change DO credentials

# Resources

* Big daddy - https://serverfault.com/questions/218005/how-do-i-deal-with-a-compromised-server
* https://serverfault.com/questions/117098/how-to-check-my-linux-server-isnt-spamming