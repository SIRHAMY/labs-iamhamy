---
title: "Ethereum vs Polygon for NFTs"
date: 2021-12-29T21:44:13Z
tags: [
    'nfts',
    'blockchain',
    'polygon',
    'ethereum',
    'opensea'
]
comments: false
---

# Overview

So you've got your NFT idea ready to go - some media, a name, some metadata - but you're faced with one final choice - which blockchain to create it on?

In this post, we'll explore the tradeoffs between creating your NFT on Ethereum vs Polygon.

<iframe width="560" height="315" src="https://www.youtube.com/embed/00zWpR-1gaI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Ethereum vs Polygon

We'll be comparing Ethereum and Polygon through several different categories. Up first: what are Ethereum and Polygon?

[Ethereum](https://ethereum.org/en/) is a leading blockchain that houses the cryptocurrency ETH. In addition to a cryptocurrency, it allows developers to build decentralized apps and supports NFTs.

[Polygon](https://polygon.technology/) is an extension of Ethereum that allows it to scale by leveraging superior algorithms and the abilities of other chains alongside Ethereum.

## Trust

There's always some faith involved when choosing a blockchain - they're relatively new technologies so there's a base level of risk involved. As creators we want to choose a trustworthy blockchain so that we have confidence that our investments will hold value into the future.

**Ethereum** is one of the most popular blockchains around. It has a lot of contributors and money flowing in its ecosystem. This makes it reasonably trustworthy because there's so many people working to try and make it succeed.

**Polygon** is a much newer technology that is still growing in adoption. That said it's already been adopted by major players in the NFT space like Opensea and Rarible.

## Cost

As a creator of NFTs, you need to be worried about cost. Many blockchains will require gas fees to be paid in order to create and transfer NFTs. This means higher cost in blockchains will raise costs for you which you'll need to pass onto your customers.

**Ethereum** - The current average gas fee to create and transfer NFTs on Ethereum is about 0.017 ETH ~= $63. That's a lot!

**Polygon** - Due to Polygon's increased scalability, minting on major services like Opensea is totally free!

## Availability

As a creator, you'll also want to make sure that your NFTs will be available on the marketplaces you want to sell on.

**Ethereum** - Available in most large NFT marketplaces (Opensea, Rarible, Nifty Gateway, etc.)

**Polygon** - Not available in all large NFT marketplaces but quickly being adopted (available in Opensea, Rarible, and more upcoming)

## Environment

Unfortunately, legacy Proof of Work blockchains like Bitcoin and Ethereum require massive amounts of energy to function which make them terrible for the environment. I've previously talked about how we can help [fight climate change by making good technology decisions](https://labs.hamy.xyz/posts/fight-climate-change-software-engineer/) and this is one of those.

**Ethereum** - [1 transaction on Ethereum costs about 229 kWh](https://digiconomist.net/ethereum-energy-consumption/) which is roughly equivalent to the amount of power required to power an average US home for 7.7 days!

**Polygon** - Polygon is approximately [40,000x more power efficient than Ethereum](https://blog.polygon.technology/polygon-the-eco-friendly-blockchain-scaling-ethereum-bbdd52201ad/) due to its superior algorithms (Proof of Stake) and scalability.

Note: Ethereum 2.0 plans to move Ethereum to Proof of Stake though timelines are TBD and initial estimates peg it at just 2000x more efficient than its Proof of Work iteration - many times less efficient than existing Proof of Stake blockchains like Polygon and Tezos.

## Opensea

If you're like me and have chosen [Opensea as your NFT marketplace](https://opensea.io/hamy-art) of choice, there are some major tradeoffs to consider when choosing between Ethereum and Polygon.

**Ethereum**

* Price: Must pay gas fees (0.017 ETH ~= $63)
* Abilities - Can only create NFTs with one copy

**Polygon**

* Price - Free to mint!
* Abilities - Can create NFTs with multiple copies

# Conclusion

For me, the low prices, wide availability, and low impact on the environment make Polygon the clear choice for NFTs when compared with Ethereum. Which did you choose?

If you want to learn more about NFTs:

* [What are NFTs?](https://labs.hamy.xyz/posts/what-are-nfts/)
* [Create your first NFT in 2 Minutes (for free!)](https://labs.hamy.xyz/posts/create-an-nft-in-2-minutes/)

