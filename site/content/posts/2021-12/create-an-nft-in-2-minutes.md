---
title: "Create your first NFT in 2 Minutes (for free!)"
date: 2021-12-26T23:51:52Z
tags: [
    'blockchain',
    'nfts',
    'opensea'
]
comments: false
---

# Overview

In this post, I'm going to share a fast, reliable, and free way to create an NFT. By the end of it, you'll have your very own NFT and know how to create and share NFTs around the world.

<iframe width="560" height="315" src="https://www.youtube.com/embed/UvUYwiuREGE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Create a wallet

The first thing you need to do is create a wallet. This is kind of like an email address in the blockchain world - it's where you'll hold coins and tokens, how people will address things to you, and how websites will know what user you are.

We'll be using Opensea to create our NFT because it's fast and easy (which is why [I create NFTs on Opensea](https://opensea.io/hamy-art)). Opensea works with many kinds of wallets so you can choose the one you like best. If you're having trouble choosing, I'd recommend Metamask because it is also fast and easy to use.

Go ahead and get that set up and come back here.

# Create your first NFT

Head on over to [Opensea](https://opensea.io/).

## Login to Opensea

* Connect your wallet to Opensea to login. You can do this by clicking the little wallet icon in the top right and following the steps for your respective wallet.

## Create a Collection

* Hover over your profile picture in the top right and select `My Collections`
* From the `Collections` page click `Create a Collection`. This is basically like a 'folder' that you'll create NFTs into that will have similar settings. You can have many collections so don't fret too much about getting this one perfect.
    * Select a `Logo image` and add a `Name`.
    * `Royalties` is how much you'll get back from resells of the NFTs you create in this collection. I typically put 10%.
    * `Blockchain` is the blockchain the NFT will be created on. I recommend choosing `Polygon` which is a more scalable blockchain built on top of Ethereum. This will allow you to create NFTs for free, create NFTs with multiple editions (i.e. more than one of a given NFT), and is also _way_ better for the environment.[ Like 40,000x more energy efficient](https://blog.polygon.technology/polygon-the-eco-friendly-blockchain-scaling-ethereum-bbdd52201ad/).
    * Complete the rest of your choices then create your collection

## Create an NFT in your Collection

* Navigate to your new collection and select `Add item` to start creating your NFT
* Add media to it
* Add a Name
* Add any other metadata you'd like
* Set `Supply` - this is the number of editions of this NFT you are creating (read more in [What are NFTs?](https://labs.hamy.xyz/posts/what-are-nfts/))
* Set `Blockchain` - again I recommend Polygon
* Click `Create`

You now have an NFT! You can send one to others by getting their wallet ID and sending it to them!

Go nuts!