---
title: "What Are NFTs?"
date: 2021-12-26T15:28:04Z
tags: [
    'nft',
    'blockchain',
    'technology'
]
comments: false
---

# Overview

NFTs are booming with marketplaces like [Opensea](https://opensea.io/hamy-art) moving billions of dollars in volume per year and celebs like Snoop Dogg, Tom Brady, and Grimes diving in. But what are NFTs and why are they moving so much money?

In this post I'm going to explain NFTs in a simple way so you can get on with your day.

<iframe width="560" height="315" src="https://www.youtube.com/embed/X7uqhIrCBkE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Blockchains as ledgers](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/blockchains_as_ledgers.png)

_Blockchains as ledgers_

# What is Blockchain?

Before we can talk about NFTs, we must first get some shared context on what blockchain is. This is because NFTs exist on blockchain.

For our purposes it is sufficient to think of blockchain like a public, trusted ledger (ledger = list of records).

* A ledger is a list of records
* A record contains some data
* Anyone can read the ledger
* The ledger has a way to 'prove' it is correct (i.e. each record is correct, the order is correct, etc)
* The ledger has a mechanism to determine what the next record to be added is

This means that at any point in time, anyone could start reading the ledger from the beginning to the end and understand everything that happened / all information available on the ledger.

In this metaphor, each blockchain would be its own ledger. This means each blockchain has its own set of records and methods of performing each of the above tasks.

_Note: The implementation details behind how each blockchain performs the above tasks is out of scope for this post._

![Fungible vs Non-Fungible](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/fungible_vs_non_fungible.png)

_Fungible vs Non-Fungible_

# What are NFTs?

Continuing our public, trusted ledger metaphor, an NFT is just a special record on the ledger.

NFT = Non-Fungible Token

Simply this means that it's a unique element as opposed to a non-unique element.

Fungible - An example of a non-unique element might be a $1 bill. $1 = $1. You can swap dollars and effectively you still have the same thing - $1.

Non-Fungible - An example of a unique element (closer to what an NFT is) might be a car. Car1 != Car2. Even if you have 2 cars that are the same make and model, they will have different vin numbers, mileage, parts, repair histories, etc. If you swap Car1 for Car2 you will have a different car.

![NFTs as Trading Cards](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/nfts_as_trading_cards.png)

_NFTs as Trading Cards_

# NFTs are like Trading Cards

Let's try a metaphor for something we're already probably familiar with - trading cards.

Trading cards have a relatively simple formula:

* There exist a set of cards
* There are types of cards within the set
    * Each type is produced some number of times (which affects its rarity)
    * Each card has some metadata associated with it
        * A picture
        * Some stats
        * Maybe some powers / descriptions
* Each individual card is owned by one person
    * Though many people may have cards of the same type

NFTs are very much like this:

* An NFT can be created with many 'editions' (i.e. you can create more than one of the token)
* An NFT can have metadata attached
    * A picture / video / audio
    * Some properties / stats
* Each NFT is owned by one person (via a wallet)
    * Though many people may have NFTs of the same type (if more than one of the token were created)

When a card (NFT) is created, it's logged on the blockchain. Some info that's stored:

* A unique ID for the card type
* A unique ID for each individual card that was created
* How many of this card type exist
* Who created the card (and currently owns it)
* Any metadata you've attached to the card (media, words, etc)

When a card (NFT) is traded, it's logged on the blockchain.

* The specific card id that was traded
* Who it went from and who it went to (in blockchain land, these will be wallet IDs)

![Trading Cards on Blockchain](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/trading_cards_on_blockchain.png)

_Trading Cards on Blockchain_

Let's look at an example of how card (NFT) creation and trading might work on blockchain.

* I create 5 editions of a card. `card created - ID = 1, Editions = 5, Owner = HAMY`
* I trade a card to Megna. `card traded - ID = 1, Edition = 1, Trade = HAMY -> Megna`
* I trade a card to Bob. `card traded - ID = 1, Edition = 2, Trade = HAMY -> Bob`
* Megna trades a card to Naveen. `card traded - ID = 1, Edition = 1, Trade = Megna -> Naveen`

We can see that the attributes of an NFT and the recording of activity on the blockchain allows anyone at any time to read the full ledger and understand the state of the world - what NFTs exist and who owns each one.

That's essentially how NFTs work.

# What use are NFTs?

NFTs are just a tool. One of their most compelling attributes is as a way to show ownership of some item.

In the traditional sense, proving ownership of something can be its own value. For instance, some rare [Pokemon cards have sold for as much as $195k](https://www.dicebreaker.com/games/pokemon-trading-card-game/best-games/rare-pokemon-cards). NFTs offer a similar ability to prove authentic ownership of a unique item and then trade that ownership for money.

But some of the cool things about NFTs that traditional ownership models don't have is that:

* You can prove a given token is authentic (i.e. no faking / forging)
* You can prove how many exist (i.e. no artificial scarcity)
* You can prove who owns it (i.e. no chasing proof of sale papers around)
* You can verify this fast (at the speed of the internet)

This has unlocked some interesting use cases:

* NFT ownership -> access to exclusive sales, content, and physical events
* Unlocking content in a game or website

And more are coming out each and every day.

An example of something like this is if you wanted to give a prize to someone who has collected the first 151 Pokemon. Traditionally you'd need them to provide some sort of proof and then take time to authenticate their claims. In NFT land (assuming your Pokemon + ownership were on blockchain) you could just check the ledger and see if the person had the Pokemon or not.

# Next steps

Want to get started with NFTs? I recommend starting on [Opensea](https://opensea.io/hamy-art) - it's fast, reliable, and free! It's also where I create and sell my own NFTs.

Here's a walkthrough to get you started: [Create your first NFT in 2 minutes](https://labs.hamy.xyz/posts/create-an-nft-in-2-minutes/)

Still have questions? Liked the explanation? Comment below or [reach out to me](https://hamy.xyz) to let me know!

Yours in decentralization,

-HAMY.OUT