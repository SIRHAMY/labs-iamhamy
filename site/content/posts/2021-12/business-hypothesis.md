---
title: "Business Hypothesis"
date: 2021-12-15T20:30:44Z
tags: [
    'business'
]
comments: false
---

The hypothesis formula:

`business = problem x solution x market`

* Problem - An opportunity for change in the world. The core purpose behind your business.
* Solution - The method by which you propose to bring about this change
* Market - The subset of the world to which you would offer this change.

We live by imperfect information. Hypotheses are often incorrect. We must design our hypotheses to be testable so that we may better (and faster) identify our miscalculations and improve.

---

In separate news I've now incorporated HAMY LABS LLC - one of my longest-held hypotheses.