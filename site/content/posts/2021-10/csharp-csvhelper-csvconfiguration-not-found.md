---
title: "C#: CSVHelper - CsvConfiguration Not Found"
date: 2021-10-30T17:29:10Z
tags: [
    'csharp',
    'csv',
    'csvhelper'
]
comments: false
---

# Problem

I'm using CSVHelper to read in CSVs for my C# projects. I'm trying to use the CsvConfiguration object to configure my reads, but I'm getting errors with:

`The type or namespace name 'CsvConfiguration' could not be found (are you missing a using directive or an assembly reference?)`

# Solution

In order to use CSVHelper, you have to create a `using` statement on the top of your files in which you reference it. To get the base library, you can add to the top of your files:

`using CsvHelper;`

However, other configuration options like `CsvConfiguration` do not live in that namespace. They live in another namespace which you can add to your file by adding:

`using CsvHelper.Configuration;`