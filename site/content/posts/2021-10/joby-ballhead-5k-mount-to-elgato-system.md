---
title: "JOBY Ballhead 5k Mount compatibility to Elgato System"
date: 2021-10-17T15:38:43Z
tags: [
    'recording',
    'system',
    'office',
    'desk'
]
comments: false
---

# Overview

I've been creating a single-stand, multi-purpose recording studio for my desk. To do this, I've been using the [Elgato mounting system](https://amzn.to/3DOvTmC) along with a [JOBY Ballhead 5k](https://amzn.to/3DPUur3) to mount my camera to the system.

However I ran into a problem because the JOBY Ballhead 5k isn't directly compatible with the Elgato mounts. It took me awhile to resolve this so I'm writing my findings here.

# Solution

TL;DR - You need an adapter from 3/8"-16 to 1/4"-20 screw hole (like [this adapter](https://amzn.to/2Z3uKbR))

It seems like most 'professional' mounts (including the JOBY Ballhead 5k) use a 3/8"-16 screw hole on their bottoms while the screw hole to go into cameras is typically 1/4"-20 (i.e. the hole on the bottom of your camera that you screw your tripod into). As such the Elgato mounting system has screws for the 1/4"-20 because they're assuming you'll be mounting into cameras.

Thus to make your JOBY Ballhead 5k compatible with the Elgato system, you need an adapter that can convert the 3/8"-16 hole in the bottom of the JOBY into the 1/4"-20 hole that is compatible with Elgato.

These are relatively easy to find once you know what too look for - I used this [adapter recommended from Amazon](https://amzn.to/2Z3uKbR).