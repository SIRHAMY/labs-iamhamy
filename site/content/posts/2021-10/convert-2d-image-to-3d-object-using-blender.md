---
title: "Convert 2d Images to 3d Objects Using Blender"
date: 2021-10-08T12:21:34Z
tags: [
    'blender',
    '3d'
]
comments: false
---

# Overview

We're going to be turning a 2D image into a 3D object using the Blender program.

<iframe width="560" height="315" src="https://www.youtube.com/embed/H-J_-3JLXpE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Steps

* Acquire an SVG version of your Image

Import SVG into Blender

* Go to `Edit > Preferences > Add-ons` and search for `SVG`
    * Ensure the `Import-Export: Scalable Vector Graphics (SVG)` add-on is enabled
* To import your SVG, click `File > Import > Scalable Vector Graphics (.svg)`
* Checkpoint: You should now see your SVG inside of your Blender scene (it may be very small)

Convert SVG to Object

* Select your SVG
* Click `Object > Convert > Mesh`

Make Object 3D (add thickness)

* Change to `Edit Mode` -> Usually can click `Object Mode > Edit Mode`
* Select all faces: Select your object in object list then press `a` to select all faces
* Extrude Faces: `Mesh > Extrude > Extrude Faces`
    * This will allow you to add 'thickness' to your object. Make as thicc as you want.
    * There will be a little menu popup in the bottom left that you can use to set an exact extrude value (how thick it is) after you extrude

Simplify 3D Object 

* Note: We want to simplify our 3D object to make any future uses behave more intuitively and reduce any processing complexity we may run into
* Go to `Edit Mode`
* Select object
* Right-click on object, select `Merge Vertices > By Distance`