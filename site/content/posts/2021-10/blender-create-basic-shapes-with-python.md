---
title: "Blender: Create Basic Shapes With Python"
date: 2021-10-17T21:03:58Z
tags: [
    'blender',
    'python',
    'scripting'
]
comments: false
---

# Overview

Blender has a robust set of actions made available via their Python API. In fact most things that you can create in Blender by hand can also be scripted with Python.

Today we're going to provide a quick overview of how to create some basic object meshes via the Python API:

* Cube
* Sphere (ico sphere)
* Triangle / Pyramid (via Cone)

<iframe width="560" height="315" src="https://www.youtube.com/embed/-aQ29TVuhBM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Create basic shapes in Blender with Python

While most actions available to you via Blender are also made available via the Python API, this doesn't mean they're super easy to implement yourself / reason about. To try and simplify things I've created some helper functions around the Python API to make things a little clearer.

As always, the best place to learn about the ins and outs of a code base is to look at the official documentation and the code itself so I've linked the official docs for each Blender Python function we use.

Here's the code:

```
import bpy  # gives access to Blender function from Python
from typing import List

# docs: https://docs.blender.org/api/current/bpy.ops.mesh.html#bpy.ops.mesh.primitive_cube_add
def add_cube(
        size: float = 1.0,
        location: List[float] = [0.0, 0.0, 0.0],
        rotation: List[float] = [0.0, 0.0, 0.0],
        scale: List[float] = [2.0, 2.0, 2.0]) -> None:
    bpy.ops.mesh.primitive_cube_add(
        size=size,
        calc_uvs=True,
        enter_editmode=False,
        align='WORLD',
        location=location,
        rotation=rotation,
        scale=scale,
    )

# docs: https://docs.blender.org/api/current/bpy.ops.mesh.html#bpy.ops.mesh.primitive_ico_sphere_add
def add_sphere(
    subdivisions: int = 4,
    radius: float = 1.0,
    location: List[float] = [0.0, 0.0, 0.0],
    rotation: List[float] = [0.0, 0.0, 0.0],
    scale: List[float] = [1.0, 1.0, 1.0]
    ) -> None:
    bpy.ops.mesh.primitive_ico_sphere_add(
        subdivisions=subdivisions,
        radius=radius,
        calc_uvs=True,
        enter_editmode=False,
        align='WORLD',
        location=location,
        rotation=rotation,
        scale=scale
    )

# docs: https://docs.blender.org/api/current/bpy.ops.mesh.html#bpy.ops.mesh.primitive_cone_add
def add_cone(
    vertices: int = 4,
    radius1: float = 1.0,
    radius2: float = 0.0,
    depth: float = 2.0,
    location: List[float] = [0.0, 0.0, 0.0],
    rotation: List[float] = [0.0, 0.0, 0.0],
    scale: List[float] = [1.0, 1.0, 1.0]
) -> None:
    bpy.ops.mesh.primitive_cone_add(
        vertices=vertices,
        radius1=radius1,
        radius2=radius2,
        depth=depth,
        end_fill_type='NGON',
        calc_uvs=True,
        enter_editmode=False,
        align='WORLD',
        location=location,
        rotation=rotation,
        scale=scale
    )

def main():
    print('Running')

    add_cube(location=[0, 0, 0])

    add_sphere(location=[0, 0, 3])

    add_cone(vertices=4, location=[0, 0, 6])

main()
```

A quick walkthrough of the code:

* We run this code via the `main()` function at the bottom
* This runs the `def main()` block which instantiates a cube, sphere, and triangle with calls to `add_cube`, `add_sphere`, and `add_cone` respectively (a cone with 4 vertices is just a pyramid in Blender world)
* We've defined each of these functions above for ease of reading and because Python scripts like this require you to define functions before you call them
* I've taken the liberty of filling in a few fields myself with sane defaults:
  * `align` - I set to `WORLD` because I think most people want their scripted objects to be world aligned
  * `calc_uvs` - Yeah you probably want to calculate uvs
  * `enter_editmode` - Probably don't want / need to be in editmode after instantiation. You can always enter it manually if you wish later.

# End

That's it - you should now be able to instantiate some basic objects in Blender!

You can find out how to instantiate more objects at the official [Blinder Python docs](https://docs.blender.org/api/current/bpy.ops.mesh.html).