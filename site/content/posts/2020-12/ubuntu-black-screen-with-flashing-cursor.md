---
title: "Ubuntu: Black screen with flashing cursor"
date: 2020-12-19T01:28:57Z
tags: [
    'ubuntu',
    'troubleshoot'
]
comments: false
---

# Problem

I recently got some notifications from Ubuntu that I was running out of space. I thought it was odd because I had allocated a ton of space to my OS but sometimes Ubuntu does this.

So I did a quick `sudo apt-get clean` to clean my temporary files and restarted my computer so it could remove any locks and clear any garbage it was holding onto.

Lo and behold, when I did that I was greeted with a black screen and a flashing cursor.

# Solution

I searched around the internet and found several posts mentioning different methods of solving. Here I'll share what worked for me so that it might help you out of a similar pickle.

## 1. Upgrade all packages

The first thing I like to do is to try and upgrade all my packages. Sometimes that just works and I can go along my merry day.

From the black screen, you can hit `CTRL + ALT + 3` to get to a terminal. Login with your username and password.

To update all packages:

* `sudo apt-get update` - this fetches all the new updates from the repos but doesn't actually install anything
* `sudo apt-get upgrade` - this will start downloading packages and installing them based on the updates found in the previous command

A quick reboot should let you know if you've fixed the black screen or not.

At this point in my journey I actually got an error saying there wasn't enough room to download the updates. This gave me a clue that the problem was likely the space I had left on my computer. Due to this, I jumped down to 3 but if I were to do this again and _hadn't_ gotten that error then I'd continue onto 2 before attempting 3.

## 2. Update drivers

The drivers are the things that allow your OS to talk to the hardware. So often when I get weird black screen errors I think about updating those. I've written a post about how to do this before in [Ubuntu: Update Nvidia Drivers](https://labs.hamy.xyz/posts/ubuntu-update-nvidia-drivers/).

Go do that and see if it helps. If not, I'll see you back here.

## 3. Allocate more disk space

Now I probably wouldn't recommend doing this unless you know that you have a disk space problem. In my case, I got an error telling me that I didn't have enough space to upgrade packages in step 1 so that was a pretty good indicator that space was the issue.

In my research I learned that you can't increase the space of the currently running root drive so even if I could've logged into Ubuntu, I still wouldn't have been able to solve this problem. Instead, I had to boot into a USB Ubuntu and use that to increase the partition of my root drive. 

Once I did that and rebooted I was welcomed by the Instagram-color-resembling splash screen that graces the default Ubuntu login page.

Hope this helps you in your Ubuntu journey!

-HAMY.OUT