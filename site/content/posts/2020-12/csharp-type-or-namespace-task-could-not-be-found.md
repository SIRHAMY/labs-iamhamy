---
title: "C#: The type or namespace name 'Task<>' could not be found"
date: 2020-12-13T19:00:47Z
tags: [
    'csharp',
    'dotnet-core',
    'task'
]
comments: false
---

# Overview

I'm building an app with .NET Core and using async Tasks in it. When I go to build it I'm faced with this error:

`error CS0246: The type or namespace name 'Task<>' could not be found`

How do I fix this?

# Solution

Usually this just means you're missing an import statement. For Tasks, you'll need to import by adding this line to the top of your file:

`using System.Threading.Tasks;`