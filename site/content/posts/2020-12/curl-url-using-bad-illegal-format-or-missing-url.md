---
title: "curl: `URL using bad/illegal format or missing URL`"
date: 2020-12-06T17:25:56Z
tags: [
    'curl',
    'troubleshoot'
]
comments: false
---

# Overview

I was browsing some API documentation and copied / pasted the curl example into my terminal. After doing that I got an error: `curl: (3) URL using bad/illegal format or missing URL`.

Why did this happen and how can you get around it?

# Solution

The problem seems to be that the URL I pasted had new lines in it. This makes sense as the example I copied and pasted was formatted to be readable. However it seems that the terminal didn't like that very much.

To fix, I removed the new lines from my command and ran it again.