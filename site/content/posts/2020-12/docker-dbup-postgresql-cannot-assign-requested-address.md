---
title: "Docker: DbUp and Postgresql -  Cannot assign requested address"
date: 2020-12-02T17:40:21Z
tags: [
    'docker',
    'dbup',
    'postgres'
]
comments: false
---

# Overview

I'm using DbUp within a Docker container to perform code-based database deployments. However when I try to connect to my Postgres container from my code container I'm getting the error:

```
Exception (99): Cannot assign requested address [::1]:5432
```

My (redacted) connection string looks like this:

```
"Host=localhost;User Id=my_user;Password=my_password;Database=my_database;Port=5432"
```

How do I fix this?

# Solution

The problem was trying to access the database via localhost. Within a container, localhost refers to the container itself. So I was trying to access the database from the code container but doing this by connecting to the code container - thus an error because the connection didn't exist (my code container listens on 8080).

To fix this, we need to use the correct host. Let's say I've named my database container `my_database_container` in docker-compose. This means that I can access it from another container by using the container name as the host, so `Host=my_database_container`. 

I was able to get past this issue by modifying my connection string to look like this:

```
"Host=my_database_container;User Id=my_user;Password=my_password;Database=my_database;Port=5432"
```