---
title: "C#: The type or namespace name 'StringContent' could not be found"
date: 2020-12-20T19:26:46Z
tags: [
    'csharp',
    'dotnet-core',
    'stringcontent'
]
comments: false
---

# Overview

I'm building a web app in C# on .NET Core and I'm running into an issue when trying to use `StringContent`. The error:

`error CS0246: The type or namespace name 'StringContent' could not be found`

# Solution

Typically errors like this mean you're missing an import. I was able to fix this by adding this import to the top of my file:

`using System.Net.Http;`
