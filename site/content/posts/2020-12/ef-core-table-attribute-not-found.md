---
title: "EF Core: Table Attribute Not Found"
date: 2020-12-09T02:59:09Z
tags: [
    'csharp',
    'dotnet-core'
]
comments: false
---

# Overview

I'm using Entity Framework Core in my .NET Core app. I've got a `Sentinel` model that I'm using to test my app's integration with my postgres database and I'm using the `Table` attribute to map the object to the name of the table where it lives. When I try to build it I keep getting this error:

`error CS0246: The type or namespace name 'Table' could not be found (are you missing a using directive or an assembly reference?)`

How can I resolve this?

# Solution

The solution is to install either `System.ComponentModel.DataAnnotations` or `Microsoft.EntityFrameworkCore` (which contains the former package as a dependency).

From there, you can import `System.ComponentModel.DataAnnotations.Schema` into your file to get the Table attribute working.

Example code:

```
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace LineTimesServer {
    [Table("sentinel_table")]
    public class Sentinel {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
```