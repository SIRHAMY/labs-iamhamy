---
title: "Dotnet Core: List names of all embedded resource files"
date: 2020-12-15T19:00:29Z
tags: [
    'dotnet-core',
    'embedded-resource'
]
comments: false
---

# Overview

I'm building a `dotnet core` web service and trying to add some embedded resources. However every time I try to access them by file name, I can't seem to find the embedded resources. How can I get all the names of the files that are embedded in my project?

# Solution

We can do this by using `Reflection` on an `Assembly` and getting the manifest resources.

Here's some example code:

```
using System;
using System.Reflection;

namespace MyApp {
    public class Program {
        public static void main(string[] args) {
            var resourceNames = Assembly.GetExecutingAssembly()
                .GetManifestResourceNames();
            foreach(string name in resourceNames) {
                Console.WriteLine($"Resource: {name}");
            }
        }
    }
}
```

This will grab the executing `Assembly` (the current 'program') and get all the embedded resource files via the `GetManifestResourceNames` call. It then loops through each of these names and prints them to the console.