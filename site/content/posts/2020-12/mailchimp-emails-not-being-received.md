---
title: "Mailchimp: Emails not being received"
date: 2020-12-20T21:08:37Z
tags: [
    'mailchimp',
    'domains',
    'emails'
]
comments: false
---

I just launched my newest project [LineTimes](https://linetimes.xyz). It's in the validation stage so I'm mostly trying to understand whether people find it useful or not. To do that, I created an MVP of functionality and added an email subscription widget for a Mailchimp list at the bottom of the page to acquire emails. If someone signs up for an email, I have a pretty good idea that they're a committed, engaged user.

The problem is that a few of my users reached out saying that they never received an email confirmation. This is bad because I have my list set to only subscribe those users that confirm - as a way to filter out any non-intentional and / or malicious sign ups from inflating my costs.

So I started looking around for potential ways to fix this.

# Solution

What I landed on was lack of [Email Domain Authentication](https://mailchimp.com/help/set-up-email-domain-authentication/). Apparently several services use this authentication to help determine whether this email ends up in a spam folder or a regular folder. 

So I followed Mailchimp's tutorial and I haven't had any problems since. I have had reports that my emails are ending up in Gmail's 'promotions' folder but I don't really know what to do about that. If you've got any leads, let me know in the comments.

Happy emailing.

-HAMY.OUT