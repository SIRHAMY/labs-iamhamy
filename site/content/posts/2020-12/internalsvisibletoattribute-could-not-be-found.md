---
title: "C#: type or namespace name 'InternalsVisibleTo' could not be found"
date: 2020-12-26T18:28:16Z
tags: [
    'csharp',
    'troubleshoot'
]
comments: false
---

# Overview

I'm trying to make it so my dotnet core test project can test the internals of my source project. To do that I'm using the `InternalsVisibleTo` attribute. However when I try to compile I'm getting the error:

`error CS0246: The type or namespace name 'InternalsVisibleTo' could not be found`

How do I fix this?

# Solution

Typically errors like this mean you're missing an import of some sort. I was able to fix this by adding this import line at the top of my file:

`using System.Runtime.CompilerServices;`