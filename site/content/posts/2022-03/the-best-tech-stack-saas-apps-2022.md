---
title: "The Best Tech Stack for Building Modern SaaS Apps in 2022"
date: 2022-03-23T01:00:50Z
tags: [
    'svelte',
    'csharp',
    'postgres',
    'technology',
    'business',
    'saas'
]
comments: false
---

A good tech stack can be a force multiplier for your service's ability to push new features, scale with user growth, and maintain high levels of service excellence. A poor tech stack can increase the time and money required to ship new features, become increasingly expensive and unpleasant to maintain, and incur hundreds of developer hours of tech debt in the tech stack's lifetime.

In this post, I share the best tech stack for building modern SaaS (Software-as-a-Service) applications in 2022. I'm pulling from years of experience as a [professional Software Engineer](https://www.linkedin.com/in/hamiltongreene/) and [building / launching my own businesses as a solo-entrepreneur](https://labs.hamy.xyz) to share my recommendations for a go-to tech stack for full-stack business apps.

# Table of Contents

* [The Best Tech Stack for SaaS Applications](#the-best-tech-stack-for-saas-applications)
* [Backend](#backend) - The brains + engine of your system
* [Frontend](#frontend) - How users interact with your system
* [Database](#database) - Where you store data
* [The Best Tech Stack](#the-best-tech-stack)

# The Best Tech Stack for SaaS Applications

There are a lot of factors that go into choosing an appropriate tech stack for your app - from the type of business you're running, to the expected usage, to the technologies your team is already familiar with. I can't go into every possible edge case to recommend the best tech stack for your specific scenario, but I can provide you with a collection of high quality, productive technologies that are a good fit for building most full-stack SaaS apps.

To do this, I'm going to walk through the major areas of a modern software service and recommend technologies and the factors I weighed when choosing them.

For each part of the stack, I'll share:

* My recommendation and why
* Features that make it my #1 pick
* Honorable mentions (in case you want to make some subsitutions)
* Some popular choices to stay away from

Hopefully this will give you a good understanding of the technologies I use and recommend with enough information to make substitutes should your specific situation call for that.

<iframe width="560" height="315" src="https://www.youtube.com/embed/nsNaSdmMH5o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Backend

The backend is the most important part of your system. This is where your core business logic lives and is thus the brains and engine of your entire service. 

**Recommendation:** C# and .NET

C# (programming language) on .NET (web framework) is one of the most popular, loved, and capable backend technologies on the planet. [.NET variants are the 1st and 2nd most popular frameworks](https://insights.stackoverflow.com/survey/2021#most-popular-technologies-misc-tech-prof) as voted by professional developers and comes in [1st for most loved framework](https://insights.stackoverflow.com/survey/2021#section-most-loved-dreaded-and-wanted-other-frameworks-and-libraries) and [2nd for most loved web framework](https://insights.stackoverflow.com/survey/2021#most-loved-dreaded-and-wanted-webframe-love-dread) - only trailing [Svelte, our pick for Frontend](#frontend).

It can handle pretty much any workload and scale you throw at it, has great development ergonomics, and is constantly being improved by Microsoft. The ecosystem is mature with hundreds of thousands of libraries and docs to use and reference in your projects. Plus people love to work with it. 

All this makes C# and .NET my pick for Backend.

**Features:**

* Huge Community
    * Most popular and loved framework
    * [1.5M questions on Stack Overflow](https://stackoverflow.com/questions/tagged/c%23) tagged C#
    * Huge library of documentation (see [Microsoft docs](https://docs.microsoft.com/en-us/dotnet/csharp/))
* Mature Ecosystem
    * Large organization backing
        * Backed by Microsoft
        * Many big orgs - MasterCard, Stack Overflow, Roblox, etc
    * Reliable package system - [Nuget](https://nuget.org)
        * 300k packages, 173B downloads
* Performant
    * .NET Core - Regularly ranks in the [top 10 most performant web frameworks](https://www.techempower.com/benchmarks/#section=data-r20&hw=ph&test=composite)
    * C# - Typically [performs comparably or better than Java](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/csharp.html)
* Good language ergonomics
    * It's like Java but better
        * Static Typing
        * Linq
        * OOP
        * Functional Programming
    * Constantly upgrading developer experience
* Runs cross-platform

**Honorable Mentions:**

* Python
* NodeJS

**Stay away from:**

* Java - It's falling out of favor and falling behind. Use C#.

# Frontend

Frontend is the second most important part of your system. Assuming your backend systems do their job, the frontend provides a way for users to interact with your service.

**Recommendation:** Svelte (and Sveltekit)

Svelte is a web framework that provides similar web app building capabilities as React, Vue, and Angular. Sveltekit is a web framework that makes it very easy to stand up a performant Svelte frontend, including things like routing, SSR (Server-Side Rendering), and data handling mechanisms right out of the box (Svelte is to Sveltekit as React is to NextJS).

Though Svelte has relatively [small usage (only 2.7%)](https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-web-frameworks) it's in the [top 5 most wanted web frameworks](https://insights.stackoverflow.com/survey/2021#most-loved-dreaded-and-wanted-webframe-want) and has been ranked the most loved web framework 2 years running ([Stack Overflow Developer Survey 2021](https://insights.stackoverflow.com/survey/2021#most-loved-dreaded-and-wanted-webframe-love-dread), [State of JavaScript 2021](https://2021.stateofjs.com/en-US/libraries/front-end-frameworks/)).

Anecdotally, Svelte is the simplest, fastest web framework I've ever used. It is dead simple to spin up a web app - as it should be.

_For more on Svelte and why I chose it over React, read: [Svelte is better than React](https://labs.hamy.xyz/posts/svelte-is-better-than-react/)._

**Features**

* Simple
* Performant - [beating out React, Vue, and Angular](https://labs.hamy.xyz/posts/svelte-is-better-than-react/#speed)
* Great documentation - Seriously, go [play with the docs](https://svelte.dev/tutorial/basics)
* Growing adoption - used by Square, The New York Times, and IBM 

**Risks**

* Relatively low adoption - community is a big factor in the usefulness and longevity of a technology. Svelte doesn't have that yet when compared with leading alternatives like React, Vue, and Angular.

**Honorable Mentions:**

* React
* Vue

**Stay away from:**

* Angular - It's falling out of favor and falling behind the other web frameworks. Use Svelte.

# Database

Your app needs a place to store data. There are a lot of options that can do this job well. Many of these options are converging towards a strcutured, easy to manipulate data model - taking the best from leading SQL and NoSQL options.

**Recommendation:** Postgres

Postgres is a db that has found stable and lasting praise from the developer community. It is the [2nd most popular database](https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-databases) (behind MySQL) and [the 2nd most loved database](https://insights.stackoverflow.com/survey/2021#section-most-loved-dreaded-and-wanted-databases) (behind Redis, way ahead of MySQL).

Postgres has all the features you'd expect of a solid relational database and a ton more advanced features you didn't. It's performant enough to scale to millions of users with little tweaking and has a large, dedicated community backed by many large organizations.

My favorite way to use Postgres is to utilize the Relational schema model to describe how my entities are related which helps maintain structure and allows the database to make more efficient query plans. Then I utilize a JSONB column within the entity to allow for data model flexibility. This mixture gives me the stability and performance of relational databases with the developer ergonomics and velocity of a non-relational document store.

**Features**

* Large community
    * Lots of docs and examples
    * Official, well-supported .NET ORM - [Npgsql](https://www.npgsql.org/efcore/)
    * Managed DB available on most major cloud platforms
* Performant
    * [Beats out MongoDB in most benchmarks](https://info.enterprisedb.com/rs/069-ALB-339/images/PostgreSQL_MongoDB_Benchmark-WhitepaperFinal.pdf)
    * [Beats out MySQL at high loads](https://hevodata.com/learn/postgresql-vs-mysql/#:~:text=PostgreSQL%20is%20faster%20when%20dealing,faster%20for%20read%2Donly%20commands.)
* Mature + Reliabile
    * Trusted by many large organizations - like Uber, Netflix, and Instagram
    * Regular, solid updates
* Flexible + Powerful
    * Can use both SQL and NoSQL modes
    * Tons of advanced features you probably don't need but v useful if you do

Honorable Mentions

* MongoDB
* MySQL

Stay away from:

* SQL Server - [Requires a commercial license](https://www.microsoft.com/en-us/sql-server/sql-server-2019-pricing) and is much less loved and used than other available options. Use Postgres.

# The Best Tech Stack

In the end, the best tech stack really depends on your situation - the problem you're solving, how you're solving it, and who's doing the solving. That said, this tech stack has been chosen by developers for developers as a highly efficient, scalable, and flexible choice for building modern business apps.

* Frontend - Svelte and Sveltekit
* Backend - C# and .NET
* Database - Postgres

I like this tech stack so much that I now build all my SaaS apps with it. To speed up my development time, I built [CloudSeed](https://cloudseed.xyz) - a project template that gives you the full source code for a fully-functional SaaS app built with these technologies. It includes a full data model, Authentication, Subscriptions + Payments, and a whole host of other things that most SaaS applications need. 

With CloudSeed I can spin up and deploy a full-stack SaaS app with this tech stack in minutes. Not months. Get it today at [CloudSeed](https://cloudseed.xyz).