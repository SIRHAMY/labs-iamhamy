---
title: "Svelte is better than React"
date: 2022-03-10T23:41:21Z
tags: [
    'svelte',
    'react',
    'cloudseed'
]
comments: false
---

![Svelte vs React](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/svelte-vs-react/svelte-vs-react-cover.png)

# Svelte vs React

Svelte is a new web framework that provides the ability to build web apps - similar in scope to React, Vue, and Angular. Though it's new, it's already become one of the most loved - ranking in the top 2 most satisfying and most loved web frameworks for the past 3 years (see [State of JS](https://2021.stateofjs.com/en-US/libraries/front-end-frameworks/) and [Stack Overflow's Developer Survey](https://insights.stackoverflow.com/survey/2021#section-most-loved-dreaded-and-wanted-web-frameworks)).

Adopting new technologies into your projects is risky - they may not be fully functional, you may not get the support you need, and they may not stick around for very long. This can leave your project in a worse state than it was before adoption.

In this post I'll share why I believe Svelte is better than React and the factors I took into account before deciding to adopt it into my own projects.

<iframe width="560" height="315" src="https://www.youtube.com/embed/nls3n5K6pZI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Simplicity

The first major win for Svelte over React is simplicity. Simplicity is important for many reasons but it all comes down to just being easier to understand.

* Easier to onboard new developers
* Easier to spot and fix mistakes
* Easier to scale to a bigger code base
* Easier to "move fast" and not break things
* etc.

The simpler your code (and easier it is to understand), the faster you can write better code which means the faster you can complete projects.

## Simplicity - Code Style

The first thing you'll notice when you look at Svelte code is that it just looks normal. It looks almost exactly like normal HTML with in-line code and styling. Normal JS, normal HTML, and just a few imports to pull in extra features.

This makes it easy to get started as long as you've seen some web code before - much easier than picking up some of the language-specific code required for React (JSX).

React example:

```
import React from 'react';

function App() {
   return (
      <h1>
         React example
      </h1>
   );
}
```

Svelte example:

```
<h1>Svelte example</h1>
```

## Simplicity - State Handling

State Handling is where you really start to see the difference in complexities between Svelte and React.

React historically didn't have a primary way to deal with state, federating this job out to third parties to handle. This leaves you with an extremely saturated ecosystem of different state handling software to use with very little standardization.

Eventually React did decide that state handling is a core part of a web framework and so introduced the idea of [Hooks](https://reactjs.org/docs/hooks-intro.html). Hooks work well if you know how to use them, but they're a vastly different programming paradigm than most people ever see / use and they have so many gotchas that they tend to trip people up with high learning curves.

All in all, React leaves you with a lot of choices to make and each of those choices incurs its own learning costs which adds to the complexity.

Example of a React Hook - Each time you click the button, the count increases

```
import React, { useState } from 'react';

function Example() {
  // Declare a new state variable, which we'll call "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <button onClick={() => setCount(count + 1)}>
        Clicked { count } times
      </button>
    </div>
  );
}
```

_Example based on [official React docs](https://reactjs.org/docs/hooks-intro.html)._

Svelte takes a more opinionated approach giving you a standardized way to deal with state right in the framework. It's easy to understand and looks and feels like normal Javascript.

Example of Svelte - Each time you click the button, the count increases

```
<script>
	let count = 0;
</script>

<button on:click={() => count = count + 1}>
	Clicked {count} times
</button>
```

_Example based on [official Svelte tutorial](https://svelte.dev/tutorial/reactive-assignments)_

As you work with more and more complex state management (like global, async updates, etc), Svelte's opinionated design continues to outpace React's in simplicity and dev velocity.

# Speed

Simplicity leads to better, faster code but it's not the only factor. For many large organizations and smaller companies looking to grow, performance is a large consideration and can be make-it-break-it for their products.

## Speed - Performance

If you're serving 1000s of users or more at a time, performant code can be a factor in keeping the user experience smooth, saving on your hosting bills, and in some cases keep your service from shutting down. 

Since we're comparing two frontend frameworks (Svelte and React), most of the performance here will be located in the user's browser and on the edge of your service (for transmitting web payloads and server-side rendering if you're doing that). But we'll still talk about it as it's a meaningful comparison and can be very important for the health and feel of your service. 

![Svelte Performance Test](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/svelte-vs-react/svelte-vs-react-vs-vue-vs-angular-performance-test.png)

In the [performance test results](https://krausest.github.io/js-framework-benchmark/2022/table_chrome_99.0.4844.51.html) shown above, we can see that overall Svelte at least paces with and often beats out React, Vue, and Angular across many different performance-related exercises. For your users, this will typically mean a snappier website and less time waiting on frontend operations.

Potentially a bigger factor - particularly in slower networks and off of wifi or at scale - will be the payload size these frameworks incur. This is important because this must be downloaded by the user's client in addition to anything else that lives on your website before they can actually see or interact with it. So for users on slow networks this can mean more time waiting for your site to load and for websites at scale this can mean a lot more egress cost for you. 

Currently for the full packages:

* [React](https://bundlephobia.com/package/react@17.0.2) (with [ReactDOM](https://bundlephobia.com/package/react-dom@17.0.2)): 42.2kB (gzipped)
* [Svelte](https://bundlephobia.com/package/svelte@3.43.2): 1.6kB

Now I want to call out that these sizes are both relatively small so on most modern sites they will often be dwarfed by the primary content you're serving. But at scale they can add up.

* [Google Homepage](https://www.google.com/): 968kB transferred (23x React)
* [New York Times Homepage](https://www.nytimes.com/): 7.0MB (166x React)
* [HAMY Homepage](https://hamy.xyz/): 456kB (10x React)

## Speed - Dev Velocity

Now while a lot of developers and analysts really like to focus on the speed and performance of the code, I still think that the number one performance issue affecting most teams and projects is actually the speed at which developers can make changes and push reliable features - their dev velocity.

Due to the Simplicity wins from Svelte, I still think Svelte offers this both in terms of onboarding new developers and maintaining mature apps. But it's still new so we'll need more data points from real projects to know for sure.

# Caveats

As I mentioned at the beginning of this post, there are a lot of risks when adopting a new technology. So it's important we weigh those against the benefits before fully commiting to it.

## Adoption + Community

The first big caveat is that Svelte has a very low adoption rate right now with just 20% of surveyed web developers saying they've used it before (see: [2021 State of JS](https://2021.stateofjs.com/en-US/libraries/front-end-frameworks/)).

* React - 80%
* Angular - 54%
* Vue - 51%
* Svelte - 20%

What this means is that there's not as much community involvement as you might get in other framework ecosystems. This will lead to:

* Less code libraries available specifically for your framework
* Less Q&A on Stack Overflow and examples on GitHub
* Less support from some of your favorite technologies

This makes sense as many developers prioritize communities with large followings but unfortunately Svelte just isn't there yet.

On the bright side, [Svelte continually places in the top spots for Satisfaction](https://2021.stateofjs.com/en-US/libraries/front-end-frameworks/), beating out every major web framework in the past 2 years (including React). So while there may not be as much adoption yet, it's definitely not hindering the experience much.

## Stability

Svelte is still relatively new in terms of maturity. There has not been a huge team supporting and backing the project which means less code pushed, less design discussions, and less overall strategy directing the project (especially when compared against React or Angular which has Facebook and Google backing them). 

So far this lack of support doesn't seem to have hurt the project much as it's extremely well-crafted and beating some giants at their own game - performance and dev velocity. But it is something to look out for in case the project takes a sharp turn and makes breaking changes or dies altogether.

On the bright side, Svelte is being adopted in major organizations and sveltekit (svelte's version of React's [NextJS](https://nextjs.org/)) has been sponsored by [Vercel](https://vercel.com/) which has a history of supporting web-friendly projects.

A few well-known orgs using Svelte (pulled from the [Svelte homepage](https://svelte.dev/)):

* IBM
* Square
* The New York Times

# Svelte is Better than React

I am a [backend engineer](https://www.linkedin.com/in/hamiltongreene/) for the most part and when I venture into the frontend (mostly by necessity) I like things to be fast and simple. Svelte provides that simplicity without sacrificing any of the modern, performant features of other major web frameworks.

So for me, Svelte is the right choice. All the benefits, none of the complexity.

![CloudSeed Architecture](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/svelte-vs-react/cloudseed-architecture-svelte.png)

I'm so bullish on this decision that I decided to migrate my SaaS boilerplate [CloudSeed](https://cloudseed.xyz) from React (NextJS) to Svelte (Sveltekit). Its goal is to make it fast and easy for any developer to spin up a fully-featured Software-as-a-Service application in minutes - complete with Authentication, Data Models + Migrations, and Checkouts / Subscriptions. As such simplicity and performance are top of mind so that any developer can use it for any kind of SaaS business. 

Anecdotally the migration from React to Svelte took just a few days and the frontend codebase was vastly simplified and improved in the process. I've still got the React code checked in so if you want to compare the differences or get your hands on a ready-to-sell SaaS project, head over to [CloudSeed](https://cloudseed.xyz).