---
title: "Shutting down my first startup"
date: 2021-05-16T22:06:06Z
tags: [
    'projects',
    'business',
    'linetimes'
]
comments: false
---

At the end of Q1, I announced I was [shutting down my first startup LineTimes](https://blog.hamy.xyz/posts/2021-q1-review/#linetimes). It's been a few weeks since then and I wanted to do a full reflection on the project before I moved on to start something new.

<iframe width="560" height="315" src="https://www.youtube.com/embed/EdZ6eH7AhSg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![LineTimes Closed](https://storage.googleapis.com/iamhamy-static/blog/2021/linetimes-closed.png)

# LineTimes

[LineTimes](/projects/linetimes) was a website that showed real-time wait times at grocery stores in NYC. I created it based on the observation that lines were ridiculously long at peak times in NYC (40+ mins pre-covid, 2+ hours during covid) and that people probably wouldn't be so inefficient if they had data to show them that.

After 3+ months running LineTimes I can pretty confidently say that's not true. But this story is about the journey, not the destination.

# A failed startup

Startups are risky ventures. According to [Investopedia](https://www.investopedia.com/articles/personal-finance/040915/how-many-startups-fail-and-why.asp) the failure rate for startups was 90% in 2019.

To hedge this risk, I created a system of win / lose constraints to force myself to stay within reasonable bounds for risks and costs. These constraints helped me understand that LineTimes wasn't on the path to winning and was detracting from my other ventures. It was time to shut it down.

Primary reasons LineTimes failed:

1. No proof of value
2. Bad value:cost

## Failure reason #1: No proof of value

I created LineTimes with a win constraint of 100 recurring visitors (4+ times / month). This was mainly for the first milestones - if I could show the project could bring in 100 recurring visitors then I knew that it was providing some value.

I settled on this constraint by thinking about how I could prove LineTimes was providing the value I wanted it to provide. I came up with trying to track whether people were actually using this to make their grocery store runs more efficient. Based on 1 grocery run / week, we'd need 4+ visits / month to prove this was useful.

![LineTimes Stats](https://storage.googleapis.com/iamhamy-static/blog/2021/linetimes-visitors-2021-q1.png)

After 3+ months, LineTimes only received 52 new users. That comes out to about 17 users / month, far less than the 100 users / month I was shooting for.

I tried a few different methods of gaining traction:

* Talking to friends and fam about the site
* Posting on my socials
* Featuring the site in my blogs and videos
* Making a sign and standing in front of long grocery store lines

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Launching my first real business of the year:<br><br> LineTimes - a site that calculates wait times for stores in your area. <br><br>Now live with Trader Joe&#39;s locations in Manhattan. <a href="https://t.co/iAgOpn3HCz">https://t.co/iAgOpn3HCz</a> <a href="https://t.co/a6hp28M2bl">pic.twitter.com/a6hp28M2bl</a></p>&mdash; Hamilton Greene (@SIRHAMY) <a href="https://twitter.com/SIRHAMY/status/1340315652377227275?ref_src=twsrc%5Etfw">December 19, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

But in the end it didn't draw the interest and engagement I was looking for.

## Failure reason #2: Bad value:cost

Now just because a venture isn't in a win state, it doesn't mean it should be killed off immediately. For instance, what if we have a site that gets nearly 0 visitors but doesn't cost anything to run? For something like that, leaving it running has little downside.

Ah but LineTimes didn't cost $0 to run and its cost plus its !winning led me to ultimately shut it down.

As a general rule, I like to determine whether something has a good value:cost ratio based on whether it's profitable or not. If it's profitable then it could reasonably run for a long period of time without much downside. If it's not profitable then it's taking resources away from other potentially profitable ventures.

LineTimes was pre-revenue so it's a little weird to put it to the profitability test but the general rule holds that its value should be higher than its cost (and thus profitable).

![LineTimes costs](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/linetimes-google-cloud-costs-march-2021.png)

I wrote a [detailed breakdown of LineTimes' costs](https://labs.hamy.xyz/posts/google-cloud-app-engine-startup-cost/) and found that it cost me ~$90 / month to run.

* $50 - cloud hosting
* $40 - data / api costs

$90 isn't _that_ much money in the grand scheme of things. But I'm big on the fail fast and cheaply train and that was far more than I was willing to pay for 17 users / month.

For some additional perspective, my total [Projects](/projects) budget is $200 / month. Which means this one bad bet was effectively siphoning half of my capital resources. Spending half my budget on a project that couldn't prove its value seemed like an iffy choice.

So I killed the project. It didn't have much value and cost too much.

Though it sucks to kill a project, I think it's worse to over invest in bad projects. By killing a bad project you free up resources (money, time, headspace, etc.) to work on new, potentially better projects.

If you systematize this fail fast and cheaply process, you end up trying out a lot of different projects with minimal investment in bad ones. I think that's a strong strategy for success - and a driving philosophy for my habit of building a project each month.

# Reflection

[I do a lot of reflection](https://blog.hamy.xyz/tags/reflections/) as a way to understand where I am, how I got here, and figure out how to do better in the future. So naturally I wanted to reflect on my failed project, analyze what went wrong, and modify my systems to build better projects next time.

## Win / Lose Constraints

I had a win constraint but no lose constraint for this project. It turned out fine because my project was so costly that it ran up against my implicit lose constraint (going over my Projects budget). But if it had been just a little less costly, I may have let it run for many more months.

In future projects I'll set an explicit lose constraint so I can find and divest from bad projects sooner.

## Talk to real users

I had a lot of data that showed me that LineTimes wasn't providing much value. But I didn't have much to go on to tell me _why_ it wasn't working as I intended.

Talking to users would've given me a lot of insight on what was working and what wasn't. Further, it could've told me if I was solving a problem that didn't exist and alerted me to a possible lose scenario much sooner.

This would've been easy to implement - particularly as I was already out and about advertising for LineTimes - but something I never executed on.

## Use better, cheaper infra

If LineTimes had been cheaper I could've let it ride for a few more months before killing it in case it just needed more exposure. This could've been a pro / con depending on the ultimate outcome but the fact remains that the cost of this project was prohibitive.

During my reflection I did some [analysis on LineTimes' hosting costs](https://labs.hamy.xyz/posts/google-cloud-app-engine-startup-cost/) and found that I could likely have hosted everything for ~$10 / month (down from the actual $50 / month). With the external APIs it would've still costed around $50 / month to run LineTimes but for many projects without this dependency, that'll bring total cost down to $10 / month - vastly expanding the number of projects I can run with my budget. 

# Fin

So that's the story of [LineTimes](/projects/linetimes) and why I felt it was the right time to shut it down. It wasn't the ending I'd hoped for but I'm trying to make the most of it by learning from it and applying those learnings to my future projects.

I share regular updates on my projects / ventures here on the blog so if you liked this, consider: [subscribing for updates](https://hamy.xyz/subscribe/).

Always Hamproving,

-HAMY.OUT