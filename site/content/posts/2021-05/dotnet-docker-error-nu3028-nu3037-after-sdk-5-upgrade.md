---
title: "dotnet core: Docker errors NU3037 and NU3028 after upgrade to sdk:5.0+"
date: 2021-05-13T01:25:05Z
tags: [
    'dotnet',
    'troubleshoot',
    'docker'
]
comments: false
---

# Problem

NuGet ran into certificate problems in January 2021 and April 2021 which were purportedly resolved by changing the target `dotnet/sdk` Docker image to something over 5.0 (i.e. `dotnet/sdk:5.0.203` would work)(source: [NuGet announcement](https://github.com/NuGet/Announcements/issues/58)).

However upon updating my target image in my Dockerfile I continued to hit these NU3037 and NU3028 errors with full messages like:

`error NU3028: Package 'Microsoft.Extensions.Options 5.0.0' from source 'https://api.nuget.org/v3/index.json': The author primary signature's timestamp found a chain building issue: UntrustedRoot: self signed certificate in certificate chain`

and

`error NU3037: Package 'Microsoft.Extensions.Options 5.0.0' from source 'https://api.nuget.org/v3/index.json': The author primary signature validity period has expired.`

# Solution

I was able to resolve this by removing the `--no-restore` flag from my Dockerfile such that it looks like this:

```
FROM mcr.microsoft.com/dotnet/sdk:5.0.203-alpine3.13 AS build
EXPOSE 8080

WORKDIR /source

COPY ./*.csproj ./
RUN dotnet restore

COPY . .
RUN dotnet publish -c release -o /app 
# --no-restore removed in line above

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "LineTimesServer.dll"]
```

I'm not totally sure why this works but my guess is that --no-restore kept the old packages in the app to be released vs refreshing them with `dotnet restore`. This meant that at publish time, we still had the offending packages.

For science, I tried it again with `--no-restore` and that failed.

Removing it allowed it to work again.

So not sure what that says.

Anyway, hope this helped.

-HAMY.OUT