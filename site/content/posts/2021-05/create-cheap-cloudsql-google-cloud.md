---
title: "Google Cloud: Create a cheap CloudSQL instance (`db-f1-micro` or `db-g1-small`)"
date: 2021-05-29T17:48:46Z
tags: [
    'google-cloud',
    'cloud-sql'
]
comments: false
---

# Problem

On Google Cloud's pricing calculator, there are two options for cheap Cloud SQL instances: `db-f1-micro` and `db-g1-small` costing about $9.92 / month and $27.80 / month respectively when outfitted with 25GB HDD storage.

However, when we use the Cloud SQL console to actually create our instances, these options are nowhere to be found. 

# Solution

Not to worry, they're there: Google Cloud is just being a little sneaky and making it hard to figure out.

You can choose the `db-f1-micro` or `db-g1-small` Cloud SQL instances from the Cloud SQL create page by:

* Expand `Extra Settings`
* Under `Machine Type` choose: `Shared core`
    * `db-f1-micro` = 1 vCPU, 0.614GB option
    * `db-g1-small` = 1 vCPU, 1.7GB option
* Be sure to select single instance and the correct storage size to lock in the low prices!