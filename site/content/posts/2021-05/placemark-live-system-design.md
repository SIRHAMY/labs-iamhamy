---
title: "Placemark: Live System Design"
date: 2021-05-23T18:10:22Z
tags: [
    'placemark',
    'projects',
    'system-design'
]
comments: false
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/JuEqgEVfKng" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Summary

[Placemark](/projects/placemark) is my project for the month of May. Its goal is to make it easy to create and share collections of locations with others.

In this video, I create a basic system design for Placemark - walking through the problems it's trying to solve, how it solves for those, and how we can build a software system to support that.