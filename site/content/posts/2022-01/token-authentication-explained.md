---
title: "How Token-based Authentication works"
date: 2022-01-30T20:09:25Z
tags: [
    'authentication',
    'tokens',
    'tutorials'
]
comments: false
---

![Token Authentication Explained](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/token-authentication-explained/token-authentication-explained-hero2.png)

# Overview

Websites need authentication to make sure that users can securely access their services and their data while making sure that bad actors cannot. In this post, we'll explain how services use tokens to authenticate users - allowing the right users in and keeping the bad actors out.

<iframe width="560" height="315" src="https://www.youtube.com/embed/giKeegmeaKw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Token-based Authentication

The basic idea behind token-based authentication is:

* The service creates a special token that can identify and prove who a user is
* The service gives the token to that user
* The user then provides that token to the service to prove their identity

Let's go over some examples.

# What is a Token?

A token can be a lot of things in different contexts. In the context of Authentication though, we can liken it to an ID card.

A real-world parallel to an authentication token is an ID card. IDs are often used to prove a person is who they say they are and that in turn can be used to determine if they have access to a given resource.

Both provide mechanisms to:

* Hold information (like who the ID belongs to)
* Prove the ID is real
    * For IDs this is often done with unique features in materials / design (e.g. a Driver's Licenses are constantly updating to [make them harder to fake](https://www.nytimes.com/2013/03/17/nyregion/new-york-state-will-fight-fake-licenses-with-new-tactics.html))
    * For Tokens this is often done cryptographically (e.g. [JWTs](https://jwt.io/) cryptographically sign the payload)

# ID-based Authentication

Because tokens and IDs are used similarly, we can explain Token-based authentication with a metaphor for ID-based authentication.

Let's start with a scenario:

* An Organization wants to make sure only members can access restricted Resources (like an office, a storage room, free snacks, etc)
* They decide to use IDs to identify members so they can give them access

![ID Authentication](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/token-authentication-explained/id-authentication.png)

How ID access works:

* `Person -> Information -> Organization`
    * When a Person joins an Organization, they will hand over information proving who they are.
* `Person <- ID <- Organization`
    * The Organization verifies that information then creates an ID that uniquely identifies this Person. The Organization may create unique features on the ID to help prove the ID is real.
    * The Person will carry the ID around for whenever they want to access a Resource.
* `Person -> ID -> Organization`
    * The next time a Person wants to access a restricted Resource, they'll present their ID to the Organization (think at Security or on a scanner). The Organization can check the ID to make sure it's real and the Person has access.

Outcomes:

* `Person <- Resource <- Organization`
    * If the ID is verified then the Organization can give the Person access to the Resource.
* `Person <- x <- Organization`
    * If the ID cannot be verified then the Organization does not give the Person access to the Resource. This is how they keep bad actors out.

# Token Authentication

Token-based authentication is largely the same as the ID-based authentication described above. We just have to swap out a few entities in our scenario for it to make sense:

* User ~= Person
* Token ~= ID
* Web Server ~= Organization

![Token Authentication is like ID Authentication](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/token-authentication-explained/token-like-id-authentication.png)

Let's playback the above scenario with Tokens:

* A Web Server wants to make sure only members can access a restricted Resource (like a web page, account details, etc.)
* They decide to use Tokens to identify members so they can give them access

![Token Authentication](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/token-authentication-explained/token-authentication.png)

How Token access works:

* `User -> Information -> Web Server`
    * When a User wants to authenticate with a Web Server they will send over information proving who they are. Commonly this might be their username / password or maybe a pin code you received via text / email.
* `User <- Token <- Web Server`
    * The Web Server will then verify the information and create a Token that will uniquely identify the User. The Web Server will typically create unique features on the Token to help prove the Token came from the Web Server.
    * Commonly this Token will be stored in their Browser somewhere (like LocalStorage, a Cookie) or sometimes in the client code or on device if this is an App of some kind.
* `User -> Token -> Web Server`
    * When the User wants to access a Resource, they'll send their Token along with the request to the Web Server (this is typically taken care of by the browser / app they're using). The Web Server can check the Token to make sure it's real and that the User can have access to the Resource.

Outcomes:

* `User <- Resources <- Web Server`
    * If the Token is verified then the Web Server can send the User the Resource.
* `User <- x <- Web Server`
    * If the Web Server cannot verify the Token is real or that the User has access to the Resource, it'll deny access.

# Next Steps

Hopefully that makes sense! Let me know if anything is still confusing or if you're curious about how other parts of web sites / services work.

I built token-based authentication into my own project [CloudSeed](https://cloudseed.xyz). It provides a lot of the core, boring functionalities required to run a full-featured web app - like users, authentication, and integrations with payment processors. You can get ahold of the full source code and use it as a starting point for your own app at the link ^.