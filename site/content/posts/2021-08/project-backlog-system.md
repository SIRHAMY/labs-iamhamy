---
title: "System: How I run my Project Backlogs with Trello"
date: 2021-08-06T14:22:07Z
tags: [
    'system',
    'projects',
    'backlog'
]
comments: false
---

# Overview

I have lots of ideas for projects, yet I can only build [some of them](/tags/projects) with the limited time, energy, and resources at my disposal.

I use a backlog to help prioritize my project ideas and decide which projects to spend my resources on and which to pass on. In this post, I'll share some principles of a good backlog and how I apply these to track and prioritize many areas of my life.

<iframe width="560" height="315" src="https://www.youtube.com/embed/IzfykUJfs_c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Why Backlog

If you're reading this, you probably already have an idea of why backlogs are important and why you may need one. But I want to rehash this here to make sure we're aligned on my perspective of the problem so we can have a better understanding of the solution and implementation going forward.

To put it simply - humans are incredibly capable but also pretty dumb. To combat this, we build systems to support and empower the former while reducing the frequency and magnitude of the latter.

I think this principle holds for project ideas as well:

* We have a lot of ideas
* But most are bad

We can validate this is true by thinking of the prevalence of failed startups ([90% of startups - investopedia](https://www.investopedia.com/articles/personal-finance/040915/how-many-startups-fail-and-why.asp)) and failed investments ([75% of venture-backed startups](https://scalefinance.com/the-venture-capital-secret-75-of-startups-fail/)), even by incredibly skilled / successful humans. 

We can't predict the future, so we go forward based on imperfect data. This leads to imperfect decisions and thus some suboptimal outcomes.

We can't always ensure we'll have good ideas - we're working off of imperfect data. But we can stack the deck in our favor to limit the amount of investment in bad ideas and increase the likelihood of picking a good idea.

A backlog can:

* Store your ideas
* Prioritize them based on given criteria

We can utilize this to:

* Sort our ideas
* Choose from the best ones

It's not a perfect solution. If all your ideas are bad then even your best ideas are still going to be bad. But everyone has some good ideas given enough thought, time, and iterations so I'd guess this will still work for you.

In the average case, a backlog can help your good ideas bubble to the top and prevent wasted cycles and resources on bad ideas.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">How to generate good ideas:<br>* Generate a bunch of ideas<br>* Pick out the best ones<br>* Improve your generation system<br>* Repeat</p>&mdash; Hamilton Greene (@SIRHAMY) <a href="https://twitter.com/SIRHAMY/status/1404127431812845571?ref_src=twsrc%5Etfw">June 13, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

# Sorting a backlog

The efficiency of a backlog really depends on its implementation. Two key factors are:

* Quality of ideas
* How you grade them

Good ideas going in will lead to good ideas coming out of the backlog. Looking at it another way - if you only have bad ideas, then you will only get bad ideas out of the backlog.

The other component is how you grade them. If your grading criteria doesn't effectively model a good idea, then good ideas won't bubble up from your backlog. A common saying for this phenomenon is: if you measure a fish by its ability to climb a tree, it will always fail. Thus it's important to make sure your grading criteria is actually measuring what you want it to measure.

Now there's no 'right' way to come up with good grading criteria. But I have found a set of heuristics that seems to get me in a pretty good spot over the course of several iterations:

* Think about what makes a good idea in the target domain
* Try those as grading criteria
* Reflect on how the backlog is working
* Refactor it based on those learnings
* Consider breaking your domain into subdomains if ideas need to be graded differently
    * e.g. you may want a 'tree climbers' backlog and a 'water swimmers' backlog to stop grading fish by its ability to climb a tree

# My Backlog

Now let's get into my backlogs and how I use these principles to organize my own project ideas. Hopefully this will give some concrete examples of how you can build your own.

## Backlog Overview

I have multiple backlogs for the different kinds of ideas I'm tracking. In practice, this roughly aligns with the kinds of projects I want to work on which in turn roughly aligns with the domains I organize my life around (see: [My Reflections](https://blog.hamy.xyz/tags/reflections/)).

For this example, we'll focus on three of my backlogs. Each of these have external outputs and should be relatively familiar to most people so you can compare how this system leads to those outputs.

* Business - The businesses I build and publish to [HAMY.LABS](https://lab.hamy.xyz)
* Art - the art I create and share on Instagram - [@hamy.art](https://www.instagram.com/hamy.art/)
* Shares - The content I produce and share around [the Hamniverse](https://hamy.xy)

Each of these backlogs has its own grading criteria based on what I value and what I think is a good signal of potential success in each area. I then sort the lists by this criteria and choose from the best candidates when looking to start another project. 

Obviously these criteria aren't perfect otherwise I would have achieved my goal of becoming Ham King of the Hamniverse already, but they're certainly better than nothing. This is where regular reflection and refactoring can be a huge help for tweaking criteria and surfacing better ideas.

Business:

* Granularity: A business problem to solve
* People affected - how many people have this problem
* Founder Fit - Am I in a good position to solve
* Am I personally affected
* How many people I've verified have this problem - Most problems aren't _real_ problems, so verification is key

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">A key question to ask someone to see if their problem is real:<br><br>&quot;How have you tried to solve it?&quot;<br><br>If the answer is &lt; 1 Google search: This isn&#39;t a problem. (at least not for them)<br><br>-Reading The Mom Test (<a href="https://t.co/9Oi1z74MM3">https://t.co/9Oi1z74MM3</a> - affiliate).</p>&mdash; Hamilton Greene (@SIRHAMY) <a href="https://twitter.com/SIRHAMY/status/1395841989191610368?ref_src=twsrc%5Etfw">May 21, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Art:

* Granularity: An art idea to create
* Purpose - I like all of my art to have purpose, so I penalize those ideas that don't
* Craft - I want to use art to push myself and my abilities
* Elegance - Also I want my art to be aesthetic
* Is it a Monolith - [Monoliths](https://labs.hamy.xyz/tags/monoliths/) are my prism of choice for conveying new artistic ideas, so I want most of my art to be a Monolith

Shares:

* Granularity: A topic to create a post / video / presentation about
* Number of people interested - How many people can I expect will want to know about this topic
* How useful is it - How hard is it to get this information / how hard is it to do x without this information
* Is it relevant to the brand I'm building - Shares are a core part of my creation cycle but I want them to be synergistic with the other things I'm doing. If they aren't, there's probably something more useful to be sharing.

![Art Backlog](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/art-backlog-screenshot.png)

_Art Backlog_

## Backlog Implementation

I use [Trello](https://trello.com/sirhamy/recommend) (referral) to store and organize my backlogs. I like it because it's:

* Simple - Just a bunch of lists and cards
* Flexible - Organize however you want
* Powerful - Tons of addons to automate whatever you're doing

Here's how I implement my backlogs in Trello:

* Each of my idea domains gets its own board - this lets me easily organize ideas of a kind all in one place.
* I add custom fields to each board based on the grading criteria I've chosen for that domain
* I add a new list to each board each month to put new ideas in and to carry over ideas from previous months I want to keep top of mind. This lets me easily add new ideas, not get overwhelmed with all the ideas I've ever had, organize and find older ideas, and always see the best ideas at the top of my lists over time.
* I add a sorting button to sort my lists by the grading criteria, meaning that the best ideas from every month are at the top

With this system, finding a 'good' idea to work on next is as simple as jumping to the appropriate domain idea board and scanning the top of the lists.

# End

So that's how I run my backlogs! I'm always looking for ways to improve so if you have your own backlogging system / any suggestions for improvement on mine I'd love to see them!

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Line from the Zen of Python (<a href="https://t.co/9YJI59wkTf">https://t.co/9YJI59wkTf</a>) I think about often:<br><br>&quot;Now is better than never.<br>Although never is often better than *right* now.&quot;<br><br>Has had a big influence on how I&#39;ve built my productivity systems.<br><br>Capture all ideas. Ruthlessly prioritize which to execute.</p>&mdash; Hamilton Greene (@SIRHAMY) <a href="https://twitter.com/SIRHAMY/status/1396161202795933698?ref_src=twsrc%5Etfw">May 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

In ruthless systematization,

-HAMY.OUT
