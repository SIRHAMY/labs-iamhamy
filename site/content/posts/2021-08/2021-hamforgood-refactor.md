---
title: "2021 HamForGood Refactor"
date: 2021-08-29T23:06:21Z
tags: [
    'hamforgood'
]
comments: false
---

# Overview

* [HamForGood](/hamforgood) is transitioning from a direct giving model to a Fund-based model
* In the Fund-based model, 10% of profits are given to a sustainably-invested Fund. Each year 1% of the assets herein will be distributed to the Causes and Charities supported by HamForGood
* The Fund will determine the allocations to each Cause and Charity

Pros:

* Much simpler model - for me personally and for accounting
* Huge long-term giving potential
* Tax-advantaged, which means more impact for the same amount of dollars
* More people can give / participate, increasing the reach and impact of HamForGood

Cons:

* Requires a bit more setup to run efficiently
* Short-term giving is reduced
* Requires passing to next generation to be effective

# Scenarios

I've put together some scenarios to see how this plays out over time. Ideally, we want to see when HamForGood fund-based works better than direct giving model - the sooner, the better.

I've put together a small script to simulate this, included below if you want to look at it.

* Initial investment: 10000, Dollars added per year: 100
    * 6%: Fund > Direct by year 40, 1389% more given by year 100
    * 5%: Fund > Direct by year 46, 635% more given by year 100
    * 4%: Fund > Direct by year 53, 278% more given by year 100
*  Initial investment: 0, Dollars added per year: 100
    * 6%: Fund > Direct by year 60, 384% more given by year 100
    * 5%: Fund > Direct by year 69, 176% more given by year 100
    * 4%: Fund > Direct by year 80, 65% more given by year 100
* Initial investment: 10000, Dollars added per year: 0
    * 6%: Fund > Direct by year 36, 2390% more given by year 100
    * 5%: Fund > Direct by year 41, 1090% more given by year 100
    * 4%: Fund > Direct by year 47, 490% more given by year 100

So over a sufficient time period, it seems like Fund-based approach should give more than a Direct approach within 80 years, assuming market conditions remain relatively the same. 

For funds that receive more initial investment than ongoing investment, the returns are much higher and faster. For funds that receive more ongoing investment than initial investment, returns are much lower and slower. This makes sense as those with large initial investments have more time for that money to work to produce additional money.

Thus, this is a very long game. But I still think the rewards and additional work are worth the effort.

# Code

Written in Python 3

```
initial_seed_dollars = 10000
additional_dollars_per_year = 0
total_simulation_time_years = 100

expected_investment_return_percent_per_year = 6
amount_to_give_percent_per_year = 1

def print_year_totals(year, total_invested, fund_total, total_given_to_charity):
    print(f"{year} - Total Invested: {total_invested}, Fund Total: {fund_total}, Total donated: {total_given_to_charity}")
    
def percent_to_percent_decimal(percent_value):
    return percent_value / 100.0

# Foreach year, calculate new amounts and output them

fund_total = initial_seed_dollars
donation_total = 0
total_invested = initial_seed_dollars
fund_better_than_direct = False
for i in range(0, total_simulation_time_years):
    donated = fund_total * percent_to_percent_decimal(amount_to_give_percent_per_year)
    fund_total_minus_donation = fund_total - donated
    donation_total = donation_total + donated
    
    interest = fund_total_minus_donation * percent_to_percent_decimal(expected_investment_return_percent_per_year)
    fund_total = fund_total_minus_donation + interest + additional_dollars_per_year
    total_invested = total_invested + additional_dollars_per_year
    
    print_year_totals(i, total_invested, fund_total, donation_total)
    
    # Print out a line for when Fund > Direct for ease of finding
    if donation_total > total_invested and not fund_better_than_direct:
        fund_better_than_direct = True
        print("FUND BETTER LINE")

```
