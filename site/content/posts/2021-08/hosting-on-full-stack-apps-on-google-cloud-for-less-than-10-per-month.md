---
title: "Hosting Full Stack Apps on Google Cloud for less than $10 per month"
date: 2021-08-31T01:24:35Z
tags: [
    'google-cloud',
    'business',
    'technology'
]
comments: false
---

# Overview

In this post I'm going to share how I host my full stack [business apps](/tags/business) for less than $10 / month on Google Cloud. I'll also go into why keeping costs low is so crucial to my business strategy as a solopreneur and why I chose Google Cloud as my host of choice.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wVncTzd2VHY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Keeping Costs Low

I love building [projects](/projects). That's what this site is all about. As part of my project-building practice, I build a diverse portfolio of businesses on my path to sustainable solopreneurship. 

A key part of this strategy is keeping costs low so that I can:

* Iterate on more problems
* Increase my odds of finding a good problem fit
* Keep my profit margins as high as possible

As a solopreneur, I have a very tight resource budget. It's just me with no corporate engine backing me. As such, I have to keep a close eye on my expenditures, particularly wrt:

* Finance
* Effort
* Ongoing maintenance costs

A blown budget in any of these could seriously hamstring my ability to iterate quickly and thus impact my long-term chances of success.

# My Hosting Strategy

Taking this context into account, I basically have three primary values when it comes to picking a good host:

* Full stack functionality - I need a platform that lets me build anything a general application would need: serve to the public, do intense compute operations, and save my state. Generally if it can do a web, app, and db server then we're good.
* Minimal Costs - I need the costs to be reasonable wrt finance, effort, and ongoing maintenance
* Ability to scale - I want to pick a platform that can scale with my needs. I don't ever want to be limited by choices I made in the past.

# My Implementation

I've implemented this strategy by choosing a fully featured cloud provider - Google Cloud. I like it because it fits many of my needs:

* Full stack functionality
* Ability to scale - by providing industry-leading tools and services
* Serverless (at a reasonable price) - brings with it some super powers

One superpower of Google Cloud (that it shares with some of the other big providers, but not many of the littler ones) is serverless. Serverless is pay-as-you-go so you only pay for what you use. For many of my projects, they get minimal traffic so using serverless can minimize my ongoing costs from projects that aren't doing so hot. This saves resources for new projects, without having to kill the old ones right away ([a problem I've faced before](https://labs.hamy.xyz/posts/shutting-down-my-first-startup/)).

Another superpower is that serverless deals with the machines for you. This means less dealing with the command line, installing things, dealing with dependencies, etc. You can set some configs and have your code run and scale without clicking another button.

For these reasons, serverless is a pretty good choice for me as a solopreneur and solo developer.

Now there are some drawbacks to serverless that I see. Namely:

* They can get expensive at scale
* There's vendor lockin - you code directly into the provider's APIs so would need to change that if you ever moved

Now this isn't a huge problem if your usage scales with your customers (hopefully turning a profit) and / or you're okay being with that provider for awhile / paying the cost of leaving. 

For me, I'm not okay with that - I don't want to get bit by my past decisions. So I like to look for a very specific type of serverless that runs containerized applications. This brings with it a few benefits:

* Build the same way anywhere - if it builds locally, it'll build in the cloud
* Portable - no lock in to any one provider

This often costs a bit more than traditional serverless due to container spinup times _but_ can often scale at a much more linear rate and has no vendor lockin. I'm okay paying a bit more on the lowend to have a smoother ramp up and future of development. So I choose technologies like this.

# Hosting on Google Cloud

## Why Google Cloud

Before we get into the specific technologies I use with Google Cloud, I wanted to quickly touch on why Google Cloud vs other big providers. For me, it comes down to a few things I like:

* Full Featured
* Trusted
* Reasonable Prices
* UI is simple
* Docs are straightforward

Many of these traits are shared with other big players, but Google Cloud scored well in all of these for me so I went with it and haven't had any regrets.

![System Design](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/system-design.png)

_System Design_

## System Design

My usual system design is a simple Web, App, DB setup. Web deals with external traffic, App deals with the API layer and app logic, and the DB deals with storage. This setup (or a subset of it) can typically handle most workloads up to 100k customers and commonly up to 1M with a bit of configuration. It's simple. It works. I like it.

## Technologies

For specific technologies, I use:

* Cloud Run - Hosts the Docker containers for Web and App tier. Has autoscaling and will scale to 0 when not in use.
* Cloud SQL - I use a managed Postgres instance, the smallest on offer
* Scale - For scalability, we can add all sorts of addons though I don't usually need these so I won't add them to the tally
    * Additional instances
    * Message Queues
    * Caches

## Cost

So with my typical system design:

* 1x Cloud Run - Web
* 1x Cloud Run - App
* 1x Cloud SQL - DB

It comes out to about $8 / month for most of my projects. A majority of this cost comes from Cloud SQL because it always runs. My Cloud Run instances are almost always covered in the free tier because I don't get much traffic - even across all my hosted projects. [Cloud Run's free tier](https://cloud.google.com/run/pricing) is pretty generous and some [informal studies](https://dev.to/pcraig3/cloud-run-vs-app-engine-a-head-to-head-comparison-using-facts-and-science-1225) that even frequent spin up / spin down of Cloud Run is far cheaper than other similar options.

# Next Steps

Let me know how you host your full stack apps for cheap! I'm always looking for ways to make my apps cheaper and more efficient.

I'm currently working on some resources and project files that make building and launching projects with this setup super streamlined. The goal is to make it easy to launch a business that can scale to 1M users in less than an hour - something I've been struggling with myself for the past few months.

Stay tuned for more on that and happy building!

-HAMY.OUT