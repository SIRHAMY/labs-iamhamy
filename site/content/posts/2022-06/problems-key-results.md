---
title: "Problems + Key Results > Objectives + Key Results"
date: 2022-06-10T16:20:28Z
tags: []
comments: false
---

The more time I spend building products and businesses, the more I come back to the adage "Don't fix what ain't broke". It's a succinct and memorable reminder to focus on the impact of the work, not the work itself.

# The Problem with OKRs

I've done a good amount of research and experimentation into effective direction-setting and impact measurement for teams and orgs as an [engineer at Instagram](https://www.linkedin.com/in/hamiltongreene/) and in my work to build an effective, generalizable [Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle/). OKRs (Objectives + Key Results) were a frequent result / recommendation for doing this effectively at scale. Yet, I found myself continually fighting with the terminology, trying to make it better fit my vision of an effective process.

I liken this struggle to the code refactoring process -> particularly in naming. You will never know if you've gotten it "right" but you will know when you've gotten it wrong - it won't feel like it accurately describes the purpose of the entity.

After many cycles of refactoring OKRs, I think I've finally found the root cause of the issue:

* Objectives are essentially Solutions. 
* Solutions-based thinking often leads to "fixing what ain't broke".
* It focuses on "what" / "how" you can do something rather than "why" you're doing it in the first place
* At scale, this leads to large misalignments between scopes (business > org > team > individual) (similar to [the Telephone Game](https://en.wikipedia.org/wiki/Chinese_whispers) phenomena) 

In my experience, this is one of the leading causes of wasted resources in product lifecycles. Too many companies, teams, and products focus on the "what" and "how" of what they're doing vs the "why" -> often leading to a bunch of work that doesn't really accomplish anything (more specifically it accomplishes _something_ but not necessarily something useful / valuable).

Some poor objectives I've seen recently:

* Write more / better documentation
* Write more / better tests
* Write x lines of code / y PRs

The problem is that it is too far removed from the "why" of what we're doing -> prescribing a "what" / "how" that pretty obviously misaligns with any "why", if you'd just compare the two. _More on this in [Goal Triangulation](https://labs.hamy.xyz/posts/goal-triangulation/)._

Now I'm sure there are some PMs / product people out there saying that this argument is null. Something like "if you use OKRs right, they'll be directly tied to a 'why'". While you may be right, my response is that no robust system relies on what people _should_ do. Rather it works to constrain what people _can_ do to push them into [the pit of success](https://blog.codinghorror.com/falling-into-the-pit-of-success/). As [Murphy's law](https://en.wikipedia.org/wiki/Murphy%27s_law) states - anything that can go wrong, will go wrong.

# Problems + Key Results

Unfortunately, the concept of OKRs lives outside the realm of code so it's not so easy for me to slap on some validations, integration tests, and a new API to prevent the misuse of the OKR framework. So we must go back to the Problem at hand (the Why) and consider ways to solve this.

Problem: People abuse the OKR framework leading to solution-based goaling / direction setting.

To solve this, we want to:

* Prevent people from setting solution-based goals
* Don't add additional complexity / overhead

The solution I came up with: Change OKRs (Objectives + Key Results) -> PKRs (Problems + Key Results).

I like Problems-based direction setting for many reasons:

* Encourages you to focus on real impact / value (has an implicit "why")
* Easier to quantify the impact of a problem (is this worth solving? Is this even a problem?)
* Easier to determine whether you've "solved" it or not
* At scale, can pass a large amount of the top-level "why" through different scopes (business > org > team > ic) with minimal overhead - minimizing misalignment

At the end of the day, if there is no problem then it ain't broke. And if it ain't broke, we don't want / need to fix it.

# Using PKRs

The beauty of PKRs is that we don't have to change much about how we goal and set direction. You only need to use Problems in place of Objectives. 

Just like a good refactor - the system maintains its overall functionality but does it in a more efficient manner. In this case, we narrow the description of the framework so that it's very obvious when you're misusing it.

* OKR: Write more / better documentation
    * PKR
        * This is a solution, not a problem. Why do we want to do that?
        * High customer attrition due to long onboarding times?
        * High developer support hours due to misunderstanding API usage
* OKR: Write more / better tests
    * PKR
        * This is a solution, not a problem. Why do we want to do that?
        * Frequent system breakage due to lack of understanding all edge cases
        * Automated tools flagging code for poor code coverage
* Improvements
    * By centering the problem, we can better determine the importance of this issue - or whether it's even an issue at all. (automated tool flagging your code? May not be a problem!)
    * By directly encoding the "why" we actually have a much better idea of what solutions might help and which might not. Instead of prescribing a solution top-down, we give the ICs all the info they need to make better decisions.

# Further Reading

* [The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle/)
* [Goal Triangulation](https://labs.hamy.xyz/posts/goal-triangulation/)
* [Building a startup is not about building a product, it's about solving a problem](https://www.ycombinator.com/library/5O-building-a-startup-is-not-about-building-a-product-it-s-about-solving-a-problem) - Y Combinator