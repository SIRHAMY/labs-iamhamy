---
title: "Applying The Creation Cycle"
date: 2022-06-29T14:40:01Z
tags: [
    'systems',
    'creation-cycle',
    'product'
]
comments: false
---

The point of Creation is to produce impact. The nature of our world is such that there are many unknowns.

Thus effective Creation:

* Attempts to produce impact
* Improves our understanding of unknowns

Doing so creates a virtuous cycle of:

* Capturing opportunities to produce impact
* Improving future opportunities for producing impact

# The Creation Cycle

The Creation Cycle is a generalized, streamlined, repeatable process for effective Creation. It is essentially the applied scientific method for Creation.

* Each Creation Cycle represents a Minimum Viable Experiment (a better name for Minimum Viable Product).
* The inputs of each cycle are:
    * Mission: What is the point of this Creation Cycle? What impact are you trying to have?
    * Domain: Where are we playing?
* The outputs are:
    * Creations: The artifacts we created in the cycle (an attempt to capture an opportunity / produce impact)
    * Validated Learnings: Improved understanding of unknowns

Together, these outputs result in:

* Creations (with potential impact towards your Mission)
* Improved understanding of relevant unknowns:
    * The world
    * Your Mission
    * Your Creations

Which can be applied to your next Creation Cycle to improve your opportunities for impact. Thus a virtuous cycle of Creation.

![The Applied Creation Cycle](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/the-creation-cycle/applied-creation-cycle.png)

_The Applied Creation Cycle_

# Applying The Creation Cycle

In this section, we'll dive into the 3 stages of The Creation Cycle:

* Observe
* Create
* Reflect

## Observe

* In: 
    * Mission
    * Domain
* Out:
    * Hypotheses

In the Observe stage we're creating explicit hypotheses for the state of the world and how we can impact it. 

* **Understand the Domain** - The first step is making sure we understand the domain. If we don't understand something it's going to be hard to be impactful in it. Also known as Background Research.
* **Mission PKRs** - Before we can accomplish something, we must understand what it is we are trying to accomplish and how we'll know whether we accomplished it or not. For this I recommend using the [PKRs (Problem Key Results)](https://labs.hamy.xyz/posts/problems-key-results/) framework to formalize what success / failure / impact means.
* **Identify Problems** - Based on our understanding of the domain and our Mission PKRs, we are going to identify the biggest problems in the way of us accomplishing / making progress for our Mission. The biggest Problems will become our biggest Opportunities and thus the most valuable things for us to try to solve as part of our pursuit of our Mission.
* **Ideate Solutions** - Now that we have an idea of what we're trying to accomplish and what stands in our way, we want to come up with Solutions to try and solve these. Note that every solution is itself a Hypothesis for how to solve these Problems. We'll make that explicit in the next step.
* **Identify Assumptions -> Hypotheses** - Next we will go through all of our domain understanding, Mission PKRs, Problems, and Solutions to identify all the assumptions we've made in the process -> What must be true in order for this to make sense. We've most likely made several of these and it's important we are explicit about them so we can test them to validate / invalidate our view of the world. We treat these assumptions as Hypotheses.

## Create

* In:
    * Hypotheses
* Out:
    * Creations
    * Data

Now that we have an understanding of what we're trying to accomplish and hypotheses about how the world works and how to impact it, we're going to try to come up with Minimum Viable Experiments (a better term for Minimum Viable Products) to test them. 

* **MVE Ideation** - The first step is to try and figure out how we might test these hypotheses in the form of Minimum Viable Experiments. Remember that these MVEs will double as output Creations.
* **MVE Design** - Next we'll start designing the most promising ideas we've come up with. We want to get our hypotheses answered with the least amount of resources necessary but we also want these MVEs to be reasonable standalone Creations if they succeed. 
* **Prioritized Experimentation** - Finally we'll take our most promising (Hypotheses, MVEs) pairs and put them into action. 

## Reflect

* In:
    * Creations
    * Data
* Out:
    * Validated Learnings

Now that our MVEs have gathered data, we can start to reflect on our process to identify Validated Learnings to pass into the next Cycles. The main things we want to look at are:

* **Data <> Hypotheses** - What does our data say about our Hypotheses? Were we able to prove / disprove any?
* **Creations Learnings** - What did we learn about our MVEs / Creations? Any takeaways?
* **Followups** - What future opportunities did we uncover?

## Share

[Previous iterations of The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle/) have included a Share stage. I think Share is still really important as a means of further validated your learnings and as a way of expanding the value of your Creations. However I don't think it needs to be a set step in every cycle. 

Some cycles will benefit from it, some will benefit from a few cycles without it. Instead, I think it's better off to think of any of the steps' outputs as a shareable artifact. This keeps sharing top of mind but doesn't necessitate a static step for it to be done in.

# References

The Creation Cycle builds on numerous previous systems of thought to provide an effective framework fine-tuned for the application of Creation:

* Validated Learning: The scientific method
* Applied scientific method: [Lean Startup](https://amzn.to/3y2gJc0), Lean Manufacturing
* Virtuous Cycles and Compounding Improvements: 1% improvements ([Atomic Habits](https://blog.hamy.xyz/posts/atomic-habits-your-systems-or-your-life/), compound interest)