---
title: "Managed Postgres Database for less than $10 per month (Google CloudSQL)"
date: 2022-06-13T02:50:20Z
tags: [
    'google-cloud',
    'database',
    'postgres'
]
comments: false
---

Cloud platforms provide loads of tools to build scalable software systems. Unfortunately, many of these tools can cost loads of cash to maintain if you're not careful.

In this post, I'm going to show you how to spin up a managed Postgres database on Google Cloud that costs less than $10 per month.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0OR89wszEWA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Table of Contents

* [CloudSQL](#cloudsql)
* [$10 Postgres Database](#10-postgres-database)
* [Deploy your Database](#deploy-your-database)

## CloudSQL

Managing your own servers can cost you a lot of time and headache. This often offsets any $ savings you have, particularly if you're running a small shop.

When it comes to databases, you don't want to get this tradeoff wrong. Data is the lifeblood of any company so corrupt / lost data can put you out of business.

CloudSQL helps with this by providing an easy way to spin up and maintain databases. It automates most of the maintenance so you never need to ssh into the machine - unless you want to.

## $10 Postgres Database

Playing around with the [Google Cloud pricing page](https://cloud.google.com/products/calculator#id=2d8abbe9-115d-4cbe-97fb-6d00ed8c80c4, you can get a decently powered Postgres sql for < $10 per month.

* SQL Instance Type: Shared Core - DB-F1-MICRO
* Storage: 10 GB (SSD or HDD)
* Backup: 10 GB

![CloudSQL Performance Estimates](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/10usd-cloud-sql-postgres/10usd-cloud-sql-performance-estimates.png)

Scale Estimates:

* Disk Throughput (MB/s):
    * Read: 4.8
    * Write: 4.8
* IOPS:
    * Read: 300
    * Write: 300

While this can't handle huge load, it can scale quite well - definitely enough to handle a hobby project and typically enough to handle most early startup usage. 

## Deploy your Database

Now let's deploy your $10 Managed Postgres Database.

* In your Google Cloud Project, go to `Cloud SQL`
* Click `Create Instance > Choose PostgeSQL`
* Configure your instance
    * Zonal Availability: Single Zone
    * Machine Type: Shared core - 1 vCPU, 0.614GB
    * Storage: SSD - 10GB
    * Backups: Automate Backups (costs an extra ~$2)

Wait a few minutes for the database to spin up and you're good to go!

## Further Reading

* [Hosting Full Stack Apps on Google Cloud for less than $10 per month](https://labs.hamy.xyz/posts/hosting-on-full-stack-apps-on-google-cloud-for-less-than-10-per-month/)
* [The Best Tech Stack for Building Modern SaaS Apps in 2022](https://labs.hamy.xyz/posts/the-best-tech-stack-saas-apps-2022/)