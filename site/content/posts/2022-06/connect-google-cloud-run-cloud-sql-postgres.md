---
title: "Connect Google Cloud Run with CloudSQL Postgres"
date: 2022-06-22T13:18:16Z
tags: [
    'google-cloud',
    'cloud-sql',
    'postgres',
    'database',
    'cloud-run'
]
comments: false
---

So you've got a service up and running on Google Cloud Run and a Postgres database instance in CloudSQL. But how do you actually use that database from Cloud Run?

In this post, I'm going to share step-by-step instructions for configuring your Google Cloud Project and Cloud Run service so it can access CloudSQL.

## Table of Contents

* [Pre-Requisites](#pre-requisites)
* [Configuring Google Cloud](#configuring-google-cloud)
* [Connecting from Cloud Run](#connecting-from-cloud-run)

<iframe width="560" height="315" src="https://www.youtube.com/embed/vMQPrVRBGdQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Pre-requisites

For this post, we're going to assume that you already have:

* Cloud Run service created
* Postgres CloudSQL database instance created

_I have a post going over [how to setup a CloudSQL Postgres database for <$10 per month](https://labs.hamy.xyz/posts/managed-postgres-instance-for-less-than-10-per-month-google-cloud-sql/) if you still need to do that._

## Configuring Google Cloud

The easiest way to connect Cloud Run and CloudSQL is through the CloudSQL Proxy. This requires your CloudSQL instance to be configured with a Public IP but makes it very simple to connect to your instance with minimal extra configuration required.

![Cloud Run <> Cloud SQL Proxy](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/cloud-run-cloud-sql/cloud-run-cloud-sql-proxy.png)
_Cloud Run <> Cloud SQL Proxy_

To allow Cloud Run to connect to CloudSQL via the CloudSQL Proxy:

* Go to `Cloud Run`
* Select the service you want to connect to CloudSQL from
* Click `Edit & Deploy New Revision > Connections`
* Click `Add Connection` under `Cloud SQL Connections`
    * You may need to Enable the `Cloud SQL Admin API`
* Select the CloudSQL instance you want to connect to

Once the new revision is deployed, you should be able to connect to CloudSQL from Cloud Run!

## Connecting from Cloud Run

Now that our permissions / connections are configured, we now need to do the actual connection to CloudSQL from Cloud Run. How you actually create this connection will largely depend on your technologies of choice, but most connections require the same core parameters so we'll list those here.

![Example connection parameters](https://storage.googleapis.com/iamhamy-static/labs/posts/2022/cloud-run-cloud-sql/cloud-sql-proxy-connection-parameters.png)
_Example connection parameters_

* Host: `/cloudsql/CONNECTIONNAME`
    * CONNECTIONNAME - You can get this from your CloudSQL instance page
* Database User: defaults to `postgres`
* Database name: defaults to `postgres`
* Database password: Created when you created your cloudSQL instance

## Further Reading

* [Managed Postgres Database for less than $10 per month (Google CloudSQL)](https://labs.hamy.xyz/posts/managed-postgres-instance-for-less-than-10-per-month-google-cloud-sql/)
* [The Best Tech Stack for Building Modern SaaS Apps in 2022](https://labs.hamy.xyz/posts/the-best-tech-stack-saas-apps-2022/)