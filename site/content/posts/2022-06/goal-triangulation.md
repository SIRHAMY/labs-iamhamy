---
title: "Goal Triangulation"
date: 2022-06-08T13:08:24Z
tags: [
    'systems',
    'product-management',
    'creation-cycle'
]
comments: false
---

Goals are a useful tool for simplifying the tracking / evaluation of impact / progress in a given domain. The problem is that it's easy to set goals that over-simplify and misrepresent the domain, leading to mismatched incentives where progress towards the goal != progress in the domain.

This leads to a sleuth of bad outcomes:

* **Bad business outcomes** - You solved the wrong problem, unlikely it had the intended effect
* **Wasted resources** - HeadCount * Hours * CostPerHour, Opportunity Cost
* **Bad morale** - "I poured my soul into it and it wasn't valuable"
* **Win the battle / lose the war** - We hit our goal but the solution caused a huge increse in cost / dependency on flakey systems / regulatory pressure and we might fail.

## Sources of Goal:Domain Mismatch

In my own experience, I most often see this mismatch in incentives between goal and domain caused by two things:

**Insufficient understanding of the domain / Mission.** If you don't have a good understanding of the the reason _why_ we're doing some work, it's going to be next to impossible for you to effectively track impact in this space.

Examples of this might be:

* Domain: Track impact of individual Software Engineers
    * Bad goal: Write x lines of code
        * Is _any_ line of code a good line of code?
        * How do we even know this helps the business?

* Domain: Make it easier to work in a codebase
    * Bad goal: Write x pieces of documentation
        * Is _any_ documentation good documentation?
        * How do we even know this is helping?
        * What about stale documentation? Surely that's a negative.
        * -> On the surface, it may seem that more documentation == easier to use codebase. But is that true? Without a better understanding of why the codebase is hard to work in, it's impossible to answer this question and thus to set a good goal.
    * Bad goal: Write more unit tests
        * Is _any_ unit test useful? 
        * How do we even know this is helping? Are we sure that unit tests are the most effective thing to spend cycles on?
        * What about poorly written unit tests? 
        * -> Again, on the surface it seems like more unit tests are a good thing, but it doesn't really track well with making a codebase easier to work in and thus may lead to poor outcomes.

**Over-simplifying a domain / Mission.** Many domains / missions are complicated and thus require nuance in correctly tracking. So even if you understand the domain, tracking against an over-simplified goal may lead to negative consequences / outcomes effectively leading to "winning the battle, losing the war".

Examples of this might be:

* Domain: Building a business
    * Bad goal: Build a profitable business this year
        * What if this business requires 7, 12 hour days per week?
        * What if it bankrupts you?
        * -> While building a profitable business seems to be a good goal in this domain, there are many clear "anti-goals" we'd want to avoid before declaring victory.

* Domain: Blocking / Removing Fraudulent Data
    * Bad goal: Remove x amount of fraud data
        * What about false positives (i.e. "real" data)?
        * What does x mean in context? Is it 100% or 0.001%?
        * -> While removing fraudulent data is certainly aligned with what we want to do, there are many negative outcomes that could come as part of this and it's so simplified that it's impossible to know what the real big picture impact on this domain is.

## Setting Better Goals

As I create [The Creation Cycle](https://labs.hamy.xyz/posts/the-creation-cycle/), I've been thinking more and more about how to better measure impact. This is key I think in effectively creating anything.

The point of goals is to simplify our domain so that it's easier to iterate against. But it's clear that over-simplification and misrepresentation are common issues.

The best thing I've come up with thus far to combat this is to:

* Improve understanding of the domain / Mission
* Improve modeling of the domain / Mission via Goal Triangulation

Where a single goal gives a 1-dimensional model of the domain, we can use additional goals to provide additional dimensions. These additional dimensions help further define the domain and thus can lead to higher quality cycles -> assuming they are correctly modeling it.

Currently, I think good triangulation can be achieved through a few buckets of Impact Measures:

* Impact - How are we measuring this problem space? What other things are closerly related / important?
* Success - What does success look like here? How do we measure that?
* Failure - What does failure look like here? How do we measure that?

If we take the "Building a business" domain from above, this may lead to a set of Impact Measures that can be curated into a set of triangulated goals:

* Impact
    * Revenue
    * CostDollars
    * CostHours
* Success
    * Revenue > CostDollars
* Failure
    * Revenue < CostDollars
    * >40 hours / week
    * >$10k spent

# Further Reading

Other takes on this idea:

* [Anti-goals](https://twitter.com/SahilBloom/status/1525466667249180672?s=20&t=eOj7yQXPMmMgF9OM1_ZcBg) - Sahil Bloom
* [Do Things You Can Sustain](https://jamesclear.com/upper-bound) - James Clear