---
title: "Settings to Improve OBS Audio Recording Quality"
date: 2022-02-11T20:27:54Z
tags: [
    'obs',
    'audio',
    'shares'
]
comments: false
---

# Overview

For a long time the audio I was recording with OBS Studio was consistently worse than the audio recorded through other software (like PowerDirector) even though I was using the exact same equipment for each. I was finally able to get my audio quality to satisfactory levels so I'm sharing out the settings I changed to make it that way.

# OBS Settings for Good Quality Audio while Recording

* Go to `Settings`
* Go to `Output` tab
    * Select `Output Mode = Advanced`
    * Under `Audio` tab, set each Track's `Audio Bitrate` to the highest setting
    * Apply the changes

Some notes:

* I do not stream, only record so this may be too heavy for some people's setups
* The audio is still not great compared to other software I've used to record but it's at least good which is satisfactory for me to keep using

For an example of the difference in quality between my two audio recording sources, you can check out the below video.

* When I'm in the frame -> OBS Studio, bad quality
* When I'm out of the frame -> PowerDirector, great quality

<iframe width="560" height="315" src="https://www.youtube.com/embed/VHf6UEqhGhQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>