---
title: "Unity: How to set Android bundle identifier"
date: 2020-05-18T19:12:15-04:00
tags: [
    "unity",
    "android"
]
comments: false
---

# problem

I'm using Unity to build a mobile game and am trying to build it for Android. I keep getting an error about the "bundle identifier" being missing. How do I add the Android bundle identifier?

# solution

To add a bundle identifier for your Android project, do this:

* Go to `File > Build Settings`
* At the bottom left of the `Build Settings` window, click `Player Settings`
* Select the `Player` tab
* Select the Android icon in the player tab
* Expand `Other Settings`
* Find the `Identification` section then fill in the `Package Name` with your bundle identifier