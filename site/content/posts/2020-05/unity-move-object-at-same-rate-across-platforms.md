---
title: "Unity: Move object at same rate across platforms"
date: 2020-05-25T18:26:38-04:00
tags: [
    "unity",
    "framerate",
    "crossplatform"
]
comments: false
---

# problem

I was building a mobile game, coffeedash, and found that when I played my game on an actual mobile device, my character moved a lot slower than it did when I ran the Unity game on my computer.

# solution

The problem was that I was moving my object in the `Update` function. The Unity `Update` function is called once per frame and you can imagine that the frame rate of a mobile device is likely a lot lower than the frame rate of, say, a gaming computer.

To fix this, you can use the `FixedUpdate` function which gets called 50 times / second regardless of what device you're playing on.