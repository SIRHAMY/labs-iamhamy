---
title: "WSL: Can't install `gcc`, can't install `patchelf`"
date: 2020-05-11T23:02:01-04:00
tags: [
    "wsl",
    "gcc",
    "patchelf",
    "brew"
]
comments: false
---

# problem

I just moved back over to Windows and am trying to set up my dev environment on Windows Subsystem for Linux (wsl). In the process of trying to `brew` install something, I ran into the error `gcc must be installed`. So I tried to `brew install gcc` but it threw an error `patchelf must be installed`. Then I tried to `brew install patchelf` but it throws the error `gcc must be installed`! So now I'm confused because I can't have patchelf without gcc and can't have gcc without patchelf.

# solution

There's an instruction in the installation guide for homebrew that mentions running `sudo apt-get install build-essential`. If you run that then you should be able to go ahead and install gcc with homebrew.

Note that if you're having problems running the install command for `build-essential`, you should try refreshing the package repos with `sudo apt-get update`