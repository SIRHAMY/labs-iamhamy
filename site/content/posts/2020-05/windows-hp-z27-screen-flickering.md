---
title: "HP z27: Screen flickering on Windows"
date: 2020-05-04T22:54:41-04:00
tags: [
    "hp",
    "windows",
    "troubleshoot"
]
comments: false
---

# problem

I just bought an HP z27 4k monitor but when I hook it up to my Windows computer, the screen keeps flickering.

# solution

The solution for me was to change the resolution through the NVIDIA control panel.

* Open up NVIDIA Control Panel
* Navigate to `Display > Change Resolution`
* Select your HP z27 display
* In the Resolution picker, choose the "native" resolution which should be 3840 x 2160. Note: you should click on it even if it looks like it's already set to that.
* Once you've selected an option, an `Apply` button will appear. Click on that.

The screens should flash again and then your monitor should be fixed.