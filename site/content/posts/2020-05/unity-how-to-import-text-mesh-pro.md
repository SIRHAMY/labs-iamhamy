---
title: "Unity: How to import Text Mesh Pro into a C# script"
date: 2020-05-13T23:38:33Z
tags: [
    "unity",
    "textmeshpro"
]
comments: false
---

# problem

I'm building my first mobile game - [coffeedash](/projects/coffeedash) - and trying to use TextMeshPro to display some in-game text. However, whenever I declare an object of type TextMeshPro in my script, I'm getting the error:

```
error CS0234: The type or namespace name 'TextMeshPro' does not exist in the namespace 'Unity' (are you missing an assembly reference?)
```

I tried `using Unity.TextMeshPro` but that doesn't seem to work, so wondering what I need to import into my script to get this working.

# solution

To import TextMeshPro, you need to add the using declaration `using TMPro;`.