---
title: "Unity: Character model falls over when animated"
date: 2020-04-13T11:39:54-04:00
tags: [
    "unity",
    "model",
    "3d",
    "troubleshoot",
    "rigidbody"
]
comments: false
---

# problem

I like building [projects](/projects). My newest project is a 3d endless runner game built in Unity. While I was building this I ran into a problem where my character model would fall over like a ragdoll whenever I triggered an animation. 

I have "ground" that consists of objects with mesh colliders and my character model has a capsule collider and rigidbody (for physics animation).

# solution

It turns out that the Rigidbody was causing the character to fall over - likely due to the friction between it and the ground and no constraint to keep it upright.

To keep my character from falling over, I:

* Selected my character model
* Found the `Rigidbody` attached to it
* Expanded the `Constraints` section
* Selected the `Freeze Rotation` checkboxes next to x, y, and z

Now this works for me because my endless runner game doesn't require rotations of any kind. Your program might be different so selecting each axis will tell the rigid body not to rotate in that direction.

Happy coding

-HAMY.OUT