---
title: "Python: How use use lru_cache with staticmethod"
date: 2020-04-27T21:15:13-04:00
tags: [
    "python",
    "lru-cache",
    "staticmethod"
]
comments: false
---

# problem

I'm trying to use `lru_cache` with a `staticmethod` function.

# solution

To use `lru_cache` with `staticmethod`, you just have to make sure that `lru_cache` is directly on top of the callable function.

Example:

```
@staticmethod
@lru_cache(maxsize=8)
async def a_static_method():
    ...
```