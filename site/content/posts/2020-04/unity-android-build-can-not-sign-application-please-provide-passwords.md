---
title: "Unity: Android build - `Unable to sign the application; please provide passwords!`"
date: 2020-04-12T17:01:35Z
tags: [
    "unity",
    "android"
]
comments: false
---

# problem

I'm working on my first mobile game - [coffeedash](/projects/coffeedash) - and trying to build it for Android. I keep running into this error when I try to build the app bundle for the Google Play Store:

```
UnityException: Can not sign the application
Unable to sign the application; please provide passwords!
UnityEngine.GUIUtility:ProcessEvent(Int32, IntPtr, Boolean&) (at /home/builduser/buildslave/unity/build/Modules/IMGUI/GUIUtility.cs:189)
```

What's going on and how can I solve this?

# solution

In order to build for the Play store you need a signing key which is stored in a file called a `keystore`. This signing key is what allows the Play store and other consumers to verify that the app was indeed made / modified by you which helps protect your IP from being stolen as well as consumers from downloading malicious apps masquerading as your own.

If you're getting this error then that likely means you've already created and added a Keystore to your project (otherwise it would just tell you you need a key). This means all we're missing is the password to unlock that keystore so Unity can use it to sign the app.

To do this:

* Go to `File > Build Settings > Player Settings` - this will put you out on the `Player` tab of `Project Settings`
* Select the `Android` tab
* Navigate to the `Publishing Settings` section
* Fill in the `Password` text inputs for the `Project Keystore` and `Project Key`. You would've created these passwords when you created the keystore.

That's it. Happy coding!

-HAMY.OUT