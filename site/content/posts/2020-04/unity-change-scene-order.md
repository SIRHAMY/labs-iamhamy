---
title: "Unity: Change scene order"
date: 2020-04-29T16:46:58Z
tags: [
    "unity",
    "scenes"
]
comments: false
---

# problem

I'm building my first mobile game with Unity - [coffeedash](/projects/coffeedash) - but when I build my app, it's starting with the wrong scene. How can I tell Unity to play a different scene first?

# solution

The UI for scene ordering / selection isn't great so this is a common question / issue. To change the ordering of scenes in your project:

* Go to `File > Build Settings`
* Find the `Scenes in Build` section - this will contain all of the scenes in your project that have been added to the build.
* Next to each listed scene, you should see a number which indicates its build index which is the order in which the scene will appear in the build. Note that after 0 (the first scene), this doesn't really matter as you'll likely have a controller script in your game that handles scene switching for the lifespan of the process.
* Find the scene you want to move and drag and drop it to the desired index.
* profit

Happy coding!

-HAMY.OUT