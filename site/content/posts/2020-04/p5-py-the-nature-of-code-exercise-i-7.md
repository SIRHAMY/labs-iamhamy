---
title: "P5 Python: The Nature of Code exercise i.7"
date: 2020-04-11T00:24:38-04:00
tags: [
    "p5",
    "python",
    "natureofcode"
]
comments: false
---

I'm working my way through [The Nature of Code](https://amzn.to/3a3OVGa) using Python's p5 package and posting my creations as I go. Exercise i.7's prompt was to use Perlin Noise to modify the walker's step offsets. I decided to do something a little different, preferring to draw horizontal lines made of "noise". Here's what I came up with:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yLE0N4p0eu4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

And here's the code I used to make it happen:

```
# i.7.py
# prompt - blah blah do something with perlin noise

from p5 import *
from random import randint
from typing import Optional
from utils.walker import Walker

should_start: bool = False

location_x: int = 0
y_increment: int = 100
y_base: float = y_increment

def setup():
    size(640, 640)
    stroke(Color("#be403b"))
    background(204)


def draw():
    global should_start, location_x, y_value, octaves, y_increment, y_base

    if should_start:
        # code here
        value_2 = noise(location_x*10) * 10
        y_value = y_base + value_2
        
        circle((location_x, y_value), 1)

        location_x = location_x + 1
        if location_x >= 640:
            location_x = 0
            y_base = y_base + y_increment

        if y_value > 640:
            y_value = 640

        if y_value < 0:
            y_value = 0


def key_pressed():
    global should_start

    if key == " ":
        should_start = True

def main():
    run()

main()
```

```
# walker.py
from p5 import * 

class Walker:
    x: float
    y: float

    def __init__(self, x: float = 0, y: float = 0) -> None:
        self.x = x
        self.y = y

    def move(self, x_offset: float, y_offset: float) -> None:
        self.x += x_offset
        self.y += y_offset

    def display(self) -> None:
        circle((self.x, self.y), 1)
```

As you can see, I basically just draw a line across the sketch and each time it hits the other side, I reset the x value and increment the y value by a predetermined offset. I use noise to create some jitter which I use to draw a "noisey" line.