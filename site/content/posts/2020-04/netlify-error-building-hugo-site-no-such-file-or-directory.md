---
title: "Netlify: Error building Hugo site - `Error: Error building site: open /opt/build/repo/content: no such file or directory`"
date: 2020-04-06T22:15:26-04:00
tags: [
    "netlify",
    "hugo",
    "error",
    "deploy"
]
comments: false
---

# problem

I was trying to set up a new Hugo site to host my new project [coronation](/projects/coronation) but when I attempted to build and deploy it on Netlify, I kept getting the error `Error: Error building site: open /opt/build/repo/content: no such file or directory`. Here's the full stack trace:

```
7:01:32 PM: Build ready to start
7:03:42 PM: build-image version: 6b66e79d54a8f76b01392124644c3052ac0d32ef
7:03:42 PM: build-image tag: v3.3.6
7:03:42 PM: buildbot version: 2ba6f2db8150441c8469e6ae3ab3561a2c9d03fa
7:03:42 PM: Fetching cached dependencies
7:03:42 PM: Failed to fetch cache, continuing with build
7:03:42 PM: Starting to prepare the repo for build
7:03:43 PM: No cached dependencies found. Cloning fresh repo
7:03:43 PM: git clone git@gitlab.com:SIRHAMY/coronation
7:03:44 PM: Preparing Git Reference refs/heads/master
7:03:45 PM: Starting build script
7:03:45 PM: Installing dependencies
7:03:47 PM: v10.19.0 is already installed.
7:03:48 PM: Now using node v10.19.0 (npm v6.13.4)
7:03:49 PM: Attempting ruby version 2.6.2, read from environment
7:03:50 PM: Using ruby version 2.6.2
7:03:51 PM: Using PHP version 5.6
7:03:51 PM: Started restoring cached go cache
7:03:51 PM: Finished restoring cached go cache
7:03:51 PM: unset GOOS;
7:03:51 PM: unset GOARCH;
7:03:51 PM: export GOROOT='/opt/buildhome/.gimme/versions/go1.12.linux.amd64';
7:03:51 PM: export PATH="/opt/buildhome/.gimme/versions/go1.12.linux.amd64/bin:${PATH}";
7:03:51 PM: go version >&2;
7:03:51 PM: export GIMME_ENV='/opt/buildhome/.gimme/env/go1.12.linux.amd64.env';
7:03:51 PM: go version go1.12 linux/amd64
7:03:51 PM: Installing missing commands
7:03:51 PM: Verify run directory
7:03:51 PM: Executing user command: rm -rf ./data && hugo
7:03:51 PM: Building sites …
7:03:51 PM: Total in 22 ms
7:03:51 PM: Error: Error building site: open /opt/build/repo/content: no such file or directory
7:03:51 PM: Skipping functions preparation step: no functions directory set
7:03:51 PM: Caching artifacts
7:03:51 PM: Started saving pip cache
7:03:52 PM: Finished saving pip cache
7:03:52 PM: Started saving emacs cask dependencies
7:03:52 PM: Finished saving emacs cask dependencies
7:03:52 PM: Started saving maven dependencies
7:03:52 PM: Finished saving maven dependencies
7:03:52 PM: Started saving boot dependencies
7:03:52 PM: Finished saving boot dependencies
7:03:52 PM: Started saving go dependencies
7:03:52 PM: Finished saving go dependencies
7:03:58 PM: Error running command: Build script returned non-zero exit code: 255
7:03:58 PM: failed during stage 'building site': Build script returned non-zero exit code: 255
7:03:58 PM: Failing build: Failed to build site
7:03:58 PM: Finished processing build request in 15.923533775s
```

# solution

What I found was that this can happen if you don't have any contents inside of your `content` directory. For those new to Hugo, this is where all your actual post content lives, so it's reasonable for Hugo to expect that this directory at least exists. When you go through the build phase of Netlify, it's using git to pull down your files to build. With git, if there's an empty directory it will often skip pulling it down, thus the issue.

To fix it, simply add a file into your Hugo site's `content` directory. This could be real content or even just a simple `placeholder.txt`. Anything would work in this case.

Happy coding!