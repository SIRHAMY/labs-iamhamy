---
title: "CSharp: `error CS0103: The name 'DateTimeOffset' does not exist in the current context`"
date: 2020-04-20T12:07:52-04:00
tags: [
    "csharp",
    "error",
    "datetimeoffset"
]
comments: false
---

# problem

I'm building a project in C# and trying to use the DateTimeOffset class. However, I'm getting the error `CS0103: The name 'DateTimeOffset' does not exist in the current context`. How do I fix this?

# solution

Errors like this usually signal that you're not importing a required library that defines the object / reference. In this case, the `DateTimeOffset` class is defined in the `System` library, so to fix this you can simply add an import at the top of your file:

```
using System;
```
