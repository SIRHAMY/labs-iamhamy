---
title: "Ubuntu: Mount SSD to home directory"
date: 2020-11-22T18:12:58Z
tags: [
    'ubuntu',
    'ssd'
]
comments: false
---

# Overview

I have a new Ubuntu install on my Lenovo P720. I want to mount the SSD I have to the home directory so I can save programs and files on it that I want to benefit from the faster read and write speeds.

# Solution

We can't directly mount a drive inside another drive. But what we can do is mount our drive and link to it from inside another drive. This allows us to interact with it as if it was a drive inside another drive.

To mount an SSD:

* Open `Disk Utility` program
* Find the SSD you want to mount
* Create or locate a partition that is mountable and selet it
* Click the play button to mount
* Note the mount location that should now be displayed on the screen

To link a drive to another drive.

* Open a terminal
* Run `ln -s MOUNT_LOCATION LINK_LOCATION`

For example, in my case I wanted my SSD to be linked to from my home directory like `~/SSDDrive`. My drive was mounted at `/media/hamy/SSDDrive`. So to create a link such that my SSD was accessible from my home directory, I ran:

`ln -s /media/hamy/SSDDrive ~/SSDDrive`