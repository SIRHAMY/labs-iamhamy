---
title: "16 sites I used for inspiration in my portfolio redesign"
date: 2020-11-12T04:26:06Z
tags: [
    'iamhamy',
    'portfolio',
    'design'
]
comments: false
---

# Overview

I recently [redesigned my websites](/projects/iamhamy-3/) to make them more efficient, sustainable, and accessbile than ever before. Redesigning a website (in my case 3) takes a lot of work so I wanted to make sure that the design I came up with would serve me today and into the future.

To do that, I gathered inspiration from sites around the web that I liked and compiled a [Pinterest board](https://www.pinterest.com/hamylabs/iamhamy-30/) to help me organize my thoughts. Here I'll share those sites along with why I wanted to use it as inspiration for my own sites.

# Inspiration

## Medium

[medium.com](https://medium.com/better-programming/are-you-happy-with-your-remote-stand-ups-1163cff73e46)

Medium has always had a beautiful reading and publishing experience. Their new design puts a spin on their traditional sleak and minimal styles, making more elmements 'pop' with greater contrast between their neighbors. 

I wanted my sites to be accessible first and I thought Medium did a good job of bridging accessibility with aesthetics which is why I added them to this list.

## The New York Times

[nytimes.com](https://www.nytimes.com/2020/10/16/us/trump-taxes.html?action=click&module=Top+Stories&pgtype=Homepage)

Similar to Medium, I'm an avid reader of The New York Times. They have a very to-the-point manner of writing and I think that mentality is also displayed in their web design. It's simple with good accessibility and shows you as much as it can without feeling over-cluttered.

I like to write so this idea of keeping it minimal while still showing people as much as you could appealed to me - if it can work for the New York Times' readership, it could likely work for me.

## Herokidstudio

[herokidstudio.es](http://herokidstudio.es)

Herokidstudio has a beautiful site as one might expect from a design studio. But what really caught my eye about this site (and something I've been meaning to implement in my new site) is the clean minimal design with a focus on a creative coding element. [I'm a Creative Technologist](https://hamy.xyz/about/) and when I saw this page I immediately felt a connection with it - like "that's what my landing page needs to look like.", and [it kind of does](https://hamy.xyz/) (sans creative technology)

I really liked this landing page because it's straight to the point with it's messaging, giving the user the most important info first but then it has this element that draws your attention and urges you to linger - I think that's part of building a memorable experience and getting return visitors. Plus, for me, it could double as an advertisement for my skills and maybe even lead to some collabs down the line.

## K2

https://www.k2.pl/en

Another digital agency with a beautiful site. I love the simplicity and bold messaging. It's just what you need to know and nothing more.

I really liked the post lists available on the site too, I thought it was clean while allowing enough list elements to not feel sparse.

## itsnickki

https://itsnickki.com/

I liked this site because of its clean design but I saved it because of its project list. I liked how simple it was, how easy it was to understand what each was, and how it didn't feel overcrowded.

It's [something that I didn't quite get right in this redesign](/projects) but I'll keep tweaking it.

## Allen Tan

https://www.tanmade.com/

Similar to Herokidstudio, this site features an eye-catching, mouse-drawing example of creative coding. Moreover, I think it does a great job of pushing the important stuff front and center and moving everything else to the fringes.

The [work section](https://www.tanmade.com/work/) of this site also made the inspo board due to its excellent image-heavy project portfolio.

## Devon Stank

https://www.devonstank.com/squarespace-pricing-tables

This inspo may seem a little odd as it's more about the method than the actual example. Here the author is demoing their product directly in their writing as opposed to writing about it. I think there's room for more showing and less telling so I included that here as a reminder.

## Angela Nguyen M

https://angelanm.com/

This site is bold and up front in its messaging and in pushing you to look at projects. I build a lot of projects - it's a big passion of mine - so putting them front and center has always been on my mind. This site does this extremely well and even adds in a bit of creative coding for flair.

## Steph Parrot

https://www.stephparrott.com/plantd/

One thing I really love about this site is its flow - it feels like I could just keep scrolling. In particular, when you reach the end of the page there's a section that recommends featured projects to keep reading into. I think it's brilliant and could definitely get a few more projects in front of people. 

Another thing I like about the site is the [landing page](https://www.stephparrott.com/) and its inclusion of links directly in the primary text. To me it makes it feel a lot less formal and more human.

## Brittany Chiang

https://brittanychiang.com/

There are a lot of things I like about this site but for me the coolest part - and what landed it on the list - was the live GitHub data being displayed in a widget at the very bottom. I like building projects on real data - I find it super satisfying - so this was a rmeminder that maybe I should do that too.

## Qode Interactive

https://qodeinteractive.com/catalog/

There are two things I really like about this site. The first is the minimal and bold text lists for their projects. The second is the full page dropdown menu as it gives you more screen real estate to add calls to action like subscribing to your newsletter and learning more about you, all while being cool enough not to feel like you're being advertised to.

## wearebackstage

https://wearebackstage.com/#

Similarly, this site has a nifty full-page expanding menu.

## We make.

https://wemake.be/en/cases/

I really, really like the way they made the asymmetric projects lists look great. I don't really know why it looks great because it certainly doesn't align but it does. I should proabably take some pointers from them on my own projects lists.

Another thing I liked about this site was its simple, bold approach to messaging and the color-blocked calls to action at the end of the page. (see https://wemake.be/en/)

## Tobias Ahlin

https://tobiasahlin.com/

This is an all-around cool site but I think the standout feature is the color-blocked, interactive link grids. They're beautiful and they're so fun to play with you'll probably end up clicking one.

Another thing I like is the huge call to action available at the end of each page.

## Patrick David

https://bepatrickdavid.com/

It's an all-around beautiful site

## Wokine

http://wokine.com/

This site gets me from 'Hello.'. I really want to incorporate more of these bold, animated elements in my sites.

# Wrapping up

So those were all the sites I used as inspiration in [my latest site redesign](https://labs.hamy.xyz/projects/iamhamy-3/). If you liked any of them, let me know and maybe even drop the creator a compliment.

Hopefully this list gave you some good inspiration to use in your own redesign. I know looking back at these got me inspired to make my site even better.

Peace, love, code.

-HAMY.OUT