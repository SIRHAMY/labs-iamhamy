---
title: "Dual booting Ubuntu and Windows 10 on my Lenovo Thinkstation"
date: 2020-11-21T21:02:26Z
tags: [
    'ubuntu',
    'windows',
    'lenovo'
]
comments: false
---

I've spent the better part of a day installing Ubuntu on my new Lenovo Thinkstation. It took me several hours, several tries, and numerous Google searches to get right. But here I am with a working dual-booted ThinkStation. This post documents my learnings in an effort to make this process efficient and repeatable.

Before we begin I want to make it clear that I am by no means an expert. I have run Ubuntu as my primary OS for several years and have loaded it onto ~6 computers in that time so I have experience but all I can share is my experience. So keep that in mind as you go through the rest of this post and take each step at your own discretion.

# My Setup

First let's talk about my setup. The general gist of dual booting Ubuntu will be the same regardless of your computer but there will be a few things I go into in this guide that are specific to my setup. I will explicitly call out why I'm taking each step and mention where other, more common, setups may take different routes.

* Make: Lenovo
* Model: ThinkStation P720
* GPU: Nvidia GTX 4000
* RAM: 32 GB
* Drives:
    * 1 TB SSD
    * 6 TB HDD
* OS: Factory-configured Windows 10
* Monitors: 2 external hooked up with DisplayPort - HDMI

Don't worry about memorizing this or anything, I'll be calling out specifics in each step. This is mostly for posterity so I can reference it in the future.

# Dual booting Ubuntu

Onto the dual booting. Essentially a dual boot means that you have two full operating systems coexisting in the same machine. This is essentially done by making space for the second operating system to store things in your drives (SSD / HDD) and then configuring your BIOS (kinda like the entrypoint to the computer) so it knows that another operating system is in there and can be run.

To do this, we have a few steps we need to take.

* Create a bootable Ubuntu
* Make room for Ubuntu
* Install Ubuntu

Let's get dual booting!

# Create a bootable Ubuntu

The first thing we need to do is download the version of Ubuntu you are trying to install. We can get this directly from the [Ubuntu website](https://ubuntu.com/download/desktop). I recommend grabbing an LTS (Long Term Support) version as these will be the most stable but you can choose another if you wish. At time of writing this was Ubuntu 20.04 LTS.

Once we have it, we are going to use a special tool to turn that download into a bootable usb. All this means is that it's going to reformat the usb and the files that you downloaded so that you can run it from the USB. Yes - you heard that right - we're basically going to make a runnable Ubuntu from your USB!

If you have an Ubuntu OS at your disposal I recommend using the pre-loaded Startup Disk Creator program for this job. If you're running Windows then I recommend Rufus.

I have a full guide for each of these here:

* [Create a bootable Ubuntu USB from Ubuntu](/posts/create-a-bootable-ubuntu-usb-from-ubuntu)
* [Create a bootable Ubuntu USB from Windows 10](/posts/create-a-bootable-ubuntu-usb-from-windows)

Go create a bootable Ubuntu USB then we can continue on to the next step.

# Make room for Ubuntu

Now we have a bootable USB which will allow us to install Ubuntu and the next step is to create some space for Ubuntu to live in. Many setups will be okay skipping this step and instead doing it as part of the Ubuntu install wizard. However I recommend at least understanding why you might take this step and then choosing for yourself.

I like to make room for Ubuntu before going through the install because it gives me more control over what space is being allocated and it gives me more security that I'm not accidentally overwriting important files for my existing Windows install. 

The way I think about it is that Windows understands which files are important to it but Ubuntu can't really know that as it's not the owning OS. So I prefer to make space from Windows to ensure I'm only freeing up space that Windows doesn't need.

To create partitions:

* Ubuntu - use the built-in Disks utility
* Windows - use the Control Panel's partition manager (if you search 'partition' in your Start Menu it should come up)

We're going to free up space in each of the drives you want Ubuntu to use. It's important to make sure that you aren't creating new partitions where there's already data as this could lead to data loss. If there's already data, one way is to modify the partition and shrink it so that it can still hold its data.

A few things to keep in mind:

* Ubuntu OS needs at least 20GB of space to function optimally - this is where all of your OS functions will be taking place
* Swap needs to be at least 2x you RAM to function optimally - this is like short-term memory for your computer and will be written / read from a lot
* Home is where all of your files and stuff go so make it as big as necessary

In my case, I'm trying to dual-boot Windows and Ubuntu. I main Ubuntu but I still want my computer to be a great Windows comp as well. So I freed up ~500 GB of my SSD and ~3TB of my HDD for Ubuntu use.

# Install Ubuntu

Now we have a bootable USB and room in our drives for Ubuntu to live. All that's left is to install Ubuntu in the space we've made for it.

I've written a post to cover the basics and best practices of installing Ubuntu. Before I link you over there, there's a few things I want to call out about our dual-booting situation.

During installation there will be a step for you to choose your 'Installation Type'. There will be three options:

1. Install Ubuntu alongside another Operating System
2. Erase disk and install Ubuntu
3. Something else

Because we're dual booting (rather than overwriting everything to make Ubuntu our one and only OS), we do _not_ want to go with 2. In most cases 1 will work fine for most people. 3 allows a lot more control but you need to make sure you set everything correctly otherwise you may run into issues.

In my case I went with 3. This is because I wanted to do something that diverged from the standard installation path. For me, I wanted to make sure that my Ubuntu OS lived on my SSD so it could be as fast as possible. Everything else - my swap and home partitions - I wanted to live in the HDD.

The main thing to call out is that I used the free space I made available earlier to install Ubuntu.

Follow my guide to complete your installation: [Install Ubuntu from a Live USB](/posts/install-ubuntu-from-a-live-usb)

These were my settings for 3:

* root - Creating a 40 GB partition in my SSD as ext4 and mounting to '/'. Mounting at '/' marks this partition as the 'root' partition and tells Ubuntu that this is where the OS will live. Note that root really only needs to be ~20 GB in most cases but I'm pretty memory rich with my drives so I made it bigger in the hopes I wouldn't have to revisit this anytime soon.
* swap - Creating a 128 GB partition in my HDD and setting the type to 'swap partition'. By setting the type to swap partition, it tells Ubuntu that it can use this for its swap operations - kinda like its short-term memory / notes. This gets written to quite a lot and SSD's are typically better for read-most rather than write-most so I chose to put this in my HDD. Moreover the general rule of thumb is to make this ~2x bigger than your system's RAM but I decided to make it 4x bigger because my system has plenty of memory to spare and I wanted to be sure I wouldn't have to change this soon.
* home - Finally I created a home partition where Ubuntu will store all of my files. I created this with the remaining free memory in my HDD (~3 TB) and mounted it to '/home'. Doing this tells Ubuntu that it can store all of the user-level files in there.

# Fin

And there you have it - an efficient, hopefully-repeatable process for dual booting Ubuntu with Windows.

Ubuntu.

-HAMY.OUT
