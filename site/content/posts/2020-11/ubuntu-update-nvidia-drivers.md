---
title: "Ubuntu: Update Nvidia Drivers"
date: 2020-11-22T17:44:49Z
tags: [
    'ubuntu',
    'nvidia',
    'troubleshoot'
]
comments: false
---

# Overview

When I have problems with my monitors / graphics on Ubuntu, one of the first things I look at is whether Nvidia has new driver updates. This fixes the problem for me most of the time.

Here I'll explain how I update my Nvidia drivers on Ubuntu.

There are two ways to do this and I would recommend doing them in this order:

* Manual Nvidia driver updates
* GUI Nvidia driver updates

We'll first start with the manual driver updates as this will allow us to clear out any of the gunk libs that may be causing compatibility problems. If this doesn't work, we'll fall back to the GUI version. The GUI version is flakey if there are gunk libs but now that we've manually cleared them out it will likely work for us.

## Manual Nvidia driver updates

Here we're going to clear out all of our existing Nvidia drivers and install new ones.

* Open up a terminal
* Run `sudo apt-get update`
* Run `sudo apt-get upgrade`
* Run `sudo apt-get remove --purge nvidia-* -y`
* Run `sudo ubuntu-drivers install`
* Reboot your computer

First we fetch all package updates and upgrade all of our packages. This is always a good first step to do to better ensure we're not breaking compatibility with anything.

Next we remove all of our existing nvidia packages. This removes the 'gunk libs' I talked about earlier.

Then we use `ubuntu-drivers` to find the recommended packages for our system configuration and install them.

Reboot your computer so that these changes can take effect and see if your external monitor is working. If not, we'll move to the next step.

## GUI Nvidia driver updates

We don't really want to have to do these updates through the GUI. It's a bit flakey. But if you've done the manual update already and that didn't work then the GUI actually has a high success rate. If you didn't do the manual update please do that first as the GUI will sometimes get confused if there are 'gunk libs' laying around.

* Open the `Software & Updates` program on Ubuntu
* Click the `Additional Drivers` tab and wait for it to load
* Choose another Nvidia driver version and hit `Apply Changes`. Note - you should take this step _even if you're already on the version that you want!_
* Wait until it says it's done updating. This may take several minutes. If you find it's taking way longer (like you've been waiting an hour), it's likely that the GUI has already finished and is just hanging. In my experience it's safe to kill the GUI and reopen and the driver will be set correctly.
* Reboot your system.
* Now do the whole process over but this time choose the Nvidia driver version that you actually want (likely the latest one)
* Reboot system

After doing this, you should have up-to-date drivers and, hopefullly, all of your graphics-related woes will be solved for.