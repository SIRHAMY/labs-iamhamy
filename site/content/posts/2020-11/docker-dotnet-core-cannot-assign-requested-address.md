---
title: "Docker: dotnet core - Cannot assign requested address'"
date: 2020-11-26T19:22:47Z
tags: [
    'docker',
    'dotnet-core',
    'port'
]
comments: false
---

# Overview

I'm running a dotnet core dockerized app and trying to change its default port to `http://localhost:8080` via the `appsettings.json` `Urls` field. However, I keep getting this error when running the container:

`Unable to bind to http://localhost:8080 on the IPv6 loopback interface: 'Cannot assign requested address'.`

How can I fix this?

# Solution

The solution seems to be to change `localhost` to `host.docker.internal`. I don't really know why this is but that worked for me.