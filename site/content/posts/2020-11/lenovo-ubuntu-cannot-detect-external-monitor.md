---
title: "Lenovo: Running Ubuntu and can't detect external monitor"
date: 2020-11-02T02:05:20Z
tags: [
    'lenovo',
    'ubuntu'
]
comments: false
---

# problem

I have a Lenovo P1 Gen 2 and have been running Ubuntu happily for the last several months. Recently I updated a bunch of libraries and some firmware and my laptop would no longer connect to my external monitors. I ran `xrandr` in my terminal and even found that, when connected, Ubuntu didn't register the external monitors.

What's going on and how can I get Ubuntu to register the external monitors?

# solution

The way I fixed this was to turn off Secure Boot. For whatever reason, Secure Boot was preventing my laptop from detecting the external monitors and then from showing anything on them.

I did this by:

* restart my computer
* press Enter to view more setup options when the Lenovo logo appears
* Select the Bios Setup Utility
* Search through the menus for `Secure Boot`
* Set `Secure Boot` to `Disabled`
* Saving my changes

After saving my changes and restarting my computer, it was working with the external monitors!
