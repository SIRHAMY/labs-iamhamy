---
title: "Create a bootable Ubuntu Live USB from Windows"
date: 2020-11-22T14:18:20Z
tags: [
    'ubuntu',
    'usb',
    'windows'
]
comments: false
---

# Overview

I've recently completed my latest [dual boot of Ubuntu with Windows 10](/posts/dual-boot-ubuntu-with-windows-10-lenovo-thinkstation) and wanted to document my process. In this post I'll walk through the steps I recommend for creating a bootable Ubuntu Live USB from Windows.

# Create a bootable Ubuntu Live USB

There are a few steps for creating a bootable Live USB:

1. Download an Ubuntu ISO
2. Create a bootable Ubuntu USB

## 1. Download an Ubuntu ISO

The first thing we need to do is download a version of Ubuntu that we want to use. This will be the version of Ubuntu that you're loading from your USB so be sure you pick accordingly.

I recommend choosing an LTS (Long Term Support) version as it's guaranteed to have recent updates and long term maintenance. You can find available Ubuntu ISOs on [Ubuntu's website](https://ubuntu.com/download/desktop).

Go download the one you want and come back here.

## 2. Create a bootable Ubuntu USB

Now that we have the Ubuntu version we want to use, we need to create a bootable USB from it. I suggest using a USB that is either empty or that you don't mind losing data on.

For this step we'll use Rufus - a lightweight tool built just for creating bootable USBs.

* Download [Rufus](https://rufus.ie/)
* Run Rufus
* Click the 'SELECT' button under `Boot selection` and then point it to the Ubuntu ISO you downloaded earlier
* Click 'START'
* Wait for the USB to be written to (this may take a few minutes)
* Eject the USB

There are a few options in Rufus that you may want to modify like the `Partition scheme`, `Target system`, and `File system`. In most cases the defaults will work fine and are what you want but if you have a non-standard use case this is where you'll make those configuration changes.

# Fin

That's it, you should have a bootable Ubuntu usb now. You can run Ubuntu directly from the USB and even use it to [install Ubuntu](/posts/install-ubuntu-from-a-live-usb) directly on your machine.