---
title: "Install Ubuntu from a Live USB"
date: 2020-11-22T15:18:07Z
tags: []
comments: false
---

# Overview

I've just finished my latest install of Ubuntu and wanted to document the process for future reference and efficiency. In this post I'll share the steps and best practices I take to create successful Ubuntu installations.

I have a separate guide for [dual booting Ubuntu with Windows](/posts/dual-boot-ubuntu-with-windows-10-lenovo-thinkstation) which has a few pointers on where that process differs from this process. If you're dual booting, give that a read then come back here.

# Installing Ubuntu

There are several steps to creating a successful Ubuntu install:

1. Create an Ubuntu Live USB
2. Configure computer BIOS 
3. Boot into Ubuntu Live USB
4. Install Ubuntu
5. Update Ubuntu

I'll go through each step here, the decisions you need to make, and some context to help you make those decisions.

Before we jump into the guide I quickly wanted to share the setup that I installed Ubuntu on. I'll be using this as an example throughout the guide to better illustrate the choices I made and why.

* Make: Lenovo
* Model: ThinkStation P720
* GPU: Nvidia GTX 4000
* RAM: 32 GB
* Drives:
    * 1 TB SSD
    * 6 TB HDD
* OS: Factory-configured Windows 10
* Monitors: 2 external hooked up with DisplayPort - HDMI

Now let's get installing.

## Create an Ubuntu Live USB

The first thing we need to do is create an Ubuntu Live USB. This is basically a USB that has a full copy of the Ubuntu OS on it. We'll be using this USB to run Ubuntu and to install Ubuntu on your machine.

I have guides for how to do this from both Ubuntu and Windows. If you're on a Mac and would like a guide, let me know in the comments below and I'll create one.

* [Create a bootable Ubuntu USB from Ubuntu](/posts/create-a-bootable-ubuntu-usb-from-ubuntu)
* [Create a bootable Ubuntu USB from Windows](/posts/create-a-bootable-ubuntu-usb-from-windows)

Go create a bootable Ubuntu Live USB if you haven't already then we can continue.

## Configure Computer BIOS

Before we can install Ubuntu, there's a few settings we need to set on our machine. These will allow us to boot into Ubuntu and run Ubuntu without a problem.

First we need to get into the BIOS settings:

* Power off the machine
* Start the machine
* Enter the BIOS menu
* Select 'Settings'

The exact steps for how to 'Enter the BIOS menu' differ from machine to machine. It's best to look this up specifically for your machine if you're having trouble. In my case, I'm using a Lenovo machine so to enter BIOS I need to press `Enter` or `F12` repeatedly as soon as the Lenovo logo shows up during startup.

Once in settings you will need to search through the options for the correct fields and set them accordingly. Many BIOS are different so there's not a great way to guide you here other than to tell you what needs to be changed.

* SATA = AHCI - This sets how you want the OS and machine to talk to the drives. For Ubuntu, this needs to be set to AHCI
* Secure Boot = Disabled - This limits what can be booted on your machine. In order to install and run Ubuntu effectively, we need to turn it off.

Now that we've got our machine configured, let's boot into our Live USB.

## Boot into Ubuntu Live USB

Now that we have a bootable Ubuntu Live USB we can start installing Ubuntu.

First we need to boot into our Live USB. We can do this by:

* Power off the machine you want to install Ubuntu on
* Plug your Live USB into the machine
* Start the machine
* Enter the BIOS menu
* From the BIOS menu, choose the Live USB as the startup device

The BIOS menu will likely have a lot of startup options on it. There should only be one that looks like it's a USB drive so you should choose that one. If you're still having trouble finding it, I recommend disconnecting all other USBs from your machine and seeing what your USB is called by plugging it into your machine normally before doing this step.

## Install Ubuntu

Now that we've booted into Ubuntu from our Live USB, we can start the installation.

After choosing your Live USB from the BIOS menu, you should find yourself in the GRUB menu. This is essentially the same thing as the BIOS menu but it's Ubuntu's version of it. You should be faced with a few options here, but most importantly are two that look like this:

* Ubuntu
* Ubuntu (Safe Graphics)

For most people, choosing `Ubuntu` will be fine. Choose that.

If you're having problems getting past this step (like the process crashes every time you select `Ubuntu`) then try going with `Ubuntu (Safe Graphics)`. This selection does basically the same thing but it runs its graphics in a way that are compatible with more graphics cards. 

In my case, I'm running graphics with an Nvidia GTX 4000 and the regular `Ubuntu` option didn't work for me, but `Ubuntu (Safe Graphics)` did. See: [Ubuntu: USB Installer `Initramfs unpacking failed: Decoding failed`](/posts/ubuntu-usb-initramfs-unpacking-failed)

If everything's going correctly we should find ourselves in an Ubuntu install wizard / workflow. I'll walk through each section and how to decide what to put there.

### Updates and other software

`What apps whould you like to install to start with?`

For most people I recommend choosing `Normal installation`. This will install a lot of nice quality-of-life utilities that you'll want in a normal desktop environment.

`Other options`

Here I recommend selecting both `Download updates while installing Ubuntu` and `Install third-party software for graphics and Wi-Fi hardware and additional media formats`. 

The first will begin updating Ubuntu while you're doing your installation. By having an up-to-date Ubuntu when you first load into the OS we can skip a lot of troublesome problems that may be caused by running an older version.

The second will pre-install a lot of drivers for you. These drivers support a lot of common actions - like making sure your monitors work well and that you can connect to the internet better. By selecting this, the install will take a little longer but we should end up with a more stable desktop environment when we finish our install.

With your selections made, click `Continue`

### Installation Type

Here you'll be faced with a few different installation types:

1. Install Ubuntu alongside another OS
2. Erase disk and install Ubuntu
3. Something else

Your options may be different depending on the state of your computer - usually depending on whether another OS is already installed or not.

If you're not dual booting (i.e. just want Ubuntu to be your only OS on your machine) then I recommend going with 2 or 3.

If you are dual booting then your options are 1 or 3. If you're dual booting I recommend reading my [guide to dual booting Ubuntu and Windows](/posts/dual-boot-ubuntu-with-windows-10-lenovo-thinkstation) before proceeding.

If you have a simple one-drive setup then I recommend going with 1 if you're dual booting and 2 if you want Ubuntu-only. This will allow Ubuntu to make some opinionated decisions about your computer setup for you that are good for most people and reduce the complexity of setup.

If you went with 1 or 2 in this step then continue on to the next section. If you instead went with 3 then read on.

`[Optional] 3 - Something Else`

If you have a more complex setup then we need to go with 3 - `Something else`. With 3 we need to be careful about how we set things as this can be a make-it / break-it step for our installation.

There are many, many ways to configure your installation. I can't cover them all here but I will cover a basic, tried-and-true paradigm that covers most non-standard use cases and is flexible to most other non-standard configurations should you need to further modfiy it.

A base Ubuntu installation consists of 3 data partitions:

* root - mounted at '/', ext4 - this is where the OS will live. It's where OS files will be written and read from. Official documentation mentions that Ubuntu needs at least 15 GB with at least 20 GB recommended.
* swap - uses a special partion type - this is where the OS will log much of its short-term memory. You can think of this as a place where it stores its notes on what it's doing / where it throws its clothes when they've got another half day of wearing before they need to be washed. This is a write and read heavy partition which may mean that an SSD isn't the best choice since they get worn out from heavy writing. It's recommended that this partition be > 2x your available RAM.
* home - mounted at '/home', ext4 - this is where all your files will be stored like documents, photos, videos, etc.
* Bootloader install location - Where you want the Bootloader to be installed

In my case I had an SSD and an HDD. I wanted to put my OS on the SSD so it would run faster and put both my swap and home on the HDD. Here were my settings:

* root - mounted at '/', ext4 - 40 GB on SSD
* swap - swap partition - 128 GB (4x my RAM) on HDD
* home - mounted at '/home', ext4 - ~3 TB (the rest of my free space)
* Bootloader install location - I chose my HDD because that's where my Windows bootloader was also installed

### Installation

Once you've chosen your installation type, click `Install Now` to begin installation.

You should now see a few screens to set your location and username and password. Fill out these screens and continue. 

Full installation will likely take ~1 hour depending on your installation settings and your internet connections.

Once done, you should be prompted to restart your computer. Do that.

## Update Ubuntu

Once we've finished installing, there are a few more things we should do before using our computer.

The first is to verify our installation. To do this, we just need to boot up Ubuntu and make sure it's working. We can do this by:

* Start computer
* Enter BIOS menu
* Select Ubuntu

If Ubuntu starts up correctly then our installation was likely a success! If you can't find the Ubuntu option or something funky is happening you may need to try the installation process again.

In the case with my Lenovo I had to do 2 full installs before it worked.

Once we're in Ubuntu, we should update a few things.

* Open your terminal
* Run `sudo apt-get update`
* Run `sudo apt-get upgrade`
* Run `sudo ubuntu-drivers install`
* Reboot system

These commands will 1) Fetch the latest updates for all libraries your system relies on, 2) update its libraries, and 3) install the latest drivers for your system. We reboot at the end to give the OS a chance to make any changes it needs to to support all the new things we updated.

In my case, there were a few more steps I wanted to take to get my computer up and running.

1. For my Lenovo I found that one of my external monitors wasn't working. I fixed this by updating my Nvidia drivers. See: [Ubuntu: External monitor not detected with Nvidia Graphics Card](/posts/ubuntu-external-monitor-not-detected-with-nvidia-graphics-card)
2. I wanted to mount my SSD to my /home directory so I could easily save programs and files that I wanted to be super fast to it. See: [Ubuntu: Mount SSD to home directory](/posts/ubuntu-mount-ssd-to-home-directory)

# Fin

That's it! I came out the other side with a pristine, up-to-date Ubuntu install. 

I hope this helped you through your Ubuntu install process and that you continue to use Ubuntu in the future. It's my favorite OS and I'd love for it to become yours as well.

If you have any feedback on this post - what worked, what didn't - please comment below so I can make changes as necessary.

Ubuntu.

-HAMY.OUT