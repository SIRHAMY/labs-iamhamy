---
title: "Create a bootable Ubuntu Live USB from Ubuntu"
date: 2020-11-22T14:18:13Z
tags: [
    'ubuntu',
    'usb'
]
comments: false
---

# Overview

I recently finished my latest [dual boot of Ubuntu](/posts/dual-boot-ubuntu-with-windows-10-lenovo-thinkstation) on one of my machines. Here I'll lay out the process I recommend for creating a bootable Ubuntu Live USB from within an Ubuntu OS. The Live USB can then be used for running and installing Ubuntu.

# Creating a bootable Ubuntu Live USB from Ubuntu

There are many tools with which you can create an Ubuntu Live USB but I recommend sticking with the default Startup Disk Creator that comes pre-installed on most Ubuntu installations. It just works, is likely already installed, and has a high likelihood of being maintained into the future.

To create a bootable Ubuntu Live USB from Ubuntu:

1. Download an Ubuntu ISO
2. Create a bootable USB

## 1. Download an Ubuntu ISO

The first thing we need to do is grab an Ubuntu ISO. This is basically an entire copy of an Ubuntu OS. This will be the OS that you'll be running your Live USB from so make sure you pick accordingly.

At time of writing Ubuntu 20.04 was the latest LTS (Long Term Support) version. I like LTS because they'll get regular updates and maintenance for several years making it a very stable OS to run on until the next LTS comes out.

You can find Ubuntu ISOs to download on [Ubuntu's website](https://ubuntu.com/download/desktop)

## 2. Create a bootable USB

Now that we have the ISO downloaded we'll configure a USB to hold it. Note that you'll likely want to use a blank USB / a USB you don't mind erasing for this step.

Steps to follow:

* Plug in your USB
* Open Startup Disk Creator
* Startup Disk Creator usually detects the ISO you downloaded as well as the USB you plugged in. If not, check that the download is complete, try plugging your USB in again, and try setting the ISO manually by clicking 'Other...'
* Click 'Make Startup Disk' in Startup Disk Creator
* Wait until the USB has been completely written (this can take several minutes)
* Eject the USB from your device

If everything went smoothly then you should have a Live USB with Ubuntu on it! You can now start running Ubuntu by plugging in the USB and selecting it from the BIOS menu! You can even use it to [install Ubuntu](/posts/install-ubuntu-from-a-live-usb) directly on your machine!

