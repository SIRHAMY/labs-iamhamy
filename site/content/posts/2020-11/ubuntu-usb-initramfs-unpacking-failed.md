---
title: "Ubuntu: USB Installer `Initramfs unpacking failed: Decoding failed`"
date: 2020-11-22T17:35:18Z
tags: [
    'ubuntu',
    'usb',
    'troubleshoot'
]
comments: false
---

# Overview

I was trying to [dual boot Ubuntu](/posts/dual-boot-ubuntu-with-windows-10-lenovo-thinkstation) and continually running into an issue running my Ubuntu Live USB on my Lenovo P720. Whenever I would boot into my Live USB I would get the error `Initramfs unpacking failed: Decoding failed` and a black screen with glitchy colors on it.

# Solution

I solved this by doing two things:

1. Disabling Secure Boot
2. Running Ubuntu in Safe Graphics mode

## Disabling Secure Boot

To disable secure boot:

* Enter your system's BIOS menu
* Go to BIOS settings
* Find the Secure Boot configuration setting
* Set Secure Boot to disabled (or false on some machines)

## Run Ubuntu in Safe Graphics mode

The next thing I did was run Ubuntu in Safe Graphics mode. Safe Graphics mode runs Ubuntu with graphics configurations that play nice with most graphics cards. In my case, I was running an Nvidia GTX 4000 and that seemed to be crashing the normal Ubuntu install.

To run in Safe Graphics mode:

* Enter your system's BIOS menu
* Select your Live USB to boot into Ubuntu GRUB menu
* From GRUB, you should see two options `Ubuntu` and `Ubuntu (Safe Graphics)`, select the latter

