---
title: "C#: Default to DateTimeOffset over DateTime"
date: 2020-11-30T17:12:13Z
tags: [
    'csharp',
    'date',
    'time'
]
comments: false
---

Note to self: Default to using DateTimeOffset for dates rather than DateTime.

# DateTimeOffset

* Date and time
* Offset indicating difference from UTC

Is basically DateTime plus it has awareness of time zones.

# DateTime

* Date and time

# Conclusion

DateTimeOffset is typically what I'll want to use. It does everything DateTime does plus has some additional data on time zones. This makes it easier to do date arithmetic after the fact if time zones need to be taken into account.

# References

* [Choose between DateTime, DateTimeOffset, TimeSpan, and TimeZoneInfo](https://docs.microsoft.com/en-us/dotnet/standard/datetime/choosing-between-datetime)