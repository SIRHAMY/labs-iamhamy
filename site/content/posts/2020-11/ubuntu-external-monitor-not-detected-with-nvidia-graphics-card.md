---
title: "Ubuntu: External monitor not detected with Nvidia Graphics Card"
date: 2020-11-22T18:00:10Z
tags: [
    'ubuntu',
    'nvidia',
    'graphics'
]
comments: false
---

# Overview

I just [dual booted Ubuntu](/posts/dual-boot-ubuntu-with-windows-10-lenovo-thinkstation) on my Lenovo ThinkStation p720 running on an Nvidia GTX 4000 graphics card. The problem is that only one of my monitors is being detected. What's going on and how can I fix this?

# Solution

The way I fixed this was to update my Nvidia drivers. You can read about how to do this in [Ubuntu: Update Nvidia drivers](/posts/ubuntu-update-nvidia-drivers)

I would recommend updating them in this order:

* Manual Nvidia driver updates
* GUI Nvidia driver updates

The manual version updates the drivers very simply. The GUI does some other black magic in the background so I think it's best to start simple before relying on the GUI.

After updating your drivers, your external monitor _should_ work if this is indeed a driver problem. If it didn't work then I'd suggest rechecking all your connections and trying them with other machines to help narrow down the source.