---
title: "Threejs How to Make an Object Invisible"
date: 2020-10-26T14:26:29Z
tags: [
    'threejs',
    'object',
    'visible'
]
comments: false
---

# problem

I'm using threejs to create an audiovisualizer to accompany the release of an upcoming music track. As part of this, I want to be able to make an object invisible (and then to make it visible again). How can I make an object visible and invisible in threejs?

# solution

Most objects in threejs (assuming you're in 3D mode) are an `Object3D`. These objects have a property on them called `visible` that lets the renderer know whether or not they should be rendered - setting this determines whether or not they actually appear, thus rendering them visible or invisible.

To make an object visible or invisible, set the `visible` field on it to `true` or `false`

```
invisibleObject.visible = false
visibleObject.visible = true
```