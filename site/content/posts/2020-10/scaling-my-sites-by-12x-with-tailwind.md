---
title: "Scaling my sites by 12x by moving to Tailwind"
date: 2020-10-27T22:44:31Z
tags: [
    'iamhamy',
    'tailwind',
    'netlify',
    'semantic-ui',
    'cases'
]
comments: false
---

# Overview

This month I redesigned my sites to be more efficient, more sustainable, and more accessible than ever in an effort I've dubbed [iamhamy 3.0](/projects/iamhamy-3/). A core component of this redesign was moving from [Semantic UI](https://semantic-ui.com/introduction/getting-started.html) to [tailwindcss](https://tailwindcss.com/) for my site-styling needs. Through this redesign I calculate that I've raised the total number of visitors I can serve per month [from ~500k](/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free) to around [4.4 million - an ~12x increase in scalability](#scalability).

(Yes, I realize those numbers don't match up. Keep calm and read on.)

In this post I'll touch on the redesign, my experience with Tailwind, how I host my sites, and how I got these numbers.

# iamhamy

Before we go onto the scalability numbers, it's first useful to understand how I serve my sites and how I'm defining scalability. I've written about this before in [How I scale my sites to 500k users per month for free](/posts/how-i-scale-my-sites-to-500k-users-per-month-for-free) and most of my hosting paradigm remains the same so here I'll just go over the basics.

I use Netlify to host my sites. They provide a clean, easy-to-use hosting platform that just works. I love that.

Through them, I currently host 3 iamhamy sites, each auto pushed from their code repo. An important thing to note is that I host my static files on Google Cloud Storage.

![iamhamy site architecture](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-scale-my-sites-to-500k-free/site-architecture.jpg)

# Scalability

I run all my sites on the Netlify free tier. In this tier, I get 100GB of bandwidth for free. The 100GB limit is how I measure the scalability of my sites. 

This includes all assets served from Netlify to the end user - my HTML, my CSS, my JS, etc. This is why I called out that my static files (like images and videos) are served through Google Cloud Storage as they do not contribute to this 100GB total.

Thus my measure of scalability is how many people I can serve without having to make a change to my sites / hosting plans. With this context, we can now move into how Tailwind affected this measure of scalability and how scalable my sites are today.

## Tailwind

I talk a lot more about my recent redesign in [iamhamy 3.0](/projects/iamhamy-3/) but for our purposes we can note that the core values I designed with were efficiency, sustainability, and accessibility. When I say efficiency here, a major component of that is its ability to scale payload-wise.

In my [analysis of my site's scalability with Semantic UI](/posts/how-many-visitors-can-the-netlify-free-tier-support/), I found that the average payload of my webpages served from Netlify was 107.8 Kb and users navigated to ~1.74 pages / session on the high end. Put those together, and my sites could serve ~533,000 people / month.

_Enter Tailwind - stage left._

Whereas Semantic UI had a served footprint of 101kb, Tailwind comes in at just 7.6 Kb! This tiny footprint is one of the major reasons why I ended up choosing it.

In total, my pages now come out to ~12.9 Kb of data served from Netlify. We can calculate the `total_visitor_capacity` with a pretty simple function:

```
def total_visitor_capacity(
    pages_per_visit,
    payload_per_page,
    payload_per_plan
):
    '''
    This function assumes that payload_per_page and payload_per_plan
    are in the same units
    '''
    return payload_per_plan / (payload_per_page * pages_per_visit)
```

We can then run this analysis with inputs:

```
pages_per_visit: float = 1.74
payload_per_page_kb: float = 12.9
kb_per_gb: int = 1000000
gb_per_plan: int = 100
kb_per_plan = kb_per_gb * gb_per_plan
```

We then find that with a ~12.9 Kb payload and 1.74 visits / page, `iamhamy 3.0` can scale to ~4,455,136 users / month - an 8x improvement over the previous Semantic UI site!

For my previous analysis, I actually looked at a relatively small page - [Connect](https://hamy.xyz/connect/) - so for a more accurate scalability comparison we should use the payload of that same page (now built with Tailwind) which comes out to about 8.8 Kb / page load. Running those numbers, we get a (contrived) scalability measure of ~6,530,825 users / month - a 12x increase over Semantic UI!

(This is why the people-served and scalability multiplier number in the Overview don't agree.)

# Debrief

So that's how moving to Tailwind from Semantic UI allowed me to increase the scalability of my sites by 12x and to serve 4.4 milllion users / month for free. Will I ever need this scale? One can hope but [I served less than 4,000 / month in Q3](https://blog.hamy.xyz/posts/2020-q3-review/#iamhamy) so likely not soon.

A more important question is did I have fun with a close followup being would I do it again - and the answer to both of those is a yes so like / subscribe / share to keep this fun chugging!

Thank you for your time.

-HAMY.OUT