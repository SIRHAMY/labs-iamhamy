---
title: "Darktable: Cycle through images"
date: 2020-10-05T13:38:47Z
tags: [
    'darktable',
    'ubuntu'
]
comments: false
---

# problem

I'm new to Darktable - and image manipulation on Ubuntu in general - and am having some troubles understanding the controls. My main concern is how I can cycle through my available images using hotkeys / the keyboard. I often take a lot of images in one go so hotkeys makes this proces _way_ faster and more bearable.

How can I cycle through images in Darktable using the keyboard?

# solution

There are actually several ways to do this based on your settings in Darktable. Let's first talk about the default controls in Darktable and then we can talk about how to customize them for you.

## default controls

In Darktable there are two primary modes, `lighttable` and `darkroom`, each with their own default controls for cycling through images.

### `lighttable` default controls

![Lighttable in culling mode - images via [@hamy.see](https://instagram.com/hamy.see)](https://storage.googleapis.com/iamhamy-static/labs/posts/one-off/darktable-culling-mode-callout-web.png)

_Lighttable in culling mode - images via [@hamy.see](https://instagram.com/hamy.see)_

`lighttable`'s cyclable view type is `culling`. You can select the `culling` view from the dropdown located in the bar below the main frame.  In this view you can cycle through images using the `left arrow` and `right arrow` on your keyboard.

### `darkroom` default controls

`darkroom` is the photo manipulation mode for Darktable. In this mode, you can cycle through images using the `space bar` and `backspace` keys.

## custom controls

Darktable is highly customizable so if you don't like the default controls you can just change them.

To change the controls:

* click the cog / gear button in the top right corner of the main window
* Select the `shortcuts` tab
* Find the appropriate section for the action you're trying to shortcut and change them


If you wanted to change the `lighttable` controls for cycling images, go to `views > lighttable` then change the `move left` and `move right` buttons from the `shortcuts` tab.

If you wanted to change the `darkroom` controls for cycling images, go to `views > darkroom` then change the `image forward` and `image back` buttons from the `shortcuts` tab.

Profit!