---
title: "Google Cloud: Quota 'GPUS_ALL_REGIONS' exceeded. Limit: 0.0 globally."
date: 2020-09-12T03:24:04Z
tags: [
    'google-cloud',
    'gpu',
    'troubleshoot'
]
comments: false
---

# problem

I'm trying to use Google Cloud Platform to create some virtual machines with attached GPUs to help speed up some of my machine learning projects. To do this, I'm creating a new VM with a GPU attached.

However, right after I create the VM, I'm getting this error (after which it seems like my new VM is deleted):

`Quota 'GPUS_ALL_REGIONS' exceeded. Limit: 0.0 globally.`

How can I create my VM with an attached GPU?

# solution

If you're running into this issue, then it's likely you've never updated your GPU quota. Currently, your global quota for GPUs is set to 0. So in order to actually create an instance with a GPU, you'll need to raise that quota to 1 (or whatever number of desired GPUs you need). To do this, we'll need to submit an account request and get it approved.

Here's how you can raise your quota for GPUs:

* Navigate to `IAM & Admin`
* Click into the `Quotas` section
* Filter for the quota that we exceeded - in this case `GPUS_ALL_REGIONS`
* Select the quota by clicking on it. This should pull up a `Quota Panel` with a box specifying a `Global` limit (likely set to 0 given our quota exception)
* Select the `Global` quota by clicking the radio button and hit `Edit Quotas`
* Here you'll be asked to enter the new quota you'd like to be set and to enter a description. Set your quota to something reasonable and enter a short description of what you hope to use it for then hit `Submit Request`. Be careful - this may actually be read by a person so don't request 1000 GPUs if you're just trying to play League of Legends on weekends.

Once you've sent your request, it'll either be approved or rejected. This can take anywhere from 10 minutes to 3 days so you may have to wait a bit. Once it's approved, allow up to 20 minutes for the quota to be reflected across GCP (at which point you'll be able to allocate GPUs up to your quota).

Hope this helps!

-HAMY.OUT