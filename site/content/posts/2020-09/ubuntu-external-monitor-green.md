---
title: "Ubuntu: External monitor is green"
date: 2020-09-28T13:32:22Z
tags: [
    'troubleshoot',
    'ubuntu'
]
comments: false
---

# problem

I'm trying to hook up my computer running Ubuntu 20.04 to an external monitor. When I do, my desktop displays but it has a greenish tint to it. How can I fix the colors on my external display?

# solution

This can happen for a myriad of reasons. The quick fix is to try restarting your computer.

* restart your computer
* plug in the external display

If that doesn't work, you may need to check for updates for your computer and, specifically, install new drivers.

* check for updates and install them using `sudo apt-get upgrade`
* check for new drivers and install them using `sudo ubuntu-drivers autoinstall`
* restart your computer to ensure your changes have taken effect

After doing this, you should be good to go! If the problem persists, you may need to check whether the specific monitor you're using has additional requirements to work with your setup.