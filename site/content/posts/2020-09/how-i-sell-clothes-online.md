---
title: "How I make and sell clothes online"
date: 2020-09-04T01:46:23Z
tags: [
    'store',
    'shopify',
    'printful',
    'hamy.shop'
]
comments: false
---

Goal: share how I sell clothes online and how other people can as well

<iframe width="560" height="315" src="https://www.youtube.com/embed/0li-qIA4XTU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I run [HAMY.SHOP](https://shop.hamy.xyz/), an online store where I sell clothing, art, and prints that I create. Since its inception, I've gotten lots of questions about how things work behind the scenes. Here I'll share how I do it and the thought and work that went into building it.

[![HAMY.SHOP front page](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-make-and-sell-clothing-online/hamy-shop-front-page.png)](https://shop.hamy.xyz)

# How a shop works

Before diving into how I run my shop, I think it's important to first get an understandng of what kind of shop I run and the components required to actually run a shop.

Currently I sell prints and clothing featuring my designs. It's important to note that while I'm creating the designs and how they're arranged on the clothing, I'm not designing the clothing itself. This is okay for me because it fits into my philosophy for selling things online which I'll go into later.

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/CEZLtnjFJlo/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CEZLtnjFJlo/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CEZLtnjFJlo/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by HAMY (@hamy.art)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2020-08-27T12:54:24+00:00">Aug 27, 2020 at 5:54am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

Let's explore what components are required to run a shop online by diving into an example. Here's a piece from [dump45](/projects/dump45) - my newest collection.

![The buyer journey](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-make-and-sell-clothing-online/buyer-journey.png)

You can buy this shirt by heading to [HAMY.SHOP](https://shop.hamy.xyz) and it'll be shipped to you like any other online purchase. But what exactly goes into making that happen?

![The full journey](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-make-and-sell-clothing-online/full-journey.png)

In order to get one of these sweatshirts, there's a whole pipeline of things that needs to occur:

* design - Something must design the item - with no design, there's nothing to make.
* manufacture - Something must manufacture the item - with no manufacture, the item would never be made.
* inventory / shipping - Something must inventory / ship the item - without this the item would never get to its destination and / or you'd be selling items that didn't exist.
* point of sale - Something must handle the sale of the item - without the sale, nothing else in this list really matters.

Now there are numerous ways to run your shop, but they'll likely all require each of the above steps in the pipeline. 

In the example of the dump45 sweatshirt, I designed the dump45 image then designed the sweatshirt to have the image on the back and my logo on the front. When people go to HAMY.SHOP and buy it, it gets manufactured, inventoried, and shipped out to the buyer.

Let's get into how you might do that.

# Running a shop

In this day and age, we have a plethora of choices when it comes to building out our business pipeline. We could design the items ourselves using programs like Photoshop or hiring someone. We could create shirts ourselves, buy them from a factory overseas, or hire a service to handle all of that for you. We could build a website to sell our wares or use a service for that.

There's no right answer for how to do this, but I think there are better pipelines depending on what you value and what you're trying to get out of your shop.

My philosophy for selling physical goods online is that 1) it must work and 2) it must be relatively hands off. I want 1) because if it doesn't actually work then there's no point in doing it. I want 2) because while the ability to create and sell physical goods online is cool, that's not what I'm really passionate about. I'm really passionate about creating the designs and programs that produce the end images so I want to spend as much time and effort as possible on that front. This means minimizing the time and effort spent on the logistics of distribution like taking orders, printing, and shipping.

Life is about tradeoffs so this philosophy has its pros and cons.

![Hands-on creator](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-make-and-sell-clothing-online/hands-on-creator.png)

You can imagine that if you're more involved in the process, you'll have more control and thus have a greater ability to optimize. You could design the base clothing, make non-standard designs and prints, utilize any materials you can get your hands on, customize shipping packaging, etc., etc. The flexibility here is as limitless as your available resources. 

Herein is the tradeoff - Being a hands-on creator will afford you greater control of your entire process in exchange for greater resources - like time, effort, and money.

In the example of the dump45 sweatshirt, you might create your own design using Photoshop. Then visit many different potential manufacturers before picking one that has the right sweatshirt material and tailoring for your liking. Perhaps you even work with some tailors to come up with the base sweatshirt design. Maybe you put in an order of 500 from the manufacturer so that you can get a bulk discount, load it on your custom-built site, and ship it out in branded packaging.

_This is certainly an over-generalization as you could, for instance, use this extra control to optimize for costs, likely lowering your costs but taking extra time and effort to do so._

![Hands-off creator](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-make-and-sell-clothing-online/hands-off-creator.png)

Given my philosophy, I don't really want to be a hands-on creator. For me, it distracts from the main work I really want to do. Instead, I prefer to be more of a hands-off creator.

As a hands-off creator, I want just as much control as I need to be able to customize my products to my liking. Typically there will be limited options for doing things in exchange for most of the backend processes being automated. For example, most print-on-demand services will have a selection of tshirts you can print designs on. By picking one of these shirts from this service, they'll take care of the actual shirt acquisition, printing, packing, and shipping.

In the example of the dump45 sweatshirt, you might make the design and use the configuration tools of your chosen print-on-demand service to customize it to your liking. Then you might log in to your point-of-sale service to create your shirt listing - setting the price, adding pictures, and deciding how it shows up on the site. After that, you share your link to IG and go about your day while people buy stuff and your services handle all the logistics.

Before closing out this section, I want to note that the hands-on and hands-off paradigms are on a spectrum, you can pick and choose which processes you want to be involved in and which you don't - affording you different levels of control and involvement - based on your wants / needs. 

# HAMY.SHOP

Now that we understand what goes into selling clothing online and some different philosophies for approaching that problem, I'll explain how I actually do this on HAMY.SHOP.

![HAMY.SHOP](https://storage.googleapis.com/iamhamy-static/labs/posts/2020/how-i-make-and-sell-clothing-online/hamy-shop.png)

As I mentioned before, I'm very much a hands-off creator when it comes to selling stuff online. I want to focus on creating things, not selling them. In my paradigm, I use various services to handle all of these logistics. This allows me to focus solely on the design of the product and how it appears on my shop, leaving the 'boring' stuff to other people.

Below I'll walk through all of the services I'm using and what they do for me. Note that I will be using some affiliate / referral links because $$$. Please click them and support me. ty.

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/CCYgyoRl7V1/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CCYgyoRl7V1/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CCYgyoRl7V1/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by HAMY (@hamy.art)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2020-07-08T13:36:34+00:00">Jul 8, 2020 at 6:36am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

## design

For the majority of items on my shop, I'm using software that I wrote to create the images. I use a few other image manipulation programs for various things like text, cropping, etc. - mostly [GIMP](https://www.gimp.org/) and [Affinity Designer](https://affinity.serif.com/en-us/designer/).

You can learn more about my custom software on my [projects page](/projects) and browse images on my Instagram [@hamy.art](https://www.instagram.com/hamy.art/).

## manufacture / inventory / shipping

I'm actually using one service for manufacturing, inventory, and shipping. That service is  [Printful](https://www.printful.com/a/1451646:3fdb65bd8e059459e1de1fb1e27475f9).

Printful allows me to choose from a [large selection of products](https://www.printful.com/custom-products/a/1451646:3fdb65bd8e059459e1de1fb1e27475f9) to customize with my designs then takes care of the manufacture, inventory, and shipment for me. With this paradigm, I really just have to do the designing and they take care of the rest.

So far I've found the selection of products and produced quality and consistency to be pretty good. The turn around time can be a bit long - like sometimes the item won't be shipped for a week or so - but I think that's a fair tradeoff considering they're making and shipping the item on demand.

_You can see some examples of real products form Prinful in my [comparison of Printful shirt offerings](/posts/comparing-printful-shirt-offerings/)._

One of the things I really like is that you only pay for what you use - there's no monthly subscription. This means I'm only paying when I'm selling (which should mean you're only making money, not losing it).

I considered a few other services like Society6 but the product selection and control over customization, pricing, and distribution that Printful offers won me over.

Pretty sweet.

## point of sale

The last part of the process is the point of sale. This is how people actually access my store and purchase from it. I considered building my own site that hit Printful's APIs but in the end decided it was best to just get up and running and then I could optimize later.

I ended up going with [Shopify](https://www.shopify.com/) based on the recommendations of the internet. It's a bit expensive, coming in at $30 / month regardless of your sales, but it really does take care of a lot and just works. It has really good integrations with Printful, allowing me to design everything on Printful and have it pushed directly to HAMY.SHOP without any additional steps.

It also comes with a lot of different shop themes and customization options which look pretty good so by paying this premium I was able to save a lot of time / effort and so far it's been a pretty good tradeoff. I have thought about moving to a custom solution to save on the subscription cost but haven't yet and haven't really felt any urgency to do so.

# fin

So that's how I run [HAMY.SHOP](https://shop.hamy.xyz)! If you have any questions / comments let me know and I'll answer as best I can. 

If you've got a few minutes, check out my latest collection dump45 on the shop and throw me a follow on Instagram at [@hamy.art](https://www.instagram.com/hamy.art/).

Happy creating!

-HAMY.OUT