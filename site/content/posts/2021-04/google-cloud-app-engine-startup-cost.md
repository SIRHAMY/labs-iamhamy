---
title: "The cost of running my startup on Google Cloud"
date: 2021-04-26T00:22:44Z
tags: [
    'google-cloud',
    'business',
    'linetimes'
]
comments: false
---

Last month [I shut down my first startup](https://blog.hamy.xyz/posts/2021-q1-review/#linetimes) LineTimes. It wasn't a successful startup but I did end up learning a ton from the process. One of those learnings was the actual cost of running a business on Google Cloud. 

In this post I'll be sharing a snapshot of my startup and its associated costs so future devs can use it as data points in their next creation.

# Startup System Design

Before we go into the costs, we should first talk about the system itself - what it was trying to accomplish and how it tried to do that - as this has a direct effect on what infrastructure I used and thus the projects' bottom line.

[LineTimes](https://labs.hamy.xyz/projects/linetimes/) provided near real-time data on the length (in time) of lines at hot grocery stores in Manhattan.

![LineTimes System Design](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/linetimes-system-design.png)
_LineTimes System Design_

The system design was pretty straight forward:

* DB
    * Stored raw data, location info, and time estimates
* Application Server
    * Periodically pulled from data sources and made time estimate calculations
    * Served estimates to requestors
* Web server
    * Serves the front end

# Google Cloud

I chose Google Cloud as my hosting platform as choice for several reasons, but mostly it was for its capability, stability, and supportive / active ecosystem. I subscribe heavily to the 'boring technology' philosophy when building my businesses and Google Cloud checks those boxes.

I did have a few more specific values for the experience I wanted with a platform, though this was a larger constraint on how I used a specific platform than choosing between different platforms (at least the larger ones).

* Cloud agnostic (as much as possible)
* Use technologies I enjoy - C# .NET Core, NextJS, Postgres
* Do as little DevOps as possible (minimal sshing pls)

These constraints and technology choices led me to this infra stack on Google Cloud. I'll list monthly cost next to each piece of infra.

![LineTimes Google Cloud costs](https://storage.googleapis.com/iamhamy-static/labs/posts/2021/one-off/linetimes-google-cloud-costs-march-2021.png)
_LineTimes Google Cloud costs_

* DB - Managed Cloud SQL for Postgres ($10)
* Application Server - C# .NET Core in App Engine Flex Environment ($22)
    * I chose the Flex Environment rather than the Standard Environment because Standard doesn't support .NET Core.
* Web Server - NextJS (Node) in App Engine Flex Environment ($22)
    * I chose Flex as (at the time) App Engine Standard didn't support the latest Node (14) that NextJS required

All in all this stack cost me approximately $54 / month. This doesn't include any ingress / egress costs but I didn't have much of those with just 52 users over 3 months.

# Learnings

## Move from App Engine Flex -> Cloud Run

$54 / month isn't a crazy amount to run a business but as a solo developer with hopes to run a portfolio of profitable businesses I felt that this was too high. Ideally I'd be able to run an entire business stack for < $20 / month, at least until I started generating some real revenue. I'm pretty set in my tech choices right now so I decided to research alternative hosting options and lo-and-behold found that I was way overpaying for my hosting.

The major problem with the hosting stack I chose for LineTimes is that I wasn't serving many users but I was paying for the infra 24/7 anyway. This is because I was using the App Engine Flex Environment which requires at least 1 instance at all times - a choice I made due to my personal tech preferences.

In my research into alternatives, I found this [dev.to post lauding Google Cloud Run](https://dev.to/pcraig3/google-cloud-run-the-best-host-platform-for-dynamic-apps-4ma6). Upon further research I found that it seems to support my workloads for _a lot_ less, like 60-80% less.

* Handles arbitrary container loads (.NET Core, NextJS supported)
* Only runs when requested
* Cloud agnostic (since running containers, not code)
* Generous free tier

I did some very basic pricing estimates with Google's Pricing Calculator and found that this same LineTimes architecture serving ~100k requests / month (with each request taking a very long 5s and no parallelism), would cost just $10-20 / month! All this saving without sacrificing the scalability or capability of the solution!

## Dive into things that don't feel "right"

I think it's a good practice to dive into things that feel off and see if they can be fixed. This is how I found out about Cloud Run and how it better served my needs, potentially saving me oodles of dollars in the coming months and years. I'm sure this practice will uncover many more potential optimizations as well.

# Fin

That's all I've got for now. If you've got a tech stack and associated cost you'd like to share, comment it down below! 

Saving dat Money.

-HAMY.OUT